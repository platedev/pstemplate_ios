//
//  PSTemplateUITests.swift
//  PSTemplateUITests
//
//  Created by Edimax on 2021/12/23.
//

import XCTest
import SwiftUI
import NetworkExtension

class PSTemplateUITests: XCTestCase {
    
    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
   func testAccessLocation() throws {
        let app = XCUIApplication()
        app.resetAuthorizationStatus(for: .location)
        app.launch()
        var alertPressed: Bool = false
       _ = addUIInterruptionMonitor(withDescription: "Sophos") { [self] element -> Bool in
        alertPressed = clickButtons(element: element, text: "Allow While Using App")
            return alertPressed
        }
        wait {
            app.tap()
        }
        waitAndCheck {
            alertPressed
        }
    }
    func clickButtons(element: XCUIElement, text: String) -> Bool {
        let allowButton = element.buttons[text]
        if allowButton.exists {
            allowButton.tap()
            return true
        }
        return false
    }
  func testAccessOtherSSID()
    {
        let app = XCUIApplication()
        app.launch()
        let myAP = app.buttons["myAPPressed"]
        XCTAssertTrue(myAP.waitForExistence(timeout: 120))
        if myAP.isHittable {
        myAP.tap()
        }
        let newAPPressed = app.buttons["newAPPressed"]
        XCTAssertTrue(newAPPressed.waitForExistence(timeout: 120))
        if newAPPressed.isHittable {
            newAPPressed.tap()
        }
        sleep(8)
        if myAP.isHittable {
            myAP.tap()
        }
        sleep(5)
        let menu = app.images["Menu"]
        XCTAssertTrue(menu.waitForExistence(timeout: 120))
        menu.tap()
        let about = app.staticTexts["menu_About"]
        XCTAssertTrue(about.waitForExistence(timeout: 60))
        about.tapAtPosition(position: CGPoint(x: 23.75, y: 20.0))
        sleep(2)
        menu.tap()
        sleep(2)
        let setting = app.staticTexts["menu_Settings"]
        XCTAssertTrue(setting.waitForExistence(timeout: 60))
        setting.tapAtPosition(position: CGPoint(x: 23.75, y: 20.0))
        
        sleep(2)
        
        let openfaceID = app.switches["OpenfaceID"]
        XCTAssertTrue(openfaceID.waitForExistence(timeout: 60))
        openfaceID.tapAtPosition(position: CGPoint(x: 334.3, y: 10.33))
        sleep(2)
        
        let closeFaceErr = app.buttons["closeFaceErr"]
        XCTAssertTrue(closeFaceErr.waitForExistence(timeout: 60))
        closeFaceErr.tap()
        sleep(2)
        
        let opentouchID = app.switches["OpentouchID"]
        XCTAssertTrue(opentouchID.waitForExistence(timeout: 60))
        opentouchID.tapAtPosition(position: CGPoint(x: 334.3, y: 10.33))
        sleep(2)
        
        let closetouch = app.buttons["closetouch"]
        XCTAssertTrue(closetouch.waitForExistence(timeout: 60))
        closetouch.tap()
        sleep(2)
        opentouchID.tapAtPosition(position: CGPoint(x: 334.3, y: 10.33))
        sleep(2)
        closetouch.tap()
        sleep(2)
        
        menu.tap()
        sleep(2)
        
        let goAP = app.buttons["goAP"].firstMatch
        XCTAssertTrue(goAP.waitForExistence(timeout: 120))
        goAP.tap()
        sleep(2)
        
    }
    func testAddAccessPointsFlow() {
        let app = XCUIApplication()
        app.launch()
        takeScreenshot(name: "SplashScreen")

        sleep(40)
        
        let updateMy = app.images["updateMy00:00:1e:00:01:af"] // .accessibilityIdentifier("pointbuttonPressed")
        XCTAssertTrue(updateMy.waitForExistence(timeout: 120))
        if updateMy.isHittable {
            updateMy.tap()
        }
        
        sleep(1)
        
        let newAP = app.buttons["AddAllNewPressed"] // .accessibilityIdentifier("pointbuttonPressed")
        XCTAssertTrue(newAP.waitForExistence(timeout: 120))
        if newAP.isHittable {
        newAP.tap()
        }
        
        let addallaps = app.buttons["addallaps"] // .accessibilityIdentifier("pointbuttonPressed")
        XCTAssertTrue(addallaps.waitForExistence(timeout: 120))
        if addallaps.isHittable {
        addallaps.tap()
        }
        let myAP = app.buttons["myAPPressed"]
        XCTAssertTrue(myAP.waitForExistence(timeout: 120))
        if myAP.isHittable {
        myAP.tap()
        }
        let newAPPressed = app.buttons["newAPPressed"]
        XCTAssertTrue(newAPPressed.waitForExistence(timeout: 120))
        if newAPPressed.isHittable {
            newAPPressed.tap()
        }
        if myAP.isHittable {
            myAP.tap()
        }
        
        let seachBar = app.otherElements.textFields["seachBar"]
        XCTAssertTrue(seachBar.waitForExistence(timeout: 120))
        seachBar.tap() //.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        if seachBar.isHittable {
            seachBar.typeText("420")
        }
        sleep(5)
        
//        sleep(10)
        let searchX = app.buttons["searchX"].firstMatch
        XCTAssertTrue(searchX.waitForExistence(timeout: 120))
        searchX.tap()
        
        sleep(5)
        let search = app.buttons["search"].firstMatch
        XCTAssertTrue(search.waitForExistence(timeout: 120))
        search.tap()
        sleep(5)
        
        let scrollAPlist = app.scrollViews["scrollAPlist"].firstMatch
        XCTAssertTrue(scrollAPlist.waitForExistence(timeout: 120))
        scrollAPlist.swipeUp()
        sleep(5)
        
        scrollAPlist.swipeUp()
        sleep(5)
        
        let central = app.buttons["checkLogin00:00:1e:00:01:af"]
        
        XCTAssertTrue(central.waitForExistence(timeout: 120))
        central.tap()
        sleep(10)
        app.images["back20"].tap()
        sleep(5)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        sleep(5)
        
        let name = app.otherElements.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        if name.isHittable {
            name.typeText("admin")
        }
        
        sleep(5)
        
        let password = app.otherElements.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
        
        let system = app.buttons["system"].firstMatch
        XCTAssertTrue(system.waitForExistence(timeout: 120))
        system.tap()
        
        sleep(5)
        app.buttons["Edit"].tap()
        sleep(5)
        
        app.images["back20"].tap()
        sleep(5)
        
        let lanport = app.buttons["lanport"].firstMatch
        XCTAssertTrue(lanport.waitForExistence(timeout: 120))
        lanport.tap()
        sleep(5)
        app.buttons["Edit"].tap()
        sleep(5)
        
        let laniptype = app.buttons["laniptype"].firstMatch
        XCTAssertTrue(laniptype.waitForExistence(timeout: 120))
        laniptype.tap()
        
        sleep(5)
        
        let lanscroll = app.scrollViews["lanscroll"].firstMatch
        XCTAssertTrue(lanscroll.waitForExistence(timeout: 120))
        lanscroll.swipeRight()
        
        sleep(5)
        laniptype.tap()
        
        sleep(5)
        laniptype.tap()
        
        sleep(5)
        
        let laniptypeone = app.buttons["laniptypeoneStatic IP"].firstMatch
        XCTAssertTrue(laniptypeone.waitForExistence(timeout: 120))
        laniptypeone.tap()
        
        sleep(5)
        
        let ipaddress = app.textFields["ipaddress"]
        XCTAssertTrue(ipaddress.waitForExistence(timeout: 120))
        ipaddress.tap()
        sleep(5)
        app.images["back20"].tap()
        
        sleep(5)
        
        let clients = app.buttons["Wireless Clients"].firstMatch
        XCTAssertTrue(clients.waitForExistence(timeout: 120))
        clients.tap()
        sleep(5)
        
        let connectclients = app.buttons["connectclients"].firstMatch
        XCTAssertTrue(connectclients.waitForExistence(timeout: 120))
        connectclients.tap()
        sleep(5)
        
        let scroll = app.scrollViews["Main"].firstMatch
        XCTAssertTrue(scroll.waitForExistence(timeout: 120))
        scroll.swipeLeft()
        sleep(10)
        
        let date = app.buttons["Date And Time"].firstMatch
        XCTAssertTrue(date.waitForExistence(timeout: 120))
        date.tap()
        sleep(5)
        app.buttons["Edit"].tap()
        sleep(5)
        app.images["back20"].tap()
        
        sleep(5)
        app.images["back20"].tap()
        
        sleep(10)
        
        let myDevice3A = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice3A.waitForExistence(timeout: 120))
        myDevice3A.tap()
        
        sleep(5)

        let name3A = app.textFields["username"]
        XCTAssertTrue(name3A.waitForExistence(timeout: 120))
        name3A.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name3A.typeText("admin")

        sleep(2)

        let password3A = app.secureTextFields["password"]
        XCTAssertTrue(password3A.waitForExistence(timeout: 120))
        password3A.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password3A.isHittable {
            password3A.typeText("#Sophos12345678")
        }
        let login3A = app.buttons["Login"].firstMatch
        XCTAssertTrue(login3A.waitForExistence(timeout: 120))
        login3A.tap()
       
        let clients3A = app.buttons["Wireless Clients"].firstMatch
        XCTAssertTrue(clients3A.waitForExistence(timeout: 120))
        clients3A.tap()
        sleep(10)
        let client3A = app.buttons["client"].firstMatch
        XCTAssertTrue(client3A.waitForExistence(timeout: 120))
        client3A.swipeLeft()
        sleep(10)
        
        let deleteclient03A = app.buttons["deleteclient"].firstMatch
        XCTAssertTrue(deleteclient03A.waitForExistence(timeout: 120))
        deleteclient03A.tap()
        sleep(5)
        
        let connectclients3A = app.buttons["connectclients"].firstMatch
        XCTAssertTrue(connectclients3A.waitForExistence(timeout: 120))
        connectclients3A.tap()
        sleep(15)
       
    }
    func testCallBack()
    {
        let app = XCUIApplication()
        app.launch()
        sleep(20)
        let device = XCUIDevice.shared
        device.press(.home)
        sleep(20)
        app.activate()
        sleep(10)
        
        let central = app.buttons["checkLogin00:00:1e:00:01:af"]
        
        XCTAssertTrue(central.waitForExistence(timeout: 120))
        central.tap()
        sleep(10)
        
        device.press(.home)
        sleep(70)
        app.activate()
        sleep(10)
        
        app.images["back20"].tap()
        
        sleep(10)
    }
//    func testCallClients420X()
//    {
//        let app = XCUIApplication()
//        app.launch()
//
//        sleep(20)
//
//        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
//        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
//        myDevice.tap()
//
//        sleep(5)
//
//        let name = app.textFields["username"]
//        XCTAssertTrue(name.waitForExistence(timeout: 120))
//        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
//        name.typeText("admin")
//
//        sleep(2)
//
//        let password = app.secureTextFields["password"]
//        XCTAssertTrue(password.waitForExistence(timeout: 120))
//        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
//        if password.isHittable {
//            password.typeText("1234")
//        }
//        let login = app.buttons["Login"].firstMatch
//        XCTAssertTrue(login.waitForExistence(timeout: 120))
//        login.tap()
//
//        sleep(10)
//
//        let clients = app.buttons["Wireless Clients"].firstMatch
//        XCTAssertTrue(clients.waitForExistence(timeout: 120))
//        clients.tap()
//        sleep(5)
//
//        let client = app.buttons["client"].firstMatch
//        XCTAssertTrue(client.waitForExistence(timeout: 120))
//        client.swipeLeft()
//        sleep(10)
//
//        let deleteclient0 = app.buttons["deleteclient"].firstMatch
//        XCTAssertTrue(deleteclient0.waitForExistence(timeout: 120))
//        deleteclient0.tap()
//        sleep(5)
//
//        let connectclients = app.buttons["connectclients"].firstMatch
//        XCTAssertTrue(connectclients.waitForExistence(timeout: 120))
//        connectclients.tap()
//        sleep(5)
//
//        app.images["back20"].tap()
//        sleep(20)
//    }
    func testCallReboot0840E1Flow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(20)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)
        
        app.buttons["reboot"].tap()
        sleep(5)
        app.buttons["rebootno"].tap()
        sleep(5)
        app.buttons["reboot"].tap()
        sleep(5)
        app.buttons["rebootok"].tap()
        sleep(10)
        app.buttons["rebootokok"].tap()
        sleep(180)
        
    }
    func testCallReboot0840E2Flow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(20)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)
        
        app.buttons["reboot"].tap()
        sleep(5)
        app.buttons["rebootno"].tap()
        sleep(5)
        app.buttons["reboot"].tap()
        sleep(5)
        app.buttons["rebootok"].tap()
//        sleep(10)
//        app.buttons["rebootokok"].tap()
        sleep(180)
        
    }
    func testCallReboot1420X1Flow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(20)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("#Sophos12345678")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)
        
        app.buttons["reboot"].tap()
        sleep(5)
        app.buttons["rebootno"].tap()
        sleep(5)
        app.buttons["reboot"].tap()
        sleep(5)
        app.buttons["rebootok"].tap()
        sleep(10)
        let rebootokok = app.buttons["rebootokok"].firstMatch
        XCTAssertTrue(rebootokok.waitForExistence(timeout: 120))
        rebootokok.tap()
        sleep(180)
        
    }
    func testCallReboot1420X2Flow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(30)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()

        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("#Sophos12345678")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()

        sleep(10)
        
        let reboot = app.buttons["reboot"].firstMatch
        XCTAssertTrue(reboot.waitForExistence(timeout: 120))
        reboot.tap()
        
        sleep(5)
        app.buttons["rebootno"].tap()
        sleep(5)
        reboot.tap()
        sleep(5)
        app.buttons["rebootok"].tap()
//        sleep(10)
//        app.buttons["rebootokok"].tap()
        sleep(130)
        
    }
   
    func testDetailBands420XFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(30)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:DE:DA:3A"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("#Sophos12345678")
        }
        sleep(2)
        let keep = app.switches["keep"]
        XCTAssertTrue(keep.waitForExistence(timeout: 120))
        keep.tapAtPosition(position: CGPoint(x: 143.75, y: 10.33))
        sleep(2)
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        let mSSID2GNUM = app.buttons["SSID2GNUM"].firstMatch
        XCTAssertTrue(mSSID2GNUM.waitForExistence(timeout: 120))
        mSSID2GNUM.tap()
        sleep(10)
        
        let mSSID2GNUM8 = app.buttons["SSID2GNUM2"].firstMatch
        XCTAssertTrue(mSSID2GNUM8.waitForExistence(timeout: 120))
        mSSID2GNUM8.tap()
        sleep(10)
        
        
        let mSSID2GAuth1 = app.buttons["SSID2GAuth1"].firstMatch
        XCTAssertTrue(mSSID2GAuth1.waitForExistence(timeout: 120))
        mSSID2GAuth1.tap()
        sleep(10)
        
        let auth0 = app.buttons["auth0"].firstMatch
        XCTAssertTrue(auth0.waitForExistence(timeout: 120))
        auth0.tap()
        sleep(5)
        
        
        mSSID2GAuth1.tap()
        sleep(5)
        let auth1 = app.buttons["auth1"].firstMatch
        XCTAssertTrue(auth1.waitForExistence(timeout: 120))
        auth1.tap()
        sleep(5)
        
        let psk2G2 = app.textFields["psk2G2"]
        XCTAssertTrue(psk2G2.waitForExistence(timeout: 120))
        psk2G2.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk2G2.clearText()
        psk2G2.typeText("Edimax1234")
        sleep(5)
        
        let mSSIDscroll = app.scrollViews["SSIDscroll"].firstMatch
        XCTAssertTrue(mSSIDscroll.waitForExistence(timeout: 120))
        mSSIDscroll.swipeDown()
        sleep(5)
        
        let psk2G3 = app.textFields["psk2G3"]
        XCTAssertTrue(psk2G3.waitForExistence(timeout: 120))
        psk2G3.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(2)
        psk2G3.clearText()
        sleep(2)
        psk2G3.typeText("1")
        sleep(5)
        
        mSSIDscroll.swipeDown()
        
        let psk2G6 = app.textFields["psk2G6"]
        XCTAssertTrue(psk2G6.waitForExistence(timeout: 120))
        psk2G6.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(2)
        psk2G6.clearText()
        sleep(2)
        psk2G6.typeText("Edimax1234")
        sleep(5)
        mSSIDscroll.swipeDown()
        sleep(5)
        let psk2G7 = app.textFields["psk2G7"]
        XCTAssertTrue(psk2G7.waitForExistence(timeout: 120))
        psk2G7.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        
        mSSIDscroll.swipeUp()
        
        sleep(5)
        
        //5g
        let mSSID5GNUM = app.buttons["SSID5GNUM"].firstMatch
        XCTAssertTrue(mSSID5GNUM.waitForExistence(timeout: 120))
        mSSID5GNUM.tap()
        sleep(10)
        
    //    let SSID5GNUM8 = app.buttons["SSID5GNUM2"].firstMatch
    //    XCTAssertTrue(SSID5GNUM8.waitForExistence(timeout: 120))
        mSSID2GNUM8.tap()
        sleep(10)
        
        
        let mSSID5GAuth1 = app.buttons["SSID5GAuth1"].firstMatch
        XCTAssertTrue(mSSID5GAuth1.waitForExistence(timeout: 120))
        mSSID5GAuth1.tap()
        sleep(10)
        
        //let auth0 = app.buttons["auth0"].firstMatch
        //XCTAssertTrue(auth0.waitForExistence(timeout: 120))
        auth0.tap()
        sleep(5)
        
        mSSID5GAuth1.tap()
        sleep(5)
        
        auth1.tap()
        sleep(5)
        
        let psk5G2 = app.textFields["psk5G2"]
        XCTAssertTrue(psk5G2.waitForExistence(timeout: 120))
        psk5G2.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk5G2.clearText()
        psk5G2.typeText("Edimax1234")
        sleep(5)
        mSSIDscroll.swipeLeft()
        sleep(5)
        let psk5G3 = app.textFields["psk5G3"]
        XCTAssertTrue(psk5G3.waitForExistence(timeout: 120))
        psk5G3.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
    //    SSIDscroll.swipeLeft()
        mSSIDscroll.swipeUp()
        sleep(5)
        let psk5G6 = app.textFields["psk5G6"]
        XCTAssertTrue(psk5G6.waitForExistence(timeout: 120))
        psk5G6.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk5G6.clearText()
        psk5G6.typeText("Edimax1234")
        sleep(5)
        
        mSSIDscroll.swipeUp()
        
        sleep(5)

        app.images["back20"].tap()
        sleep(20)
        
        funcbands.tap()
        sleep(5)
        
        app.buttons["Edit"].tap()
        sleep(5)
        app.buttons["Save"].tap()
        sleep(60)
        
        
    }
    func testDetailBands840EFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(20)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(10)
        
        let Edit = app.buttons["Edit"].firstMatch
        XCTAssertTrue(Edit.waitForExistence(timeout: 120))
        Edit.tap()
        sleep(5)
        
        let mSSID2GNUM = app.buttons["SSID2GNUM"].firstMatch
        XCTAssertTrue(mSSID2GNUM.waitForExistence(timeout: 120))
        mSSID2GNUM.tap()
        sleep(10)
        
        let mSSID2GNUM8 = app.buttons["SSID2GNUM2"].firstMatch
        XCTAssertTrue(mSSID2GNUM8.waitForExistence(timeout: 120))
        mSSID2GNUM8.tap()
        sleep(10)
        
        
        let mSSID2GAuth1 = app.buttons["SSID2GAuth1"].firstMatch
        XCTAssertTrue(mSSID2GAuth1.waitForExistence(timeout: 120))
        mSSID2GAuth1.tap()
        sleep(10)
        
        let auth0 = app.buttons["auth0"].firstMatch
        XCTAssertTrue(auth0.waitForExistence(timeout: 120))
        auth0.tap()
        sleep(5)
        
        
        mSSID2GAuth1.tap()
        sleep(5)
        let auth1 = app.buttons["auth1"].firstMatch
        XCTAssertTrue(auth1.waitForExistence(timeout: 120))
        auth1.tap()
        sleep(5)
        
        let psk2G0 = app.textFields["psk2G0"]
        XCTAssertTrue(psk2G0.waitForExistence(timeout: 120))
        psk2G0.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk2G0.clearText()
        psk2G0.typeText("Sophos AP6 840E CCDE20_2")
        sleep(5)
        
        let mSSIDscroll = app.scrollViews["SSIDscroll"].firstMatch
        XCTAssertTrue(mSSIDscroll.waitForExistence(timeout: 120))
        mSSIDscroll.swipeDown()
        sleep(5)
        
        let psk2G2 = app.textFields["psk2G2"]
        XCTAssertTrue(psk2G2.waitForExistence(timeout: 120))
        psk2G2.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk2G2.clearText()
        psk2G2.typeText("Edimax1234")
        sleep(5)
        
        mSSIDscroll.swipeDown()
        sleep(5)
        
        let psk2G3 = app.textFields["psk2G3"]
        XCTAssertTrue(psk2G3.waitForExistence(timeout: 120))
        psk2G3.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(2)
        psk2G3.clearText()
        sleep(2)
        psk2G3.typeText("1")
        sleep(5)
        
        mSSIDscroll.swipeDown()
        
        let psk2G6 = app.textFields["psk2G6"]
        XCTAssertTrue(psk2G6.waitForExistence(timeout: 120))
        psk2G6.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(2)
        psk2G6.clearText()
        sleep(2)
        psk2G6.typeText("Edimax1234")
        sleep(5)
        mSSIDscroll.swipeDown()
        sleep(5)
        let psk2G7 = app.textFields["psk2G7"]
        XCTAssertTrue(psk2G7.waitForExistence(timeout: 120))
        psk2G7.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        
        mSSIDscroll.swipeUp()
        
        sleep(5)
        
        //5g
        let mSSID5GNUM = app.buttons["SSID5GNUM"].firstMatch
        XCTAssertTrue(mSSID5GNUM.waitForExistence(timeout: 120))
        mSSID5GNUM.tap()
        sleep(10)
        
    //    let SSID5GNUM8 = app.buttons["SSID5GNUM2"].firstMatch
    //    XCTAssertTrue(SSID5GNUM8.waitForExistence(timeout: 120))
        mSSID2GNUM8.tap()
        sleep(10)
        
        
        let mSSID5GAuth1 = app.buttons["SSID5GAuth1"].firstMatch
        XCTAssertTrue(mSSID5GAuth1.waitForExistence(timeout: 120))
        mSSID5GAuth1.tap()
        sleep(10)
        
        //let auth0 = app.buttons["auth0"].firstMatch
        //XCTAssertTrue(auth0.waitForExistence(timeout: 120))
        auth0.tap()
        sleep(5)
        
        mSSID5GAuth1.tap()
        sleep(5)
        
        auth1.tap()
        sleep(5)
        
        let psk5G0 = app.textFields["psk5G0"]
        XCTAssertTrue(psk5G0.waitForExistence(timeout: 120))
        psk5G0.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk5G0.clearText()
        psk5G0.typeText("Sophos AP6 840E CCDE20_5")
        sleep(5)
        mSSIDscroll.swipeLeft()
        sleep(5)
        
        let psk5G2 = app.textFields["psk5G2"]
        XCTAssertTrue(psk5G2.waitForExistence(timeout: 120))
        psk5G2.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk5G2.clearText()
        psk5G2.typeText("Edimax1234")
        sleep(5)
        mSSIDscroll.swipeLeft()
        sleep(5)
        let psk5G3 = app.textFields["psk5G3"]
        XCTAssertTrue(psk5G3.waitForExistence(timeout: 120))
        psk5G3.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        mSSIDscroll.swipeLeft()
//        SSIDscroll.swipeUp()
//        sleep(5)
        let psk5G6 = app.textFields["psk5G6"]
        XCTAssertTrue(psk5G6.waitForExistence(timeout: 120))
        psk5G6.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk5G6.clearText()
        psk5G6.typeText("Edimax1234")
        sleep(5)
        
        mSSIDscroll.swipeUp()
        
        sleep(5)
        
        //6g
        let mSSID6GNUM = app.buttons["SSID6GNUM"].firstMatch
        XCTAssertTrue(mSSID6GNUM.waitForExistence(timeout: 120))
        mSSID6GNUM.tap()
        sleep(10)
        
     //   let SSID2GNUM8 = app.buttons["SSID2GNUM2"].firstMatch
     //   XCTAssertTrue(SSID2GNUM8.waitForExistence(timeout: 120))
        mSSID2GNUM8.tap()
        sleep(10)
        
        
        let mSSID6GAuth1 = app.buttons["SSID6GAuth1"].firstMatch
        XCTAssertTrue(mSSID6GAuth1.waitForExistence(timeout: 120))
        mSSID6GAuth1.tap()
        sleep(10)
        
        let auth06G = app.buttons["auth0"].firstMatch
        XCTAssertTrue(auth06G.waitForExistence(timeout: 120))
        auth06G.tap()
        sleep(5)
        mSSIDscroll.swipeUp()
        sleep(5)
        
        mSSID6GAuth1.tap()
        sleep(5)
        let auth16G = app.buttons["auth2"].firstMatch
        XCTAssertTrue(auth16G.waitForExistence(timeout: 120))
        auth16G.tap()
        sleep(5)
        
        mSSID6GAuth1.tap()
        sleep(5)
        let auth26G = app.buttons["auth1"].firstMatch
        XCTAssertTrue(auth26G.waitForExistence(timeout: 120))
        auth26G.tap()
        sleep(5)
        
        let psk6G0 = app.textFields["psk6G0"]
        XCTAssertTrue(psk6G0.waitForExistence(timeout: 120))
        psk6G0.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk6G0.clearText()
        psk6G0.typeText("Sophos AP6 840E CCDE20_6")
        sleep(5)
        mSSIDscroll.swipeUp()
        sleep(5)
        
        let psk6G2 = app.textFields["psk6G2"]
        XCTAssertTrue(psk6G2.waitForExistence(timeout: 120))
        psk6G2.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk6G2.clearText()
        psk6G2.typeText("Edimax1234")
        sleep(5)
        mSSIDscroll.swipeUp()
        sleep(5)
        
        let psk6G3 = app.textFields["psk6G3"]
        XCTAssertTrue(psk6G3.waitForExistence(timeout: 120))
        psk6G3.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        mSSIDscroll.swipeUp()
        sleep(5)
        
        
        let psk6G6 = app.textFields["psk6G6"]
        XCTAssertTrue(psk6G6.waitForExistence(timeout: 120))
        psk6G6.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        psk6G6.clearText()
        psk6G6.typeText("Edimax1234")
        sleep(5)
        mSSIDscroll.swipeUp()
        sleep(5)
        let psk6G7 = app.textFields["psk6G7"]
        XCTAssertTrue(psk6G7.waitForExistence(timeout: 120))
        psk6G7.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        mSSIDscroll.swipeUp()
        sleep(5)
        //
        app.buttons["Save"].tap()
        sleep(100)
        
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        app.images["back20"].tap()
        sleep(60)
    }
    
    func testDetailBandsoff2GFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        let mBands2G = app.switches["Bands2G"].firstMatch
        XCTAssertTrue(mBands2G.waitForExistence(timeout: 120))
        mBands2G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(1)
        
        app.buttons["Save"].tap()
        sleep(60)
        
    }
    func testDetailBandsoff5GFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        let mSSIDscroll = app.scrollViews["SSIDscroll"].firstMatch
        XCTAssertTrue(mSSIDscroll.waitForExistence(timeout: 120))
        //SSIDscroll.swipeUp()
        mSSIDscroll.swipeUp(velocity: 20.0)
        sleep(5)
        
        let mBands5G = app.switches["Bands5G"].firstMatch
        XCTAssertTrue(mBands5G.waitForExistence(timeout: 120))
        mBands5G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(1)
        
        app.buttons["Save"].tap()
        sleep(40)
        
    }
    func testDetailBandsoff6GFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(20)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(5)

        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(5)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        let mSSIDscroll = app.scrollViews["SSIDscroll"].firstMatch
        XCTAssertTrue(mSSIDscroll.waitForExistence(timeout: 120))
        mSSIDscroll.swipeUp()
        //SSIDscroll.swipeUp(velocity: 20.0)
        sleep(5)
        
        let mBands6G = app.switches["Bands6G"].firstMatch
        XCTAssertTrue(mBands6G.waitForExistence(timeout: 120))
        mBands6G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(1)
        
        app.buttons["Save"].tap()
        sleep(60)
        
    }
    func testDetailBandson2GFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        let mBands2G = app.switches["Bands2G"].firstMatch
        XCTAssertTrue(mBands2G.waitForExistence(timeout: 120))
        mBands2G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(1)
        
        app.buttons["Save"].tap()
        sleep(60)
        
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        
        mBands2G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(1)
        
        let mSSIDscroll = app.scrollViews["SSIDscroll"].firstMatch
        XCTAssertTrue(mSSIDscroll.waitForExistence(timeout: 120))
        //SSIDscroll.swipeUp()
        mSSIDscroll.swipeUp(velocity: 20.0)
        sleep(5)
        
        let mBands5G = app.switches["Bands5G"].firstMatch
        XCTAssertTrue(mBands5G.waitForExistence(timeout: 120))
        mBands5G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(1)
        
        app.buttons["Save"].tap()
        sleep(60)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        
        mSSIDscroll.swipeDown()
        sleep(5)
        
        mBands2G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(1)
        mSSIDscroll.swipeUp(velocity: 20.0)
        sleep(5)
        mBands5G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(1)
        app.buttons["Save"].tap()
        sleep(60)
    }
    func testDetailBandson5GFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        let mSSIDscroll = app.scrollViews["SSIDscroll"].firstMatch
        XCTAssertTrue(mSSIDscroll.waitForExistence(timeout: 120))
        //SSIDscroll.swipeUp()
        mSSIDscroll.swipeUp(velocity: 20.0)
        sleep(5)
        
        let mBands5G = app.switches["Bands5G"].firstMatch
        XCTAssertTrue(mBands5G.waitForExistence(timeout: 120))
        mBands5G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(1)
        
        app.buttons["Save"].tap()
        sleep(60)
        
    }
    func testDetailBandson6GFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(30)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        let mSSIDscroll = app.scrollViews["SSIDscroll"].firstMatch
        XCTAssertTrue(mSSIDscroll.waitForExistence(timeout: 120))
        mSSIDscroll.swipeUp()
        
        sleep(5)
        
        let mBands6G = app.switches["Bands6G"].firstMatch
        XCTAssertTrue(mBands6G.waitForExistence(timeout: 120))
        mBands6G.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(1)
        
        app.buttons["Save"].tap()
        sleep(120)
        
    }
    func testDetailDateAndTimeFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(30)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)
        
        let scroll = app.scrollViews["Main"].firstMatch
        XCTAssertTrue(scroll.waitForExistence(timeout: 120))
        scroll.swipeLeft()
        sleep(10)
        
        app.buttons["Date And Time"].tap()
        sleep(10)
        
        app.buttons["Edit"].tap()
        sleep(5)
        
        
        let mDatescroll = app.scrollViews["Datescroll"].firstMatch
        XCTAssertTrue(mDatescroll.waitForExistence(timeout: 120))
        mDatescroll.swipeUp()
        sleep(5)
        
        let mOpentimezone = app.buttons["Opentimezone"].firstMatch
        XCTAssertTrue(mOpentimezone.waitForExistence(timeout: 120))
        mOpentimezone.tap()
        sleep(5)
        
        
        let zone = app.buttons["zone(GMT-12:00) Eniwetok, Kwajalein, International Date Line West"].firstMatch
        XCTAssertTrue(zone.waitForExistence(timeout: 120))
        //zone.tap()
        zone.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        
        let mNTPTimeServerHours = app.textFields["NTPTimeServerHours"]
        XCTAssertTrue(mNTPTimeServerHours.waitForExistence(timeout: 120))
        mNTPTimeServerHours.tap()
        sleep(5)
        
        let mFromNTPServerName = app.textFields["FromNTPServerName"]
        XCTAssertTrue(mFromNTPServerName.waitForExistence(timeout: 120))
        mFromNTPServerName.tap()
        sleep(5)
        
        let mNTPServerType = app.buttons["NTPServerType"].firstMatch
        XCTAssertTrue(mNTPServerType.waitForExistence(timeout: 120))
        mNTPServerType.tap()
        sleep(5)
        
        let mNTPServerType0 = app.buttons["NTPServerTypeGlobal"].firstMatch
        XCTAssertTrue(mNTPServerType0.waitForExistence(timeout: 120))
        mNTPServerType0.tap()
        sleep(5)
        
        
        let mToggleDaylightSaving = app.buttons["ToggleDaylightSaving"].firstMatch
        XCTAssertTrue(mToggleDaylightSaving.waitForExistence(timeout: 120))
        mToggleDaylightSaving.tap()
        sleep(5)
        
        mToggleDaylightSaving.tap()
        sleep(5)
        
        let mToggleNTPServer = app.switches["ToggleNTPServer"].firstMatch
        XCTAssertTrue(mToggleNTPServer.waitForExistence(timeout: 120))
        mToggleNTPServer.tapAtPosition(position: CGPoint(x: 334.3, y: 20.33))
        sleep(5)
        
        mDatescroll.swipeDown()
        sleep(5)
        
        let mTimeFromYourPhone = app.buttons["TimeFromYourPhone"].firstMatch
        XCTAssertTrue(mTimeFromYourPhone.waitForExistence(timeout: 120))
        mTimeFromYourPhone.tap()
        sleep(5)
       
        
        let mLocalTime0 = app.buttons["LocalTime0"].firstMatch
        XCTAssertTrue(mLocalTime0.waitForExistence(timeout: 120))
        mLocalTime0.tap()
        sleep(5)
        
        let mLocalTimeYear = app.buttons["LocalTimeYear2023"].firstMatch
        XCTAssertTrue(mLocalTimeYear.waitForExistence(timeout: 120))
        mLocalTimeYear.tap()
        sleep(5)
        
        let mLocalTime1 = app.buttons["LocalTime1"].firstMatch
        XCTAssertTrue(mLocalTime1.waitForExistence(timeout: 120))
        mLocalTime1.tap()
        sleep(5)
        
        let mLocalTimeMonth = app.buttons["LocalTimeMonthMarch"].firstMatch
        XCTAssertTrue(mLocalTimeMonth.waitForExistence(timeout: 120))
        mLocalTimeMonth.tap()
        sleep(5)
        
        let mLocalTime2 = app.buttons["LocalTime2"].firstMatch
        XCTAssertTrue(mLocalTime2.waitForExistence(timeout: 120))
        mLocalTime2.tap()
        sleep(5)
        
        let LocalTimeDay = app.buttons["LocalTimeDay15"].firstMatch
        XCTAssertTrue(LocalTimeDay.waitForExistence(timeout: 120))
        LocalTimeDay.tap()
        sleep(5)
        
        let mLocalTime3 = app.buttons["LocalTime3"].firstMatch
        XCTAssertTrue(mLocalTime3.waitForExistence(timeout: 120))
        mLocalTime3.tap()
        sleep(5)
        
        let mLocalTimeHours = app.buttons["LocalTimeHours15"].firstMatch
        XCTAssertTrue(mLocalTimeHours.waitForExistence(timeout: 120))
        mLocalTimeHours.tap()
        sleep(5)
        
        let mLocalTime4 = app.buttons["LocalTime4"].firstMatch
        XCTAssertTrue(mLocalTime4.waitForExistence(timeout: 120))
        mLocalTime4.tap()
        sleep(5)
        
        let mLocalTimeMinutes = app.buttons["LocalTimeMinutes15"].firstMatch
        XCTAssertTrue(mLocalTimeMinutes.waitForExistence(timeout: 120))
        mLocalTimeMinutes.tap()
        sleep(5)
        
        let mLocalTime5 = app.buttons["LocalTime5"].firstMatch
        XCTAssertTrue(mLocalTime5.waitForExistence(timeout: 120))
        mLocalTime5.tap()
        sleep(5)
        
        let mLocalTimeSeconds = app.buttons["LocalTimeSeconds15"].firstMatch
        XCTAssertTrue(mLocalTimeSeconds.waitForExistence(timeout: 120))
        mLocalTimeSeconds.tap()
        sleep(5)
        
        app.images["back20"].tap()
        sleep(20)
        app.buttons["Date And Time"].tap()
        sleep(5)
        app.buttons["Edit"].tap()
        sleep(5)
        app.buttons["Save"].tap()
        sleep(20)
 
    }
    func testDetailLANPortFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)
        
        let lanport = app.buttons["lanport"].firstMatch
        XCTAssertTrue(lanport.waitForExistence(timeout: 120))
        lanport.tap()

        sleep(5)

        app.buttons["Edit"].tap()
        sleep(5)

        let laniptype = app.buttons["laniptype"].firstMatch
        XCTAssertTrue(laniptype.waitForExistence(timeout: 120))
        laniptype.tap()

        sleep(5)
        
        let laniptypedhcp = app.buttons["laniptypeoneDHCP"].firstMatch
        XCTAssertTrue(laniptypedhcp.waitForExistence(timeout: 120))
        laniptypedhcp.tap()

        sleep(5)
        
        //GateWayflag , GateWayflagFrom DHCP , PriAddrflag , PriAddrflagFrom DHCP , SecAddrflag , SecAddrflagFrom DHCP
        let gateWayflag = app.buttons["GateWayflag"].firstMatch
        XCTAssertTrue(gateWayflag.waitForExistence(timeout: 120))
        gateWayflag.tap()

        sleep(5)
        
        let gateWayflagUserDefined = app.buttons["GateWayflagUser-Defined"].firstMatch
        XCTAssertTrue(gateWayflagUserDefined.waitForExistence(timeout: 120))
        gateWayflagUserDefined.tap()
        sleep(5)
        
        let gateWayTypeTextDHCP = app.textFields["GateWayTypeTextDHCP"]
        XCTAssertTrue(gateWayTypeTextDHCP.waitForExistence(timeout: 120))
        gateWayTypeTextDHCP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(15)
        
        gateWayflag.tap()
        sleep(5)
        
        let gateWayflagFromDHCP = app.buttons["GateWayflagFrom DHCP"].firstMatch
        XCTAssertTrue(gateWayflagFromDHCP.waitForExistence(timeout: 120))
        gateWayflagFromDHCP.tap()

        sleep(5)
        
        let priAddrflag = app.buttons["PriAddrflag"].firstMatch
        XCTAssertTrue(priAddrflag.waitForExistence(timeout: 120))
        priAddrflag.tap()

        sleep(5)
        
        let priAddrflagUserDefined = app.buttons["PriAddrflagUser-Defined"].firstMatch
        XCTAssertTrue(priAddrflagUserDefined.waitForExistence(timeout: 120))
        priAddrflagUserDefined.tap()

        sleep(5)
        
        let priAddTypeTextDHCP = app.textFields["PriAddTypeTextDHCP"]
        XCTAssertTrue(priAddTypeTextDHCP.waitForExistence(timeout: 120))
        priAddTypeTextDHCP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(15)
        
        
        priAddrflag.tap()

        sleep(5)
        
        let priAddrflagFromDHCP = app.buttons["PriAddrflagFrom DHCP"].firstMatch
        XCTAssertTrue(priAddrflagFromDHCP.waitForExistence(timeout: 120))
        priAddrflagFromDHCP.tap()

        sleep(5)
        
        let secAddrflag = app.buttons["SecAddrflag"].firstMatch
        XCTAssertTrue(secAddrflag.waitForExistence(timeout: 120))
        secAddrflag.tap()

        sleep(5)
        
        let secAddrflagUserDefined = app.buttons["SecAddrflagUser-Defined"].firstMatch
        XCTAssertTrue(secAddrflagUserDefined.waitForExistence(timeout: 120))
        secAddrflagUserDefined.tap()

        sleep(5)
        
        let secAddTypeTextDHCP = app.textFields["SecAddTypeTextDHCP"]
        XCTAssertTrue(secAddTypeTextDHCP.waitForExistence(timeout: 120))
        secAddTypeTextDHCP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        
        
        SecAddrflag.tap()

        sleep(5)
        
        
        
        let secAddrflagFromDHCP = app.buttons["SecAddrflagFrom DHCP"].firstMatch
        XCTAssertTrue(secAddrflagFromDHCP.waitForExistence(timeout: 120))
        secAddrflagFromDHCP.tap()

        sleep(5)
        
        laniptype.tap()

        sleep(5)

        let laniptypeone = app.buttons["laniptypeoneStatic IP"].firstMatch
        XCTAssertTrue(laniptypeone.waitForExistence(timeout: 120))
        laniptypeone.tap()

        sleep(5)
        
        //SETUP STATIC IP
        
        let ipaddress = app.textFields["ipaddress"]
        XCTAssertTrue(ipaddress.waitForExistence(timeout: 120))
        ipaddress.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        ipaddress.clearText()
        ipaddress.typeText("192.168.18.100")
        sleep(5)
        
        let subnetTypetext = app.textFields["SubnetTypetext"]
        XCTAssertTrue(subnetTypetext.waitForExistence(timeout: 120))
        subnetTypetext.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        
        let lanscroll = app.scrollViews["lanscroll"].firstMatch
        XCTAssertTrue(lanscroll.waitForExistence(timeout: 120))
        lanscroll.swipeUp()

        sleep(5)
        
        let priAddTypeText = app.textFields["PriAddTypeText"]
        XCTAssertTrue(priAddTypeText.waitForExistence(timeout: 120))
        priAddTypeText.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        
        let secAddTypeText = app.textFields["SecAddTypeText"]
        XCTAssertTrue(secAddTypeText.waitForExistence(timeout: 120))
        secAddTypeText.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        
        let dHCPserver = app.switches["DHCPserver"].firstMatch
        XCTAssertTrue(dHCPserver.waitForExistence(timeout: 120))
        dHCPserver.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        sleep(20)
        dHCPserver.tapAtPosition(position: CGPoint(x: 334.3, y: 5.33))
        
        sleep(5)

        lanscroll.swipeUp()

        sleep(5)
        
        let startIP = app.textFields["startIP"]
        XCTAssertTrue(startIP.waitForExistence(timeout: 120))
        startIP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        lanscroll.swipeUp()
        sleep(5)
        let secIP = app.textFields["secIP"]
        XCTAssertTrue(secIP.waitForExistence(timeout: 120))
        secIP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        lanscroll.swipeUp()
        sleep(5)
        let lANDHCPServer1 = app.textFields["LANDHCPServer1"]
        XCTAssertTrue(lANDHCPServer1.waitForExistence(timeout: 120))
        lANDHCPServer1.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        lanscroll.swipeUp()
        sleep(5)
        let lANDHCPServer2 = app.textFields["LANDHCPServer2"]
        XCTAssertTrue(lANDHCPServer2.waitForExistence(timeout: 120))
        lANDHCPServer2.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        lanscroll.swipeUp()
        sleep(5)
        let lANDHCPServer4 = app.textFields["LANDHCPServer4"]
        XCTAssertTrue(lANDHCPServer4.waitForExistence(timeout: 120))
        lANDHCPServer4.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        sleep(5)
        lanscroll.swipeUp()
        sleep(5)

        let leasetime = app.buttons["DHCPleasetime"].firstMatch
        XCTAssertTrue(leasetime.waitForExistence(timeout: 120))
        leasetime.tap()
        sleep(5)
        
        let cancelDialog = app.buttons["leasetime0One Month"].firstMatch
        XCTAssertTrue(cancelDialog.waitForExistence(timeout: 120))
        cancelDialog.tap()

        sleep(5)

        app.images["back20"].tap()
        sleep(20)
        lanport.tap()
        sleep(5)
        app.buttons["Edit"].tap()
        sleep(5)
        
        //SETUP STATIC IP
        laniptype.tap()
        sleep(5)
        laniptypeone.tap()

        sleep(5)
        ipaddress.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        ipaddress.clearText()
        ipaddress.typeText("192.168.18.100")
        sleep(5)
        let gateWayTypetext = app.textFields["GateWayTypetext"]
        XCTAssertTrue(gateWayTypetext.waitForExistence(timeout: 120))
        gateWayTypetext.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        gateWayTypetext.clearText()
        gateWayTypetext.typeText("0.0.0.0")
        sleep(5)
        app.buttons["Save"].tap()
        sleep(20)
        
        gateWayTypetext.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        gateWayTypetext.clearText()
        gateWayTypetext.typeText("192.168.18.1")
        sleep(5)
        
        lanscroll.swipeDown()
        sleep(3)

        laniptype.tap()

        sleep(5)
        laniptypedhcp.tap()

        sleep(5)
        app.buttons["Save"].tap()
        sleep(120)
    }
    func testDetailPingFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(20)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
         
        sleep(10)
        let scroll = app.scrollViews["Main"].firstMatch
        XCTAssertTrue(scroll.waitForExistence(timeout: 120))
        scroll.swipeLeft()
        sleep(10)
        
        app.buttons["Ping Test"].tap()
        let pingIP = app.textFields["pingIP"]
        XCTAssertTrue(pingIP.waitForExistence(timeout: 120))
        pingIP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
      //  if password.isHittable {
        pingIP.typeText("8.8.8.8")
      //  }
        app.buttons["pingIPExecute"].tap()
        sleep(15)
        
        pingIP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
      //  if password.isHittable {
        pingIP.clearText()
        sleep(5)
        pingIP.typeText("2404:6800:4012:2::2004")
      //  }
        app.buttons["pingIPExecute"].tap()
        sleep(15)
        
    }
    func testDetailSystemFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        
        XCTAssertTrue(name.waitForExistence(timeout: 120))

        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))//x:143.75, y: 72.0
        sleep(5)
        name.typeText("admin")
        sleep(5)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)
   
        app.buttons["Edit"].tap()
        sleep(5)
        
        let prodname = app.textFields["prodname"]
        XCTAssertTrue(prodname.waitForExistence(timeout: 120))
        prodname.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
       // if prodname.isHittable {
            prodname.clearText()
            sleep(5)
        prodname.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        prodname.clearText()
        sleep(5)
            prodname.typeText("cAP00AABBCCDE20test中")
       // }
        
        app.buttons["Save"].tap()
        sleep(5)
        prodname.clearText()
        sleep(5)
        prodname.typeText("cAP00AABBCCDE20test1")
        app.buttons["Save"].tap()
        sleep(60)
         
    }
    func testDetailTracerouteFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(20)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)
        let scroll = app.scrollViews["Main"].firstMatch
        XCTAssertTrue(scroll.waitForExistence(timeout: 120))
        scroll.swipeLeft()
        sleep(10)
        
        app.buttons["Traceroute Test"].tap()
        let tracerouteIP = app.textFields["tracerouteIP"]
        XCTAssertTrue(tracerouteIP.waitForExistence(timeout: 120))
        tracerouteIP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
      //  if password.isHittable {
        tracerouteIP.typeText("8.8.8.8")
      //  }
        app.buttons["tracerouteIPExecute"].tap()

        sleep(20)
    
    }
    func testDetailWiFiDisableFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(10)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)
        
        app.buttons["wifidisable"].tap()
        sleep(8)
        app.buttons["offline"].tap()
        sleep(8)
        app.buttons["wifidisable"].tap()
        sleep(8)
        app.buttons["online"].tap()
        sleep(60)
        
    }
    func testDetailWiFiEnableFlow() throws
    {
        let app = XCUIApplication()
        app.launch()
        
        sleep(30)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
       
        sleep(10)

        app.buttons["wifienable"].tap()
        sleep(120)
  
    }
    func testMenuFlow() {
        
        let app = XCUIApplication()
        app.launch()

        sleep(30)
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("123456")
        }
        sleep(5)
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
        sleep(10)
//        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
//        password.clearText()
//        sleep(5)
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")
        sleep(1)
        
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        sleep(1)
        
        let keep = app.switches["keep"]
        XCTAssertTrue(keep.waitForExistence(timeout: 120))
        keep.tapAtPosition(position: CGPoint(x: 143.75, y: 10.33))
        
        sleep(1)
        
        login.tap()
       
        sleep(5)
    
        let detailMenu = app.buttons["DetailMenu"].firstMatch
        XCTAssertTrue(detailMenu.waitForExistence(timeout: 120))
        detailMenu.tap()
        
        sleep(10)
       }
    func testUnknow()
    {
        sleep(10)
        let app = XCUIApplication()
        app.launch()
       
        sleep(20) // change wifi

        
        let central = app.buttons["checkLogin00:00:1e:00:01:af"]
        
        XCTAssertTrue(central.waitForExistence(timeout: 120))
        central.tap()
        sleep(10)
       
        let detailMenu = app.buttons["DetailMenu"].firstMatch
        XCTAssertTrue(detailMenu.waitForExistence(timeout: 120))
        detailMenu.tap()
        sleep(5)
        
        let about = app.staticTexts["menu_About"]
        XCTAssertTrue(about.waitForExistence(timeout: 30))
        about.tapAtPosition(position: CGPoint(x: 23.75, y: 20.0))
        
        sleep(5)
        detailMenu.tap()
        sleep(5)
        let setting = app.staticTexts["menu_Settings"]
        XCTAssertTrue(setting.waitForExistence(timeout: 30))
        setting.tapAtPosition(position: CGPoint(x: 23.75, y: 20.0))
        
        sleep(5)
        
        app.images["back20"].tap()
        sleep(5)
    }
    func testZero() throws
    {
        var sanityTakeTime = ""
//        sleep(10)
        let app = XCUIApplication()
        app.launch()

        sleep(20)
        
        let myDevice = app.buttons["checkLogin00:AA:BB:CC:DE:20"].firstMatch
        XCTAssertTrue(myDevice.waitForExistence(timeout: 120))
        myDevice.tap()
        
        sleep(5)

        let name = app.textFields["username"]
        XCTAssertTrue(name.waitForExistence(timeout: 120))
        name.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        name.typeText("admin")

        sleep(2)

        let password = app.secureTextFields["password"]
        XCTAssertTrue(password.waitForExistence(timeout: 120))
        password.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        if password.isHittable {
            password.typeText("1234")
        }
        let login = app.buttons["Login"].firstMatch
        XCTAssertTrue(login.waitForExistence(timeout: 120))
        login.tap()
        sleep(5)
        let date1 = Date()
        let delete1 = app.buttons["delete00:AA:BB:CC:DE:20"]
        XCTAssertTrue(delete1.waitForExistence(timeout: 120))
        delete1.tap()
        let deleteOther1 = app.buttons["checkLogin00:AA:BB:DE:DA:3A"]
        XCTAssertTrue(deleteOther1.waitForExistence(timeout: 120))
        if deleteOther1.isEnabled {
            sanityTakeTime += "Remove : \(Date().timeIntervalSince(date1))" + "\n"
            debugPrint(s: "iOS", data: sanityTakeTime)
        }
        
        sleep(5)
        
        app.images["back20"].tap()
        sleep(5)
//
//        let central = app.buttons["checkLogin00:00:1e:00:01:af"]
//
//        XCTAssertTrue(central.waitForExistence(timeout: 120))
//        central.tap()
//        sleep(10)
//
//        let delete2 = app.buttons["deleteAPnotFound00:00:1e:00:01:af"]
//        XCTAssertTrue(delete2.waitForExistence(timeout: 120))
//        delete2.tap()
//
//        sleep(5)
        
//        let deleteOther1 = app.buttons["checkLogin00:AA:BB:DE:DA:3A"]
//        XCTAssertTrue(deleteOther1.waitForExistence(timeout: 120))
        deleteOther1.tap()
        sleep(2)
        let date2 = Date()
        let lanport = app.buttons["lanport"].firstMatch
        XCTAssertTrue(lanport.waitForExistence(timeout: 120))
        lanport.tap()
        //if lanport.isEnabled {
        sanityTakeTime += "Click LANPort : \(Date().timeIntervalSince(date2))" + "\n"
        debugPrint(s: "iOS", data: sanityTakeTime)
        
        //}
        sleep(2)
        
        let funcbands = app.buttons["Funcbands"].firstMatch
        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(2)
        
        let scroll = app.scrollViews["Main"].firstMatch
        XCTAssertTrue(scroll.waitForExistence(timeout: 120))
        scroll.swipeLeft()
        sleep(2)
        
        app.buttons["Date And Time"].tap()
        sleep(2)
        
        app.images["back20"].tap()
        sleep(1)
        
        let Menu = app.images["Menu"]
        XCTAssertTrue(Menu.waitForExistence(timeout: 120))
        Menu.tap()
        
        
        let About = app.staticTexts["menu_About"]
        XCTAssertTrue(About.waitForExistence(timeout: 60))
        About.tapAtPosition(position: CGPoint(x: 23.75, y: 20.0))
        
        sleep(2)
        Menu.tap()
        sleep(2)
        let setting = app.staticTexts["menu_Settings"]
        XCTAssertTrue(setting.waitForExistence(timeout: 60))
        setting.tapAtPosition(position: CGPoint(x: 23.75, y: 20.0))
        
        sleep(2)
        
        let OpenfaceID = app.switches["OpenfaceID"]
        XCTAssertTrue(OpenfaceID.waitForExistence(timeout: 60))
        OpenfaceID.tapAtPosition(position: CGPoint(x: 334.3, y: 10.33))
        sleep(2)
        
        let closeFaceErr = app.buttons["closeFaceErr"]
        XCTAssertTrue(closeFaceErr.waitForExistence(timeout: 60))
        closeFaceErr.tap()
        sleep(2)
        
        Menu.tap()
        sleep(2)
        
        let goAP = app.buttons["goAP"].firstMatch
        XCTAssertTrue(goAP.waitForExistence(timeout: 120))
        goAP.tap()
        sleep(1)
        
//        let deleteOther1 = app.buttons["checkLogin00:AA:BB:DE:DA:3A"]
//        XCTAssertTrue(deleteOther1.waitForExistence(timeout: 120))
        deleteOther1.tap()
        sleep(2)
        
        app.buttons["Edit"].tap()
        sleep(2)
        let prodname = app.textFields["prodname"]
        XCTAssertTrue(prodname.waitForExistence(timeout: 120))
        prodname.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
        prodname.clearText()
        sleep(2)
        prodname.typeText("AP00AABBCCDE20test66")
        app.buttons["Save"].tap()
        sleep(2)
        
        let notconnect = app.buttons["notconnect"].firstMatch
        XCTAssertTrue(notconnect.waitForExistence(timeout: 120))
        notconnect.tap()

        sleep(2)
        app.images["back20"].tap()
        sleep(1)
        notconnect.tap()
        sleep(2)

//        let lanport = app.buttons["lanport"].firstMatch
//        XCTAssertTrue(lanport.waitForExistence(timeout: 120))
        lanport.tap()

        sleep(2)
        notconnect.tap()

        sleep(2)
        app.buttons["Edit"].tap()
        sleep(2)
//        app.buttons["Save"].tap()
//        sleep(2)
//        notconnect.tap()
//        sleep(2)
        app.images["back20"].tap()
        sleep(2)
        
        notconnect.tap()

        sleep(2)
        
//        let funcbands = app.buttons["Funcbands"].firstMatch
//        XCTAssertTrue(funcbands.waitForExistence(timeout: 120))
        funcbands.tap()
        sleep(2)
        notconnect.tap()
        sleep(2)
        app.buttons["Edit"].tap()
        sleep(2)
//        app.buttons["Save"].tap()
//        sleep(2)
//        notconnect.tap()
//        sleep(2)
        app.images["back20"].tap()
        sleep(2)
        notconnect.tap()
        sleep(2)
        
        let clients = app.buttons["Wireless Clients"].firstMatch
        XCTAssertTrue(clients.waitForExistence(timeout: 120))
        clients.tap()
        sleep(2)
        
        notconnect.tap()
        sleep(2)
        
        
//        let scroll = app.scrollViews["Main"].firstMatch
//        XCTAssertTrue(scroll.waitForExistence(timeout: 120))
        scroll.swipeLeft()
        sleep(2)
        
        app.buttons["Date And Time"].tap()
        sleep(2)
        
        notconnect.tap()
        sleep(2)
        app.buttons["Edit"].tap()
        sleep(2)
//        app.buttons["Save"].tap()
//        sleep(2)
//        notconnect.tap()
//        sleep(2)
        app.images["back20"].tap()
        sleep(2)
        notconnect.tap()
        sleep(2)
        
        app.buttons["Ping Test"].tap()
        let pingIP = app.textFields["pingIP"]
        XCTAssertTrue(pingIP.waitForExistence(timeout: 120))
        pingIP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
      //  if password.isHittable {
        pingIP.typeText("8.8.8.8")
      //  }
        app.buttons["pingIPExecute"].tap()
        sleep(2)
        
        notconnect.tap()
        sleep(2)
        
        app.buttons["Traceroute Test"].tap()
        let tracerouteIP = app.textFields["tracerouteIP"]
        XCTAssertTrue(tracerouteIP.waitForExistence(timeout: 120))
        tracerouteIP.tapAtPosition(position: CGPoint(x: 143.75, y: 20.0))
      //  if password.isHittable {
        tracerouteIP.typeText("8.8.8.8")
      //  }
        app.buttons["tracerouteIPExecute"].tap()
        sleep(2)
        
        notconnect.tap()
        sleep(2)
        
        
        app.buttons["wifidisable"].tap()
        sleep(2)
        app.buttons["online"].tap()
        sleep(5)
        
        notconnect.tap()
        sleep(2)
        
      //  app.buttons["reboot"].tap()
        let reboot = app.buttons["reboot"]
        XCTAssertTrue(reboot.waitForExistence(timeout: 120))
        reboot.tap()
        sleep(2)
        app.buttons["rebootok"].tap()
        sleep(2)
        
        notconnect.tap()
        sleep(2)
        
        app.images["back20"].tap()
        sleep(5)
        deleteOther1.tap()
        sleep(10)

        let deletedeleteOther1 = app.buttons["deleteAPnotFound00:AA:BB:DE:DA:3A"]
        XCTAssertTrue(deletedeleteOther1.waitForExistence(timeout: 120))
        deletedeleteOther1.tap()
        sleep(10)
    }
    func takeScreenshot(name: String) {
      let fullScreenshot = XCUIScreen.main.screenshot()

      let screenshot = XCTAttachment(uniformTypeIdentifier: "public.png", name: "Screenshot-\(name)-\(UIDevice.current.name).png", payload: fullScreenshot.pngRepresentation, userInfo: nil)
      screenshot.lifetime = .keepAlways
      add(screenshot)
    }
    func debugPrint(s:String,data:String)
    {
      var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
      let documentsDirectory = paths[0]
      let formatter = DateFormatter()
      formatter.dateFormat = "dd-MM-yyyy"
      let dateString = formatter.string(from: Date())
      let fileName = "\(dateString).log"
      let logFilePath = (documentsDirectory as NSString).appendingPathComponent(fileName)
      var dump = ""

      if FileManager.default.fileExists(atPath: logFilePath) {
          dump =  try! String(contentsOfFile: logFilePath, encoding: String.Encoding.utf8)
      }

      do {
          // Write to the file
          try  data.write(toFile: logFilePath, atomically: true, encoding: String.Encoding.utf8)
          
      } catch let error as NSError {
          print("Failed writing to log file: \(logFilePath), Error: " + error.localizedDescription)
      }
    }
}
extension XCUIElement {
    func clearText() {
            guard let stringValue = self.value as? String else {
                return
            }

            var deleteString = String()
            for _ in stringValue {
                deleteString += XCUIKeyboardKey.delete.rawValue
            }
            typeText(deleteString)
        }
}
extension XCUIElement {
    func tapAtPosition(position:CGPoint) {
        let coor = self.self.coordinate(withNormalizedOffset: CGVector(dx: 0.0, dy: 0.0)).withOffset(CGVector(dx: position.x, dy: position.y))
        coor.tap()
    }
}
extension XCTestCase {
    func waitAndCheck(_ description: String = "", _ timeout: Double = 0.5, callback: () -> Bool) {
        let exp = self.expectation(description: description)
        let result = XCTWaiter.wait(for: [exp], timeout: timeout)
        if result == XCTWaiter.Result.timedOut {
            XCTAssertTrue(callback())
        }
        else {
            XCTFail("Timeout waiting \(timeout) for \(description)")
        }
    }

    func wait(_ description: String = "", _ timeout: Double = 0.5, callback: () -> Void) {
        let exp = self.expectation(description: description)
        let result = XCTWaiter.wait(for: [exp], timeout: timeout)
        if result == XCTWaiter.Result.timedOut {
            callback()
        }
        else {
            XCTFail("Timeout waiting \(timeout) for \(description)")
        }
    }
}
