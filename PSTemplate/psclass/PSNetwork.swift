//
//  PSNetwork.swift
//  pslib
//
//  Created by Edimax on 2021/10/28.
//

import SwiftUI
import Network
import Darwin
import OSLog
//import Socket

class PSNetwork: ObservableObject {
    
    var url:String?
    var method:String?
    var mDigestAccount:String?
    var mDigestPw:String?
    var body:Data?
   // @Published
    var result:Data?
    @Published var tResult:Decodable? {
        didSet {
            self.objectWillChange.send()
        }
    }
    
    func getResult<T:Decodable>(model: T.Type,completion:@escaping(Int)->()) {
        let newurl = URL(string: self.url!)
        var request = URLRequest(url: newurl!)
        request.httpMethod = self.method
        if self.method == "POST"
        {
            request.httpBody = self.body
        }
        let config = URLSessionConfiguration.default
            //config.isDiscretionary = true
            config.sessionSendsLaunchEvents = true
        
        let adapter = CustomURLSessionAdapter(configuration: config)
        adapter.mDigestAccount = mDigestAccount
        adapter.mDigestPw = mDigestPw
        adapter.urlSession.dataTask(with: request) { data, response, error in
       // URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let datas = data else{
                completion(500)
                return
            }
          //  let str = String(decoding: datas, as: UTF8.self)
         //   print("data : \(str)")
            //os_log("response error:\(error as! NSObject)")
           // os_log("response:\(str)")
           // if error == nil || (response as! HTTPURLResponse).statusCode != 200 {
           //     print("(HTTPURLResponse).statusCode: \((response as! HTTPURLResponse).statusCode)")
           //     let fields = (response as! HTTPURLResponse).allHeaderFields as! [String:String]
          //  print("fields: \(fields)")
              //  let realm:String = self.getAuthenticationKey(headerInfo: fields,key: "realm")!
              //  let nonce:String = self.getAuthenticationKey(headerInfo: fields,key: "nonce")!
           // }
            
            DispatchQueue.main.async { [unowned self] in //background
                self.result = datas
                
                do{
                    self.tResult  = try model.init(jsonData: self.result!)
                    self.result = nil
                    completion(200)
                } catch {
                //    print(error)
                    self.result = nil
                    completion(400)
                }
            }
        }.resume()
    }
}
extension Decodable {
    init(jsonData: Data) throws {
        self = try JSONDecoder().decode(Self.self, from:jsonData)
    }
}
class CustomURLSessionAdapter: URLSessionAdapter {
    var mDigestAccount:String?
    var mDigestPw:String?
//    convenience init() {
//        self.init(configuration: URLSessionConfiguration.default)
//    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if challenge.previousFailureCount == 0 {
            
            let credential = URLCredential(user: mDigestAccount!, password: mDigestPw!, persistence: .none)
            completionHandler(.useCredential, credential)
        } else {
            challenge.sender?.cancel(challenge)
            completionHandler(.cancelAuthenticationChallenge, nil)
        }
    }

}
