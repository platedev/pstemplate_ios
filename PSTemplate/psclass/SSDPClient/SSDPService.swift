import Foundation

public class SSDPService {
    /// The host of service
    public internal(set) var host: String
    /// The value of `LOCATION` header
    public internal(set) var location: String?
    /// The value of `SERVER` header
    public internal(set) var server: String?
    /// The value of `ST` header
    public internal(set) var searchTarget: String?
    /// The value of `USN` header
    public internal(set) var uniqueServiceName: String?
    
    public internal(set) var vendor: String?
    public internal(set) var mac: String?
    public internal(set) var model: String?
    public internal(set) var name: String?
    public internal(set) var operatorMode: String?
    public internal(set) var identity: String?
    public internal(set) var firmwareVersion: String?
    public internal(set) var cPUUtilization: String?
    public internal(set) var connectedUsers: String?
    public internal(set) var sNR: String?
    public internal(set) var memoryUtilization: String?
    public internal(set) var wiFiRadio: String?
    public internal(set) var sMID: String?
    public internal(set) var sOPHOSINFO: String?

    // MARK: Initialisation

    /**
        Initialize the `SSDPService` with the discovery response.

        - Parameters:
            - host: The host of service
            - response: The discovery response.
    */
    init(host: String, response: String) {
        self.host = host
        self.location = self.parse(header: "LOCATION", in: response)
        self.server = self.parse(header: "SERVER", in: response)
        self.searchTarget = self.parse(header: "ST", in: response)
        self.uniqueServiceName = self.parse(header: "USN", in: response)
        
        self.vendor = self.parse(header: "VENDOR", in: response)
        self.mac = self.parse(header: "MAC", in: response)
        self.model = self.parse(header: "MODEL", in: response)
        self.name = self.parse(header: "NAME", in: response)
        self.operatorMode = self.parse(header: "OPERATORMODE", in: response)
        self.identity = self.parse(header: "IDENTITY", in: response)
        self.firmwareVersion = self.parse(header: "FIRMWAREVERSION", in: response)
        self.cPUUtilization = self.parse(header: "CPUUTILTZATION", in: response)
        self.connectedUsers = self.parse(header: "CONNECTEDUSERS", in: response)
        self.sNR = self.parse(header: "SNR", in: response)
        self.memoryUtilization = self.parse(header: "MEMORYUTILIZATION", in: response)
        self.wiFiRadio = self.parse(header: "WIFIRADIO", in: response)
        self.sMID = self.parse(header: "SM_ID", in: response)
        self.sOPHOSINFO = self.parse(header: "SOPHOS_INFO", in: response)
    }

    // MARK: Private functions

    /**
        Parse the discovery response.

        - Parameters:
            - header: The header to parse.
            - response: The discovery response.
    */
    private func parse(header: String, in response: String) -> String? {
        
//        if let range = response.range(of: "\(header): .*", options: .regularExpression) {
//            var value = String(response[range])
//            value = value.replacingOccurrences(of: "\(header): ", with: "")
//            return value
//        }
//        return nil
        
        let responseDict = parseResponseIntoDictionary(response)
        if let value = responseDict[header] {
            return value
        }
        return nil
    }
    private func parseResponseIntoDictionary(_ response: String) -> [String: String] {
           var elements = [String: String]()
           for element in response.split(separator: "\r\n") {
               let keyValuePair = element.split(separator: ":", maxSplits: 1)
               guard keyValuePair.count == 2 else {
                   continue
               }

               let key = String(keyValuePair[0]).uppercased().trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
               let value = String(keyValuePair[1]).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

               elements[key] = value
           }

           return elements
       }
}
