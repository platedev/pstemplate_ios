//
//  ManageViews.swift
//  PSTemplate
//
//  Created by Edimax on 2021/12/24.
//

import SwiftUI
import Combine
import Foundation

final class ManageViews: ObservableObject {
    @Published var page = 0 {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var name = ""
    @Published var newFlag = false
    @Published var myFlag = false
    @Published var newAccess = [NewAccessBody]()
    @Published var myAccess = [MyAccessBody]()
    @Published var accessDetail = MyAccessBody(appname: "", mac: "", health: "", client: "",location: "",central: "",wifiradio: "",model: "",unknow: 7 )
    @Published var aPaccount = ""
    @Published var aPpw = ""
    @Published var aPaddress = ""
    @Published var getLoginResult:GETresponse?
    @Published var value = ""
    @Published var value1 = ""
    @Published var lANType: [Bool] = []
    @Published var myWireless = [WirelessBody]()
    @Published var wirelessType: [Bool] = []
    @Published var central = false
    @Published var showAboutFlag = false
    @Published var showSettingFlag = false
    @Published var hideMenu = false
    @Published var rebootFlag = false
    @Published var mSSDPDiscvoer : [String:String] = ["":""]
    @Published var mSSDPSearchAlert = false
    static let shared = ManageViews()
    private init() { }
}
extension Color {
    init(hex: Int, opacity: Double = 1.0) {
        let red = Double((hex & 0xff0000) >> 16) / 255.0
        let green = Double((hex & 0xff00) >> 8) / 255.0
        let blue = Double((hex & 0xff) >> 0) / 255.0
        self.init(.sRGB, red: red, green: green, blue: blue, opacity: opacity)
    }
    static var healthExcellent: Color{
        let hex = 0x00851D //0x2BA50D
        return Color(hex: hex)
    }
    static var healthGood: Color{
        let hex = 0xFFAF11 //0xF6A122
        return Color(hex: hex)
    }
    static var healthPoor: Color{
        let hex = 0xDA3E00 //0xF62F22
        return Color(hex: hex)
    }
    static var c0x005CC8:Color { //0x1987CB WiFi mobile APP
        let hex = 0x005CC8
        return Color(hex: hex)
    }
    static var c0x005BC8:Color { //0x1987CB WiFi mobile APP
        let hex = 0x005BC8
        return Color(hex: hex)
    }
    static var c0x001A47:Color {
        let hex = 0x001A47
        return Color(hex: hex)
    }
    static var c0x242527:Color {
        let hex = 0x242527
        return Color(hex: hex)
    }
    static var c0x242527_30:Color {
        let hex = 0x242527
        return Color(hex: hex,opacity: 0.3)
    }
    static var c0x696A6B:Color {
        let hex = 0x696A6B
        return Color(hex: hex)
    }
    static var c0xCED1D5:Color {
        let hex = 0xCED1D5
        return Color(hex: hex)
    }
    static var c0x00851D:Color {
        let hex = 0x00851D
        return Color(hex: hex)
    }
    static var c0xDA3E00:Color {
        let hex = 0xDA3E00
        return Color(hex: hex)
    }
    static var c0xFFFFFF_10:Color {
        let hex = 0xFFFFFF
        return Color(hex: hex,opacity: 0.05)
    }
}
extension Font {
    static var InterRegular24: Font {
        Font.custom("Inter-Regular", fixedSize: 24.0)
    }
    static var InterRegular18: Font {
        Font.custom("Inter-Regular", fixedSize: 18.0)
    }
    static var InterRegular16: Font {
        Font.custom("Inter-Regular", fixedSize: 16.0)
    }
    static var InterRegular14: Font {
        Font.custom("Inter-Regular", fixedSize: 14.0)
    }
    static var InterRegular13: Font {
        Font.custom("Inter-Regular", fixedSize: 13.0)
    }
    static var InterRegular12: Font {
        Font.custom("Inter-Regular", fixedSize: 12.0)
    }
    static var InterRegular10: Font {
        Font.custom("Inter-Regular", fixedSize: 10.0)
    }
    static var InterMedium18: Font {
        Font.custom("Inter-Medium", fixedSize: 18.0)
    }
    static var InterMedium14: Font {
        Font.custom("Inter-Medium", fixedSize: 14.0)
    }
    static var InterMedium12: Font {
        Font.custom("Inter-Medium", fixedSize: 12.0)
    }
    static var InterMedium10: Font {
        Font.custom("Inter-Medium", fixedSize: 10.0)
    }
    static var InterSemiBold36: Font {
        Font.custom("Inter-SemiBold", fixedSize: 36.0)
    }
    static var InterSemiBold18: Font {
        Font.custom("Inter-SemiBold", fixedSize: 18.0)
    }
    static var InterSemiBold14: Font {
        Font.custom("Inter-SemiBold", fixedSize: 14.0)
    }
    static var InterBold24: Font {
        Font.custom("Inter-Bold", fixedSize: 24.0)
    }
    static var InterBold10: Font {
        Font.custom("Inter-Bold", fixedSize: 10.0)
    }
}
struct SearchTextFieldStyle: TextFieldStyle {
    @Binding var focused: Bool
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
        .padding(10)
        .foregroundColor(Color.c0x242527) //0xCDCED0
        .background(
            RoundedRectangle(cornerRadius: 5, style: .continuous)
                .stroke(focused ? Color(hex: 0x3F85C5) : Color.c0x696A6B, lineWidth: 1)
        ).padding()
    }
}
struct ClearButton: ViewModifier {
    @Binding var text: String
    public func body(content: Content) -> some View {
        ZStack(alignment: .trailing)
        {
            content
            if !text.isEmpty
            {
                Button(action:
                {
                    self.text = ""
                })
                {
                    Image("searchX")
                        .resizable()
                        .frame(width:14,height: 14)
                        .foregroundColor(Color(UIColor.opaqueSeparator))
                }
                .padding(.trailing, 30)
                .accessibilityIdentifier("searchX")
            }
            else
            {
                Button(action:
                {
                    self.text = ""
                })
                {
                    Image("search")
                        .resizable()
                        .frame(width:25,height:25)
                        .foregroundColor(Color(UIColor.opaqueSeparator))
                }
                .padding(.trailing, 30)
                .accessibilityIdentifier("search")
            }
        }
    }
}
struct MyTableTextFieldStyle: TextFieldStyle {
    @Binding var lastHoveredId: String
    var id: String
    var width: CGFloat
    var height: CGFloat
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
            .frame(maxWidth: .infinity,maxHeight: 40)
            .padding(10)
            .foregroundColor(self.lastHoveredId==id ? Color.c0x242527: Color.c0x242527)
        .background(
            RoundedRectangle(cornerRadius: 3, style: .continuous)
                //.stroke(Color.black.opacity(0.4), lineWidth: 1)
                .stroke(self.lastHoveredId==id ? Color.c0x005BC8 : Color.c0x696A6B, lineWidth: 1)
        )
    }
}
struct MyWiBandsTableTextFieldStyle: TextFieldStyle {
    @Binding var lastHoveredId: String
    var id: String
    var width: CGFloat
    var height: CGFloat
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
        .frame(width: width,height: height)
        .foregroundColor(self.lastHoveredId==id ? Color.c0x242527: Color.c0x242527)
        .padding(.leading,10)
        .background(
            RoundedRectangle(cornerRadius: 5, style: .continuous)
                //.stroke(Color.black.opacity(0.4), lineWidth: 1)
                .stroke(self.lastHoveredId==id ? Color.c0x005BC8 : Color.c0x696A6B, lineWidth: 1)
                .padding(.trailing,3)
        )
    }
}
struct MyNewTableTextFieldStyle: TextFieldStyle {
    @Binding var lastHoveredId: String
    var id: String
    var width: CGFloat
    var height: CGFloat
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
        .frame(width: width,height: height)
        .foregroundColor(self.lastHoveredId==id ? Color.c0x242527: Color.c0x242527)
        .padding(.leading,10)
        .background(
            RoundedRectangle(cornerRadius: 3, style: .continuous)
                //.stroke(Color.black.opacity(0.4), lineWidth: 1)
                .stroke(self.lastHoveredId==id ? Color.c0x005BC8 : Color.c0x696A6B, lineWidth: 1)
                .padding(.leading,-10)
               // .padding(.trailing,10)
        )
    }
}
struct LoginTextFieldStyle: TextFieldStyle {
    @Binding var lastHoveredId: String
    var id: String
    var width: CGFloat
    var height: CGFloat
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
        .frame(width: width,height: height)
        .foregroundColor(self.lastHoveredId==id ? Color(hex: 0x000000): Color.c0x696A6B)
        //.padding(.leading,10)
        .background(
            RoundedRectangle(cornerRadius: 5, style: .continuous)
                //.stroke(Color.black.opacity(0.4), lineWidth: 1)
                .stroke(self.lastHoveredId==id ? Color.c0x005BC8 : Color.c0x696A6B, lineWidth: 1)
                .padding(.leading,-10)
               // .padding(.trailing,10)
        )
    }
}
struct GradientProgressStyle<Stroke: ShapeStyle, Background: ShapeStyle>: ProgressViewStyle {
    var stroke: Stroke
    var fill: Background
    var caption: String = ""
    var cornerRadius: CGFloat = 3
    var height: CGFloat = 16
    var animation: Animation = .easeInOut
    func makeBody(configuration: Configuration) -> some View {
        let fractionCompleted = configuration.fractionCompleted ?? 0
        
        return VStack {
            ZStack(alignment: .topLeading) {
                GeometryReader { geo in
                    Rectangle()
                        .fill(fill)
                        .frame(maxWidth: geo.size.width * CGFloat(fractionCompleted))
                        .animation(animation)
                }
            }
            .frame(height: height)
            .cornerRadius(cornerRadius)
            .overlay(
                RoundedRectangle(cornerRadius: cornerRadius)
                        .stroke(stroke, lineWidth: 0)
            )
            if !caption.isEmpty {
                Text("\(caption)")
                    .font(.caption)
                    .cornerRadius(cornerRadius)
                    .background(Color.black.opacity(0.2))
            }
        }
    }
}
struct ProgressBar: View {
    @Binding var progress: Float
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(lineWidth: 5.0)
                .opacity(0.3)
                .foregroundColor(Color.gray)
            Circle()
                .trim(from: 0.0, to: CGFloat(min(self.progress, 1.0)))
                .stroke(style: StrokeStyle(lineWidth: 5.0, lineCap: .round, lineJoin: .round))
                .foregroundColor(Color.gray)
                .rotationEffect(Angle(degrees: 270.0))
                .animation(.spring())
        }
    }
}
struct CustomButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding(15) // *make it thinner*
            .frame(maxWidth: .infinity) // expand horizontally
            .foregroundColor(.purple)
            .background(
                Rectangle()
                    .stroke(Color.purple, lineWidth: 1)
                    .background(Color.c0x005BC8) // *change the button background color to black*
                    .cornerRadius(3)
            )
            //.padding(.horizontal, 5) // *margin of 10px on each side*
            .padding([.leading,.trailing],50)
            .opacity(configuration.isPressed ? 0.7 : 1)
    }
}
struct RoundedCorner: Shape {
    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}
/* scrollOnOverflow */
struct OverflowContentViewModifier: ViewModifier {
    @State private var contentOverflow: Bool = false
    func body(content: Content) -> some View {
        GeometryReader { geometry in
            content
            .background(
                GeometryReader { contentGeometry in
                    Color.clear.onAppear {
                        contentOverflow = contentGeometry.size.height > geometry.size.height
                    }
                }
            )
            .wrappedInScrollView(when: contentOverflow)
        }
    }
}
extension View {
    @ViewBuilder
    func wrappedInScrollView(when condition: Bool) -> some View {
        if condition {
            ScrollView {
                self
            }
        } else {
            self
        }
    }
}
extension View {
    func scrollOnOverflow() -> some View {
        modifier(OverflowContentViewModifier())
    }
}
struct GridStack<Content: View>: View {
    let rows: Int
    let columns: Int
    let content: (Int, Int) -> Content
    var body: some View {
        VStack(spacing:0) {
            ForEach(0 ..< rows, id: \.self) { row in
                HStack(spacing:0) {
                    ForEach(0 ..< columns, id: \.self) { column in
                        content(row, column)
                    }
                }
            }
        }
    }
    init(rows: Int, columns: Int, @ViewBuilder content: @escaping (Int, Int) -> Content) {
        self.rows = rows
        self.columns = columns
        self.content = content
    }
}
public let drag = DragGesture( coordinateSpace: .global).onChanged { _ in
    //NSLog("x : \(A.location.x) , y : \(A.location.y)")
}
    .onEnded { _ in }
struct CustomWiBandsToggleStyle: ToggleStyle {
    @Binding var edit: Bool
    func makeBody(configuration: Configuration) -> some View {
        return HStack {
            configuration.label
                .font(.InterRegular18)
                .foregroundColor(Color.c0x242527)
                //.foregroundColor(edit ? Color(hex:0x313030) : Color(hex:0xC4C4C4))
            Spacer()
            if edit {
                Image(configuration.isOn ? "disable" : "radioEnable")
                    .resizable()
                    .frame(width:18,height: 16)
                    .gesture(drag)
            }
            else
            {
                Image(configuration.isOn ? "revDisable" : "reradioEnable")
                    .resizable()
                    .frame(width:18,height: 16)
                    .gesture(drag)
            }
            Text("Radio")
                .font(.InterRegular14)
                .foregroundColor(edit ? Color.c0x242527 : Color.c0x242527_30)
                .gesture(drag)
//            Text("Off")
//                .font(.SophosSansRegular_14)
//                .foregroundColor(edit ? Color(hex:0x313030) : Color(hex:0xC4C4C4))
            Button(action: { configuration.isOn.toggle() } )
            {
                if edit {
                    Image(configuration.isOn ? "ToggleONedit":"ToggleOFFedit" )
                        .gesture(drag)
                }
                else
                {
                    Image(configuration.isOn ? "ToggleON":"ToggleOFF")
                        .gesture(drag)
                }
            }
        }.frame(height: 45.0)
            .gesture(drag)
    }
}
struct CustomDefault1ToggleStyle: ToggleStyle {
    @Binding var edit: Bool
    func makeBody(configuration: Configuration) -> some View {
        return HStack {
            configuration.label
                .font(.InterRegular14)
                .foregroundColor(Color.c0x242527)
                .gesture(drag)
            Spacer()
            Button(action: { configuration.isOn.toggle() } )
            {
                if edit {
                    Image(configuration.isOn ? "ToggleONedit":"ToggleOFFedit" )
                        .gesture(drag)
                        
                }
                else
                {
                    Image(configuration.isOn ? "ToggleON":"ToggleOFF")
                        .gesture(drag)
                }
            }
        }.frame(height: 25.0)
            .gesture(drag)
    }
}
struct CustomNTP1ToggleStyle: ToggleStyle {
    @Binding var edit: Bool
    func makeBody(configuration: Configuration) -> some View {
        return HStack {
            configuration.label
                .font(.InterRegular14)
                .foregroundColor(edit ? Color.c0x242527 : Color.c0x242527)
                .gesture(drag)
            Spacer()
            Button(action: { configuration.isOn.toggle() } )
            {
                if edit {
                    Image(configuration.isOn ? "ToggleONedit":"ToggleOFFedit" )
                        .gesture(drag)
                        
                }
                else
                {
                    Image(configuration.isOn ? "ToggleON":"ToggleOFF")
                        .gesture(drag)
                }
            }
        }.frame(height: 25.0)
            .gesture(drag)
    }
}
struct CustomSettingsToggleStyle: ToggleStyle {
    @Binding var edit: Bool
    var index = 0
    @StateObject var datas = ManageDatas()
    func makeBody(configuration: Configuration) -> some View {
        return HStack {
            configuration.label
                .font(.InterRegular14)
                .foregroundColor(configuration.isOn ? Color.c0x242527 : Color.c0x242527)
            Spacer()
            Button(action: {
                configuration.isOn.toggle()
                if index == 0
                {
                    datas.localStore.set(edit, forKey: "faceid")
                    if edit == false
                    {
                        datas.localStore.set(edit, forKey: "passcode")
                    }
                }
                else if index == 1
                {
                    datas.localStore.set(edit, forKey: "touchid")
                    if edit == false
                    {
                        datas.localStore.set(edit, forKey: "passcode")
                    }
                }
                else if index == 2
                {
                    datas.localStore.set(edit, forKey: "passcode")
                }
            } )
            {
                    Image(edit ? "ToggleONedit":"ToggleOFFedit" )
            }
        }.frame(height: 45.0)
    }
}
struct AuthButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .foregroundColor(configuration.isPressed ? Color.c0x005BC8:Color.c0x242527)
            .background(Color.white)
    }
}
struct RadioButton: View {
    @Environment(\.colorScheme) var colorScheme
    let id: String
    let callback: (String)->()
    let selectedID : String
    let size: CGFloat
    let color: Color
    let textSize: CGFloat
    init(
        _ id: String,
        callback: @escaping (String)->(),
        selectedID: String,
        size: CGFloat = 20,
        color: Color = Color.primary,
        textSize: CGFloat = 14
        ) {
        self.id = id
        self.size = size
        self.color = color
        self.textSize = textSize
        self.selectedID = selectedID
        self.callback = callback
    }
    var body: some View {
        Button(action:{
            self.callback(self.id)
        }) {
            HStack(alignment: .center, spacing: 10) {
                Image(self.selectedID == self.id ? "radioBtn" : "radioNoBtn")
                    .resizable()
                    .frame(width: 20, height: 20)
               Text(id)
                    .font(.InterRegular13)
                    .frame(width: 280,height:30,alignment:.leading)
                    .padding(.leading,5)
                Spacer()
            }
        }
        .padding(.leading , 20)
        Divider()
            .padding(.leading , 40)
            .padding(.trailing,20)
    }
}
struct RadioButtonGroup: View {
    let items : [String]
    @State var selectedId: String = ""
    let callback: (String) -> ()
    var body: some View {
        VStack {
            ForEach(0..<items.count) { index in
                RadioButton(self.items[index], callback: self.radioGroupCallback, selectedID: self.selectedId)
                    .accessibility(addTraits: .isButton)
                    .accessibilityIdentifier("auth\(index)")
                    .buttonStyle(AuthButtonStyle())
            }
        }
    }
    func radioGroupCallback(id: String) {
        selectedId = id
        callback(id)
    }
}
/*checkbox*/
struct CheckboxStyle: ToggleStyle {
    @Binding var isON:Bool
    func makeBody(configuration: Self.Configuration) -> some View {
        return HStack {
            Button(action: { configuration.isOn.toggle()
            } )
            {
                Image(systemName: isON ? "checkmark.square.fill":"square")
                .resizable()
                .frame(width: 24, height: 24)
                .padding(.leading,0)
                .foregroundColor(isON  ? Color.c0x005BC8 : Color.c0x005BC8)
                .font(.system(size: 20, weight: .regular, design: .default))
                
                configuration.label
                    .padding(.leading,-15)
            }
        }
    }
}
/*Ping Test*/
func timeString(time: Int) -> String {
     //   let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        //return String(format:"%02i minutes %02i seconds left", minutes, seconds)
    return String(format:"%1i seconds left", seconds)
 }
extension String {
func isIPv4() -> Bool {
    var sin = sockaddr_in()
    return self.withCString({ cstring in inet_pton(AF_INET, cstring, &sin.sin_addr) }) == 1
}
func isIPv6() -> Bool {
    var sin6 = sockaddr_in6()
    return self.withCString({ cstring in inet_pton(AF_INET6, cstring, &sin6.sin6_addr) }) == 1
}
func isIpAddress() -> Bool { return self.isIPv6() || self.isIPv4() }
func widthOfString(usingFont font: UIFont) -> CGFloat {
    let fontAttributes = [NSAttributedString.Key.font: font]
    let size = self.size(withAttributes: fontAttributes)
    return size.width
}
var binaryToInt: Int { return Int(strtoul(self, nil, 2)) }
var integerValue: Int { return Int(self) ?? 0 }
    func regex (pattern: String) -> [String] {
        do {
          let regex = try NSRegularExpression(pattern: pattern, options:[])
          let nsstr = self as NSString
          let all = NSRange(location: 0, length: nsstr.length)
          var matches : [String] = [String]()
            regex.enumerateMatches(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: all) { (result : NSTextCheckingResult?, _, _) in
            if let r = result {
                let result = nsstr.substring(with: r.range) as String
              matches.append(result)
            }
          }
          return matches
        } catch {
          return [String]()
        }
      }
}
extension Int {
    var binaryString: String { return String(self, radix: 2) }
}
struct MarqueeText : View {
@State var text = ""
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)
var body : some View {
    let stringWidth = text.widthOfString(usingFont: UIFont.systemFont(ofSize: 15))
    return ZStack {
            GeometryReader { geometry in
                Text(self.text).lineLimit(1)
                    .font(.InterRegular14)
                    .foregroundColor(Color.c0x242527)
                    .font(.subheadline)
                    .offset(x: self.animate ? -stringWidth * 1 : 0)
                    .animation(self.animationOne)
                    .onAppear() {
                        if geometry.size.width < stringWidth {
                             self.animate = true
                        }
                }
                .fixedSize(horizontal: true, vertical: false)
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .topLeading)
                
                Text(self.text).lineLimit(1)
                    .font(.InterRegular14)
                    .foregroundColor(Color.c0x242527)
                    .font(.subheadline)
                    .offset(x: self.animate ? 0 : stringWidth * 3)
                    .animation(self.animationOne)
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .topLeading)
            }
        }
}
}
struct MarqueeSystemText : View {
    @StateObject var mDetail = AppDetailModel()
    var width = CGFloat(150.0)
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)
var body : some View {
    let stringWidth = mDetail.system[1].value!.widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
        }
    }
    else if width - stringWidth >= 0
    {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
          //  GeometryReader { geometry in
                Text(mDetail.system[1].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(Color.c0x242527)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1 : 0, y: 0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text(mDetail.system[1].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 2, y: 0)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
       //     }
    }
}
}
struct MarqueeSystemDNSText : View {
    @StateObject var mDetail = AppDetailModel()
    var width = CGFloat(150.0)
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)
var body : some View {
    let stringWidth = mDetail.system[9].value!.widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
        }
    }
    else if width - stringWidth >= 0
    {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
          //  GeometryReader { geometry in
                
                Text(mDetail.system[9].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(Color.c0x242527)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1 : 0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text(mDetail.system[9].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 2)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
       //     }
    }
}
}
struct MarqueeSystemIPV6Text : View {
    @StateObject var mDetail = AppDetailModel()
    var width = CGFloat(150.0)
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)
var body : some View {
    let stringWidth = mDetail.system[11].value!.widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
        }
    }
    else if width - stringWidth >= 0
    {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
          //  GeometryReader { geometry in
                Text(mDetail.system[11].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(Color.c0x242527)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1 : 0, y:0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                    Text(mDetail.system[11].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 2, y: 0)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
       //     }
    }
}
}
struct MarqueeSystemIPV6LinkText : View {
    @StateObject var mDetail = AppDetailModel()
    var width = CGFloat(150.0)
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)
var body : some View {
    let stringWidth = mDetail.system[12].value!.widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
        }
    }
    else if width - stringWidth >= 0
    {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
          //  GeometryReader { geometry in
                Text(mDetail.system[12].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(Color.c0x242527)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1 : 0, y: 0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                    Text(mDetail.system[12].value!).lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 2, y: 0)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
                
       //     }
    }
}
}
struct MarqueeWiBands24GText : View {
@StateObject var mDetail = AppDetailModel()
    @State var index:Int
    var width = CGFloat(150.0)
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)

var body : some View {
    let stringWidth = "\(mDetail.wireless24G[index].value!)".widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
        }
    }
    else if width - stringWidth >= 0
    {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
          //  GeometryReader { geometry in
                Text("\(mDetail.wireless24G[index].value!)").lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(Color.c0x242527)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1 : 0, y: 0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text("\(mDetail.wireless24G[index].value!)").lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 2, y: 0)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
       //     }
    }
}
}
struct MarqueeWiBands5GText : View {
@StateObject var mDetail = AppDetailModel()
    @State var index:Int
    var width = CGFloat(150.0)
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)

var body : some View {
    let stringWidth = "\(mDetail.wireless5G[index].value!)".widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
            
        }
    }
    else if width - stringWidth >= 0
    {
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
          //  GeometryReader { geometry in
                
                Text("\(mDetail.wireless5G[index].value!)").lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(Color.c0x242527)
                        //.font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1 : 0, y: 0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text("\(mDetail.wireless5G[index].value!)").lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 2, y: 0)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
                
       //     }
    }
}
}
struct MarqueeWiBands6GText : View {
@StateObject var mDetail = AppDetailModel()
    @State var index:Int
    var width = CGFloat(150.0)
@State private var animate = false
    private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)

var body : some View {
    let stringWidth = "\(mDetail.wireless6G[index].value!)".widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    if width - stringWidth < 0 {
        DispatchQueue.main.async {
         self.animate = true
            
        }
    }
    else if width - stringWidth >= 0
    {
       // NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless6G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
           // GeometryReader { geometry in
                
                Text("\(mDetail.wireless6G[index].value!)").lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(Color.c0x242527)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1 : 0, y: 0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text("\(mDetail.wireless6G[index].value!)").lineLimit(1)
                        .font(.InterRegular12)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                      //  .font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 2, y: 0)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
                
         //   }
    }
}
}
struct MarqueeTimeZoneText : View {
    @StateObject var mDetail = AppDetailModel()
    var width = CGFloat(150.0)
@State private var animate = false
    @State private var stringWidth = 200.0
    @State private var animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)
    @State private var bool = false
    @State private var time_zone = "(GMT-12:00) Eniwetok, Kwajalein"

var body : some View {
    if !bool {
        DispatchQueue.main.async {
            time_zone = mDetail.mTimeZone//TimeZoneList[(Detail.getDateAndTimeResult?.data?.time_zone)!]
        stringWidth = time_zone.widthOfString(usingFont: UIFont.systemFont(ofSize: 24))
        animationOne = Animation.linear(duration: Double(stringWidth) / 30).delay(3.5).repeatForever(autoreverses: false)
        bool = true
        }
    }
    if width - stringWidth < 0 {
        DispatchQueue.main.async {
         self.animate = true
        }
    }
    else if width - stringWidth >= 0
    {
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
          //  GeometryReader { geometry in
                
                Text(time_zone).lineLimit(1)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 0.8 : 0, y: 0)
                        .animation(self.animate ? self.animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text(time_zone).lineLimit(1)
                        .font(.InterRegular14)
                        .foregroundColor(self.animate ? Color.c0x242527:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 0.8, y: 0)
                        .animation(self.animate ? self.animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
                
       //     }
    }
}
}
struct MarqueeTitleText : View {
@State var text = ""
@State private var animate = false
private let animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)

var body : some View {
    let stringWidth = text.widthOfString(usingFont: UIFont.systemFont(ofSize: 12))
    return ZStack {
            GeometryReader { geometry in
                Text(self.text).lineLimit(1)
                    .font(.InterMedium12)
                    .foregroundColor(Color.black)
                    .offset(x: self.animate ? -stringWidth * 1 : 0, y: 0)
                    .animation(self.animate ? animationOne : .default)
                    .onAppear() {
                        if geometry.size.width < stringWidth {
                             self.animate = true
                        }
                     
                }
                .fixedSize(horizontal: true, vertical: false)
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                .clipped()
                
                Text(self.text).lineLimit(1)
                    .font(.InterMedium12)
                    .foregroundColor(Color.black)
                    .offset(x: self.animate ? 0 : stringWidth * 3, y: 0)
                    .animation(self.animate ? animationOne : .default)
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
            }
        }
}
}
struct MarqueeDetailTitleText : View {
    @StateObject var backPress = ManageViews.shared
    var width = CGFloat(150.0)
    @State private var animate = false
    @State private var stringWidth = 200.0
    @State private var animationOne = Animation.linear(duration: 8.0).delay(3.5).repeatForever(autoreverses: false)
    @State private var bool = false
  
var body : some View {
   
    if !bool {
        DispatchQueue.main.async {
        stringWidth = backPress.accessDetail.appname!.widthOfString(usingFont: UIFont.systemFont(ofSize: 24))
        animationOne = Animation.linear(duration: Double(stringWidth) / 30).delay(3.5).repeatForever(autoreverses: false)
        bool = true
        }
    }
    if width - stringWidth < 0 {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) true")
        DispatchQueue.main.async {
         self.animate = true
            
        }
    }
    else if width - stringWidth >= 0
    {
      //  NSLog("width :\(index) \(width) \(stringWidth) \(Detail.wireless24G[index].value!) false")
        DispatchQueue.main.async {
        self.animate = false
        }
    }
    return ZStack {
            //GeometryReader { geometry in
                Text(backPress.accessDetail.appname!).lineLimit(1)
                        .font(.InterBold24)
                        .foregroundColor(Color.white)
                       // .font(.subheadline)
                        .offset(x: self.animate ? -stringWidth * 1.5 : 0, y: 0)
                        .animation(self.animate ? animationOne : .default )
                    .fixedSize(horizontal: true, vertical: false)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                    .clipped()
                  
                    Text(backPress.accessDetail.appname!).lineLimit(1)
                        .font(.InterBold24)
                        .foregroundColor(self.animate ? Color.white:Color.clear)
                        //.font(.subheadline)
                        .offset(x: self.animate ? 0 : stringWidth * 1.5, y: 0)
                        .animation(self.animate ? animationOne : .default )
                        .fixedSize(horizontal: true, vertical: false)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 40, alignment: .leading)
                        .clipped()
           // }
        }
}
}
struct AdaptsToKeyboard: ViewModifier {
    @State var currentHeight: CGFloat = 0
    
    func body(content: Content) -> some View {
        GeometryReader { geometry in
            content
                .padding(.bottom, self.currentHeight)
                .onAppear(perform: {
                    NotificationCenter.Publisher(center: NotificationCenter.default, name: UIResponder.keyboardWillShowNotification)
                        .merge(with: NotificationCenter.Publisher(center: NotificationCenter.default, name: UIResponder.keyboardWillChangeFrameNotification))
                        .compactMap { notification in
                            withAnimation(.easeOut(duration: 0.16)) {
                                notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
                            }
                    }
                    .map { rect in
                        rect.height - geometry.safeAreaInsets.bottom
                    }
                    .subscribe(Subscribers.Assign(object: self, keyPath: \.currentHeight))
                    
                    NotificationCenter.Publisher(center: NotificationCenter.default, name: UIResponder.keyboardWillHideNotification)
                        .compactMap { notification in
                            CGFloat.zero
                    }
                    .subscribe(Subscribers.Assign(object: self, keyPath: \.currentHeight))
                })
        }
    }
}

extension View {
    func adaptsToKeyboard() -> some View {
        return modifier(AdaptsToKeyboard())
    }
}
extension Binding {
     func toUnwrapped<T>(defaultValue: T) -> Binding<T> where Value == Optional<T>  {
        Binding<T>(get: { self.wrappedValue ?? defaultValue }, set: { self.wrappedValue = $0 })
    }
}
let releaseDate = Date()
    
let stackDateFormat: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return formatter
}()
func uptimeString(inputDate: String) -> String {
  //  NSLog("Systm information uptime0")
    let day = inputDate.components(separatedBy: " days ")
    let uptime = inputDate.regex(pattern: "\\d{2}")
    if uptime.count == 3
    {
        var uptime0 = 0, uptime1 = 0, uptime2 = 0
        uptime2 = Int(uptime[2])! + 1
        if uptime2 == 60
        {
            uptime2 = 0
            uptime1 = Int(uptime[1])! + 1
            if uptime1 == 60
            {
                uptime1 = 0
                uptime0 = Int(uptime[0])! + 1
                if uptime0 == 24
                {
                    if day.count == 2
                    {
                        return "\(Int(day[0])! + 1) days 00:\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))"
                    }
                    else
                    {
                        let day = inputDate.components(separatedBy: " day ")
                        if day.count == 2
                        {
                            return "2 days 00:\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))"
                        }
                        else
                        {
                            return "1 day 00:\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))"
                        }
                    }
                }
            }
            else
            {
                uptime0 = Int(uptime[0])!
            }
        }
        else
        {
            uptime0 = Int(uptime[0])!
            uptime1 = Int(uptime[1])!
        }
        if day.count == 2
        {
            return "\(day[0]) days \(String(format: "%02d",uptime0)):\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))"
        }
        else
        {
            let day = inputDate.components(separatedBy: " day ")
            if day.count == 2
            {
                return "\(day[0]) day \(String(format: "%02d",uptime0)):\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))"
            }
        }
      //  NSLog("Systm information uptime1")
        return "\(String(format: "%02d",uptime0)):\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))"
    }
    return inputDate
 }
func systemTimeString(inputDate: String) -> String {
  //  NSLog("Systm information system time0")
    let newinput = inputDate.split(separator: " ")
    let yymmdd = String(newinput[0]).replacingOccurrences(of: "/", with: "-")
    let uptime = String(newinput[1]).regex(pattern: "\\d{2}")
    if uptime.count == 3
    {
        var uptime0 = 0, uptime1 = 0, uptime2 = 0
        uptime2 = Int(uptime[2])! + 1
        if uptime2 == 60
        {
            uptime2 = 0
            uptime1 = Int(uptime[1])! + 1
            if uptime1 == 60
            {
                uptime1 = 0
                uptime0 = Int(uptime[0])! + 1
            }
            else
            {
                uptime0 = Int(uptime[0])!
            }
        }
        else
        {
            uptime0 = Int(uptime[0])!
            uptime1 = Int(uptime[1])!
        }
   //     NSLog("Systm information system time1")
        return "\(yymmdd) \(String(format: "%02d",uptime0)):\(String(format: "%02d",uptime1)):\(String(format: "%02d",uptime2))"
    }
    return inputDate
 }
