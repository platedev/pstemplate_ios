//
//  ManageDatas.swift
//  PSTemplate
//
//  Created by EDIOS on 2022/3/29.
//

import SwiftUI
import CoreData

class ManageDatas: ObservableObject {
//    @Published var secure = PSKeyStore() {
//        willSet {
//            self.objectWillChange.send()
//        }
//    }
    @Published var localStore = UserDefaults()
//    {
//        willSet {
//            self.objectWillChange.send()
//        }
//    }
    
    func decodeLocalStore<T : Decodable>(from data : Data) throws -> T {
        return try JSONDecoder().decode(T.self, from: data)
    }
    public func deleteMyap(_ name: String) -> Bool {
        guard var database = localStore.dictionary(forKey: "MultiMyAP") as? [String:Data], var _ = database[name] else {
            return false
        }
        for (key, _) in database {
            if key == name {
                //database.removeValue(forKey: name)
               // database.remove(at: database.index(forKey: name)!)
               
                if database.removeValue(forKey: name) != nil {
                ///         print("The value \(value) was removed.")
                   // print(database[name])
                    localStore.set(database, forKey: "MultiMyAP")
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        
        return false
    }
//    public func getMyAPMatch( _ name: String) -> SSDP {
//        let a = Data("{\"LOCATION\":\"\", \"SERVER\":\"\", \"ST\":\"\", \"USN\":\"\", \"Vendor\":\"\", \"Mac\":\"\",\"Model\":\"\", \"Name\":\"\", \"OperatorMode\":\"\", \"Identity\":\"\", \"FirmwareVersion\":\"\", \"CPUUtilization\":\"\", \"ConnectedUsers\":\"\", \"SNR\":\"\", \"MemoryUtilization\":\"\", \"WiFiRadio\":\"\",\"live\":\"\",\"SOPHOS_INFO\":\"\"}".utf8)
//        var t : Decodable?
//        t = try? SSDP.init(jsonData: a)
//        guard let database = localStore.dictionary(forKey: "MultiMyAP") as? [String:Data],
//              let databaseCheck = database[name] else {
//                  let creat = SSDP(LOCATION: "", SERVER: "", ST: "", USN: "", Vendor: "", Mac: "", Model: "", Name: "", OperatorMode: "", Identity: "", FirmwareVersion: "", CPUUtilization: "", ConnectedUsers: "", SNR: "", MemoryUtilization: "", WiFiRadio: "", live: "", SOPHOS_INFO: "")
//                  var initLogin: [String :Data]?
//                  
//                  // Create JSON Encoder
//                  let encoder = JSONEncoder()
//
//                  // Encode Note
//                  let data = try? encoder.encode(creat)
//
//                  // Write/Set Data
//                  //initLogin.init()
//                  initLogin = [name:data!]
//                  localStore.set(initLogin, forKey: "MultiMyAP")
//           
//                  return t as! SSDP
//                  
//              }
//     //   print("databaseCheck : \(databaseCheck)")
//        
//        let data = try? decodeLocalStore(from: databaseCheck) as SSDP
//
//        return data!
//    }
    public func checkMyAPAmount()->[String:Data] {
        let database = localStore.dictionary(forKey: "MultiMyAP") as? [String:Data]
        return database ?? ["":Data()]
    }
//    public func checkMyAPMatch( _ name: String, _ newInfo: SSDP) -> Bool {
//        guard let database = localStore.dictionary(forKey: "MultiMyAP") as? [String:Data],
//          let databaseCheck = database[name] else { return false }
//
//        // Create JSON Encoder
//        let encoder = JSONEncoder()
//
//        // Encode Note
//        let data = try? encoder.encode(newInfo)
//        return databaseCheck == data!
//    }
    public func saveToMyAP( _ name: String, _ info: SSDP) -> Bool {
       // var newEntry : [String: SSDP]
        var database = localStore.dictionary(forKey: "MultiMyAP") as? [String:Data]
        guard var database = localStore.dictionary(forKey: "MultiMyAP") as? [String:Data],
              var _ = database[name] else {
                  
                  if database == nil
                  {
                     // let creat = login(admin_account: "", admin_pw: "", keep: 0)
                      var initMyAP: [String :Data]?
                      // Create JSON Encoder
                      let encoder = JSONEncoder()

                      // Encode Note
                      let data = try? encoder.encode(info)

                      // Write/Set Data
                      //initLogin.init()
                      initMyAP = [name:data!]
                      localStore.set(initMyAP, forKey: "MultiMyAP")
                      return true
                  }
                  else
                  {
                      // Create JSON Encoder
                      let encoder = JSONEncoder()

                      // Encode Note
                      let data = try? encoder.encode(info)

                      // Write/Set Data
                      //initLogin.init()
                     
                      database!.updateValue(data!, forKey: name)
                      localStore.set(database, forKey: "MultiMyAP")
                      return true
                  }
              }
        
        // Create JSON Encoder
        let encoder = JSONEncoder()

        // Encode Note
        let data = try? encoder.encode(info)

        // Write/Set Data
        //initLogin.init()

        database.updateValue(data!, forKey: name)
        localStore.set(database, forKey: "MultiMyAP")
        return true
    }
    public func deleteLogin(_ name: String) -> Bool {
       // var database = localStore.dictionary(forKey: "MultiLogin") as? [String:Data]
        guard var database = localStore.dictionary(forKey: "MultiLogin") as? [String:Data],
              var _ = database[name] else { return false}
        
        for (key, _) in database {
            if key == name {
                //database.removeValue(forKey: name)
               // database.remove(at: database.index(forKey: name)!)
               
                if database.removeValue(forKey: name) != nil {
                ///         print("The value \(value) was removed.")
               //     print(database[name])
                    localStore.set(database, forKey: "MultiLogin")
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        
        return false
        
       // return ((database.removeValue(forKey: name)) != nil)
    }
    public func getLoginMatch( _ name: String) -> login {
        let aperson = Data("{\"admin_account\":\"\",\"admin_pw\":\"\",\"keep\":0}".utf8)
        var tResult : Decodable?
        tResult = try? login.init(jsonData: aperson)
        var database = localStore.dictionary(forKey: "MultiLogin") as? [String:Data]
        guard let database = localStore.dictionary(forKey: "MultiLogin") as? [String:Data],
              let databaseCheck = database[name] else {
                  let creat = login(admin_account: "", admin_pw: "", keep: 0)
                  // Create JSON Encoder
                  let encoder = JSONEncoder()

                  // Encode Note
                  let data = try? encoder.encode(creat)

                  // Write/Set Data
                  //initLogin.init()
                  if database == nil
                  {
                      database = [name:data!]
                  }
                  else
                  {
                      database![name] = data!
                  }
                  localStore.set(database, forKey: "MultiLogin")
                  
                  return tResult as! login
                  
              }
      //  print("databaseCheck : \(databaseCheck)")
        
        let data = try? decodeLocalStore(from: databaseCheck) as login

        return data!
    }
//    public func checkLoginAmount()->[String:Data] {
//        let database = localStore.dictionary(forKey: "MultiLogin") as? [String:Data]
//        return database!
//    }
//    public func checkLoginMatch( _ name: String, _ number: String) -> Bool {
//        guard let database = localStore.dictionary(forKey: "MultiLogin") as? [String:String],
//          let databaseCheck = database[name] else { return false }
//
//       return databaseCheck == number
//    }
    public func saveToLogin( _ name: String, _ info: login) -> Bool {
       // var newEntry : [String: login]
    //    let database = localStore.dictionary(forKey: "MultiLogin") as? [String:Data]
        guard var database = localStore.dictionary(forKey: "MultiLogin") as? [String:Data],
              let databaseCheck = database[name] else {
                  
                  return false
                  
              }
        do {
            
            let data = try! decodeLocalStore(from: databaseCheck) as login
            if data.admin_account == ""
            {
               // var initLogin: [String :Data]?
                    // Create JSON Encoder
                    let encoder = JSONEncoder()
                    // Encode Note
                    let data = try encoder.encode(info)

                    // Write/Set Data
                    //initLogin.init()
                   
                    database[name] = data
                    localStore.set(database, forKey: "MultiLogin")
                    return true
               
            }
            else
            {
                let encoder = JSONEncoder()

                // Encode Note
                let data = try encoder.encode(info)

                // Write/Set Data
                //initLogin.init()
               
                database.updateValue(data, forKey: name)
                localStore.set(database, forKey: "MultiLogin")
                return true
            }
        }
        catch {
         //  print("Unable to Encode Note (\(error))")
       }
       
        return false
    }
    public func saveToCurrentEable( _ name: String, _ info: SSDP) -> Bool {
       // var newEntry : [String: SSDP]
        var database = localStore.dictionary(forKey: "EableMyAP") as? [String:Data]
        guard var database = localStore.dictionary(forKey: "EableMyAP") as? [String:Data],
              var _ = database[name] else {
                  
                  if database == nil
                  {
                     // let creat = login(admin_account: "", admin_pw: "", keep: 0)
                      var initMyAP: [String :Data]?
                      // Create JSON Encoder
                      let encoder = JSONEncoder()

                      // Encode Note
                      let data = try? encoder.encode(info)

                      // Write/Set Data
                      //initLogin.init()
                      initMyAP = [name:data!]
                      localStore.set(initMyAP, forKey: "EableMyAP")
                      return true
                  }
                  else
                  {
                      // Create JSON Encoder
                      let encoder = JSONEncoder()

                      // Encode Note
                      let data = try? encoder.encode(info)

                      // Write/Set Data
                      //initLogin.init()
                     
                      database!.updateValue(data!, forKey: name)
                      localStore.set(database, forKey: "EableMyAP")
                      return true
                  }
                  
              }
        
        // Create JSON Encoder
        let encoder = JSONEncoder()

        // Encode Note
        let data = try? encoder.encode(info)

        // Write/Set Data
        //initLogin.init()
       
        database.updateValue(data!, forKey: name)
        localStore.set(database, forKey: "EableMyAP")
        return true
    }
    public func getCurrentEableMatch( _ name: String) -> SSDP  {
       // let a = Data("{\"LOCATION\":\"\", \"SERVER\":\"\", \"ST\":\"\", \"USN\":\"\", \"Vendor\":\"\", \"Mac\":\"\",\"Model\":\"\", \"Name\":\"\", \"OperatorMode\":\"\", \"Identity\":\"\", \"FirmwareVersion\":\"\", \"CPUUtilization\":\"\", \"ConnectedUsers\":\"\", \"SNR\":\"\", \"MemoryUtilization\":\"\", \"WiFiRadio\":\"\",\"live\":\"\",\"SOPHOS_INFO\":\"\"}".utf8)
       // var t : Codable?
       // t = try? SSDP.init(jsonData: a)
      //  let database = localStore.dictionary(forKey: "EableMyAP") as? [String:Data]
        guard let database = localStore.dictionary(forKey: "EableMyAP") as? [String:Data],
              let databaseCheck = database[name] else {
                  let creat = SSDP(LOCATION: "", SERVER: "", ST: "", USN: "", Vendor: "", Mac: "", Model: "", Name: "", OperatorMode: "", Identity: "", FirmwareVersion: "", CPUUtilization: "", ConnectedUsers: "", SNR: "", MemoryUtilization: "", WiFiRadio: "", live: "", SOPHOS_INFO: "")
                  var initLogin: [String :Data]?
                  // Create JSON Encoder
                  let encoder = JSONEncoder()

                  // Encode Note
                  let data = try? encoder.encode(creat)

                  // Write/Set Data
                  //initLogin.init()
                  initLogin = [name:data!
                  ]
                  localStore.set(initLogin, forKey: "EableMyAP")
                  return creat
                  
              }
      //  print("databaseCheck : \(databaseCheck)")
        let data = try? decodeLocalStore(from: databaseCheck) as SSDP
        return data!
    }
}

