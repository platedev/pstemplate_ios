//
//  ContentView.swift
//  PSTemplate
//
//  Created by Edimax on 2021/12/23.
//

import SwiftUI
import CoreData
import Combine

struct ContentView: View {
    @Environment(\.scenePhase) var scenePhase
    @Environment(\.managedObjectContext) private var viewContext
    @StateObject var backPress = ManageViews.shared
    @StateObject var mLocalAuth = SSBKeychain()
    @StateObject var datas = ManageDatas()
    @State var show = true
    @State var backgroung = false
    @State var client = SSDPDiscovery()
    @State var mSSDPResult = 0 //20220418 enable SSDP function
    @State var flag = false
    @State var unknpwn = 0
    @State var locationManager = LocationManager()
    @State private var date1: Date = Date()
    @State private var date2: Date = Date()
    @State var unknown = 0 //XCTest 202207
    
    init() {
        print("APP init")
        
        
    }
    
    var body: some View {
        NavigationView {
           
            showMainView()
                
            //20220314 disable SSDP function
                .onReceive(Just(mSSDPResult)) { _ in
                  //  NSLog("SSDPResult \(a): \(SSDPResult)")
                    if backPress.page != 0
                    {
                        if flag == false {
                            if backPress.rebootFlag == false {
                            backPress.mSSDPSearchAlert = true
                            NSLog("-------Start SSDPsearch-------")
                            self.sSDPsearch()
                            }
                        }
                    }
                }
         }
        .onAppear {
            for data in datas.checkMyAPAmount() {
                if data.key != ""
                {
                    
                    do {

                        let result = try! datas.decodeLocalStore(from: data.value) as SSDP
                        //print("result.CPUUtilization : \(result.CPUUtilization)")
                        backPress.myAccess.append(MyAccessBody(appname:result.Name ?? "rename" , mac: result.Mac ?? "00:00:1e:00:01:af" , health: result.CPUUtilization ?? "Good" , client: result.ConnectedUsers ?? "10", location: result.LOCATION, central:result.OperatorMode, wifiradio: result.WiFiRadio,model: result.Model,unknow: 7))
                        backPress.newFlag = true
                        backPress.myFlag = true
                        
                    }
                }
            }
        }
        .onReceive(backPress.$page) { page in
        //    showMainView()
            
//            let name = try? secure.get(forKey: "name")
//           // let face = try? secure.get(forKey: "face")
//            print("secure name : \(name ?? "")")
//            if name == nil {
//                try? secure.set(string: backPress.name, forKey: "name")
//            }
//            let astronauts = Bundle.main.decode("schema.json")
//           // for data in astronauts {
//            print("dhcp_lease_time : \(astronauts.NetworkIPv4Config?.properties?.dhcp_lease_time?.description)")
//          //  }
            
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
        .environmentObject(backPress)
        .preferredColorScheme(.dark)
        .onChange(of: scenePhase) { newPhase in
        //    print("newPhase :\(newPhase) ")
            if newPhase == .active {
                print("Active")
                backgroung = false
                mLocalAuth.checkAuth()
                date2 = Date()
                let nInterval = date2.timeIntervalSince(date1)
                if nInterval > 60
                {
                    unknpwn = 6
                    backPress.newFlag = true
                    backPress.page = 1
                }
               
            } else if newPhase == .inactive {
                print("Inactive")
                backgroung = false
                mLocalAuth.checkAuth()
                
            } else if newPhase == .background {
                print("Background")
                backgroung = true
                mLocalAuth.checkAuth()
                date1 = Date()
               
               
            }
        }
    }
    @ViewBuilder
    func showMainView() -> some View {
        if backPress.page == 0
        {
            Text("").fullScreenCover(isPresented: $show, content: {
                MainView.init()
            })
        }
        else if backPress.page == 1
        {
            VStack(spacing: 20) {
                Text("").fullScreenCover(isPresented: $show, content: {
                    ZStack {
                        AppTemplate.init(presented: $show)
                            .opacity(backgroung == true ? 0.0:1.0)
                           
                        GeometryReader { geometry in
                        VStack {
                            
                            HStack {

                            }
                            .frame(height: abs(geometry.size.height/2 - 120))
                            
                            Image("WiFi6")
                                .resizable()
                                .frame(width: 69, height:60 )
                            Text("Wireless")
                                .font(.InterSemiBold36)
                                .foregroundColor(Color(hex:0xFFFFFF))
                            HStack {

                            }
                            .frame(height: abs(geometry.size.height/2 - 30))
                            .background(Color.clear)
                            Image("SophosAP")
                                .padding(.bottom,60)
                            
                        }
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                        .background(Color.c0x005CC8)
                        .edgesIgnoringSafeArea(.all)
                        .navigationBarHidden(true)
                        .opacity(backgroung == true ? 1.0:0.0)
                        }
                    }
                })
                
            }
        }
        else if backPress.page == 2
        {
            VStack(spacing: 20) {
                Text("").fullScreenCover(isPresented: $show, content: {
                    ZStack {
                        AppDetail.init(presented: $show)
                           // .environmentObject(ManageViews())
                            
                        GeometryReader { geometry in
                        VStack {
                            
                            HStack {

                            }
                            .frame(height: abs(geometry.size.height/2 - 120))
                            
                            Image("WiFi6")
                                .resizable()
                                .frame(width: 69, height:60 )
                            Text("Wireless")
                                .font(.InterSemiBold36)
                                .foregroundColor(Color(hex:0xFFFFFF))
                            HStack {

                            }
                            .frame(height: abs(geometry.size.height/2 - 30))
                            .background(Color.clear)
                            Image("SophosAP")
                                .padding(.bottom,60)
                            
                        }
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                        .background(Color.c0x005CC8)
                        .edgesIgnoringSafeArea(.all)
                        .navigationBarHidden(true)
                        .opacity(backgroung == true ? 1.0:0.0)
                        }
                    }
                })
                
            }

          
        }
//        else if backPress.page == 3
//        {
//            VStack(spacing: 20) {
//                Text("").fullScreenCover(isPresented: $show, content: {
//                    about.init(presented: $show)
//                })
//            }
//
//        }
//        else if backPress.page == 4
//        {
//            VStack(spacing: 20) {
//                Text("").fullScreenCover(isPresented: $show, content: {
//                    settings.init(presented: $show)
//                })
//            }
//          
//        }
    }
    func sSDPsearch() {
//        if SSDPResult == 0 ||  SSDPResult == 1 {
//            flag = false
//            NSLog("SSDP 0 , 1 : \(SSDPResult)")
//            client.discoverService(forDuration: 0.1, searchTarget: "www.sophos.com:device:WLANAccessPointDevice:1", port: 1900)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//              //  NSLog("100 ms")
//                SSDPResult += 1
//            }
//        }
//        else if SSDPResult == 2  && flag == false
//        {
//            flag = true
//            NSLog("SSDP 2 : \(SSDPResult)")
        if flag == false {
            flag = true
            mSSDPResult = 0
            client.SSDPResults.removeAll(keepingCapacity: true)
            NSLog("----SSDP discoverService seconds----")
            client.discoverService(forDuration: 3.0, searchTarget: "www.sophos.com:device:WLANAccessPointDevice:1", port: 1900)
            DispatchQueue.main.asyncAfter(deadline: .now() + 13.0) {
                //Start 3次 Discover
                NSLog("10 seconds")
                flag = false
                mSSDPResult = 1
            }
            
           
               DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                   NSLog("3 seconds")
//                   do{
//                   let name = try? datas.secure.get(forKey: "face")
//                   print("secure name : \(name ?? "")")
//                   }
//                   catch{}
                   //XCTest 202207
                   if datas.localStore.integer(forKey: "gotoUnknowSSID") == 3
                   {
                       client.SSDPResults.removeAll(keepingCapacity: true)
                       return
                   }
                   else if datas.localStore.integer(forKey: "gotoUnknowSSID") == 4
                   {
                       client.SSDPResults.removeAll(keepingCapacity: true)
                       unknpwn = 1
                       unknown += 1
                       if unknown == 10
                       {
                           unknpwn = 6
                       }
                   }
                   
                   backPress.mSSDPSearchAlert = false
                   backPress.mSSDPDiscvoer = client.SSDPResults
                   if client.SSDPResults.isEmpty && unknpwn <= 5
                   {
                       unknpwn += 1
                   }
                   if unknpwn == 6
                   {
                       for apuniq in 0..<backPress.myAccess.count {
                           let amyap = MyAccessBody(appname: backPress.myAccess[apuniq].appname ?? "rename", mac: backPress.myAccess[apuniq].mac ?? "00:00:1e:00:01:af", health: "", client: backPress.myAccess[apuniq].client ?? "10", location: backPress.myAccess[apuniq].location ?? "192.168.1.2:10443", central: backPress.myAccess[apuniq].central ?? "0" ,wifiradio: backPress.myAccess[apuniq].wifiradio ?? "3",model: backPress.myAccess[apuniq].model ?? "AP6 420E",unknow: 7)
                           backPress.myAccess[apuniq] = amyap
                       }
                   }
                    //filter 3次 Discover 結果
                   if backPress.myAccess.isEmpty
                   {
                       for respone in client.SSDPResults
                        {
                           unknpwn = 0
                            let result = SSDPService(host: respone.key, response: respone.value)
                            if result.mac != nil
                            {
                                _ = SSDP(LOCATION: result.location, SERVER: result.server, ST: result.searchTarget, USN: result.uniqueServiceName, Vendor: result.vendor, Mac: result.mac, Model: result.model, Name: result.name, OperatorMode: result.operatorMode, Identity: result.identity, FirmwareVersion: result.firmwareVersion, CPUUtilization: result.cPUUtilization, ConnectedUsers:result.connectedUsers, SNR: result.sNR, MemoryUtilization: result.memoryUtilization, WiFiRadio: result.wiFiRadio, live: "", SOPHOS_INFO: result.sOPHOSINFO)
                               // if !datas.checkMyAPMatch(result.Mac!,ssdp)
                               // {
                                if backPress.newAccess.filter({$0.mac == result.mac}).isEmpty && backPress.myAccess.filter({$0.mac == result.mac}).isEmpty
                                    {
                                        //420 系列 connected user 0-256 220 219 141
                                        //840 系列 connected user 0-512 440 439 282
                                        
                                        let mCPUUtilization = Int(result.cPUUtilization ?? "0")!
                                        let mMemoryUtilization = Int(result.memoryUtilization ?? "0")!
                                        let mConnectedUsers = Int(result.connectedUsers ?? "0")!
                                        
                                        var health = "Excellent"
                                        if ((result.model?.contains("420")) != nil){
                                            if mCPUUtilization >= 86 || mMemoryUtilization >= 86 || mConnectedUsers >= 220
                                            {
                                                health = "Poor"
                                            }
                                            else if (mCPUUtilization <= 85 && mCPUUtilization >= 56 ) || (mMemoryUtilization <= 85 && mMemoryUtilization >= 56 ) || ( mConnectedUsers <= 219 && mConnectedUsers >= 141   )
                                            {
                                                health = "Good"
                                            }
                                        }
                                        else if ((result.model?.contains("840")) != nil){
                                            if mCPUUtilization >= 86 || mMemoryUtilization >= 86 || mConnectedUsers >= 440
                                            {
                                                health = "Poor"
                                            }
                                            else if (mCPUUtilization <= 85 && mCPUUtilization >= 56 ) || (mMemoryUtilization <= 85 && mMemoryUtilization >= 56 ) || ( mConnectedUsers <= 439 && mConnectedUsers >= 282   )
                                            {
                                                health = "Good"
                                            }
                                        }
                                        
                                        backPress.newAccess.append(NewAccessBody(appname:result.name ?? "rename" , mac: result.mac ?? "00:00:1e:00:01:af" , health: health , client: result.connectedUsers ?? "10",location: result.location ?? "192.168.1.2:10443",central: result.operatorMode ?? "0",wifiradio: "3",model: "AP6 420E",unknow: 0))
                                        
                                        backPress.newFlag = true
                                    }
                            }//if result.Mac != nil
                        }//for
                   }
                   else{
                   for apuniq in 0..<backPress.myAccess.count
                   {
                   var check = false
                   for respone in client.SSDPResults
                    {
                       unknpwn = 0
                        let result = SSDPService(host: respone.key, response: respone.value)
                        if result.mac != nil
                        {
                            _ = SSDP(LOCATION: result.location, SERVER: result.server, ST: result.searchTarget, USN: result.uniqueServiceName, Vendor: result.vendor, Mac: result.mac, Model: result.model, Name: result.name, OperatorMode: result.operatorMode, Identity: result.identity, FirmwareVersion: result.firmwareVersion, CPUUtilization: result.cPUUtilization, ConnectedUsers:result.connectedUsers, SNR: result.sNR, MemoryUtilization: result.memoryUtilization, WiFiRadio: result.wiFiRadio, live: "", SOPHOS_INFO: result.sOPHOSINFO)
                           // if !datas.checkMyAPMatch(result.Mac!,ssdp)
                           // {
                            if backPress.newAccess.filter({$0.mac == result.mac}).isEmpty && backPress.myAccess.filter({$0.mac == result.mac}).isEmpty
                                {
                                    //420 系列 connected user 0-256 220 219 141
                                    //840 系列 connected user 0-512 440 439 282
                                    let mCPUUtilization = Int(result.cPUUtilization ?? "0")!
                                    let mMemoryUtilization = Int(result.memoryUtilization ?? "0")!
                                    let mConnectedUsers = Int(result.connectedUsers ?? "0")!
                                    
                                    var health = "Excellent"
                                    if ((result.model?.contains("420")) != nil){
                                        if mCPUUtilization >= 86 || mMemoryUtilization >= 86 || mConnectedUsers >= 220
                                        {
                                            health = "Poor"
                                        }
                                        else if (mCPUUtilization <= 85 && mCPUUtilization >= 56 ) || (mMemoryUtilization <= 85 && mMemoryUtilization >= 56 ) || ( mConnectedUsers <= 219 && mConnectedUsers >= 141   )
                                        {
                                            health = "Good"
                                        }
                                    }
                                    else if ((result.model?.contains("840")) != nil){
                                        if mCPUUtilization >= 86 || mMemoryUtilization >= 86 || mConnectedUsers >= 440
                                        {
                                            health = "Poor"
                                        }
                                        else if (mCPUUtilization <= 85 && mCPUUtilization >= 56 ) || (mMemoryUtilization <= 85 && mMemoryUtilization >= 56 ) || ( mConnectedUsers <= 439 && mConnectedUsers >= 282   )
                                        {
                                            health = "Good"
                                        }
                                    }
                                    
                                    backPress.newAccess.append(NewAccessBody(appname:result.name ?? "rename" , mac: result.mac ?? "00:00:1e:00:01:af" , health: health , client: result.connectedUsers ?? "10",location: result.location ?? "192.168.1.2:10443",central: result.operatorMode ?? "0",wifiradio: "3",model: "AP6 420E",unknow: 0))
                                    
                                    backPress.newFlag = true
                                }
                                else
                                {
                                  //  let status =  datas.saveToMyAP((result.Mac)!,ssdp)
                                  //  print("saveToMyAP: \((result.Mac)!) \(status)")
//                                    for ap in 0..<backPress.myAccess.count
//                                    {
                                        if backPress.myAccess[apuniq].mac == result.mac
                                        {
                                            let mCPUUtilization = Int(result.cPUUtilization ?? "0")!
                                            let mMemoryUtilization = Int(result.memoryUtilization ?? "0")!
                                            let mConnectedUsers = Int(result.connectedUsers ?? "0")!
                                            
                                            var health = "Excellent"
                                            if mCPUUtilization >= 86 || mMemoryUtilization >= 86 || mConnectedUsers >= 220
                                            {
                                                health = "Poor"
                                            }
                                            else if (mCPUUtilization <= 85 && mCPUUtilization >= 56 ) || (mMemoryUtilization <= 85 && mMemoryUtilization >= 56 )
                                            {
                                                health = "Good"
                                            }
                                            
                                            let amyap = MyAccessBody(appname: result.name ?? "rename", mac: result.mac ?? "00:00:1e:00:01:af", health: health, client: result.connectedUsers ?? "10", location: result.location ?? "192.168.1.2:10443", central: result.operatorMode ?? "0" ,wifiradio: result.wiFiRadio ?? "3",model: result.model ?? "AP6 420E",unknow: 0)
                                            backPress.myAccess[apuniq] = amyap
                                            check = true
                                            NSLog("seconds update SSDP disvover")
                                        }
                                //    }//for
                                    
                                }//else
                        }//if result.Mac != nil
                    }//for
                       if check == false
                       {
                           var health = backPress.myAccess[apuniq].health
                           if (backPress.myAccess[apuniq].unknow ?? 0) == 5
                           {
                               health = ""
                           }
                           
//                           let a = MyAccessBody(appname: backPress.myAccess[ap].appname ?? "rename", mac: backPress.myAccess[ap].mac ?? "00:00:1e:00:01:af", health: health, client: backPress.myAccess[ap].client ?? "10", location: backPress.myAccess[ap].location ?? "192.168.1.2:10443", central: backPress.myAccess[ap].central ?? "0" ,wifiradio: backPress.myAccess[ap].wifiradio ?? "3",model: backPress.myAccess[ap].model ?? "AP6 420E",unknow: (backPress.myAccess[ap].unknow ?? 0) + 1)
//
//                           backPress.myAccess[ap] = a
                           
                           //XCTest 202207
                           if datas.localStore.integer(forKey: "gotoUnknowSSID") == 4
                           {
                               let amyap = MyAccessBody(appname: backPress.myAccess[apuniq].appname ?? "rename", mac: backPress.myAccess[apuniq].mac ?? "00:00:1e:00:01:af", health: health, client: backPress.myAccess[apuniq].client ?? "10", location: backPress.myAccess[apuniq].location ?? "192.168.1.2:10443", central: backPress.myAccess[apuniq].central ?? "0" ,wifiradio: backPress.myAccess[apuniq].wifiradio ?? "3",model: backPress.myAccess[apuniq].model ?? "AP6 420E",unknow: (backPress.myAccess[apuniq].unknow ?? 0) )
                               
                               backPress.myAccess[apuniq] = amyap
                           }
                           else if datas.localStore.integer(forKey: "gotoUnknowSSID") == 0 {
                              let amyap = MyAccessBody(appname: backPress.myAccess[apuniq].appname ?? "rename", mac: backPress.myAccess[apuniq].mac ?? "00:00:1e:00:01:af", health: health, client: backPress.myAccess[apuniq].client ?? "10", location: backPress.myAccess[apuniq].location ?? "192.168.1.2:10443", central: backPress.myAccess[apuniq].central ?? "0" ,wifiradio: backPress.myAccess[apuniq].wifiradio ?? "3",model: backPress.myAccess[apuniq].model ?? "AP6 420E",unknow: (backPress.myAccess[apuniq].unknow ?? 0) + 1)
   
                              backPress.myAccess[apuniq] = amyap
                           }
                                    
                          
                       }
                   }//for
                   }//else
                }//DispatchQueue.main.asyncAfter(deadline: .now() + 3.0)
        } // if flag == false
    }
}
