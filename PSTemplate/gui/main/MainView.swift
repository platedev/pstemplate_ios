//
//  MainView.swift
//  PSTemplate
//
//  Created by Edimax on 2021/12/24.
//

import SwiftUI
import Combine

struct MainView: View {
    @Environment(\.presentationMode) var presentationMode
    @State var fullScreen = false
    @State var show = false
    @EnvironmentObject var backPress : ManageViews
    @StateObject var mMain = MainViewModel()
    @State private var thing: String = "Sophos"
    @State private var showPopup: Bool = false
    
    @State private var location: CGPoint = CGPoint(x: 0, y: 0)
    @State private var num = "0"
    
    @StateObject var mLocalAuth = SSBKeychain()
    @StateObject var datas = ManageDatas()
    var body: some View {
        NavigationView {
            GeometryReader { geometry in
            VStack {
                HStack {

                }
                .frame(height: abs(geometry.size.height/2 - 120))
                
                Image("WiFi6")
                    .resizable()
                    .frame(width: 69, height:60 )
                Text("Wireless")
                    .font(.InterSemiBold36)
                    .foregroundColor(Color(hex:0xFFFFFF))
                HStack {

                }
                .frame(height: abs(geometry.size.height/2 - 60))
                .background(Color.clear)
                
                Image("SophosAP")
                    .padding(.bottom,60)
                //    .padding(.leading, 20)
               
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Color.c0x005CC8)
            .edgesIgnoringSafeArea(.all)
            .navigationBarHidden(true)
            }
        }
        
        .onAppear(){
           // backPress.page = 1 //20220304 模擬機可以跑
            //let LocalAuth = SSBKeychain()
            mLocalAuth.checkAuth()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {

               if datas.localStore.bool(forKey: "faceid") || datas.localStore.bool(forKey: "touchid") || datas.localStore.bool(forKey: "passcode")
               {
                   mLocalAuth.authenticateUser()
               }
               else
               {
                   backPress.page = 1
               }
            }
            //XCTEST 202207
            if (datas.localStore.object(forKey: "gotoUnknowSSID") as? Int) != nil {
               // print("gotoUnknowSSID : \(number)")
            }
            else
            {
                datas.localStore.set(3, forKey: "gotoUnknowSSID")
            }

         }
        .onReceive(mLocalAuth.$loggedIn) { (result) in
            if result
            {
             //   print("loggin")
                backPress.page = 1
            }
            else
            {
             //   print("failed")
                
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .environmentObject(backPress)
        .preferredColorScheme(.dark)
    }

}
