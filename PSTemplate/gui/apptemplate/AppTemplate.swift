//
//  AppTemplate.swift
//  PSTemplate
//
//  Created by Edimax on 2022/1/18.
//

import SwiftUI
import Combine
import CryptoKit

struct AppTemplate: View {
    @Environment(\.presentationMode) var presentationMode
    @State private var isPresented = false
    @Binding var presented: Bool
    @EnvironmentObject var backPress: ManageViews
    @State var menuOpen: Bool = false
    @State var pointbuttonPressed: Bool = false
    @State var showAlert: Bool = false
    @State private var blurAmount: CGFloat = 0
    
    @State private var searchText = ""
    @State private var num = ""
    @StateObject var mAccess = AppTemplateModel()
    @State private var gradient = LinearGradient(
        gradient: Gradient(colors: [Color.c0x005BC8, Color.c0x005BC8, Color.c0x005BC8]),
            startPoint: .topLeading,
            endPoint: .bottomTrailing
        )
    @State private var downloadAmount = 1.0
    @State var timeRemaining = 0
    @StateObject var network = PSNetwork()
    @State private var editing = false
    @State var showLOGINflag = false
    @State private var lastHoveredId = ""
    @State private var aPaccount = "Administrotor Name"
    @State private var aPpassword = "Administrotor Password"
    @State private var mKeeplogin = false
    @State private var requestErr = false
    @State private var requestErrMessage = ""
    @State private var showRequest = false
    @State private var progressValue: Float = 0.0
    @State private var showWaitRequest:Bool = false
    @StateObject var datas = ManageDatas()
    
    @State var myAPColor = Color.c0x005BC8
    
    var body: some View {
        //ZStack(alignment: .top) { // << made explicit alignment to top
            GeometryReader { geometry in
                ZStack(alignment: .top) {
            HStack{
                Image("Menu")
                    .resizable()
                    .onTapGesture {
                   //     print("SliderMenu")
                        if !self.menuOpen {
                            self.openMenu()
                             
                        }
                    }
                    .frame(width: 18.0, height: 17.0)
                    .padding(.top, 40)
                    .padding(.leading,20)
                    .accessibilityLabel("Menu")
                Image("back20")
                    .onTapGesture {
                        backPress.showAboutFlag = false
                        backPress.showSettingFlag = false
                    }
                    .frame(width: 30.0, height: 30.0)
                    .padding(.top, 40)
                    .opacity(0)
                HStack{
                    Image("icon-sophos-wireless")
                        .resizable()
                        .frame(width:28,height:24)
                    Text("Wireless")
                        .font(.InterSemiBold18)
                        .foregroundColor(Color.white)
                        .padding(.top, 2)
                        
                }
                .frame(minWidth: 0,maxWidth: .infinity, maxHeight: 100, alignment: .center)
                .padding(.top, 40)
                .padding(.leading,-100)
               
            }
            .padding(0)
            .frame(minWidth: 0,maxWidth: .infinity, maxHeight: 100, alignment: .leading)
            .background(Color.c0x001A47)
            
                    
                    GeometryReader { geometry in
                        Rectangle()
                            .frame(height: 1,alignment: .center)
                            .foregroundColor(Color.c0x696A6B)
                            .offset(x: 0, y: 158)
                            .padding([.leading,.trailing],20)
                    }
                
            HStack {
                    Button(action: {
                        if pointbuttonPressed {
                            pointbuttonPressed = false
                        }
                        if mAccess.myAccess.count == 0 && mAccess.newAccess.count == 0
                        {
                            downloadAmount = 1.0
                            timeRemaining = 1
                        }
                    }, label: {
                        
                        if !pointbuttonPressed {
                            VStack(spacing:2) {
                                Text("My Access Points(\(mAccess.myAccess.count))")
                                    .frame(height: 30)
                                    .font(.InterSemiBold14)
                                    .foregroundColor(Color.c0x242527)
                                    
                                   
                                Rectangle()
                                    .frame(width : 145, height: 2)
                                    .foregroundColor(Color.c0x005BC8)
                                    .padding(.top,4) //5
                            }
                        }
                        else {
                            VStack(spacing:2) {
                            Text("My Access Points(\(mAccess.myAccess.count))")
                                .frame(height: 30)
                                .font(.InterRegular14)
                                .foregroundColor(Color.c0x242527)
                            Rectangle()
                                .frame(width : 145, height: 1)
                                .foregroundColor(Color.c0x696A6B)
                                .padding(.top,5)
                                
                            }
                           
                        }
                    })
                    .accessibilityIdentifier("myAPPressed")
                    
                    Button(action: {
                        if !pointbuttonPressed {
                            self.num = ""
                            searchText = self.num
                            pointbuttonPressed = true
                        }
                    }, label: {
                        if pointbuttonPressed {
                            VStack(spacing:2) {
                               Text("New Access Points(\(mAccess.newAccess.count))")
                                    .frame(height: 30)
                                    .font(.InterSemiBold14)
                                    .foregroundColor(Color.c0x242527)
                                 
                                Rectangle()
                                    .frame(width : 145, height: 2)
                                    .foregroundColor(Color.c0x005BC8)
                                    .padding(.top,6) //5
                                    
                            }
                        }
                        else {
                            VStack(spacing:2) {
                            Text("New Access Points(\(mAccess.newAccess.count))")
                                .frame(height: 30)
                                .font(.InterRegular14)
                                .foregroundColor(Color.c0x242527)
                                
                            Rectangle()
                                .frame(width : 145, height: 1)
                                .foregroundColor(Color.c0x696A6B)
                                .padding(.top,5)
                                
                            }
                        }
                    })
                     .padding(8)
                     .accessibilityIdentifier("newAPPressed")
            }
            .foregroundColor(Color.black)
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 280, alignment: .center)
            .offset(x: 0, y: 0)
    
                    VStack(spacing:5) {
                   
                        TextField("Enter App Name", text: $num, onEditingChanged: { edit in
                            self.editing = edit
                        })
                            .font(.InterMedium12)
                            .frame(height: 45)
                            .padding(.top,20)
                            .padding([.leading,.trailing],5)
                            .padding(.bottom,15)
                            .modifier(ClearButton(text: $num))
                            .textFieldStyle(SearchTextFieldStyle(focused: $editing))
                            .onReceive(Just(num)) { newValue in
                                self.num = newValue
                                searchText = self.num
                               
                             }
                            .accessibilityIdentifier("seachBar")
                        showAccess()
                }
                .background(Color.white)
                .offset(x: 0, y: 160)
                .onReceive(backPress.$newAccess) { result in
                    if result != mAccess.newAccess
                    {
                        mAccess.newAccess = result
                        mAccess.update()
                    }
                }
            }
           .blur(radius: blurAmount,opaque: false)
           .disabled(showAlert)
           .onReceive(backPress.$myAccess) { result in
               if result != mAccess.myAccess
               {
                   mAccess.myAccess = result
                   mAccess.update()
               }
           }//ZStack
           .border(Color(hex: 0x001A47), width: 1.0)
           .environmentObject(backPress)
                
           showDialog(width:geometry.size.width,height:geometry.size.height, xpos:geometry.size.width / 2 , ypos:geometry.size.height/2 , content: "Would you like to add all AP’s?")
                       .blur(radius:0)
           
                about.init(presented: $backPress.showAboutFlag,width: geometry.size.width, height:geometry.size.height)
                    .animation(.default)
                settings.init(presented: $backPress.showSettingFlag,width: geometry.size.width, height:geometry.size.height)
                    .animation(.default)
            SideMenu(width: 270,
                             isOpen: self.menuOpen,
                 menuClose: self.openMenu,height:geometry.size.height)
            
            showLOGIN(tag: "", width: geometry.size.width, height: geometry.size.height, xpos: geometry.size.width / 2, ypos: geometry.size.height/2)
            showSearch(tag: "", width: geometry.size.width, height: geometry.size.height, xpos: geometry.size.width / 2, ypos: geometry.size.height/2)
                
            } // Geometry
            .gesture(
               DragGesture()
                .onChanged{_ in
                    UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                    self.lastHoveredId = ""
                          
                }
            )
       // }
        .onAppear(){
            mAccess.update()
       
//            for family in UIFont.familyNames.sorted() {
//                let names = UIFont.fontNames(forFamilyName: family)
//                print("Family: \(family) Font names: \(names)")
//            }
            mAccess.newCount = backPress.newFlag
            mAccess.myCount = backPress.myFlag
            mAccess.newAccess = backPress.newAccess
            mAccess.myAccess = backPress.myAccess
            
            if mAccess.newCount {
                downloadAmount = 0.0
                showWaitRequest = true
                blurAmount = 2.0
            }
            self.menuOpen = false
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
        .background(Color.white)
        .edgesIgnoringSafeArea(.all)
        .onReceive(ManageViews.shared.$mSSDPSearchAlert) { result in
            if result == false
            {
                self.showWaitRequest = false
                self.blurAmount = 0.0
            }
        }
    }
    func openMenu() {
        self.menuOpen.toggle()
    }
    
   var searchMyResults: [MyAccessBody] {
            if searchText.isEmpty {
              //  Access.MyAccess = backPress.myAccess
               // return Access.MyAccess.sorted(by: { $0.mac! < $1.mac! })
                return mAccess.myAccess.sorted(by: { $0.appname! < $1.appname! })
            } else {
                
                return mAccess.myAccess.filter { $0.appname!.contains(num) || $0.mac!.contains(num) }.sorted(by: { $0.mac! < $1.mac! })
            }
        }
    var searchNewResults: [NewAccessBody] {
        if searchText.isEmpty {
            
                return mAccess.newAccess.sorted(by: { $0.appname! < $1.appname! })
            } else {
                return mAccess.newAccess.filter { $0.appname!.contains(num) || $0.mac!.contains(num) }.sorted(by: { $0.mac! < $1.mac! })
            }
      }
    @ViewBuilder
    func health(hex:String) -> some View {
        if hex == "Excellent" {
            Rectangle().frame(width: 6 )
                .foregroundColor(.healthExcellent )
                
        }
        else if hex == "Good" {
            Rectangle().frame(width: 6 )
                .foregroundColor(.healthGood)
                
        }
        else if hex == "Poor" {
            Rectangle().frame(width: 6 )
                .foregroundColor(.healthPoor)
                
        }
        else if hex == "" {
            Rectangle().frame(width: 6 )
                .foregroundColor(.white)
                
        }
    }
    @ViewBuilder
    func showClient(client:String,central:String)-> some View {
        if central == "1" {
 //           if Int(client)! >= 20 {
            HStack(spacing:0) {
                Image("clientBlack30")
                    .frame(minWidth:25)
                Text("\(client)")
                    .font(.InterMedium12)
                    .foregroundColor(Color.black)
                    .frame(width:28,alignment: .center)
                VStack(spacing:0) {
                     Image("functionBlack")
                        .padding(.top,10)
                     Text("Central")
                        .font(.InterMedium10)
                        .foregroundColor(Color.black)
                        .frame(minWidth:50,alignment: .center)
                }.frame(width:30,alignment: .leading)
                    .padding(.leading,-10)
               
            }
        }
        else {
            Image("client30")
            Text("\(client)")
                .font(.InterMedium12)
                .foregroundColor(Color.c0x005BC8)
                .frame(minWidth:25,alignment: .leading)
            ZStack {
            Spacer()
            }.frame(width:20,alignment: .leading)
        }
    }
    @ViewBuilder
    func drawArrow(client:MyAccessBody)-> some View {
        if client.central == "1" {
            Image("arrowBlack")
                .frame(width:35,height:70,alignment: .leading)
        }
        else
        {
            Image("arrowBlue")
                .frame(width:35,height:70,alignment: .leading)
        }
    }
    @ViewBuilder
    func addAllNew()->some View {
       // VStack() {
            Button(action: {
                pointbuttonPressed = true
                showAlert = true
                blurAmount = 2
            }, label :{
                Text("Add all AP's")
                    .font(.InterMedium14)
                    .foregroundColor(Color.white)
                    .frame(alignment: .center)
            })
             .buttonStyle(CustomButtonStyle())
             .accessibilityIdentifier("AddAllNewPressed")
    }
    @ViewBuilder
    func showDialog(width:CGFloat,height:CGFloat,xpos:CGFloat,ypos:CGFloat,content:String) -> some View {
        if showAlert {
            ZStack(){
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                    Text(content)
                        .multilineTextAlignment(.center)
                        .font(.InterRegular13)
                        .foregroundColor(.black)
                        .position(x: geometry.size.width/2, y: geometry.size.height/3)
                    
                    HStack()  {
                        Button{
                            for iuuid in mAccess.newAccess {
                                  backPress.myAccess.append(MyAccessBody(appname:iuuid.appname ?? "rename" , mac: iuuid.mac ?? "00:00:1e:00:01:af" , health: iuuid.health ?? "Good" , client: iuuid.client ?? "10", location: iuuid.location, central:iuuid.central, wifiradio: iuuid.wifiradio,model: iuuid.model,unknow: 0))
                            }
                            mAccess.updateMyAll()
                            pointbuttonPressed = false
                            showAlert = false
                            blurAmount = 0
                        } label: {
                            Text("Yes")
                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
                                .font(.InterMedium14)
                                .foregroundColor(.white)
                                .background(Color.c0x005BC8) //0x1987CB
                                .cornerRadius(5)
                        }.frame(width: 100, height: 40)
                            .accessibilityIdentifier("addallaps")
                        
                        Spacer()
                        Button{
                            showAlert = false
                            blurAmount = 0
                        } label: {
                            ZStack{
                            RoundedRectangle(cornerRadius: 5)
                                    .stroke(Color.c0x696A6B, lineWidth: 1) //0x1987CB
                            Text("No")
                                .font(.InterMedium14)
                                .foregroundColor(Color.c0x242527)
                            }.frame(width: 100, height: 40)
                        }
                      }//HStack
                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
                    } // GeometryReader
                }
                .frame(width:width-200,height: 200,alignment: .center)
                .background(Color.white)
                
                
            }.popup(isPresented: $showAlert, with: $showAlert) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width-150,height: 200,alignment: .center)
            .position(x: xpos, y: ypos)
           // .padding()
        }
        
    }
    @ViewBuilder
    func showAccess() -> some View {
        let gradientStyle = GradientProgressStyle(
                    stroke: gradient,
                    fill: gradient,
                    caption: ""
        )
        if pointbuttonPressed {
            GeometryReader { geometry in
            ScrollView(.vertical) {
                ZStack{
                VStack {
                    Text("SSearching for new Access Points")
                        .font(.InterRegular14)
                        .foregroundColor(.black)
                    
                    Button(action: {
                        pointbuttonPressed = true
                    }, label :{
                        Text("Add all AP's")
                            .font(.InterMedium14)
                            .foregroundColor(Color.white)
                            .frame(alignment: .center)
                    })
                        .buttonStyle(CustomButtonStyle())
                }.frame(width:geometry.size.width,height:geometry.size.height-300 , alignment: .bottom)
               
                if mAccess.newCount {
                VStack( spacing: 10) {
                ForEach(searchNewResults, id: \.self) { myobjc in
                    NavigationLink(destination: Text(myobjc.appname!)) {
                        HStack{
                            
                            ZStack{
                                Rectangle().frame(width: 80,alignment: .center)
                                    .foregroundColor(Color.c0x005CC8)
                                    .padding(.leading,-6)
                                
                                Image("wifi")
                                
                                health(hex: myobjc.health!)
                                    .padding(.leading,80)
                            }
                            VStack(alignment:.leading) {
                                MarqueeTitleText(text: "\(myobjc.appname!)")
                                    .background(Color.white)
                                    .font(.InterMedium12)
                                    .frame(width: geometry.size.width*0.3,height:12, alignment:.leading)
                                    .clipped()
                                
                                Text(myobjc.mac!)
                                    .font(.InterRegular12)
                                   .frame(minWidth:120,alignment: .leading)
                                   .lineLimit(1)
                                
                                HStack(spacing:0) {
                                    Text("Health: ")
                                   .font(.InterRegular10)
                                   + Text("\(myobjc.health!)")
                                       .font(.InterBold10)
                                }
                                .padding(.top,-4)
                            }
                            .frame(width:geometry.size.width*0.3,alignment:.leading)
                            .foregroundColor(Color.black)

                            HStack {
                                showClient(client:"\(myobjc.client!)",central:"\(myobjc.central!)")
                            }.frame(width:60,alignment: .leading)
                            Spacer()
                            Image("plus")
                                .frame(width:35,height:70,alignment: .leading)
                              .onTapGesture {
                                  mAccess.updateMy(mac:myobjc.mac! )
                                  backPress.myAccess = mAccess.myAccess
                                  let ssdp = SSDP(LOCATION: myobjc.location!, SERVER: "", ST: "", USN: "", Vendor: "", Mac: myobjc.mac!, Model:"", Name: myobjc.appname , OperatorMode: myobjc.central , Identity: "", FirmwareVersion: "", CPUUtilization: "", ConnectedUsers:"", SNR: "", MemoryUtilization: "", WiFiRadio: "", live:"", SOPHOS_INFO: "")
                                  _ =  datas.saveToMyAP(myobjc.mac!,ssdp)
                               //   print("save Myapp status: \(status)")
                              }
                              .accessibilityIdentifier("updateMy\(myobjc.mac!)")
                        }
                    }
                    .frame(width:geometry.size.width-40 , height:80 , alignment: .leading)
                    .border("\(myobjc.central!)" == "1" ? Color.c0x005BC8:Color.c0x696A6B) //0x1987CB
                }
                .frame(width:geometry.size.width-40 )
                    
                    }
                    .frame(height:geometry.size.height - 250, alignment: .top )
                    .background(Color.white)
                    
                    }//newcount
                    
                } // ZStack
            }//scrollview
            .frame(height:150 * CGFloat(searchNewResults.count))
            .scrollOnOverflow()
                
             addAllNew()
                    .position(x: geometry.size.width / 2 , y: geometry.size.height-250)
          }
            
        }
        else{
            GeometryReader { geometry in
            ScrollView(.vertical) {
                
                ZStack{
                    if mAccess.myCount == false
                    {
                VStack {
                    Text("Welcome to")
                        .font(.InterRegular24)
                        .foregroundColor(Color.black)
                        .padding(.top,20)
                    HStack{
                        Image("WiFiBlue")
                        Text("Wireless")
                         .font(.InterSemiBold36)
                         .foregroundColor(Color.c0x005BC8)
                    }
                    HStack {
                        Rectangle().frame(width: geometry.size.width-150 , height: 1, alignment: .center)
                        
                    }
                    .padding([.leading,.trailing],-10)
                    .frame(alignment: .center)
                    .foregroundColor(Color.c0x696A6B)
                    
                    Text("Sophos AP helps you manage all your Sophos APs")
                        .font(.InterRegular14)
                        .foregroundColor(Color.black)
                        .padding(5)
                    Text("You currently have no APs listed")
                        .font(.InterRegular14)
                        .foregroundColor(Color.black)
                        .padding(5)
                    Text("Let scan for New APs to add to your manage list")
                        .font(.InterRegular14)
                        .foregroundColor(Color.black)
                    Spacer()
                    Text("Searching for new Access Points")
                        .font(.InterRegular14)
                        .foregroundColor(Color.black)
                   
                    HStack {
                        ProgressView(value: downloadAmount,total: 100)
                            .progressViewStyle(gradientStyle)
                            .frame(width: geometry.size.width-100, height: 16, alignment: .center)
                            .background(Color.c0xCED1D5)
                            .cornerRadius(5)
                            .onReceive(Just(timeRemaining)) { _ in
                                if downloadAmount < 100.0  && downloadAmount >= 1.0 {
                                    if downloadAmount == 99.0
                                    {
                                        downloadAmount = 100.0
                                        timeRemaining = 0
                                    }
                                    else
                                    {
                                        
                                        DispatchQueue.main.asyncAfter(deadline: .now() +  0.1) {
                                            downloadAmount += 2.0
                                            timeRemaining += 1
                                        }
                                    }
                                }
                                if downloadAmount == 100.0 {
                                   // self.timer.upstream.connect().cancel()
                                    //XCTest 202207
                                    DispatchQueue.main.asyncAfter(deadline: .now() +  20) {
                                        mAccess.updateProgress()
                                    }
                                    
                                    downloadAmount = 0.0
                                    pointbuttonPressed = true
                                    timeRemaining = 0
                                }
                            }
                       }//HStack
                    Text("\(timeString(time: Int(downloadAmount/20)))")
                        .font(.InterRegular14)
                        .foregroundColor(Color.black)
                    
                }.frame(width:geometry.size.width  , height:geometry.size.height-300 , alignment: .center)
                       // .foregroundColor(Color.black.opacity(0.7))
                } // if Access.myCount >= 1
            
            if mAccess.myCount {
            LazyVStack(spacing: 10) {
            ForEach(searchMyResults, id: \.self) { myobjc in
                   // NavigationLink(destination: Text(myobjc.appname!)) {
                Button(action: {
                    mAccess.err = ""
                    requestErr = false
                    if myobjc.unknow! >= 6
                    {
                        backPress.accessDetail = myobjc
                        backPress.aPaddress = myobjc.location!
                        backPress.aPaccount = aPaccount
                        backPress.aPpw = aPpassword
                        backPress.newAccess = mAccess.newAccess
                        backPress.myAccess = mAccess.myAccess
                        backPress.newFlag = mAccess.newCount
                        backPress.myFlag = mAccess.myCount
                        if myobjc.central == "1"
                        {
                            backPress.central = true
                        }
                        else
                        {
                            backPress.central = false
                        }
                        backPress.hideMenu = false
                        backPress.page = 2
                    }
                    else
                    {
                        if myobjc.central == "1"
                        {
                            backPress.central = true
                        }
                        else
                        {
                            backPress.central = false
                        }
                    backPress.accessDetail = myobjc
                    mAccess.aPAddress = myobjc.location!
                    let login = datas.getLoginMatch(myobjc.mac!)
                    aPaccount = login.admin_account!
                    aPpassword = login.admin_pw!.description
                    mKeeplogin = (login.keep == 1 ? true:false)
                     if mKeeplogin && aPaccount != "" && aPpassword != "" && mKeeplogin == true
                    {
                        var err = ""
                        do{
                                let aesgcm256KEY:SymmetricKey? = try GenericPasswordStore().readKey(account: "aesgcm256KEY")
                                if aesgcm256KEY != nil {
                                    let encodedData = Data(base64Encoded: aPpassword)!
                                    let sealedBox = try AES.GCM.SealedBox(combined: encodedData)
                                    let decrypted = try? AES.GCM.open(sealedBox, using: aesgcm256KEY!)
                                    aPpassword = String(decoding: decrypted!, as: UTF8.self)
                                }
                        }
                        catch let error {
                            err = error.localizedDescription
                        }
                        if err == ""
                        {
                            mAccess.digestAccount = aPaccount
                            mAccess.digestPw = aPpassword
                            mAccess.getSystem()
                        }
                        else
                        {
                            self.requestErrMessage = err
                            self.requestErr = true
                        }
                    }
                    else
                    {
                        aPaccount = ""
                        aPpassword = ""
                        self.showLOGINflag = true
                        blurAmount = 2
                        
                        let ssdp = SSDP(LOCATION: myobjc.location!, SERVER: "", ST: "", USN: "", Vendor: "", Mac: myobjc.mac!, Model:"", Name: myobjc.appname , OperatorMode: myobjc.central , Identity: "", FirmwareVersion: "", CPUUtilization: "", ConnectedUsers:"", SNR: "", MemoryUtilization: "", WiFiRadio: "", live:"", SOPHOS_INFO: "")
                        _ = datas.saveToMyAP(myobjc.mac!,ssdp)
                    }
                    }
                } , label: {
                        HStack{
                            ZStack{
                                Rectangle().frame(width: 80,alignment: .center)
                                    .foregroundColor(Color.c0x005BC8)
                                    .padding(.leading,-6)
                                
                                if myobjc.health != "" && myobjc.wifiradio != "0"
                                {
                                    Image("wifi")
                                }
                                else if myobjc.wifiradio == "0"
                                {
                                    Image("warn")
                                }
                                else
                                {
                                    Image("warn")
                                }
                                
                                health(hex: myobjc.health!)
                                    .padding(.leading,80)
                            }
                            
                            VStack(alignment:.leading) {
                                MarqueeTitleText(text: "\(myobjc.appname!)")
                                    .font(.InterMedium12)
                                    .background(Color.white)
                                    .frame(width: geometry.size.width*0.3,height:12, alignment:.leading)
                                    .clipped()
                                if myobjc.health != ""
                                {
                                    Text(myobjc.mac!)
                                        .font(.InterRegular10)
                                       .frame(minWidth:120,alignment: .leading)
                                       .lineLimit(1)
                                    
                                    HStack(spacing:0) {
                                        Text("Health: ")
                                       .font(.InterRegular10)
                                       + Text("\(myobjc.health!)")
                                           .font(.InterBold10)
                                    }
                                    .padding(.top,-4)
                                }
                                else
                                {
                                    HStack(spacing:0) {
                                        Text("Health: ")
                                       .font(.InterRegular10)
                                       + Text("Unknown")
                                           .font(.InterBold10)
                                    }
                                    Text("AP no longer detected")
                                        .font(.InterRegular10)
                                        .padding(.top,-4)
                                }
                            }
                            .frame(width:geometry.size.width*0.3,alignment:.leading)
                            .foregroundColor(Color.black)
                            
                            HStack {
                                showClient(client:"\(myobjc.client!)",central:"\(myobjc.central!)")
                            }.frame(width:60,alignment: .leading)
                            
                            Spacer()
                            drawArrow(client: myobjc)
                        }//HStack
                        .buttonStyle(PlainButtonStyle())
                        .frame(width:geometry.size.width-40, height:80, alignment: .center )
                        .border("\(myobjc.central!)" == "1" ? Color.c0x696A6B:Color.c0x005BC8) //0x1987CB
                
            })//button
                    .accessibilityIdentifier("checkLogin\(myobjc.mac!)")
                    .onReceive(mAccess.$getSystemResult) { result in
                           //    print("Access.$getSystemResult : \(String(describing: Result))")
                               if result != nil
                               {
                                   if result?.error_code == 0
                                   {
                                       backPress.aPaddress = backPress.accessDetail.location! //myobjc.location!
                                       backPress.aPaccount = aPaccount
                                       backPress.aPpw = aPpassword
                                       backPress.newAccess = mAccess.newAccess
                                       backPress.myAccess = mAccess.myAccess
                                       backPress.newFlag = mAccess.newCount
                                       backPress.myFlag = mAccess.myCount
                                       backPress.getLoginResult = result! as GETresponse
                                       backPress.hideMenu = false
                                       backPress.page = 2
                                   }
                               }
                        else if result?.error_code != 200 && !mAccess.err.isEmpty {
                                requestErrMessage = mAccess.err
                                requestErr = true
                                aPaccount = ""
                                aPpassword = ""
                                self.showLOGINflag = true
                                blurAmount = 2
                           }
                    }
                }
                .frame(width:geometry.size.width-40 )
                
                }
                //.frame(height:geometry.size.height, alignment: .top )
                .frame(height:150 * CGFloat(searchMyResults.count), alignment: .top)
                .background(Color.white)
                
                 }//mycount
               }//ZStack
            }//scrollview
            .scrollOnOverflow()
            .accessibilityIdentifier("scrollAPlist")
             
            }
        }
        
    }
    @ViewBuilder
    func showLOGIN(tag:String,width:CGFloat,height:CGFloat,xpos:CGFloat,ypos:CGFloat) -> some View {
        if self.showLOGINflag {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                    Text("Login")
                        .multilineTextAlignment(.leading)
                        .font(.InterMedium14)
                        .foregroundColor(.black)
                        .padding(.leading,20)
                        .position(x: geometry.size.width/8, y: geometry.size.height/5 - 20)
                        
                        ZStack(alignment: .leading) {
                            if aPaccount.isEmpty {
                                Text("Administrotor Name")
                                    .foregroundColor(Color.c0x696A6B)
                                                            .frame(width: width-120-50, height: 35, alignment:.leading)
                                                            .font(.InterRegular13)
                                                            .position(x: geometry.size.width/8 + (geometry.size.width-50)/2, y: geometry.size.height/5 + 20 )
                            }
                            TextField("Administrotor Name",text:$aPaccount, onEditingChanged: { (editingChanged) in
                                if editingChanged {
                                    
                                    self.lastHoveredId = "0"
//                                    NSLog("x:\( geometry.size.width/8 + (geometry.size.width-50)/2), y: \(geometry.size.height/5 + 20 )")
                                }                   
                               
                            })
                            .frame(width: width-120-50, height: 35, alignment:.leading)
                            .font(.InterRegular13)
                            .textFieldStyle(LoginTextFieldStyle(lastHoveredId: $lastHoveredId,id: "0", width:  width-120-(width-120)/5, height: 35))
                            .position(x: geometry.size.width/8 + (geometry.size.width-50)/2, y: geometry.size.height/5 + 20 )
                            .accessibilityIdentifier("username")
                        }
                        
                        ZStack(alignment:.leading) {
                            if aPpassword.isEmpty {
                                Text("Administrotor Password")
                                    .foregroundColor(Color.c0x696A6B)
                                                            .frame(width: width-120-50, height: 35, alignment:.leading)
                                                            .font(.InterRegular13)
                                                            .position(x: geometry.size.width/8 + (geometry.size.width-50)/2, y: geometry.size.height/5 + 70)
                            }
                            SecureField("Administrotor Password",text:$aPpassword)
                            .frame(width: width-120-50, height: 35, alignment:.leading)
                            .font(.InterRegular13)
                            .textFieldStyle(LoginTextFieldStyle(lastHoveredId: $lastHoveredId,id: "1", width:  width-120-(width-120)/5, height: 35))
                            .position(x: geometry.size.width/8 + (geometry.size.width-50)/2, y: geometry.size.height/5 + 70)
                            .onTapGesture {
                                self.lastHoveredId = "1"
                            }
                            .accessibilityIdentifier("password")
                        }
                        
                       
                    Toggle(isOn: $mKeeplogin) {
                        Text("Keep login Permanently")
                            .font(.InterRegular13)
                            .frame( height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(.black)
                    }
                    .toggleStyle(CheckboxStyle(isON: $mKeeplogin))
                    .position(x: geometry.size.width/8 + (width/2-50)/2 + 15, y: geometry.size.height/5 + 110)
                    .accessibilityIdentifier("keep")
                    
                    if self.showRequest
                    {
                        ProgressBar(progress: $progressValue)
                            .frame(width: 20.0, height: 20.0)
                           // .position(x: geometry.size.width/8 + (geometry.size.width/2-50)/2 , y: geometry.size.height/5 + 140)
                            .position(x: geometry.size.width/2 , y: geometry.size.height/5 + 140)
                            .onReceive(Just(progressValue)) { newvalue in
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                self.progressValue += 0.05
                               // print("progressValue : \(progressValue)")
                                }
                            }
                            
                    }
                    if self.requestErr {
                       
                        Text(self.requestErrMessage)
                            .multilineTextAlignment(.leading)
                            .font(.InterRegular13)
                            .foregroundColor(.white)
                            .background(Color.red)
                            .frame(height:40)
                            .cornerRadius(3.0)
                            .position(x: geometry.size.width/2, y: geometry.size.height - 70)
                    }
                       
                    
                        HStack(spacing:50)  {
                        Button{
                            
                            mAccess.digestAccount = aPaccount
                            mAccess.digestPw = aPpassword
                            mAccess.getSystem()
                            self.showRequest = true
                            self.requestErr = false
                        } label: {
                            Text("Login")
                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
                                .font(.InterMedium14)
                                .foregroundColor(.white)
                                .background(Color(hex: 0x055BB5))
                                .cornerRadius(5)
                        }.frame(width: 100, height: 40)
                         .accessibilityIdentifier("login")
                         .onReceive(mAccess.$getSystemResult) { result in
                             //   print("Access.$getSystemResult : \(String(describing: Result))")
                                if result != nil
                                {
                                    if result?.error_msg != ""
                                    {
                                        self.requestErrMessage = (result?.error_msg)!
                                        self.requestErr = true
                                        self.showRequest = false
                                    }
                                    else
                                    {
                                     //   NSLog("\(Keeplogin)")
                                        var encrypted = ""
                                        var err = ""
                                        do{
                                            let digest = aPpassword.data(using: .utf8)!
                                            let aesgcm256KEY:SymmetricKey? = try GenericPasswordStore().readKey(account: "aesgcm256KEY")
                                            if aesgcm256KEY == nil
                                            {
                                                  
                                                let key = SymmetricKey(size: .bits256)
                                                try GenericPasswordStore().storeKey(key, account: "aesgcm256KEY")
                                               
                                                let bsealedBox = try? AES.GCM.seal(digest, using: key) // Encrypt
                                                
                                                let sealedBoxRestored = try? AES.GCM.SealedBox(nonce: bsealedBox!.nonce, ciphertext: bsealedBox!.ciphertext, tag: bsealedBox!.tag)
                                                encrypted = sealedBoxRestored!.combined!.base64EncodedString()
                                                
                                                
                                             
                                            }
                                            else
                                            {
                                                
                                                let bsealedBox = try? AES.GCM.seal(digest, using: aesgcm256KEY!) // Encrypt
                                                let sealedBoxRestored = try? AES.GCM.SealedBox(nonce: bsealedBox!.nonce, ciphertext: bsealedBox!.ciphertext, tag: bsealedBox!.tag)
                                                encrypted = sealedBoxRestored!.combined!.base64EncodedString()
                                            }
                                        }
                                        catch let error {
                                            err = error.localizedDescription
                                            
                                        }
                                        if err == ""{
                                     //       NSLog("\(Keeplogin)")
                                        let aperson = login.init(admin_account:aPaccount , admin_pw: encrypted , keep: Int(mKeeplogin ? 1:0))
                                            _ = datas.saveToLogin(backPress.accessDetail.mac!,aperson)
                                       // print(status)

                                        self.showLOGINflag = false
                                        self.showRequest = false
                                        blurAmount = 0

                                        backPress.aPaddress = backPress.accessDetail.location! //Access.APAddress
                                        backPress.aPaccount = aPaccount
                                        backPress.aPpw = aPpassword
                                        backPress.getLoginResult = result! as GETresponse
                                        backPress.newAccess = mAccess.newAccess
                                        backPress.myAccess = mAccess.myAccess
                                        backPress.newFlag = mAccess.newCount
                                        backPress.myFlag = mAccess.myCount
//                                        backPress.Central = false
                                        backPress.hideMenu = false
                                        backPress.page = 2
                                        }
                                        else{
                                            self.requestErrMessage = err
                                            self.requestErr = true
                                            self.showRequest = false
                                        }
                                    }
                                }
                                
                            }
                        // .accessibility(identifier: "clicklogin")
                        
                        Button{
                            self.showLOGINflag = false
                            blurAmount = 0
                        } label: {
                            ZStack{
                            RoundedRectangle(cornerRadius: 5)
                                    .stroke(Color.c0x696A6B, lineWidth: 1) //0x1987CB
                            Text("Cancel")
                                .font(.InterMedium14)
                                .foregroundColor(Color.c0x242527)
                            }.frame(width: 100, height: 40)
                        }
                        .padding(.trailing,10)
                      }//HStack
                        .padding(.leading,10)
                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
                    } // GeometryReader
                }
                .frame(width:width-120,height: 260,alignment: .center)
                .background(Color.white)
                
                
            }.popup(isPresented: $showLOGINflag, with: $showLOGINflag) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width-100,height: 260,alignment: .center)
            .position(x: xpos, y: ypos)
            //.padding()
        }
    }
    @ViewBuilder
    func showSearch(tag:String,width:CGFloat,height:CGFloat,xpos:CGFloat,ypos:CGFloat)-> some View {
        if self.showWaitRequest
        {
            VStack() {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.c0x242527))
                    .scaleEffect(x: 2, y: 2, anchor: .center)
            }
            .frame(width:200.0,height: 200.0,alignment: .center)
            .background(Color.clear)
            .position(x: xpos, y: ypos)
            .onAppear
            {
                self.blurAmount = 2
            }
        }
    }
}
struct MenuContent: View {
    @EnvironmentObject var backPress : ManageViews
    let height: CGFloat
    var body: some View {
        VStack(alignment: .leading){
           
            Text("Product Menu")
                .font(.InterMedium18)
                .onTapGesture {
              //  print("Product Menu")
            }
            HStack {
               // Rectangle().frame(width: .infinity, height: 1, alignment: .center)
                RoundedRectangle(cornerRadius: 0)
                        .stroke(Color(hex: 0xFFFFFF), lineWidth: 1)
            }
            .frame(height:1)
            VStack(alignment: .leading){
                Text("AP")
                    .font(.InterRegular16)
                    .onTapGesture {
                        backPress.showSettingFlag = false
                        backPress.showAboutFlag = false
                        backPress.hideMenu = true
                        backPress.page = 1
                        
                   }
                    .accessibility(addTraits: .isButton)
                    .accessibilityIdentifier("goAP")
                
                    HStack {
                        
                    }
                    .frame(height:self.height - 150)
                
                
                HStack {
                   // Rectangle().frame(width: .infinity, height: 1, alignment: .center)
                    RoundedRectangle(cornerRadius: 0)
                            .stroke(Color(hex: 0xFFFFFF), lineWidth: 1)

                }
                .frame(height:1)
                
                Text("Settings")
                        .font(.InterRegular16)
                         .padding(.top,5)
                        .onTapGesture {
                            backPress.showSettingFlag = true
                            backPress.showAboutFlag = false
                            backPress.hideMenu = true
                        }
                        .accessibilityIdentifier("menu_Settings")
                    
                Text("About")
                        .font(.InterRegular16)
                        .padding(.top,5)
                        .onTapGesture {
                            backPress.showSettingFlag = false
                            backPress.showAboutFlag = true
                            backPress.hideMenu = true
                        }
                        .accessibilityIdentifier("menu_About")
                HStack {
                    
                }
                .frame(height:30)
            }.frame(maxWidth:.infinity,maxHeight: self.height,alignment: .leading)
        }
        .padding([.leading,.trailing],20)
        .frame(maxWidth:.infinity, alignment: .leading)
        .background(Color.clear)
        .foregroundColor(Color.white)
       
    }
}
struct SideMenu: View {
    let width: CGFloat
    let isOpen: Bool
    let menuClose: () -> Void
    let height: CGFloat
    @EnvironmentObject var backPress : ManageViews
    
    var body: some View {
        ZStack(alignment: .bottomLeading) {
            GeometryReader { geometry in
                EmptyView()
                
                //height = geometry.size.height
            }
            .background(Color(hex: 0x152E3C,opacity: 0.01))
            .opacity(self.isOpen ? 1.0 : 0.0)
            .animation(Animation.easeIn.delay(0.25))
            .onTapGesture {
                self.menuClose()
            }
            
            VStack {
                MenuContent(height:self.height-100)
                    .frame(width: self.width,height: self.height-100,alignment: .center )
                    .padding(.top,30)
                    .background(Color.c0x001A47)
                    .offset(x: self.isOpen ? 0 : -self.width)
                    .animation(.default)
                
            }
            .frame(height: self.height-100,alignment: .center)
        }
        .background(Color.clear)
        .onReceive(backPress.$hideMenu) { flag in
            if flag {
                self.menuClose()
            }
        }
    }
}
