//
//  settings.swift
//  PSTemplate
//
//  Created by EDIOS on 2022/3/29.
//

import SwiftUI
import Combine

struct settings: View {
    @Environment(\.presentationMode) var presentationMode
    @Binding var presented: Bool
    let width: CGFloat
    let height: CGFloat
    @EnvironmentObject var backPress : ManageViews
    
    @State var mFaceID = true
    @State var mTouchID = true
    @State var mPassword = true
    @State var showFaceIDAuthAlert = false
    @State var showTouchIDAuthAlert = false
   // @State var showPasscodeAlert = false
   // @StateObject var LocalAuth = SSBKeychain()
    @StateObject var datas = ManageDatas()
    @State var face = false
    @State var touch = false
    @State var update = 0
    
    var body: some View {
        ZStack {
        VStack(spacing:0) {
                HStack {
                    
                }
                .frame(width: width, height: 100)
                .background(Color.clear)
                
                VStack(alignment:.leading,spacing:0) {
                    Text("Settings")
                        .frame(width:width,height: 30,alignment: .leading)
                        .font(.InterRegular18)
                        .foregroundColor(Color.c0x242527)
                        .padding(.top,50)
                        .padding(.leading,30)
                    
                    HStack(spacing:0){
                        if datas.localStore.string(forKey: "FaceIDAuthLAError") != ""
                        {
                            Button(action: {
                                self.showFaceIDAuthAlert = true
                                
                                //XCTest 202207
                                if datas.localStore.integer(forKey: "gotoUnknowSSID") == 0
                                {
                                   // DispatchQueue.main.asyncAfter(deadline: .now() +  10) {
                                    datas.localStore.set(4, forKey: "gotoUnknowSSID")
                                   // }
                                }
                                else
                                {
                                    datas.localStore.set(0, forKey: "gotoUnknowSSID")
                                }
                            }) {
                                Toggle(isOn: $mFaceID)
                                {
                                    Text("Unlock with Face ID")
                                }
                                .toggleStyle(CustomSettingsToggleStyle(edit: $mFaceID,index: 0))
                                .frame(alignment: .center)
                                .padding(.horizontal,20)
                                .disabled(true)
                                .accessibilityIdentifier("OpenfaceID")
                            }
                        }
                        else
                        {
                            Button{
                                if datas.localStore.string(forKey: "FaceIDAuthLAError") == ""
                                {
                                    self.mFaceID.toggle()
                                    datas.localStore.set(self.mFaceID, forKey: "faceid")
                                }
                                else
                                {
                                    showFaceIDAuthAlert = true
                                }
                            } label: {
                               
                                Toggle(isOn: $mFaceID)
                                {
                                    Text("Unlock with Face ID")
                                }
                                .toggleStyle(CustomSettingsToggleStyle(edit: $mFaceID,index: 0))
                                .frame(alignment: .center)
                                .padding(.horizontal,20)
                                .disabled(false)
                                .gesture(drag)
                                .accessibilityIdentifier("OpenfaceID")
                            }
                            
                        }
                        
                    }
                    .frame(width: width)
                    .padding(.leading,10)
                    .background(Color.white)
                    .onReceive(Just(update)) { _ in
                        if self.mFaceID == false && self.mTouchID == false
                        {
                            self.mPassword = false
                            datas.localStore.set(self.mPassword, forKey: "passcode")
                           
                        }
                    }
                    HStack(spacing:0){
                        if datas.localStore.string(forKey: "TouchIDAuthLAError") != ""
                        {
                            Button(action: {
                                self.showTouchIDAuthAlert = true
                            }) {
                                Toggle(isOn: $mTouchID)
                                {
                                    Text("Unlock with Touch ID")
                                }
                                .toggleStyle(CustomSettingsToggleStyle(edit: $mTouchID,index: 1))
                                .frame(alignment: .center)
                                .padding(.horizontal,20)
                                .disabled(true)
                                .accessibilityIdentifier("OpentouchID")
                            }
                            
                        }
                        else
                        {
                            Button{
                                if datas.localStore.string(forKey: "TouchIDAuthLAError") == ""
                                {
                                    self.mTouchID.toggle()
                                    datas.localStore.set(self.mTouchID, forKey: "touchid")
                                }
                                else
                                {
                                    showTouchIDAuthAlert = true
                                }
                            } label: {
                               
                                Toggle(isOn: $mTouchID)
                                {
                                    Text("Unlock with Touch ID")
                                }
                                .toggleStyle(CustomSettingsToggleStyle(edit: $mTouchID,index: 1))
                                .frame(alignment: .center)
                                .padding(.horizontal,20)
                                .disabled(false)
                            }
                        }
                    }
                    .frame(width: width)
                    .padding(.leading,10)
                    .background(Color.white)
       
                    HStack {
                        
                    }
                    .frame(width: width, height: self.height-100-150)
                    .background(Color.clear)
                }
                .frame(width: self.width,height: self.height-100)
                .background(Color.white)
            }
                Alert()
            }
            .frame(width: self.width,height: self.height)
            .opacity(self.presented ? 1.0 : 0.0)
            .edgesIgnoringSafeArea(.all)
            .onAppear {
                
                self.mFaceID = datas.localStore.bool(forKey: "faceid")
                self.mTouchID = datas.localStore.bool(forKey: "touchid")
                self.mPassword = datas.localStore.bool(forKey: "passcode")
                           
                if self.mFaceID == false
                {
                    if datas.localStore.string(forKey: "FaceIDAuthLAError") == ""
                    {
                        face = true
                    }
                    else
                    {
                        face = false
                    }
                }
                if self.mTouchID == false
                {
                    if datas.localStore.string(forKey: "TouchIDAuthLAError") == ""
                    {
                        touch = true
                    }
                    else
                    {
                        touch = false
                    }
                }
                if face == true && self.mFaceID == false
                {
                    self.mPassword = false
                    datas.localStore.set("Unlock with Face ID or Touch ID", forKey: "PasscodeLAError")
                }
                else if touch == true && self.mTouchID == false
                {
                    self.mPassword = false
                    datas.localStore.set("Unlock with Face ID or Touch ID", forKey: "PasscodeLAError")
                }
                
            }
    }
    @ViewBuilder
    func Alert()->some View {
        if showFaceIDAuthAlert
        {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                        Text(datas.localStore.string(forKey: "FaceIDAuthLAError")!)
                        .multilineTextAlignment(.center)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .padding(.top,20)
                        .padding(.horizontal,15)
                        HStack(alignment:.center)  {
                        Button{
                            self.showFaceIDAuthAlert = false
                          //  self.showWait = true
                        } label: {
                            Text("Ok")
                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
                                .font(.InterMedium14)
                                .foregroundColor(.white)
                                .background(Color.c0x005BC8)
                                .cornerRadius(5)
                        }.frame(width: 100, height: 40)
                                .accessibilityIdentifier("closeFaceErr")
                      }//HStack
                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
                        
                    } // GeometryReader
                }
                .frame(width:180,height: 200,alignment: .center)
                .background(Color.white)
            }.popup(isPresented: $showFaceIDAuthAlert, with: $showFaceIDAuthAlert) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:200,height: 200,alignment: .center)
           // .position(x: x, y: y)
            .padding()
        }
        else if showTouchIDAuthAlert
        {
            
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                        Text(datas.localStore.string(forKey: "TouchIDAuthLAError")!)
                        .multilineTextAlignment(.center)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .padding(.top,20)
                        .padding(.horizontal,15)
                       // .position(x: geometry.size.width/2, y: geometry.size.height/3)
                    
                        HStack(alignment:.center)  {
                        Button{
                            self.showTouchIDAuthAlert = false
                          //  self.showWait = true
                        } label: {
                            Text("Ok")
                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
                                .font(.InterRegular14)
                                .foregroundColor(.white)
                                .background(Color.c0x005BC8)
                                .cornerRadius(5)
                        }.frame(width: 100, height: 40)
                                .accessibilityIdentifier("closetouch")
                      }//HStack
                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
                    } // GeometryReader
                }
                .frame(width:180,height: 200,alignment: .center)
                .background(Color.white)
                
                
            }.popup(isPresented: $showTouchIDAuthAlert, with: $showTouchIDAuthAlert) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:200,height: 200,alignment: .center)
           // .position(x: x, y: y)
            .padding()
        }
    }
}
