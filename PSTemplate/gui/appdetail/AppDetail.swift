//
//  AppDetail.swift
//  PSTemplate
//
//  Created by EDIOS on 2022/1/25.
//

import SwiftUI
import CoreAudio
import Combine
import NetworkExtension
import CryptoKit

struct AppDetail: View {
    
    @Binding var presented: Bool
    @EnvironmentObject var backPress: ManageViews
    @StateObject var mDetail = AppDetailModel()
    @State var menuOpen: Bool = false
    @State var editFlag: Bool = false
    @State var funcPressed: Int = 0
    @State private var value = ""
    @State private var value1 = ""
    @State var save: Bool = false
    @State var showWaitRequest: Bool = false
    @State var nameAlert:Bool = false
    
    private let mLANIPTypeArray = ["Static IP","DHCP"]
    private let mFromDHCPArray = ["User-Defined","From DHCP"]
    @State var mLANIPType = "Static IP"
    @State var mLANIPTypeTaggle: Bool = false
    @State var showLANIPType: Bool = false
    @State var showLeaseTimeFlag: Bool = false
    @State var mGateWayFromDHCP = "From DHCP"
    @State var mGateWayFromDHCPFlag:Bool = false
    @State var mPriAddrFromDHCP = "From DHCP"
    @State var mPriAddrFromDHCPFlag:Bool = false
    @State var mSecAddrFromDHCP = "From DHCP"
    @State var mSecAddrFromDHCPFlag:Bool = false
    
    @State var mAuthArraySelected = 0
    private let mAuthArray = ["None","WPA2-AES","WPA2/WPA3 Mixed Mode-AES","WPA3-AES","WPA2-EAP-AES","WPA3-EAP-AES","OWE"]
    private let mAuth6GArray = ["WPA3-AES","WPA3-EAP-AES","OWE"]
    @State var showBandsTaggle24G: Bool = false
    @State var showBandsTaggle5G: Bool = false
    @State var showBandsTaggle6G: Bool = false
    @State var posBandsTaggle:CGPoint = CGPoint(x:0,y:0)
    @State var mWiFiBands24G = "1"
    @State var mWiFiBands5G = "1"
    @State var mWiFiBands6G = "1"
    @State var mWireless24G: Bool = false
    @State var mWireless5G: Bool = false
    @State var mWireless6G: Bool = false
    @State var mWireless24GFlag: Bool = false
    @State var mWireless5GFlag: Bool = false
    @State var mWireless6GFlag: Bool = false
    @State var mAuthROW = 0
    @State private var wirelessValue:[String] = []

    @State private var wirelessValue24G:[String] = []
    @State private var wirelessValue5G:[String] = []
    @State private var wirelessValue6G:[String] = []
    @State var showAlert: Bool = false
    @State var showWait: Bool = false
    @State private var blurAmount: CGFloat = 0
    @State var progressValue: Float = 0.0
    @State var mRebootSuccess: Bool = false
    @State var mRebootErr: Bool = false
    @State var mAPnotFound: Bool = false
    @State var showCurrentAPReboot:Bool = false
    @State var check:Bool = false
    @State var disableRadio: Bool = false
    @State var showRadioAlert: Bool = false
    @State var showRadioONAlert: Bool = false
    @State var showAuthType24GFlag: Bool = false
    @State var showAuthType5GFlag: Bool = false
    @State var showAuthType6GFlag: Bool = false
    @State var mSSIDArray:[String] = []
    @State var showwirelessClients:Bool = false
    @State private var offsets = [CGSize](repeating: CGSize.zero, count: 50)
    
    @State var timeZone = "(GMT-06:00) Central Time (US & Canada)"
    @State var showLocalTimeYear: Bool = false
    @State var showLocalTimeMonth: Bool = false
    @State var showLocalTimeDay: Bool = false
    @State var showLocalTimeHours: Bool = false
    @State var showLocalTimeMinutes: Bool = false
    @State var showLocalTimeSeconds: Bool = false
    @State var showTimeZone: Bool = false
    
    @State var mNTPServerType = "User-Defined"
    @State var showNTPServerType: Bool = false
    @State var mFromNTPServerlag:Bool = false
    @State var onNTPServer:Bool = false
    @State var mFromNTPServer = ""
    @State var mDaylightSaving:Bool = false
    
    @State var pingIP = ""
    @State var pingMessage = ""
    @State var mNTPinterval = "24"
    
    @State var tracerouteIP = ""
    @State var tracerouteMessage = ""
    
    @State var pingIPplaceholder = "Destination Address"
    @State var tracerouteIPplaceholder = "Destination Address"
    
    @State private var lastHoveredId = ""
    private let overText:[Int] = [0,100,200]
    @GestureState var isTapping = false
    @State private var timer = Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()
    
    @State var newDate = ""
    @State var detail = MyAccessBody(appname: "", mac: "", health: "", client: "",location: "",central: "",wifiradio: "",model: "",unknow: 7 )
    @State var central = false
    @State var showErr:Bool = false
    @State var timeoutCount = 0
    @StateObject var datas = ManageDatas()
    @State var sysReload = 0
    @State var showWaitRadio:Bool = false
    @State var wifiApply:Bool = false
    //@State var delay:Double = 10.0
    @State var showWaitPingRequest:Bool = false
    @State var pskAlert:Bool = false
    @State var mSSIDAlert:Bool = false
    @State var enableSSID = 0
    @State var refreshDateandTime:Bool = false
    @State var showWIFIAlert:Bool = false
    @State var updateUptime:Bool = false
    @State var upadteSystime:Bool = false
    @State var sysReloadTime = 0
    @State var sysReloadTimeFlag = false
    @State var sysReloadTimeCout = false
    @State var saveErrFlag = false
    @State var saveErr = ""
    
    @State var requestDate = false
    @State var diableFlag = false
    
    var body: some View {
      
        ZStack(alignment: .top) { // << made explicit alignment to top
            GeometryReader { geometry in
                VStack{
            HStack{
                Image("Menu")
                    .resizable()
                    .onTapGesture {
                     //   print("SliderMenu")
                        if !self.editFlag && !self.showWaitRequest{
                            if !self.menuOpen {
                                self.menuOpen.toggle()
                                 
                            }
                            
                        }
                    }
                    .frame(width: 18.0, height: 17.0)
                    .padding(.top, 40)
                    .padding(.leading,20)
                    .accessibility(addTraits: .isButton)
                    .accessibilityIdentifier("DetailMenu")
                
                Image("back20")
                    .onTapGesture {
                        
                        if self.editFlag
                        {
                            self.editFlag = false
                            if funcPressed == 0
                            {
                                if !self.showWaitRequest
                                {
                                    mDetail.getSystem()
                                  
                                    self.showWaitRequest = true
                                    self.blurAmount = 2
                                 
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                        self.blurAmount = 0.0
                                        self.showWaitRequest = false
                                    }
                                    
                                }
                            }
                            else if funcPressed == 1
                            {
                                if !self.showWaitRequest
                                {
                                    mDetail.getLANPort()
                                  
                                    self.showWaitRequest = true
                                    self.blurAmount = 2
                                 
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                        self.blurAmount = 0.0
                                        self.showWaitRequest = false
                                    }
                                    
                                }
                            }
                            else if funcPressed == 2
                            {
                                if !self.showWaitRequest
                                {
                                    mDetail.getSSIDs(model: detail.model!)
                                  
                                    self.showWaitRequest = true
                                    self.blurAmount = 2
                                 
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                        self.blurAmount = 0.0
                                        self.showWaitRequest = false
                                    }
                                    
                                }
                            }
                            else if funcPressed == 4
                            {
                                if !self.showWaitRequest
                                {
                                    mDetail.getDateAndTime()
                                    self.showWaitRequest = true
                                    self.blurAmount = 2
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                        self.blurAmount = 0.0
                                        self.showWaitRequest = false
                                    }
                                }
                            }
                        }
                        else
                        {
                            if !self.showWaitRequest{
                               backPress.page = 1
                            }
                        }
                        
                    }.frame(width: 30.0, height: 30.0)
                    .padding(.top, 40)
                    .opacity(backPress.showSettingFlag || backPress.showAboutFlag ? 0:1)
                HStack{
                    Image("icon-sophos-wireless")
                        .resizable()
                        .frame(width:28,height:24)
                    Text("Wireless")
                        .font(.InterSemiBold18)
                        .foregroundColor(Color.white)
                        .padding(.top, 2)
                }
                .frame(minWidth: 0,maxWidth: .infinity, maxHeight: 100, alignment: .center)
                .padding(.top, 40)
                .padding(.leading,-100)
            }
            .environmentObject(backPress)
            .padding(0)
            .frame(minWidth: 0,maxWidth: .infinity, maxHeight: 100, alignment: .leading)
            .background(Color.c0x001A47)
            showDetail().environmentObject(backPress)
                    VStack(spacing: 0) {
            showButton(width:geometry.size.width,height:geometry.size.height)
            showFunc(width:geometry.size.width)
                    }
            Image("shadow")
                .resizable()
                    .aspectRatio(UIImage(named: "shadow")!.size, contentMode: .fill)
                .frame(minWidth: 0,maxWidth: geometry.size.width, maxHeight:5, alignment: .leading)
            ZStack {
            TabView(selection: $funcPressed) {
                GeometryReader { geometry in
                    ScrollView(.vertical) {
                        VStack(alignment:.leading, spacing:0){
                            system(width:geometry.size.width)
                                .gesture(drag)
                        }.frame(alignment: .center)
                            .padding(.bottom,20)
                            .gesture(drag)
                    }
                }.tag(0)
                .gesture(drag)
                
                GeometryReader { geometry in
                    ScrollView(.vertical) {
                        VStack(alignment:.leading, spacing:0){
                            lAN(width:geometry.size.width-40)
                                .gesture(drag)
                        }.frame(alignment: .center)
                            .padding(.bottom,20)
                            .gesture(drag)
                    }
                    .accessibilityIdentifier("lanscroll")
                }.tag(1)
                    .gesture(drag)
                
                GeometryReader { geometry in
                    ScrollView(.vertical) {
                      VStack(alignment:.leading, spacing:0){
                        wirelessBands(width:geometry.size.width-40)
                                .gesture(drag)

                        }.frame(alignment: .center)
                         .padding(.bottom,20)
                         .gesture(drag)
                    }
                    .accessibilityIdentifier("SSIDscroll")
                }.tag(2)
                    .gesture(drag)
                
                GeometryReader { geometry in
                    ScrollView(.vertical) {
                        VStack(alignment:.leading, spacing:0){
                           wirelessClients(width:geometry.size.width-40)
                                .gesture(drag)
                        }.frame(alignment: .center)
                            .padding(.bottom,20)
                            .gesture(drag)
                    }
                }.tag(3)
                    .gesture(drag)
                
                GeometryReader { geometry in
                    ScrollView(.vertical, showsIndicators: false) {
                        
                            VStack(alignment:.leading, spacing:0){
                               dateAndTime(width:geometry.size.width-40)
                                    .gesture(drag)
                            }.frame(alignment: .center)
                                .padding(.bottom,40)
                                .gesture(drag)
                    }
                     .accessibilityIdentifier("Datescroll")
                }.tag(4)
                    .gesture(drag)
                
                GeometryReader { geometry in
                    ScrollView(.vertical) {
                        VStack(alignment:.leading, spacing:0){
                            pingTest(width:geometry.size.width-40)
                                .gesture(drag)
                        }.frame(alignment: .center)
                            .padding(.bottom,20)
                            .gesture(drag)
                    }
                }.tag(5)
                    .gesture(drag)
                
                GeometryReader { geometry in
                    ScrollView(.vertical) {
                        VStack(alignment:.leading, spacing:0){
                           tracerouteTest(width:geometry.size.width-40)
                                .gesture(drag)
                        }.frame(alignment: .center)
                            .padding(.bottom,20)
                            .gesture(drag)
                    }
                }.tag(6)
                    .gesture(drag)
             }
             
             .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
             .onAppear(perform: {
                 
                 UITableView.appearance().alwaysBounceHorizontal = false
              })
             .adaptsToKeyboard()
             .gesture(
                    DragGesture()
                     .onChanged{_ in
                         UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                         self.lastHoveredId = ""
                     }
             )
             .opacity(self.mAPnotFound ? 0.0:1.0)
             .id(funcPressed)
             Text("This AP can no longer be detected It maybe turned off or disconnected\n\n Please check its physical connections")
                    .multilineTextAlignment(.center)
                    .font(.InterRegular18)
                        .frame(width:geometry.size.width-40,alignment: .center)
                        .foregroundColor(Color.c0x242527)
                        .opacity(self.mAPnotFound ? 1.0:0.0)
                        .position(x: geometry.size.width/2, y: geometry.size.height/4 )
                        //.padding(.top,-30)
                }//ZStack
            } // VStack
            .blur(radius: blurAmount,opaque: false)
            .disabled(blurAmount == 0 ? false : true)
           
                ZStack {
            showsave(width:geometry.size.width)
            showRebootSuccess(width:geometry.size.width)
            showDialog(tag:"reboot",width:geometry.size.width,height:geometry.size.height, xpos:geometry.size.width / 2 , ypos:geometry.size.height/2 , content: "Are you sure you wish to reboot your AP? \nClients will be disconnected")
            showWait(tag: "reboot", width: geometry.size.width, height: geometry.size.height, xpos: geometry.size.width / 2, ypos: geometry.size.height/2, content: "This AP is now rebooting\nPlease wait")
            showRadioDialog(tag: "Radio", width:geometry.size.width,height:geometry.size.height, xpos:geometry.size.width / 2 , ypos:geometry.size.height/2 , content: "Are you sure you wish to disable\nthe radio of your AP? \n\nClients will be disconnected")
            showList(width: geometry.size.width,height:geometry.size.height)
                
                about.init(presented: $backPress.showAboutFlag,width: geometry.size.width, height:geometry.size.height)
                    .animation(.default)
                settings.init(presented: $backPress.showSettingFlag,width: geometry.size.width, height:geometry.size.height)
                    .animation(.default)
                    SideMenu(width: 270,isOpen: self.menuOpen,menuClose: self.openMenu,height:geometry.size.height)
                }
                .frame(width: geometry.size.width,height:geometry.size.height)
                .background(blurAmount == 0 ? Color.clear:Color.c0xFFFFFF_10)
                .onTapGesture{
                    self.blurAmount = 0
                    self.showLANIPType = false
                    self.showLeaseTimeFlag = false
                    self.mGateWayFromDHCPFlag = false
                    self.mSecAddrFromDHCPFlag = false
                    self.mPriAddrFromDHCPFlag = false
                    self.showBandsTaggle24G = false
                    self.showBandsTaggle5G = false
                    self.showBandsTaggle6G = false
                    self.showAuthType24GFlag = false
                    self.showAuthType5GFlag = false
                    self.showAuthType6GFlag = false
                    self.showLocalTimeYear = false
                    self.showLocalTimeMonth = false
                    self.showLocalTimeDay = false
                    self.showLocalTimeHours = false
                    self.showLocalTimeMinutes = false
                    self.showLocalTimeSeconds = false
                    self.showTimeZone = false
                    self.showNTPServerType = false
                }
            }
        }//ZStack
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
        .background(Color.white)
        .edgesIgnoringSafeArea(.all)
        .disabled(showWaitRequest)
        .onAppear(){
            detail = backPress.accessDetail as MyAccessBody
            central = backPress.central
            if detail.unknow! >= 6
            {
                self.mAPnotFound = true
            }
            mDetail.update()
            mDetail.myCount = backPress.myFlag
            mDetail.myAccess = backPress.myAccess
            /*System*/
            mDetail.aPAddress = backPress.aPaddress
            mDetail.mDigestAccount = backPress.aPaccount
            mDetail.mDigestPw  = backPress.aPpw
            if backPress.getLoginResult != nil
            {
                mDetail.getSystemResult = backPress.getLoginResult! as GETresponse
                mDetail.updateR()
            }
//            else
//            {
//                if self.APnotFound == false {
//                Detail.getSystem()
//                }
//            }
//            value1 = backPress.value1
//            if value1 != ""
//            {
//                Detail.system[1].value = value1
//            }
            /*wirelessBands*/
            mDetail.initBands()
//            if backPress.myWireless != []
//            {
//                Detail.wireless24G = backPress.myWireless
//            }
            wirelessValue24G = mDetail.wireless24G.map{$0.value!}
            
            //Detail.updateWireless5G()
//            if backPress.myWireless != []
//            {
//                Detail.wireless5G = backPress.myWireless
//            }
            wirelessValue5G = mDetail.wireless5G.map{$0.value!}
            
//            Detail.updateWireless6G()
            if support6G.contains(detail.model!){
//            if backPress.myWireless != []
//            {
//                Detail.wireless6G = backPress.myWireless
//            }
            wirelessValue6G = mDetail.wireless6G.map{$0.value!}
                
            }
            if backPress.wirelessType == []
            {
                backPress.wirelessType.append(true)
                backPress.wirelessType.append(true)
              //  if support6G.contains(detail.model!){
                backPress.wirelessType.append(true)
              //  }
            }
                mWireless24G = backPress.wirelessType[0]
                mWireless5G = backPress.wirelessType[1]
            if support6G.contains(detail.model!){
                mWireless6G = backPress.wirelessType[2]
            }

            mWireless24G = (detail.wifiradio == "1" || detail.wifiradio == "3" || detail.wifiradio == "5" || detail.wifiradio == "7") ? true:false
            mWireless24GFlag = mWireless24G
            mWireless5G = (detail.wifiradio == "2" || detail.wifiradio == "3" || detail.wifiradio == "6" || detail.wifiradio == "7") ? true:false
            mWireless5GFlag = mWireless5G
            if support6G.contains(detail.model!){
                mWireless6G = (detail.wifiradio == "4" || detail.wifiradio == "5" || detail.wifiradio == "6" || detail.wifiradio == "7") ? true:false
                mWireless6GFlag = mWireless6G
            }
            disableRadio = (detail.wifiradio == "0") ? true:false
            
            if detail.model!.contains("420") {
                mSSIDArray = mSSID420Array
            }
            else if detail.model!.contains("840") {
                mSSIDArray = mSSID840Array
            }
            
            /*WirelessClients*/
            if backPress.lANType == []
            {
                backPress.lANType.append(false)
            }
            else
            {
                mLANIPTypeTaggle = backPress.lANType[0]
            }
            
            /*LocalTime*/
            mDetail.updateEditStyle()
            mDetail.initDateTime()
        }
        .onReceive(ManageViews.shared.$myAccess) { result in
           // ForEach(0..<backPress.myAccess.count, id:\.self) { iuuid in
                for iuuid in backPress.myAccess {
                    if iuuid.mac == detail.mac
                    {
                        detail = iuuid
                        backPress.accessDetail = iuuid
                      
                        mWireless24GFlag = (iuuid.wifiradio == "1" || iuuid.wifiradio == "3" || iuuid.wifiradio == "5" || iuuid.wifiradio == "7") ? true:false
                        mWireless5GFlag = (iuuid.wifiradio == "2" || iuuid.wifiradio == "3" || iuuid.wifiradio == "6" || iuuid.wifiradio == "7") ? true:false
                        if support6G.contains(iuuid.model!){
                            mWireless6GFlag = (iuuid.wifiradio == "4" || iuuid.wifiradio == "5" || iuuid.wifiradio == "6" || iuuid.wifiradio == "7") ? true:false
                        }
                        disableRadio = (iuuid.wifiradio == "0") ? true:false
                        
                        if iuuid.unknow! >= 6
                        {
                            self.mAPnotFound = true
                        }
                        else
                        {
                            self.mAPnotFound = false
                        }
                        mDetail.aPAddress = iuuid.location!
                        
                        backPress.accessDetail.appname = iuuid.appname
                        
                        //XCTEST 202207
                        if datas.localStore.integer(forKey: "gotoUnknowSSID") == 4
                        {
                            mDetail.aPAddress = iuuid.location! + "test"
                        }
                    }
                }
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
        .background(Color.white)
        .edgesIgnoringSafeArea(.all)
    }//body
    func openMenu() {
        self.menuOpen.toggle()
    }
    @ViewBuilder
    func showDetail()->some View {
        //let detail = backPress.accessDetail as MyAccessBody
       //&& !self.APnotFound
        if central && !self.mAPnotFound {
                ZStack(alignment:.top){
                    GeometryReader { geometry in
                    Image("myTopCentral150")
                            .frame(maxWidth: .infinity,maxHeight: 80,alignment:.trailing)
                            .padding(.top,25)
                            .padding(.trailing,40)
                        
                    VStack(alignment:.leading){
//                        Text("\(detail.appname!)")
//                            .font(.InterBold24)
                        
                        MarqueeDetailTitleText(backPress: backPress,width: geometry.size.width-120)
                           // .frame(width: geometry.size.width / 2, height: 30, alignment: .leading)
                            .frame(width:geometry.size.width-120, height: 30, alignment: .leading)
                            .font(.InterBold24)
                            .clipped()
                        Text("\(detail.mac!)")
                            .font(.InterRegular14)
                            
                        Text("2.4 GHz | 5GHz | 6GHz")
                            .font(.InterRegular14)
                            
                        HStack {
                            Image("detailClient")
                            Text(" \(detail.client!) connected clients")
                                .font(.InterMedium14)
                        }
                        .padding(.top , -5)
                    }
                    .frame(width:geometry.size.width,alignment:.leading)
                    .padding([.top,.leading],20)
                    
                        HStack {
                    Image("detailDelete15")
                            .resizable()
                            .frame(width:16,height:18,alignment:.trailing)
                        .padding(.top,15)
                        .onTapGesture {
                            if !self.editFlag{
                            if datas.deleteMyap(detail.mac!)
                                {
                                    mDetail.delete(mac: detail.mac!)
                                    backPress.myAccess = mDetail.myAccess
                                    backPress.page = 1
                                    _ = datas.deleteLogin(detail.mac!)
                                }
                            }
                        }
                        .accessibility(addTraits: .isButton)
                        .accessibilityIdentifier("deleteCentral\(detail.mac!)")
                    }.frame(width:geometry.size.width-15, alignment:.trailing)
                        .background(Color.clear)
                        
                    }//geometry
                }
                .frame(maxHeight: 130 , alignment: .leading)
                .background(Color.c0x005BC8)
                .padding(.top,-10)
                .disabled(self.showWaitRequest)
        }
        else if self.mAPnotFound {
            ZStack(alignment:.top){
                GeometryReader { geometry in
                Image("myTop150fail")
                        .frame(maxWidth: .infinity,maxHeight: 70,alignment:.trailing)
                        .padding(.top,30)
                        .padding(.trailing,40)
                    
                    VStack(alignment:.leading, spacing:0){
//                    Text("\(detail.appname!)")
//                        .font(.InterBold24)
                        
                        MarqueeDetailTitleText(backPress: backPress,width: geometry.size.width-120)
                         //   .frame(width: geometry.size.width / 2, height: 30, alignment: .leading)
                            .frame(width:geometry.size.width-120,height: 30, alignment: .leading)
                            .font(.InterBold24)
                            .clipped()
                    HStack {
                        Text("AP not found")
                            .font(.InterRegular14)
                    }
                    HStack {
                        Image("detailClient")
                        Text(" 0 connected clients")
                            .font(.InterMedium14)
                    }
                    .padding(.top , 10)
                }
                .frame(width:geometry.size.width,alignment:.leading)
                .padding([.top,.leading],20)
                
                    HStack {
                Image("detailDelete15")
                        .resizable()
                        .frame(width:16,height:18,alignment:.trailing)
                    .padding(.top,15)
                    .onTapGesture {
                        if !self.editFlag{
                            if datas.deleteMyap(detail.mac!)
                            {
                        mDetail.delete(mac: detail.mac!)
                        backPress.myAccess = mDetail.myAccess
                        backPress.page = 1
                            _ = datas.deleteLogin(detail.mac!)
                            }
                        }
                    }
                    .accessibility(addTraits: .isButton)
                    .accessibilityIdentifier("deleteAPnotFound\(detail.mac!)")
                }.frame(width:geometry.size.width-15, alignment:.trailing)
                    .background(Color.clear)
                }//geometry
            }
            .frame(maxHeight: 130 , alignment: .leading)
            .background(Color.c0x005BC8)
            .padding(.top,-10)
            .disabled(self.showWaitRequest)
        }
        else{
        if self.disableRadio
        {
            //let detail = backPress.accessDetail as MyAccessBody
            ZStack(alignment:.top){
                GeometryReader { geometry in
                Image("myTop150fail")
                        .frame(maxWidth: .infinity,maxHeight: 70,alignment:.trailing)
                        .padding(.top,30)
                        .padding(.trailing,40)
                    
                    VStack(alignment:.leading, spacing:0){
                        MarqueeDetailTitleText(backPress: backPress,width: geometry.size.width-120)
                            .frame(width: geometry.size.width - 120, height: 30, alignment: .leading)
                         //   .frame(width:233,height: 30, alignment: .leading)
                            .font(.InterBold24)
                            .clipped()
                      
                  
                    HStack {
                        Image("radioDisable")
                            .resizable()
                            .frame(width:20,height: 20,alignment:.leading)
                        Text("AP radio is disabled")
                            .font(.InterRegular14)
                    }
                    HStack {
                        Image("detailClient")
                        Text(" 0 connected clients")
                            .font(.InterMedium14)
                    }
                    .padding(.top , 10)
                }
                .frame(width:geometry.size.width,alignment:.leading)
                .padding([.top,.leading],20)
                
                    HStack {
                Image("detailDelete15")
                        .resizable()
                        .frame(width:16,height:18,alignment:.trailing)
                    .padding(.top,15)
                    .onTapGesture {
                        if !self.editFlag{
                            if datas.deleteMyap(detail.mac!)
                            {
                        mDetail.delete(mac: detail.mac!)
                        backPress.myAccess = mDetail.myAccess
                        backPress.page = 1
                            _ = datas.deleteLogin(detail.mac!)
                            }
                        }
                    }
                    .accessibility(addTraits: .isButton)
                    .accessibilityIdentifier("deleteRadioDisable\(detail.mac!)")
                }.frame(width:geometry.size.width-15, alignment:.trailing)
                    .background(Color.clear)
                }//geometry
            }
            .frame(maxHeight: 130 , alignment: .leading)
            .background(Color.c0x005BC8)
            .padding(.top,-10)
            .disabled(self.showWaitRequest)
        }
        else if self.disableRadio == false && central == false
        {
           // let detail = backPress.accessDetail as MyAccessBody
            ZStack(alignment:.top){
                GeometryReader { geometry in
                Image("myTop150")
                    .frame(maxWidth: .infinity,maxHeight: 80,alignment:.trailing)
                    .padding(.top,15)
                    .padding(.trailing,40)
                    
                VStack(alignment:.leading){

                    MarqueeDetailTitleText(backPress: backPress,width: geometry.size.width-120)
                       // .frame(width:233,height: 30, alignment: .leading)
                        .frame(width:geometry.size.width - 120 , height: 30, alignment: .leading )
                        .font(.InterBold24)
                        .clipped()
                        .padding(.top , -10)
 
                    Text("\(detail.mac!)")
                        .font(.InterRegular14)
                        .padding(.top , -5)
                    
//                   let wireless24G = (detail.wifiradio == "1" || detail.wifiradio == "3" || detail.wifiradio == "5" || detail.wifiradio == "7") ? true:false
//                    let wireless5G = (detail.wifiradio == "2" || detail.wifiradio == "3" || detail.wifiradio == "6" || detail.wifiradio == "7") ? true:false
//                    let wireless6G = false
//                    if support6G.contains(detail.model!){
//                        wireless6G = (detail.wifiradio == "4" || detail.wifiradio == "5" || detail.wifiradio == "6" || detail.wifiradio == "7") ? true:false
//                    }
                    
                    if mWireless24GFlag && mWireless5GFlag && mWireless6GFlag
                    {
                        Text("2.4 GHz | 5GHz | 6GHz")
                            .font(.InterRegular14)
                            .padding(.top , -5)
                    }
                    else if mWireless24GFlag && mWireless5GFlag
                    {
                        Text("2.4 GHz | 5GHz")
                            .font(.InterRegular14)
                            .padding(.top , -5)
                    }
                    else if mWireless24GFlag && mWireless6GFlag
                    {
                        Text("2.4 GHz | 6GHz")
                            .font(.InterRegular14)
                            .padding(.top , -5)
                    }
                    else if mWireless5GFlag && mWireless6GFlag
                    {
                        Text("5GHz | 6GHz")
                            .font(.InterRegular14)
                            .padding(.top , -5)
                    }
                    else if mWireless24GFlag
                    {
                        Text("2.4 GHz")
                            .font(.InterRegular14)
                            .padding(.top , -5)
                    }
                    else if mWireless5GFlag
                    {
                        Text("5GHz")
                            .font(.InterRegular14)
                            .padding(.top , -5)
                    }
                    else if mWireless6GFlag
                    {
                        Text("6GHz")
                            .font(.InterRegular14)
                            .padding(.top , -5)
                    }
                    
                        
                    HStack {
                        Image("detailClient")
                        
                        ForEach(0..<backPress.myAccess.count, id:\.self) { iuuid in
                            if backPress.myAccess[iuuid].mac == detail.mac
                            {
                                Text("\(backPress.myAccess[iuuid].client!) connected clients")
                                .font(.InterMedium14)
                                  
                            }
                        }
                        
                    }
                    .padding(.top , -3)
                }
                .frame(width:geometry.size.width,alignment:.leading)
                .padding([.top,.leading],20)
                .onAppear {
                    var currentNetworkInfo: [String: Any] = [:]
                    getNetworkInfo { (wifiInfo) in

                      currentNetworkInfo = wifiInfo
                      //  NSLog("currentNetworkInfo : \(currentNetworkInfo["SSID"])")
                    }
                    
                    _ = self.getIPAddress()
                  //  NSLog("arr : \(arr)")
                }
                
                    HStack {
                Image("detailDelete15")
                    .resizable()
                    .frame(width:16,height:18,alignment:.trailing)
                    .padding(.top,15)
                    .onTapGesture {
                        if !self.editFlag{
                            if datas.deleteMyap(detail.mac!)
                            {
                        mDetail.delete(mac: detail.mac!)
                        backPress.myAccess = mDetail.myAccess
                        backPress.page = 1
                           _ =   datas.deleteLogin(detail.mac!)
                               // print(a)
                            }
                        }
                    }
                    .accessibility(addTraits: .isButton)
                    .accessibilityIdentifier("delete\(detail.mac!)")
                        
                  }.frame(width:geometry.size.width-15, alignment:.trailing)
                        .background(Color.clear)
                }//geometry
            }
            .frame(maxHeight: 130 , alignment: .leading)
            .background(Color.c0x005BC8)
            .padding(.top,-10)
            .disabled(self.showWaitRequest)
        }
        }
    }
    @ViewBuilder
    func showButton(width:CGFloat,height:CGFloat)->some View {
        if central
        {
            HStack(spacing:0) {
                
                Link(destination: URL(string: "https://www.sophos.com/en-us/legal/sophos-end-user-terms-of-use")!, label: {
                    Text("This AP is manageed by Sophos Central. To manage this\nAP please login to Sophos Central by clicking here")
                        .underline()
                        .font(.InterRegular14)
                        .foregroundColor(Color.white)
                        .background(Color.c0x242527)
                        .frame(width: width , height: 50)
                        //.padding(.top,-10)
                        .accessibilityIdentifier("gotoSophos")
                        
                })
                .frame(width: width,height: 50)
                .background(Color.c0x242527)
            }
            .padding(.top,-10)
        }
        else if self.mAPnotFound
        {
            HStack(spacing:0) {
                Image("radioDisable")
                Text("AP not found")
                .foregroundColor(Color.c0x242527)
            }
            .frame(width: width , height: 50)
            .padding(.top,-10)
            .background(Color.white)
        }
        else{
            if self.editFlag == false {
                
                HStack(spacing: 0) {
                    Button{
                        if funcPressed != 3 ||  funcPressed != 5 || funcPressed != 6
                        {
                        self.editFlag = true
                            self.diableFlag = false
                        }
                        else
                        {
                            self.diableFlag = true
                        }
                        backPress.lANType[0] = self.mLANIPTypeTaggle
                    } label: {
                        ZStack{
                            RoundedRectangle(cornerRadius: 0)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                            HStack {
                            Image("edit")
                            
                            Text("Edit")
                                //    .frame(minWidth: 0, maxWidth: 70, minHeight: 0, maxHeight: 40,alignment: .leading)
                                .font(.InterRegular18)
                                .foregroundColor(Color.c0x242527)
                            }
                            .frame(alignment:.center)
                            .opacity(diableFlag ? 0.3:1.0)
                            
                        }
                    }.frame(width: width/3 , height: 50)
                        .buttonStyle(PlainButtonStyle())
                        .disabled(diableFlag)
                    Button{
                        self.showAlert = true
                        self.blurAmount = 2
                       // client.SSDPResults.removeAll()
                        self.timer = Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()
                    } label: {
                        ZStack{
                            RoundedRectangle(cornerRadius: 0)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                            HStack {
                        Image("reboot")
                        
                        Text("Reboot")
                            //.frame(minWidth: 0, maxWidth: 70, minHeight: 0, maxHeight: 40,alignment: .leading)
                            .font(.InterRegular18)
                            .foregroundColor(Color.c0x242527)
                            }
                            .frame(alignment:.center)
                        }
                        
                    }.frame(width: width/3 , height: 50)
                        .buttonStyle(PlainButtonStyle())
                        .accessibilityIdentifier("reboot")
                    if self.disableRadio
                   {
                       Button{

                          // self.showRadioONAlert = true
                           self.blurAmount = 2
                           let ssdp = datas.getCurrentEableMatch(detail.mac!)
                           if ssdp.WiFiRadio != "" &&  ssdp.WiFiRadio != "0"
                           {
                             //  NSLog("OffWiFi :\(ssdp.WiFiRadio)")
                               mDetail.getOffWiFiResult = 0
                               mDetail.onWiFi(wifiradoi:ssdp.WiFiRadio!,model: detail.model!)
                               self.check = true
                               
                           }
                           else
                           {
                               mDetail.getOffWiFiResult = 0
                               if support6G.contains(detail.model!){
                                   mDetail.onWiFi(wifiradoi:"7",model: detail.model!)
                               }
                               else
                               {
                                   mDetail.onWiFi(wifiradoi:"3",model: detail.model!)
                               }
                               self.check = true
                           }
                           
                       } label: {
                           ZStack{
                               RoundedRectangle(cornerRadius: 0)
                                   .stroke(Color.c0x696A6B, lineWidth: 1)
                               HStack {
                           Image("disable")
                           
                           Text("Enable")
                               //.frame(minWidth: 0, maxWidth: 70, minHeight: 0, maxHeight: 40,alignment: .leading)
                               .font(.InterRegular18)
                               .foregroundColor(Color.c0x242527)
                               }
                               .frame(alignment:.center)
                           }
                       }.frame(width: width/3 , height: 50)
                           .buttonStyle(PlainButtonStyle())
                           .onReceive(mDetail.$getOffWiFiResult) { result in
                            //   NSLog("OffWiFi Detail.getOffWiFiResult :\(Result)")
                               var requests = 2
                               if support6G.contains(detail.model!)
                               {
                                   requests = 3
                               }
                               if result == requests && self.check == true
                               {
                                   self.showRadioONAlert = false
                                   blurAmount = 0
                                   mDetail.getOffWiFiResult = 0
                                   self.check = false
                                   
                                   mDetail.postsysReload()
                                   
                                   self.showWIFIAlert = true
                                   self.blurAmount = 2
                                   self.sysReloadTimeCout = false
                                   self.sysReloadTime = 0
                               }
                           }
                           .accessibility(addTraits: .isButton)
                           .accessibilityIdentifier("wifienable")
                   }
                   else
                   {
                       Button{
                           
                           self.showRadioAlert = true
                           self.blurAmount = 2
                           
                       } label: {
                           ZStack{
                               RoundedRectangle(cornerRadius: 0)
                                   .stroke(Color.c0x696A6B, lineWidth: 1)
                               HStack {
                           Image("radioEnable")
                                       .resizable()
                                       .frame(width: 23, height: 21)
                           
                           Text("Disable")
                              // .frame(minWidth: 0, maxWidth: 70, minHeight: 0, maxHeight: 40,alignment: .leading)
                               .font(.InterRegular18)
                               .foregroundColor(Color.c0x242527)
                               }
                               .frame(alignment:.center)
                           }
                       }.frame(width: width/3 , height: 50)
                           .buttonStyle(PlainButtonStyle())
                           .accessibilityIdentifier("wifidisable")
                   }
                  
                }//HStack
                .frame(width: width , height: 50)
                .padding(.top,-10)
                .disabled(self.showWaitRequest)
            }
            else
            {
                HStack(spacing: 0) {
                    
                    Button{
                        
                        //backPress.value = value
                        backPress.value1 = value1
                        if self.funcPressed == 0
                        {
                            if mDetail.system[1].value != backPress.value1
                            {
                                if backPress.value1.count >= mSSIDMin && backPress.value1.count <= mSSIDMax {
                               // let reg = "[^\\x00-\\xff]"
                                    let reg = "^[A-Za-z0-9]+$"
                                    let rege = try? NSRegularExpression(pattern: reg, options: .caseInsensitive)
                                    let matchs = rege!.matches(in: backPress.value1, options: [], range: NSRange(location: 0, length: backPress.value1.count))
                                    if matchs.isEmpty {
                                        self.nameAlert = true
                                    }
                                    else {
                                        self.showWaitRequest = true
                                        self.blurAmount = 2
                                        self.sysReloadTimeCout = false
                                        self.sysReloadTime = 0
                                        mDetail.getAdministration(productName:backPress.value1)
                                    }
                                }
                                else {
                                    self.nameAlert = true
                                }
//                                let pre = NSPredicate(format: "SELF MATCHES %@", reg)
//
//                                if pre.evaluate(with: backPress.value1) {
//
//                                }else{
//                                    self.showWaitRequest = true
//                                    self.blurAmount = 2
//                                    self.sysReloadTimeCout = false
//                                    self.sysReloadTime = 0
//                                    Detail.getAdministration(productName:backPress.value1)
//                                }
//                                self.showWaitRequest = true
//                                self.blurAmount = 2
//                                self.sysReloadTimeCout = false
//                                self.sysReloadTime = 0
//                                Detail.getAdministration(productName:backPress.value1)
                            }
                            else
                            {
                                self.editFlag = false
                                return
                            }
                        }
                        else if self.funcPressed == 1
                        {
                    
                            self.showWaitRequest = true
                            self.blurAmount = 2
                            self.sysReloadTimeCout = false
                            self.sysReloadTime = 0
                            if let idx = self.mLANIPTypeArray.firstIndex(where: { $0 == self.mLANIPType }) {
                                mDetail.postLANPortBody.ipv4?.ipv4_type = idx
                                
                                if idx == 0
                                {
                                let result = calculatePressed(subnetMask: mDetail.mLANIPAddress[1].value!, IPAddress: mDetail.mLANIPAddress[0].value!)
                                
                                let sip = IPToInt(ip: result[0])
                                let eip = IPToInt(ip: result[1])
                                let gip = IPToInt(ip: mDetail.mLANIPAddress[2].value!)
                                let usip = IPToInt(ip: mDetail.mLANDHCPServer[0].value!)
                                let ueip = IPToInt(ip: mDetail.mLANDHCPServer[1].value!)
                                 
                                if gip < sip ||  gip > eip
                                {
                                    //Gateway
                                    
                                    self.saveErrFlag = true
                                    self.saveErr = "Default Gateway"
                                }
                                if self.mLANIPTypeTaggle {
                                    if usip < sip ||  usip > eip
                                    {
                                        //Start IP
                                        self.saveErrFlag = true
                                        if self.saveErr != ""
                                        {
                                        self.saveErr = self.saveErr + " , Starting IP Address"
                                        }
                                        else
                                        {
                                            self.saveErr = "Starting IP Address"
                                        }
                                    }
                                    if ueip < sip ||  ueip > eip
                                    {
                                        //End IP
                                        self.saveErrFlag = true
                                        if self.saveErr != ""
                                        {
                                        self.saveErr = self.saveErr + " , Ending IP Address"
                                        }
                                        else
                                        {
                                            self.saveErr = "Ending IP Address"
                                        }
                                    }
                                    if usip > ueip
                                    {
                                        self.saveErrFlag = true
                                        if self.saveErr != ""
                                        {
                                        self.saveErr = self.saveErr + "\n  Invalid Host Address Range"
                                        }
                                        else
                                        {
                                            self.saveErr = "Invalid Host Address Range"
                                        }
                                    }
                                }
                                if self.saveErr != ""
                                {
                                    self.showWaitRequest = false
                                    self.blurAmount = 0
                                    self.sysReloadTimeCout = false
                                    self.sysReloadTime = 0
                                    return
                                }
                                }
                            }
                            mDetail.postLANPortBody.ipv4?.ipv4_address = mDetail.mLANIPAddress[0].value
                            mDetail.postLANPortBody.ipv4?.ipv4_subnet_mask = mDetail.mLANIPAddress[1].value
                            if let idx = self.mFromDHCPArray.firstIndex(where: { $0 == self.mGateWayFromDHCP }) {
                                mDetail.postLANPortBody.ipv4?.ipv4_default_gateway_type = idx
                                
                                if mDetail.postLANPortBody.ipv4?.ipv4_type == 0
                                {
                                    mDetail.postLANPortBody.ipv4?.ipv4_default_gateway_type = 0
                                }
                            }
                            mDetail.postLANPortBody.ipv4?.ipv4_default_gateway = mDetail.mLANIPAddress[2].value
                            if let idx = self.mFromDHCPArray.firstIndex(where: { $0 == self.mPriAddrFromDHCP }) {
                                mDetail.postLANPortBody.ipv4?.ipv4_dns_primary_type = idx
                                
                                if mDetail.postLANPortBody.ipv4?.ipv4_type == 0
                                {
                                    mDetail.postLANPortBody.ipv4?.ipv4_dns_primary_type = 0
                                }
                            }
                            mDetail.postLANPortBody.ipv4?.ipv4_dns_primary = mDetail.mLANDNSServers[0].value
                            if let idx = self.mFromDHCPArray.firstIndex(where: { $0 == self.mSecAddrFromDHCP }) {
                                mDetail.postLANPortBody.ipv4?.ipv4_dns_secondary_type = idx
                                if mDetail.postLANPortBody.ipv4?.ipv4_type == 0
                                {
                                    mDetail.postLANPortBody.ipv4?.ipv4_dns_secondary_type = 0
                                }
                            }
                            mDetail.postLANPortBody.ipv4?.ipv4_dns_secondary = mDetail.mLANDNSServers[1].value
                            mDetail.postLANPortBody.ipv4?.dhcp_enabled = (self.mLANIPTypeTaggle  == true ? 1:0)
                            mDetail.postLANPortBody.ipv4?.dhcp_start_addr = mDetail.mLANDHCPServer[0].value
                            mDetail.postLANPortBody.ipv4?.dhcp_end_addr = mDetail.mLANDHCPServer[1].value
                            mDetail.postLANPortBody.ipv4?.dhcp_domain_name = mDetail.mLANDHCPServer[2].value
                            if let idx = webLeaseTimeArray.firstIndex(where: { $0 == mDetail.mLANDHCPServer[3].value }) {
                                mDetail.postLANPortBody.ipv4?.dhcp_lease_time = idx
                            }
                            else
                            {
                                mDetail.postLANPortBody.ipv4?.dhcp_lease_time = webLeaseTimeArray.count - 1
                            }
                           
                            mDetail.postLANPortBody.ipv4?.dhcp_dns_primary = mDetail.mLANDHCPServer[4].value
                            mDetail.postLANPortBody.ipv4?.dhcp_dns_secondary = mDetail.mLANDHCPServer[5].value
                            
                            mDetail.postLANPort()
                        }
                        else if self.funcPressed == 2
                        {
                            self.showWaitRequest = true
                            self.blurAmount = 2
                            self.sysReloadTimeCout = false
                            self.sysReloadTime = 0
                            
                            mDetail.post24GBandsBody.radio_enable = (mWireless24G ? 1:0)
                            mDetail.post24GBandsBody.enabled_ssidNum = Int(mWiFiBands24G)!
                            for iuuid in 0..<Int(mWiFiBands24G)!
                            {
                                mDetail.post24GBandsBody.ssidNames![iuuid].ssidname = mDetail.wireless24G[iuuid*4+0].value
                                mDetail.post24GSSIDSecurity[iuuid].auth_detail?.psk_value = mDetail.wireless24G[iuuid*4+2].value
                                mDetail.post24GBandsBody.ssidNames![iuuid].vlan_id = Int(mDetail.wireless24G[iuuid*4+3].value!)!
                                
                                if mDetail.wireless24G[iuuid*4+1].value == "None"
                                {
                                    mDetail.post24GSSIDSecurity[iuuid].auth_method = 0
                                }
                                else if mDetail.wireless6G[iuuid*4+1].value == "WEP"
                                {
                                    mDetail.post6GSSIDSecurity[iuuid].auth_method = 1
                                }
                                
                                else if mDetail.wireless24G[iuuid*4+1].value == "WPA2-AES"
                                {
                                    mDetail.post24GSSIDSecurity[iuuid].auth_method = 2
                                    mDetail.post24GSSIDSecurity[iuuid].auth_detail?.wpa_type = 5
                                    mDetail.post24GSSIDSecurity[iuuid].auth_detail?.encrypt_type = 3
                                }
                                else if mDetail.wireless24G[iuuid*4+1].value == "WPA2/WPA3 Mixed Mode-AES"
                                {
                                    mDetail.post24GSSIDSecurity[iuuid].auth_method = 2
                                    mDetail.post24GSSIDSecurity[iuuid].auth_detail?.wpa_type = 11
                                    mDetail.post24GSSIDSecurity[iuuid].auth_detail?.encrypt_type = 3
                                }
                                else if mDetail.wireless24G[iuuid*4+1].value == "WPA3-AES"
                                {
                                    mDetail.post24GSSIDSecurity[iuuid].auth_method = 2
                                    mDetail.post24GSSIDSecurity[iuuid].auth_detail?.wpa_type = 10
                                    mDetail.post24GSSIDSecurity[iuuid].auth_detail?.encrypt_type = 3
                                }
                               
                                else if mDetail.wireless24G[iuuid*4+1].value == "WPA2-EAP-AES"
                                {
                                    mDetail.post24GSSIDSecurity[iuuid].auth_method = 3
                                    mDetail.post24GSSIDSecurity[iuuid].auth_detail?.wpa_type = 5
                                    mDetail.post24GSSIDSecurity[iuuid].auth_detail?.encrypt_type = 3
                                }
                                else if mDetail.wireless24G[iuuid*4+1].value == "WPA3-EAP-AES"
                                {
                                    mDetail.post24GSSIDSecurity[iuuid].auth_method = 3
                                    mDetail.post24GSSIDSecurity[iuuid].auth_detail?.wpa_type = 10
                                    mDetail.post24GSSIDSecurity[iuuid].auth_detail?.encrypt_type = 3
                                }
                                else if mDetail.wireless24G[iuuid*4+1].value == "OWE"
                                {
                                    mDetail.post24GSSIDSecurity[iuuid].auth_method = 5
                                }
                            }
                            mDetail.post5GBandsBody.radio_enable = (mWireless5G ? 1:0)
                            mDetail.post5GBandsBody.enabled_ssidNum = Int(mWiFiBands5G)!
                            for iuuid in 0..<Int(mWiFiBands5G)!
                            {
                                //NSLog((mDetail.post5GBandsBody.ssidNames?[i].ssidname)!)
                                //NSLog(mDetail.wireless5G[i*4+0].value!)
                                mDetail.post5GBandsBody.ssidNames?[iuuid].ssidname = mDetail.wireless5G[iuuid*4+0].value
                                mDetail.post5GSSIDSecurity[iuuid].auth_detail?.psk_value = mDetail.wireless5G[iuuid*4+2].value
                                mDetail.post5GBandsBody.ssidNames![iuuid].vlan_id = Int(mDetail.wireless5G[iuuid*4+3].value!)!
                                
                                if mDetail.wireless5G[iuuid*4+1].value == "None"
                                {
                                    mDetail.post5GSSIDSecurity[iuuid].auth_method = 0
                                }
                                else if mDetail.wireless5G[iuuid*4+1].value == "WEP"
                                {
                                    mDetail.post5GSSIDSecurity[iuuid].auth_method = 1
                                }
                                
                                else if mDetail.wireless5G[iuuid*4+1].value == "WPA2-AES"
                                {
                                    mDetail.post5GSSIDSecurity[iuuid].auth_method = 2
                                    mDetail.post5GSSIDSecurity[iuuid].auth_detail?.wpa_type = 5
                                    mDetail.post5GSSIDSecurity[iuuid].auth_detail?.encrypt_type = 3
                                }
                                else if mDetail.wireless5G[iuuid*4+1].value == "WPA2/WPA3 Mixed Mode-AES"
                                {
                                    mDetail.post5GSSIDSecurity[iuuid].auth_method = 2
                                    mDetail.post5GSSIDSecurity[iuuid].auth_detail?.wpa_type = 11
                                    mDetail.post5GSSIDSecurity[iuuid].auth_detail?.encrypt_type = 3
                                }
                                else if mDetail.wireless5G[iuuid*4+1].value == "WPA3-AES"
                                {
                                    mDetail.post5GSSIDSecurity[iuuid].auth_method = 2
                                    mDetail.post5GSSIDSecurity[iuuid].auth_detail?.wpa_type = 10
                                    mDetail.post5GSSIDSecurity[iuuid].auth_detail?.encrypt_type = 3
                                }
                                
                                else if mDetail.wireless5G[iuuid*4+1].value == "WPA2-EAP-AES"
                                {
                                    mDetail.post5GSSIDSecurity[iuuid].auth_method = 3
                                    mDetail.post5GSSIDSecurity[iuuid].auth_detail?.wpa_type = 5
                                    mDetail.post5GSSIDSecurity[iuuid].auth_detail?.encrypt_type = 3
                                }
                                else if mDetail.wireless5G[iuuid*4+1].value == "WPA3-EAP-AES"
                                {
                                    mDetail.post5GSSIDSecurity[iuuid].auth_method = 3
                                    mDetail.post5GSSIDSecurity[iuuid].auth_detail?.wpa_type = 10
                                    mDetail.post5GSSIDSecurity[iuuid].auth_detail?.encrypt_type = 3
                                }
                                else if mDetail.wireless5G[iuuid*4+1].value == "OWE"
                                {
                                    mDetail.post5GSSIDSecurity[iuuid].auth_method = 5
                                }
                               
                            }
                            if support6G.contains(detail.model!)
                            {
                            mDetail.post6GBandsBody.radio_enable = (mWireless6G ? 1:0)
                            mDetail.post6GBandsBody.enabled_ssidNum = Int(mWiFiBands6G)!
                            
                                for iuuid in 0..<Int(mWiFiBands6G)!
                                {
                                  //  NSLog((mDetail.post6GBandsBody.ssidNames?[i].ssidname)!)
                                  //  NSLog(mDetail.wireless6G[i*4+0].value!)
                                    mDetail.post6GBandsBody.ssidNames?[iuuid].ssidname = mDetail.wireless6G[iuuid*4+0].value
                                    mDetail.post6GSSIDSecurity[iuuid].auth_detail?.psk_value = mDetail.wireless6G[iuuid*4+2].value
                                    mDetail.post6GBandsBody.ssidNames![iuuid].vlan_id = Int(mDetail.wireless6G[iuuid*4+3].value!)!
                                    
                                    if mDetail.wireless6G[iuuid*4+1].value == "WPA3-AES"
                                    {
                                       mDetail.post6GSSIDSecurity[iuuid].auth_method = 2
                                        mDetail.post6GSSIDSecurity[iuuid].auth_detail?.wpa_type = 10
                                        mDetail.post6GSSIDSecurity[iuuid].auth_detail?.encrypt_type = 3
                                    }
                                    else if mDetail.wireless6G[iuuid*4+1].value == "WPA3-EAP-AES"
                                    {
                                        mDetail.post6GSSIDSecurity[iuuid].auth_method = 3
                                        mDetail.post6GSSIDSecurity[iuuid].auth_detail?.wpa_type = 10
                                        mDetail.post6GSSIDSecurity[iuuid].auth_detail?.encrypt_type = 3
                                    }
                                    else if mDetail.wireless6G[iuuid*4+1].value == "OWE"
                                    {
                                        mDetail.post6GSSIDSecurity[iuuid].auth_method = 5
                                    }
                                }
                            }
                            if support6G.contains(detail.model!)
                            {
                                self.enableSSID = 3 + Int(mWiFiBands24G)! + Int(mWiFiBands5G)! //+ Int(WiFiBands6G)!
                            }
                            else
                            {
                                self.enableSSID = 2 + Int(mWiFiBands24G)! + Int(mWiFiBands5G)!
                            }
                            //NSLog("POST WIFI Start : \(enableSSID)")
                            mDetail.postSSIDs(model: detail.model!)
                            self.wifiApply = true
                           // self.timer = Timer.publish(every: 1.0, on: .main, in: .common).autoconnect()
                        }
                        else if self.funcPressed == 4
                        {
                            self.showWaitRequest = true
                            self.blurAmount = 2
                            self.sysReloadTimeCout = false
                            self.sysReloadTime = 0
                            mDetail.postDateAndTimeBody.local_time_year = Int(mDetail.mLocalTime[0].value!)
                            mDetail.postDateAndTimeBody.local_time_month = mDetail.month.firstIndex(where: {$0 == mDetail.mLocalTime[1].value})! + 1
                            mDetail.postDateAndTimeBody.local_time_day = Int(mDetail.mLocalTime[2].value!)
                            mDetail.postDateAndTimeBody.local_time_hour = Int(mDetail.mLocalTime[3].value!)
                            mDetail.postDateAndTimeBody.local_time_minute = Int(mDetail.mLocalTime[4].value!)
                            mDetail.postDateAndTimeBody.local_time_seconde = Int(mDetail.mLocalTime[5].value!)
                            mDetail.postDateAndTimeBody.time_zone = mTimeZoneList.firstIndex(where: {$0 == self.timeZone})
                            mDetail.postDateAndTimeBody.ntp_enabled = (self.onNTPServer == true ? 1:0)
                            mDetail.postDateAndTimeBody.ntp_server_name = self.mFromNTPServer
                            mDetail.postDateAndTimeBody.ntp_update_interval = Int(self.mNTPinterval) //Int(Detail.NTPTimeServer[2].value!)
                            mDetail.postDateAndTimeBody.auto_daylight_save = (self.mDaylightSaving == true ? 1:0)
                            mDetail.getDateAndTimeResult = nil
                            mDetail.postDateAndTime()
                            refreshDateandTime = true
                        }

                    } label: {
                        ZStack{
                            RoundedRectangle(cornerRadius: 0)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                        HStack {
                            Image("save")
                            
                            Text("Save")
                               // .frame(minWidth: 0, maxWidth: 70, minHeight: 0, maxHeight: 40,alignment: .leading)
                                .font(.InterRegular18)
                                .foregroundColor(.white)
                                .background(Color.c0x005BC8)
                         }
                        .frame(alignment:.center)
                        }
                    }.frame(width: width/3 , height: 50)
                        .background(Color.c0x005BC8)
                        .buttonStyle(PlainButtonStyle())
                        .onReceive(Just(sysReload)) { result in
                       //     NSLog("sysReload \(Result) \(Detail.postsysReloadResult)")
                            if mDetail.postsysReloadResult?.error_code == 0 && mDetail.postsysReloadResult?.data != nil
                            {
                      //          NSLog("sysReload : \(Detail.postsysReloadResult?.data?.reload_time)")
                                let delay = Double((mDetail.postsysReloadResult?.data?.reload_time)!)!
                                if sysReloadTimeCout == false
                                {
                                    self.sysReloadTime = Int(delay)
                                    sysReloadTimeCout = true
                                }
                                DispatchQueue.main.asyncAfter(deadline: .now() + delay ) {
                                   if self.save == false && mDetail.postsysReloadResult != nil {
                                            self.save = true
                                            NSLog("sysReload finish")
                                            sysReload = 0
                                            self.editFlag = false
                                            self.blurAmount = 0.0
                                            self.showWaitRadio = false
                                            self.showWaitRequest = false
                                            mDetail.postsysReloadResult = nil
                                    }
                                    else if self.save == false && self.funcPressed == 4 {
                                      //  self.save = true
                                        NSLog("sysReload finish")
                                        sysReload = 0
                                        self.editFlag = false
                                        self.blurAmount = 0.0
                                        self.showWaitRadio = false
                                        self.showWaitRequest = false
                                    }
                                }
                                DispatchQueue.main.asyncAfter(deadline: .now() + delay - 3.0 ) {
                                    if self.funcPressed == 4 && refreshDateandTime == true
                                    {
                                        NSLog("sysReload refresh")
                                        mDetail.getDateAndTime()
                                        refreshDateandTime = false
                                    }
                                    if self.funcPressed == 0 && mDetail.system[1].value != backPress.value1
                                    {
                                         mDetail.system[1].value = backPress.value1
                                    }
                                }
                              }
                              else
                              {
                                  DispatchQueue.main.asyncAfter(deadline: .now() + 1.0 ) {
                                      sysReload += 1
                                  }
                                  
                                  if funcPressed == 2 && self.wifiApply && mDetail.postfWiFiResult == self.enableSSID
                                  {
                                   //   NSLog("POST WIFI FINISH : \(Detail.postfWiFiResult) \(self.enableSSID)")
                                      mDetail.postsysReload()
                                      self.blurAmount = 2.0
                                      wifiApply = false
                                      self.showWaitRadio = true
                                      
                                  }
                              }
                           }
                    
                    Button{
                        
                    } label: {
                        ZStack{
                            RoundedRectangle(cornerRadius: 0)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                            HStack {
                        Image("revReboot")
                        
                        Text("Reboot")
                           // .frame(minWidth: 0, maxWidth: 70, minHeight: 0, maxHeight: 40,alignment: .leading)
                            .font(.InterRegular18)
                            .foregroundColor(Color.c0x242527_30)
                            }
                            .frame(alignment:.center)
                        }
                    }.frame(width: width/3 , height: 50)
                        .buttonStyle(PlainButtonStyle())
   
                    if self.disableRadio
                    {
                        Button{
                          //  showRadioAlert = true
                           // blurAmount = 2
                        } label: {
                            ZStack{
                                RoundedRectangle(cornerRadius: 0)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                HStack {
                            Image("revDisable")
                            
                            Text("Enable")
                             //   .frame(minWidth: 0, maxWidth: 70, minHeight: 0, maxHeight: 40,alignment: .leading)
                                .font(.InterRegular18)
                                .foregroundColor(Color.c0x242527_30)
                                }
                                .frame(alignment:.center)
                            }
                        }.frame(width: width/3 , height: 50)
                            .buttonStyle(PlainButtonStyle())
                    }
                    else
                    {
                        Button{
                           // showRadioAlert = true
                        } label: {
                            ZStack{
                                RoundedRectangle(cornerRadius: 0)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                HStack {
                            Image("reradioEnable")
                            
                            Text("Disable")
                                .font(.InterRegular18)
                                .foregroundColor(Color.c0x242527_30)
                                }
                                .frame(alignment:.center)
                            }
                        }.frame(width: width/3 , height: 50)
                            .buttonStyle(PlainButtonStyle())
                    }
                   
                }//HStack
                .frame(width: width , height: 50)
                .padding(.top,-10)
                .disabled(self.showWaitRequest)
            }
        }
    }
    @ViewBuilder
    func showFunc(width:CGFloat)->some View {
        
        ScrollView(.horizontal) {
           // HStack{
            VStack {
            HStack(spacing:0) {
                Button(action: {
                   // withAnimation(.easeInOut(duration: 0.1)) {
                        if !self.editFlag && !self.showWaitRequest{
                            self.diableFlag = false
                            mDetail.getSystemResult = nil
                            mDetail.getSystem()
                            if  self.funcPressed != 0
                            {
                                self.funcPressed = 0
                            }
                            self.showWaitRequest = true
                            self.blurAmount = 2
                         
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                self.blurAmount = 0.0
                                self.showWaitRequest = false
                            }
                            
                        }
                        lastHoveredId = ""
                   // }
                }, label: {
                    
                    if self.funcPressed == 0 {
                        VStack(spacing:2) {
                            Text("System")
                                .frame(height: 35)
                                .font(.InterSemiBold14)
                                .foregroundColor(Color.c0x242527)
                                .overlay(
                                    Rectangle().frame(height: 2).offset(y: -6).foregroundColor(Color.c0x005BC8)
                                              , alignment: .bottom)

                                
                        }
                        
                    }
                    else {
                        VStack(spacing:2) {
                        Text("System")
                            .frame(height: 35)
                            .font(.InterRegular14)
                            .foregroundColor(editFlag ? Color.c0x242527_30:Color.c0x242527)
                            .overlay(
                                Rectangle().frame(height: 2).offset(y: 4).foregroundColor(Color.clear)
                                          , alignment: .bottom)
                            
                        }
                        
                    }
                })
                
                //.padding(.leading,10)
                .onReceive(mDetail.$response) { result in
                    if result == 500
                    {
                        showWaitRequest = false
                        blurAmount = 0
                        showErr = true
                        _ = showDialog(tag: "", width: width, height: width * 2, xpos: width / 2 , ypos: width, content: "Invalid Username/Password")
                    }
                    else if result != 0 && result != 200
                    {
                        showWaitRequest = false
                        blurAmount = 0
                        showErr = true
                        
                        _ = showDialog(tag: "", width: width, height: width * 2, xpos: width / 2 , ypos: width, content: "The request timed out")
                    }
                }
                .padding(.leading,20)
                .accessibilityIdentifier("system")
                Button(action: {
                   // withAnimation(.easeInOut(duration: 1.0)) {
                        if !self.editFlag && !self.showWaitRequest{
                            self.diableFlag = false
                            mDetail.getLANResult = nil
                            mDetail.getLANPort()
                           
                            if  self.funcPressed != 1
                            {
                                self.funcPressed = 1
                            }
                            
                            self.showWaitRequest = true
                            self.blurAmount = 2
                           // self.sysReloadflag = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                self.blurAmount = 0.0
                                self.showWaitRequest = false
                            }
                        }
                        
                        lastHoveredId = ""
                   // }
                }, label: {
                   
                    if self.funcPressed == 1 {
                        VStack(spacing:2) {
                           Text("LAN Port")
                                .frame(height: 35)
                                .font(.InterSemiBold14)
                                .foregroundColor(Color.c0x242527)
                                .overlay(
                                    Rectangle().frame(height: 2).offset(y: -6).foregroundColor(Color.c0x005BC8)
                                              , alignment: .bottom)

                                
                        }
                        
                    }
                    else {
                        VStack(spacing:2) {
                        Text("LAN Port")
                            .frame(height: 35)
                            .font(.InterRegular14)
                            .foregroundColor(editFlag ? Color.c0x242527_30:Color.c0x242527)
                            .overlay(
                                Rectangle().frame(height: 2).offset(y: 4).foregroundColor(Color.clear)
                                          , alignment: .bottom)
                            
                        }
                        
                    }
                        
                })
                    .padding(.leading,20)
                 .accessibilityIdentifier("lanport")
                    Button(action: {
                       // withAnimation(.easeInOut(duration: 1.0)) {
                            if !self.editFlag && !self.showWaitRequest{
                                self.diableFlag = false
                                mDetail.getSSIDs(model:detail.model!)
                               
                                if  self.funcPressed != 2
                                {
                                    self.funcPressed = 2
                                }
                               
                                self.showWaitRequest = true
                                self.blurAmount = 2
                               // self.sysReloadflag = false
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    self.blurAmount = 0.0
                                    self.showWaitRequest = false
                                }
                            }
                            
                            lastHoveredId = ""
                       // }
                    }, label: {
                       
                        if self.funcPressed == 2 {
                            VStack(spacing:2) {
                               Text("Wireless Bands")
                                    .frame(height: 35)
                                    .font(.InterSemiBold14)
                                    .foregroundColor(Color.c0x242527)
                                    .lineLimit(1)
                                    .overlay(
                                        Rectangle().frame(height: 2).offset(y: -6).foregroundColor(Color.c0x005BC8)
                                                  , alignment: .bottom)
                            }
                            
                        }
                        else {
                            VStack(spacing:2) {
                            Text("Wireless Bands")
                                .frame(height: 35)
                                .font(.InterRegular14)
                                .foregroundColor(editFlag ? Color.c0x242527_30:Color.c0x242527)
                                .lineLimit(1)
                                .overlay(
                                    Rectangle().frame(height: 2).offset(y: 4).foregroundColor(Color.clear)
                                              , alignment: .bottom)
                                
                            }
                            
                        }
                            
                    })
                    .padding(.leading,20)
                    .accessibility(addTraits: .isButton)
                    .accessibilityIdentifier("Funcbands")

            Button(action: {
              //  withAnimation(.easeInOut(duration: 1.0)) {
                    if !self.editFlag && !self.showWaitRequest{
                        self.diableFlag = true
                        self.blurAmount = 2
                        mDetail.getClientsCount = 0
                        mDetail.wirelessClients.removeAll(keepingCapacity: true)
                        mDetail.mobilekick.removeAll(keepingCapacity: true)
                        mDetail.mobileIPSSID.removeAll(keepingCapacity: true)
                        DispatchQueue.main.async {
                        mDetail.getWirelessClients(GHz: "2")
                        mDetail.getWirelessClients(GHz: "5")
                        mDetail.getWirelessClients(GHz: "6")
                        }
                        if self.funcPressed != 3
                        {
                            self.funcPressed = 3
                        }
                        var currentNetworkInfo: [String: Any] = [:]
                            getNetworkInfo { (wifiInfo) in

                              currentNetworkInfo = wifiInfo
                             //   NSLog("currentNetworkInfo : \(currentNetworkInfo["SSID"])")
                                
                                let arr  = self.getIPAddress()
                            //    NSLog("arr : \(arr)")
                                
                                mDetail.mCurrentIP = arr
                                mDetail.mCurrentSSID = "\(currentNetworkInfo["SSID"])"
                       }
                        self.showwirelessClients = true
                        
                       
                    }
                    lastHoveredId = ""
             //   }
            }, label: {
                if self.funcPressed == 3 {
                    VStack(spacing:2) {
                       Text("Wireless Clients")
                            .frame(height: 35)
                            .font(.InterSemiBold14)
                            .foregroundColor(Color.c0x242527)
                            .lineLimit(1)
                            .overlay(
                                Rectangle().frame(height: 2).offset(y: -6).foregroundColor(Color.c0x005BC8)
                                          , alignment: .bottom)

                    }
                    
                }
                else {
                    VStack(spacing:2) {
                    Text("Wireless Clients")
                        .frame(height: 35)
                        .font(.InterRegular14)
                        .foregroundColor(editFlag ? Color.c0x242527_30:Color.c0x242527)
                        .lineLimit(1)
                        .overlay(
                            Rectangle().frame(height: 2).offset(y: 4).foregroundColor(Color.clear)
                                      , alignment: .bottom)
                  
                    }
                   
                }
                    
            })
                    .padding(.leading,20)
                    .accessibility(addTraits: .isButton)
             .accessibilityIdentifier("Funcclients")
            Button(action: {
               // withAnimation(.easeInOut(duration: 1.0)) {
                if !self.editFlag && !self.showWaitRequest {
                    self.diableFlag = false
                    mDetail.getDateAndTimeResult = nil
                    mDetail.getDateAndTime()
                    if  self.funcPressed != 4
                    {
                        self.funcPressed = 4
                    }
                 
                    self.requestDate = true
                    self.blurAmount = 2

                }
                lastHoveredId = ""
              //  }
            }, label: {
                if self.funcPressed == 4 {
                    VStack(spacing:2) {
                       Text("Date And Time")
                            .frame(height: 35)
                            .font(.InterSemiBold14)
                            .foregroundColor(Color.c0x242527)
                            .lineLimit(1)
                            .overlay(
                                Rectangle().frame(height: 2).offset(y: -6).foregroundColor(Color.c0x005BC8)
                                          , alignment: .bottom)

                    }
                    
                }
                else {
                    VStack(spacing:2) {
                    Text("Date And Time")
                        .frame(height: 35)
                        .font(.InterRegular14)
                        .foregroundColor(editFlag ? Color.c0x242527_30:Color.c0x242527)
                        .lineLimit(1)
                        .overlay(
                            Rectangle().frame(height: 2).offset(y: 4).foregroundColor(Color.clear)
                                      , alignment: .bottom)
                  
                    }
                    
                }
                    
            })
                    .padding(.leading,20)
             .accessibilityIdentifier("dateandtime")
            
            Button(action: {
                if  !self.editFlag && !self.showWaitRequest{
                    self.diableFlag = true
                    if  self.funcPressed != 5
                    {
                        self.funcPressed = 5
                    }
                }
                lastHoveredId = ""
            }, label: {
                if self.funcPressed == 5 {
                    VStack(spacing:2) {
                       Text("Ping Test")
                            .frame(height: 35)
                            .font(.InterSemiBold14)
                            .foregroundColor(Color.c0x242527)
                            .lineLimit(1)
                            .overlay(
                                Rectangle().frame(height: 2).offset(y: -6).foregroundColor(Color.c0x005BC8)
                                          , alignment: .bottom)
                           
                    }
                    
                }
                else {
                    VStack(spacing:2) {
                    Text("Ping Test")
                        .frame(height: 35)
                        .font(.InterRegular14)
                        .foregroundColor(editFlag ? Color.c0x242527_30:Color.c0x242527)
                        .lineLimit(1)
                        .overlay(
                            Rectangle().frame(height: 2).offset(y: 4).foregroundColor(Color.clear)
                                      , alignment: .bottom)
                    
                    }
                   
                }
                    
            })
                    .padding(.leading,20)
             .accessibilityIdentifier("pingtest")
            
            Button(action: {
                if !self.editFlag && !self.showWaitRequest{
                    self.diableFlag = true
                    if  self.funcPressed != 6
                    {
                        self.funcPressed = 6
                    }
                }
                lastHoveredId = ""
            }, label: {
                if self.funcPressed == 6 {
                    VStack(spacing:2) {
                       Text("Traceroute Test")
                            .frame(height: 35)
                            .font(.InterSemiBold14)
                            .foregroundColor(Color.c0x242527)
                            .lineLimit(1)
                            .overlay(
                                Rectangle().frame(height: 2).offset(y: -6).foregroundColor(Color.c0x005BC8)
                                          , alignment: .bottom)

                    }
                    
                }
                else {
                    VStack(spacing:2) {
                    Text("Traceroute Test")
                        .frame(height: 35)
                        .font(.InterRegular14)
                        .foregroundColor(editFlag ? Color.c0x242527_30:Color.c0x242527)
                        .lineLimit(1)
                        .overlay(
                            Rectangle().frame(height: 2).offset(y: 4).foregroundColor(Color.clear)
                                      , alignment: .bottom)
                   
                    }
                    
                }
                    
            })
                    .padding(.leading,20)
                    .frame(width:150)
             .accessibilityIdentifier("traceroutetest")
            
        }
        .foregroundColor(Color.black.opacity(0.7))
        
        .frame(height: 35)
            }.padding(.top,5) //VStack
                .frame(height: 48)
            
        }
        .opacity(self.mAPnotFound ? 0.3:1.0)
        .disabled(self.mAPnotFound)
        .accessibilityIdentifier("Main")
        
        
    }
    @ViewBuilder
    func system(width:CGFloat)->some View {
        if funcPressed == 0 {
       // if editFlag {
            Text("System Information")
                    .frame(height: 30)
                    .font(.InterRegular18)
                    .foregroundColor(Color.c0x242527)
                    .padding(.leading,20)
                    
            
            GridStack(rows: 13, columns: 1) { row, col in
                if row == 0 {
                    Text("\(mDetail.system[row].name!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-50, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .topLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                   
                    Text("\(mDetail.system[row].value!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-50, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .topRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                }
                else if row == 1
                {
                    Text("\(mDetail.system[row].name!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-50, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomLeft)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                    ZStack{
                       // Text("\(Detail.system[row].value!)")
                        MarqueeSystemText(mDetail: mDetail, width: width/2-50)
                        .font(.InterMedium12)
                        .frame(width: width/2-50, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                       if self.editFlag {
                            TextField("\(mDetail.system[row].value!)",text:$value1, onEditingChanged: { (editingChanged) in
                                if editingChanged {
                                    
                                    self.lastHoveredId = mDetail.lasttag[row]
                                }
                            })
                            .frame(width: width/2-50, height: 35, alignment:.leading)
                            .background(Color.white)
                            .font(.InterMedium12)
                            .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[row], width: width/2-50, height: 35))
                            .onAppear {
                                value1 = "\(mDetail.system[row].value!)"
                            }
                            .accessibilityIdentifier("prodname")
//                            .onReceive(Just(Detail.system[row].value)) { _ in
//                                let isAlphanumerics = Detail.system[row].value!.rangeOfCharacter(from: nonAlphanumericCharacters) == nil
//                                if isAlphanumerics
//                                {
//                                    self.SSIDAlert = false
//                                }
//                                else
//                                {
//                                    self.SSIDAlert = true
//                                }
//                            }
                        }
                    }//ZStack
                }
                else
                {
                    Text("\(mDetail.system[row].name!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-50, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                   if row == 3
                   {
                           // Fallback on earlier versions
                          
                           Text("\(mDetail.system[3].value!)")
                               .font(.InterMedium12)
                               .frame(width: width/2-50, height: 40, alignment:.leading)
                               .padding(.horizontal, 15)
                               .foregroundColor(Color.c0x242527)
                               .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                               .onReceive(Just(mDetail.system[3].value)) { _ in
                                   if upadteSystime == false
                                   {
                                       upadteSystime = true
                                       DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                           mDetail.system[3].value = systemTimeString(inputDate: mDetail.system[3].value!)
                                           upadteSystime = false
                                       }
                                   }
                               }
                   }
                   else if row == 2
                   {
                       
                       Text("\(mDetail.system[2].value!)")
                           .font(.InterMedium12)
                           .frame(width: width/2-50, height: 40, alignment:.leading)
                           .padding(.horizontal, 15)
                           .foregroundColor(Color.c0x242527)
                           .overlay(
                               RoundedCorner(radius: 3,corners: .bottomRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                            )
                           .onReceive(Just(mDetail.system[2].value)) { _ in
                                if updateUptime == false {
                                   updateUptime = true
                                   DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                      mDetail.system[2].value = uptimeString(inputDate: mDetail.system[2].value!)
                                      updateUptime = false
                                   }
                               }
                           }
                   }
                   else if row == 9
                   {
                       MarqueeSystemDNSText(mDetail: mDetail, width: width/2-50)
                           .font(.InterMedium12)
                           .frame(width: width/2-50, height: 40, alignment:.leading)
                           .padding(.horizontal, 15)
                           .foregroundColor(Color.c0x242527)
                           .overlay(
                               RoundedCorner(radius: 3,corners: .bottomRight)
                                   .stroke(Color.c0x696A6B, lineWidth: 1)
                            )
                   }
                    else if row == 11
                    {
                        MarqueeSystemIPV6Text(mDetail: mDetail, width: width/2-50)
                            .font(.InterMedium12)
                            .frame(width: width/2-50, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                    else if row == 12
                    {
                        MarqueeSystemIPV6LinkText(mDetail: mDetail, width: width/2-50)
                            .font(.InterMedium12)
                            .frame(width: width/2-50, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                   else {
                       
                    Text("\(mDetail.system[row].value!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-50, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                    }
                }//else 
             }
             .padding(.leading,20)
             .padding(.top,5)
        }

    }
    @ViewBuilder
    func lAN(width:CGFloat)->some View {
        if funcPressed == 1 {
        Text("LAN-side IP Address")
                .frame(height: 30)
                .font(.InterRegular18)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
        
        HStack {
            Text("IP Address Assignment")
                .frame(width:width*2/3,height: 30,alignment: .leading)
                .font(.InterRegular14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
         
          //  GeometryReader { geometry in
            Button(action: {
                self.showLANIPType = true
               // posBandsTaggle = geometry.frame(in: .global).origin
            }) {
                HStack(spacing:15) {
                    Text(self.mLANIPType)
                        .frame(width:width*1/5, height: 25, alignment: .trailing)
                        .padding(.leading,5)
                        .padding(.trailing ,10)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .gesture(drag)
                        .background(Color.white)
                    if editFlag {
                        
                        Image("down")
                            .resizable()
                            .frame(width: 10, height: 6)
                            .rotationEffect(showLANIPType ?  .degrees(180):.degrees(0))
                            .disabled(true)
                            .background(Color.clear)
                            .padding(.trailing ,10)
                        
                    }
                }
                .gesture(drag)
            
            }
            .onTapGesture {
                self.showLANIPType = true
            }
            .frame(width:width*1/3, height: 40, alignment: .trailing)
            .disabled(!self.editFlag)
            .accessibilityIdentifier("laniptype")
        }
        .padding(.top,5)
        .onReceive(mDetail.$getLANResult) { result in
          //  NSLog("getLANResult : \(result)")
            if result != nil
            { //FromDHCPArray = ["User-Defined","From DHCP"]
                self.mLANIPType =  self.mLANIPTypeArray[(result?.data?.ipv4?.ipv4_type)!]
                self.mGateWayFromDHCP = self.mFromDHCPArray[(result?.data?.ipv4?.ipv4_default_gateway_type)!]
                self.mPriAddrFromDHCP = self.mFromDHCPArray[(result?.data?.ipv4?.ipv4_dns_primary_type)!]
                self.mSecAddrFromDHCP = self.mFromDHCPArray[(result?.data?.ipv4?.ipv4_dns_secondary_type)!]
                self.mLANIPTypeTaggle = (result?.data?.ipv4?.dhcp_enabled == 1 ? true:false)
            }
        }
        
        if self.editFlag {
            GridStack(rows: mDetail.mLANIPAddress.count, columns: 1) { row, col in
                if row == 0
                {
                    if self.mLANIPType == "DHCP"
                    {
                        Text("\(mDetail.mLANIPAddress[row].name!)")
                              .font(.InterMedium12)
                              .frame(width: width/2-30, height: 40, alignment:.leading)
                              .padding(.horizontal, 15)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                     

                     
                      Text("\(mDetail.mLANIPAddress[row].value!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius: 3,corners: .topRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                    }
                    else
                    {
                        Text("\(mDetail.mLANIPAddress[row].name!)")
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                                  .font(.InterMedium12)
                                  .foregroundColor(Color.c0x242527)
                                  .overlay(
                                      RoundedCorner(radius: 3,corners: .topLeft)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                   )
                        
                        ZStack{
                            TextField("\(mDetail.mLANIPAddress[row].value!)",text:$mDetail.mLANIPAddress[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                if editingChanged {
                                    self.lastHoveredId = mDetail.lasttag[row]
                                  
                                }
                                else
                                {
                                    let result = calculatePressed(subnetMask: mDetail.mLANIPAddress[1].value!, IPAddress: mDetail.mLANIPAddress[0].value!)
                                    mDetail.mLANDHCPServer[0].value = result[0]
                                    mDetail.mLANDHCPServer[1].value = result[1]
                                    
                                }
                            })
                                .frame(width: width/2-50, height: 35, alignment:.leading)
                                .background(Color.white)
                                .font(.InterMedium12)
                                .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[row], width: width/2-50, height: 35))
                                .keyboardType(.decimalPad)
                                .accessibilityIdentifier("ipaddress")
                               
                                
                        }.frame(width: width/2, height: 40, alignment:.center)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .topRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                }
                else if row == mDetail.mLANIPAddress.count-1
                {
                    if self.mLANIPType == "DHCP"
                    {
                        Text("\(mDetail.mLANIPAddress[row].name!)")
                            .frame(width: width/2-30, height: 80, alignment:.leading)
                            .padding(.horizontal, 15)
                                  .font(.InterMedium12)
                                  .foregroundColor(Color.c0x242527)
                                  .overlay(
                                      RoundedCorner(radius: 3,corners: .bottomLeft)
                                     .stroke(Color.c0x696A6B, lineWidth: 1)
                                   )
                       
                            ZStack(alignment:.top){
                                VStack(spacing:0) {
                                HStack(spacing:0) {
                                    Text(self.mGateWayFromDHCP)
                                        .frame(width:width*1/3 + 20, height: 35, alignment: .leading)
                                        .padding(.leading,0)
                                        .font(.InterMedium12)
                                        .foregroundColor(Color.c0x242527)
                                        .gesture(drag)

                                    Image("down")
                                        .resizable()
                                        .frame(width: 10, height: 6)
                                        .rotationEffect(mGateWayFromDHCPFlag ?  .degrees(180):.degrees(0))
                                        .disabled(true)
                                        .background(Color.clear)
                                        .padding(.trailing ,0)
                                }
                                .gesture(drag)
                                .onTapGesture {
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                    self.mGateWayFromDHCPFlag = true
                                    }
                                }
                                .accessibility(addTraits: .isButton)
                                .accessibilityIdentifier("GateWayflag")
                                if self.mGateWayFromDHCP == self.mFromDHCPArray[0]{
                                    TextField("\(mDetail.mLANIPAddress[row].value!)",text:$mDetail.mLANIPAddress[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                        if editingChanged {
                                            
                                            self.lastHoveredId = mDetail.lasttag[row]
                                        }
                                    })
                                        .frame(width: width/2-50, height: 35, alignment:.leading)
                                        .background(Color.white)
                                        .font(.InterMedium12)
                                        .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[row], width: width/2-50, height: 35))
                                        .keyboardType(.decimalPad)
                                        .accessibilityIdentifier("GateWayTypeTextDHCP")
                                }
                                else
                                {
                                    Text("\(mDetail.mLANIPAddress[row].value!)")
                                        .frame(width: width/2-50, height: 35, alignment:.leading)
                                        .background(Color.white)
                                        .foregroundColor(.black)
                                        .font(.InterMedium12)
                                        .padding(.leading,-20)
                                }
                                
                           }
                                
                        }.frame(width: width/2, height: 80, alignment:.center)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                    else
                    {
                        if self.mLANIPType == "DHCP"
                        {
                            Text("\(mDetail.mLANIPAddress[row].name!)")
                                  .font(.InterMedium12)
                                  .frame(width: width/2-30, height: 40, alignment:.leading)
                                  .padding(.horizontal, 15)
                                  .foregroundColor(Color.c0x242527)
                                  .overlay(
                                      RoundedCorner(radius: 0,corners: .bottomLeft)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                   )
                         

                         
                          Text("\(mDetail.mLANIPAddress[row].value!)")
                              .font(.InterMedium12)
                              .frame(width: width/2-30, height: 40, alignment:.leading)
                              .padding(.horizontal, 15)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 0,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                        }
                        else
                        {
                            Text("\(mDetail.mLANIPAddress[row].name!)")
                                .frame(width: width/2-30, height: 40, alignment:.leading)
                                .padding(.horizontal, 15)
                                      .font(.InterMedium12)
                                      .foregroundColor(Color.c0x242527)
                                      .overlay(
                                          RoundedCorner(radius: 3,corners: .bottomLeft)
                                            .stroke(Color.c0x696A6B, lineWidth: 1)
                                       )
                            
                            ZStack{
                                TextField("\(mDetail.mLANIPAddress[row].value!)",text:$mDetail.mLANIPAddress[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                    if editingChanged {
                                        
                                        self.lastHoveredId = mDetail.lasttag[row]
                                      
                                    }
                                })
                                    .frame(width: width/2-50, height: 35, alignment:.leading)
                                    .background(Color.white)
                                    .font(.InterMedium12)
                                    .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[row], width: width/2-50, height: 35))
                                    .keyboardType(.decimalPad)
                                    .accessibilityIdentifier("GateWayTypetext")
                                
                            }.frame(width: width/2, height: 40, alignment:.center)
                                .overlay(
                                    RoundedCorner(radius: 3,corners: .bottomRight)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                 )
                        }
                        
                    }
                   
                }
                else
                {
                    if self.mLANIPType == "DHCP"
                    {
                        Text("\(mDetail.mLANIPAddress[row].name!)")
                              .font(.InterMedium12)
                              .frame(width: width/2-30, height: 40, alignment:.leading)
                              .padding(.horizontal, 15)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 0,corners: .bottomLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                     

                     
                      Text("\(mDetail.mLANIPAddress[row].value!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius: 0,corners: .bottomRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                    }
                    else
                    {
                        Text("\(mDetail.mLANIPAddress[row].name!)")
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                                  .font(.InterMedium12)
                                  .foregroundColor(Color.c0x242527)
                                  .overlay(
                                      RoundedCorner(radius: 0,corners: .bottomRight)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                   )
                        
                        ZStack{
                            TextField("\(mDetail.mLANIPAddress[row].value!)",text:$mDetail.mLANIPAddress[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                if editingChanged {
                                    
                                    self.lastHoveredId = mDetail.lasttag[row]
                                    
                                }
                                else
                                {
                                    let result = calculatePressed(subnetMask: mDetail.mLANIPAddress[1].value!, IPAddress: mDetail.mLANIPAddress[0].value!)
                                    mDetail.mLANDHCPServer[0].value = result[0]
                                    mDetail.mLANDHCPServer[1].value = result[1]
                                   
                                }
                            })
                                .frame(width: width/2-50, height: 35, alignment:.leading)
                                .background(Color.white)
                                .font(.InterMedium12)
                                .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[row], width: width/2-50, height: 35))
                                .keyboardType(.decimalPad)
                                .accessibilityIdentifier("SubnetTypetext")
                                
                        }.frame(width: width/2, height: 40, alignment:.center)
                            .overlay(
                                RoundedCorner(radius: 0,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                    
                }
                            
             }
            .frame(width:width,alignment: .center)
            .padding(.leading,20)
            .padding(.top,10)
            
           
        }
        else
        {
            GridStack(rows: mDetail.mLANIPAddress.count, columns: 1) { row, col in
                if row == 0
                {
                    Text("\(mDetail.mLANIPAddress[row].name!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius: 3,corners: .topLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                 

                 
                  Text("\(mDetail.mLANIPAddress[row].value!)")
                      .font(.InterMedium12)
                      .frame(width: width/2-30, height: 40, alignment:.leading)
                      .padding(.horizontal, 15)
                      .foregroundColor(Color.c0x242527)
                      .overlay(
                          RoundedCorner(radius: 3,corners: .topRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                       )
                }
                else if row == mDetail.mLANIPAddress.count-1
                {
                    Text("\(mDetail.mLANIPAddress[row].name!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius: 3,corners: .bottomLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                 

                 
                  Text("\(mDetail.mLANIPAddress[row].value!)")
                      .font(.InterMedium12)
                      .frame(width: width/2-30, height: 40, alignment:.leading)
                      .padding(.horizontal, 15)
                      .foregroundColor(.black)
                      .overlay(
                          RoundedCorner(radius: 3,corners: .bottomRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                       )
                }
                else
                {
                    Text("\(mDetail.mLANIPAddress[row].name!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(.black)
                          .overlay(
                              RoundedCorner(radius: 0,corners: .bottomLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                 

                 
                  Text("\(mDetail.mLANIPAddress[row].value!)")
                      .font(.InterMedium12)
                      .frame(width: width/2-30, height: 40, alignment:.leading)
                      .padding(.horizontal, 15)
                      .foregroundColor(.black)
                      .overlay(
                          RoundedCorner(radius: 0,corners: .bottomRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                       )
                }
                  
                   
                    }
            .frame(width:width,alignment: .center)
            .padding(.leading,20)
                .padding(.top,10)
        }
        
        Text("LAN-side DNS Servers")
                .frame(height: 30)
                .font(.InterRegular14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
                .padding(.top,10)
        
        if self.editFlag {
            GridStack(rows: mDetail.mLANDNSServers.count, columns: 1) { row, col in
                
                if row == 0
                {
                    
                    if self.mLANIPType == "DHCP"
                    {
                        Text("\(mDetail.mLANDNSServers[row].name!)")
                            .frame(width: width/2-30, height: 80, alignment:.leading)
                            .padding(.horizontal, 15)
                                  .font(.InterMedium12)
                                  .foregroundColor(Color.c0x242527)
                                  .overlay(
                                      RoundedCorner(radius: 3,corners: .topLeft)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                   )
                        
                        ZStack(alignment:.top){
                            VStack(spacing:0) {
                            HStack(spacing:0) {
                                Text(self.mPriAddrFromDHCP)
                                    .frame(width:width*1/3 + 20, height: 35, alignment: .leading)
                                    .padding(.leading,0)
                                    .font(.InterMedium12)
                                    .foregroundColor(Color.c0x242527)
                                    .gesture(drag)

                                Image("down")
                                    .resizable()
                                    .frame(width: 10, height: 6)
                                    .rotationEffect(mPriAddrFromDHCPFlag ?  .degrees(180):.degrees(0))
                                    .disabled(true)
                                    .background(Color.clear)
                                    .padding(.trailing ,0)
                            }
                            .gesture(drag)
                            .onTapGesture {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                self.mPriAddrFromDHCPFlag = true
                                }
                            }
                            .accessibility(addTraits: .isButton)
                            .accessibilityIdentifier("PriAddrflag")
                                
                                if self.mPriAddrFromDHCP == self.mFromDHCPArray[0]{
                                    TextField("\(mDetail.mLANDNSServers[row].value!)",text:$mDetail.mLANDNSServers[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                        if editingChanged {
                                            
                                            self.lastHoveredId = mDetail.lasttag[row+overText[1]]
                                        }
                                    })
                                        .frame(width: width/2-50, height: 35, alignment:.leading)
                                        .background(Color.white)
                                        .font(.InterMedium12)
                                        .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[row+overText[1]], width: width/2-50, height: 35))
                                        .keyboardType(.decimalPad)
                                        .accessibilityIdentifier("PriAddTypeTextDHCP")
                                }
                                else
                                {
                                    Text("\(mDetail.mLANDNSServers[row].value!)")
                                        .frame(width: width/2-50, height: 35, alignment:.leading)
                                        .background(Color.white)
                                        .foregroundColor(Color.c0x242527)
                                        .font(.InterMedium12)
                                        .padding(.leading,-20)
                                }
                            
                            
                            }
                                
                        }.frame(width: width/2, height: 80, alignment:.center)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                    else
                    {
                        Text("\(mDetail.mLANDNSServers[row].name!)")
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                                  .font(.InterMedium12)
                                  .foregroundColor(Color.c0x242527)
                                  .overlay(
                                      RoundedCorner(radius: 3,corners: .topLeft)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                   )
                        
                        ZStack{
                            TextField("\(mDetail.mLANDNSServers[row].value!)",text:$mDetail.mLANDNSServers[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                if editingChanged {
                                    
                                    self.lastHoveredId = mDetail.lasttag[row+overText[1]]
                                }
                            })
                                .frame(width: width/2-50, height: 35, alignment:.leading)
                                .background(Color.white)
                                .font(.InterMedium12)
                                .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[row+overText[1]], width: width/2-50, height: 35))
                                .keyboardType(.decimalPad)
                                .accessibilityIdentifier("PriAddTypeText")
                            
                        }.frame(width: width/2, height: 40, alignment:.center)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .topRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                    
                    
                }
                else if row == mDetail.mLANDNSServers.count-1
                {
                    if self.mLANIPType == "DHCP"
                    {
                        Text("\(mDetail.mLANDNSServers[row].name!)")
                            .frame(width: width/2-30, height: 80, alignment:.leading)
                            .padding(.horizontal, 15)
                                  .font(.InterMedium12)
                                  .foregroundColor(Color.c0x242527)
                                  .overlay(
                                      RoundedCorner(radius:3 ,corners: .bottomLeft)
                                        .stroke(Color.c0x696A6B, lineWidth: 1)
                                   )
                        
                        ZStack(alignment:.top){
                            VStack(spacing:0) {
                            HStack(spacing:0) {
                                Text(self.mSecAddrFromDHCP)
                                    .frame(width:width*1/3 + 20, height: 35, alignment: .leading)
                                    .padding(.leading,0)
                                    .font(.InterMedium12)
                                    .foregroundColor(Color.c0x242527)
                                    .gesture(drag)

                                Image("down")
                                    .resizable()
                                    .frame(width: 10, height: 6)
                                    .rotationEffect(mSecAddrFromDHCPFlag ?  .degrees(180):.degrees(0))
                                    .disabled(true)
                                    .background(Color.clear)
                                    .padding(.trailing ,0)
                            }
                            .gesture(drag)
                            .onTapGesture {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                self.mSecAddrFromDHCPFlag = true
                                }
                            }
                            .accessibility(addTraits: .isButton)
                            .accessibilityIdentifier("SecAddrflag")
                           
                              
                            if self.mSecAddrFromDHCP == self.mFromDHCPArray[0]{
                                TextField("\(mDetail.mLANDNSServers[row].value!)",text:$mDetail.mLANDNSServers[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                    if editingChanged {
                                        
                                        self.lastHoveredId = mDetail.lasttag[row+overText[1]]
                                    }
                                })
                                    .frame(width: width/2-50, height: 35, alignment:.leading)
                                    .background(Color.white)
                                    .font(.InterMedium12)
                                    .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[row+overText[1]], width: width/2-50, height: 35))
                                    .keyboardType(.decimalPad)
                                    .accessibilityIdentifier("SecAddTypeTextDHCP")
                            }
                            else
                            {
                                Text("\(mDetail.mLANDNSServers[row].value!)")
                                    .frame(width: width/2-50, height: 35, alignment:.leading)
                                    .background(Color.white)
                                    .foregroundColor(Color.c0x242527)
                                    .font(.InterMedium12)
                                    .padding(.leading,-20)
                            }
                                
                            }
                                
                        }.frame(width: width/2, height: 80, alignment:.center)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                    else
                    {
                    Text("\(mDetail.mLANDNSServers[row].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius:3 ,corners: .bottomLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    
                    ZStack{
                        TextField("\(mDetail.mLANDNSServers[row].value!)",text:$mDetail.mLANDNSServers[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                            if editingChanged {
                                
                                self.lastHoveredId = mDetail.lasttag[row+overText[1]]
                            }
                        })
                            .frame(width: width/2-50, height: 35, alignment:.leading)
                            .background(Color.white)
                            .font(.InterMedium12)
                            .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[row+overText[1]], width: width/2-50, height: 35))
                            .keyboardType(.decimalPad)
                            .accessibilityIdentifier("SecAddTypeText")
                        
                    }.frame(width: width/2, height: 40, alignment:.center)
                        .overlay(
                            RoundedCorner(radius:3 ,corners: .bottomRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                    }
                }
                else
                {
                    Text("\(mDetail.mLANDNSServers[row].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius:0 ,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    
                    ZStack{
                        TextField("\(mDetail.mLANDNSServers[row].value!)",text:$mDetail.mLANDNSServers[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                            if editingChanged {
                                
                                self.lastHoveredId = mDetail.lasttag[row+overText[1]]
                            }
                        })
                            .frame(width: width/2-50, height: 35, alignment:.leading)
                            .background(Color.white)
                            .font(.InterMedium12)
                            .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[row+overText[1]], width: width/2-50, height: 35))
                            .keyboardType(.decimalPad)
                    }.frame(width: width/2, height: 40, alignment:.center)
                        .overlay(
                            RoundedCorner(radius:0 ,corners: .topLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                    
                }
                
                
                            
             }
            .frame(width:width,alignment: .center)
            .padding(.leading,20)
            .padding(.top,10)
            
           
        }
        else
        {
            GridStack(rows: mDetail.mLANDNSServers.count, columns: 1) { row, col in
                
                if row == 0
                {
                    Text("\(mDetail.mLANDNSServers[row].name!)")
                           .font(.InterMedium12)
                           .frame(width: width/2-30, height: 40, alignment:.leading)
                           .padding(.horizontal, 15)
                           .foregroundColor(Color.c0x242527)
                           .overlay(
                               RoundedCorner(radius:3 ,corners: .topLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                            )
                  

                  
                   Text("\(mDetail.mLANDNSServers[row].value!)")
                       .font(.InterMedium12)
                       .frame(width: width/2-30, height: 40, alignment:.leading)
                       .padding(.horizontal, 15)
                       .foregroundColor(Color.c0x242527)
                       .overlay(
                           RoundedCorner(radius:3 ,corners: .topRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                        )
                }
                else if row == mDetail.mLANDNSServers.count-1
                {
                    Text("\(mDetail.mLANDNSServers[row].name!)")
                           .font(.InterMedium12)
                           .frame(width: width/2-30, height: 40, alignment:.leading)
                           .padding(.horizontal, 15)
                           .foregroundColor(Color.c0x242527)
                           .overlay(
                               RoundedCorner(radius:3 ,corners: .bottomLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                            )
                  

                  
                   Text("\(mDetail.mLANDNSServers[row].value!)")
                       .font(.InterMedium12)
                       .frame(width: width/2-30, height: 40, alignment:.leading)
                       .padding(.horizontal, 15)
                       .foregroundColor(Color.c0x242527)
                       .overlay(
                           RoundedCorner(radius:3 ,corners: .bottomRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                        )
                }
                else
                {
                    Text("\(mDetail.mLANDNSServers[row].name!)")
                           .font(.InterMedium12)
                           .frame(width: width/2-30, height: 40, alignment:.leading)
                           .padding(.horizontal, 15)
                           .foregroundColor(Color.c0x242527)
                           .overlay(
                               RoundedCorner(radius:0 ,corners: .bottomRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                            )
                  

                  
                   Text("\(mDetail.mLANDNSServers[row].value!)")
                       .font(.InterMedium12)
                       .frame(width: width/2-30, height: 40, alignment:.leading)
                       .padding(.horizontal, 15)
                       .foregroundColor(Color.c0x242527)
                       .overlay(
                           RoundedCorner(radius:0 ,corners: .bottomRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                        )
                }
                
                
                
                   
             }
            .frame(width:width,alignment: .center)
            .padding(.leading,20)
                .padding(.top,10)
        }
        
        if self.mLANIPType != "DHCP" {
        
        Text("DHCP server")
                .frame(height: 30)
                .font(.InterRegular14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
                .padding(.top,10)
            
        Toggle(isOn: $mLANIPTypeTaggle)
        {
            Text("DHCP server")
        }
        .toggleStyle(CustomDefault1ToggleStyle(edit: $editFlag))
        .frame(width:width,alignment: .center)
        .padding(.leading,20)
        .onTapGesture {
            backPress.lANType[0] = self.mLANIPTypeTaggle
        }
        .disabled(!editFlag)
        .padding(.top,5)
        .accessibilityIdentifier("DHCPserver")
        
        if self.editFlag {
            GridStack(rows: mDetail.mLANDHCPServer.count, columns: 1) { row, col in
                
                if row == 0
                {
                    Text("\(mDetail.mLANDHCPServer[row].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius:3 ,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    
                    ZStack{
                      
                            TextField("\(mDetail.mLANDHCPServer[row].value!)",text:$mDetail.mLANDHCPServer[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                if editingChanged {
                                    
                                    self.lastHoveredId = mDetail.lasttag[row+overText[2]]
                                }
                            })
                                    .frame(width: width/2-50, height: 35, alignment:.leading)
                                    .background(Color.white)
                                    .font(.InterMedium12)
                                    .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[row+overText[2]], width: width/2-50, height: 35))
                                    .keyboardType(.decimalPad)
                                    .accessibilityIdentifier("startIP")
                        
                    }.frame(width: width/2, height: 40, alignment:.center)
                        .overlay(
                            RoundedCorner(radius:3 ,corners: .topRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                }
                else if row == mDetail.mLANDHCPServer.count-1
                {
                    Text("\(mDetail.mLANDHCPServer[row].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius:3 ,corners: .bottomLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    
                    ZStack{
                     
                            TextField("\(mDetail.mLANDHCPServer[row].value!)",text:$mDetail.mLANDHCPServer[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                if editingChanged {
                                    
                                    self.lastHoveredId = mDetail.lasttag[row+overText[2]]
                                }
                            })
                                    .frame(width: width/2-50, height: 35, alignment:.leading)
                                    .background(Color.white)
                                    .font(.InterMedium12)
                                    .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[row+overText[2]], width: width/2-50, height: 35))
                                    .keyboardType(.decimalPad)
                                    .accessibilityIdentifier("secIP")
                            
                    }.frame(width: width/2, height: 40, alignment:.center)
                        .overlay(
                            RoundedCorner(radius:3 ,corners: .bottomRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                }
                else
                {
                    Text("\(mDetail.mLANDHCPServer[row].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius:0 ,corners: .topRight)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    
                    VStack{
                      //  GeometryReader { geometry in
                        if mDetail.mLANDHCPServer[row].name == "Lease Time" {
                            HStack(spacing:0){
                            Text("\(mDetail.mLANDHCPServer[row].value!)")
                                    .frame(width: width/2-50, height: 35, alignment:.leading)
                                    .background(Color.white)
                                    .padding(.leading, 15)
                                    .font(.InterMedium12)
                                    .foregroundColor(Color.c0x242527)
                                    .gesture(drag)
                                
                                Image("down")
                                    .resizable()
                                    .frame(width: 10, height: 6)
                                    .rotationEffect(showLeaseTimeFlag ?  .degrees(180):.degrees(0))
                                    .disabled(true)
                                    .background(Color.clear)
                                    .padding(.trailing ,20)
                            }
                            .gesture(drag)
                            .onTapGesture {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                self.showLeaseTimeFlag = true
                                  //  posBandsTaggle = geometry.frame(in: .global).origin
                                }
                               
                            }
                            .accessibility(addTraits: .isButton)
                            .accessibilityIdentifier("DHCPleasetime")
                        }
                        else
                        {
                            TextField("\(mDetail.mLANDHCPServer[row].value!)",text:$mDetail.mLANDHCPServer[row].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                                if editingChanged {
                                    
                                    self.lastHoveredId = mDetail.lasttag[row+overText[2]]
                                }
                            })
                                    .frame(width: width/2-50, height: 35, alignment:.leading)
                                    .background(Color.white)
                                    .font(.InterMedium12)
                                    .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[row+overText[2]], width: width/2-50, height: 35))
                                    .keyboardType(mDetail.mLANDHCPServer[row].name == "Domain Name" ? .default:.decimalPad)
                                    .accessibilityIdentifier("LANDHCPServer\(row)")
                        }
 
                            
                    }.frame(width: width/2, height: 40, alignment:.center)
                        .overlay(
                            RoundedCorner(radius:0 ,corners: .topRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                }
                
                
            
                            
             }
            .frame(width:width,alignment: .center)
            .padding(.leading,20)
            .padding(.top,10)
        }
        else
        {
            GridStack(rows: mDetail.mLANDHCPServer.count, columns: 1) { row, col in
                if row == 0
                {
                    Text("\(mDetail.mLANDHCPServer[row].name!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius:3 ,corners: .topLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                 
                  Text("\(mDetail.mLANDHCPServer[row].value!)")
                      .font(.InterMedium12)
                      .frame(width: width/2-30, height: 40, alignment:.leading)
                      .padding(.horizontal, 15)
                      .foregroundColor(Color.c0x242527)
                      .overlay(
                          RoundedCorner(radius:3 ,corners: .topRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                       )
                }
                else if row == mDetail.mLANDHCPServer.count-1
                {
                    Text("\(mDetail.mLANDHCPServer[row].name!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius:3 ,corners: .bottomLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                 

                 
                  Text("\(mDetail.mLANDHCPServer[row].value!)")
                      .font(.InterMedium12)
                      .frame(width: width/2-30, height: 40, alignment:.leading)
                      .padding(.horizontal, 15)
                      .foregroundColor(Color.c0x242527)
                      .overlay(
                          RoundedCorner(radius:3 ,corners: .bottomRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                       )
                }
                else
                {
                    Text("\(mDetail.mLANDHCPServer[row].name!)")
                          .font(.InterMedium12)
                          .frame(width: width/2-30, height: 40, alignment:.leading)
                          .padding(.horizontal, 15)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius:0 ,corners: .topRight)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                 

                 
                  Text("\(mDetail.mLANDHCPServer[row].value!)")
                      .font(.InterMedium12)
                      .frame(width: width/2-30, height: 40, alignment:.leading)
                      .padding(.horizontal, 15)
                      .foregroundColor(Color.c0x242527)
                      .overlay(
                          RoundedCorner(radius:0 ,corners: .topRight)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                       )
                }
                   
            }
            .frame(width:width,alignment: .center)
            .padding(.leading,20)
                .padding(.top,10)
        }
        }
        }
    }
    @ViewBuilder
    func wirelessBands(width:CGFloat)->some View { //420E 840E support 6G
        if funcPressed == 2 {
        Text("Wireless Bands")
                .frame(height: 30)
                .font(.InterRegular18)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
         
        Toggle(isOn: $mWireless24G)
        {
            Text("2.4Ghz")
        }
        .toggleStyle(CustomWiBandsToggleStyle(edit: $editFlag))
        .frame(width:width,alignment: .center)
        .padding(.leading,20)
        .padding(.top,5)
        .onTapGesture {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            backPress.wirelessType[0] = self.mWireless24G
            }
        }
        .disabled(!editFlag)
        .gesture(drag)
        .accessibilityIdentifier("Bands2G")
        
        HStack {
            Text("Enable SSID number")
                .frame(width:width*2/3,height: 25,alignment: .leading)
                .font(.InterRegular14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
         
            GeometryReader { geometry in
            Button(action: {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showBandsTaggle24G.toggle()
               // posBandsTaggle = geometry.frame(in: .global).origin
                }
            }) {
                    
                HStack(spacing:0) {
                    Text(self.mWiFiBands24G)
                        .frame(width:width*1/10, height: 25, alignment: .trailing)
                        .padding(.trailing ,10)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .gesture(drag)
                    if editFlag {

                        Image("down")
                            .resizable()
                            .frame(width: 10, height: 6)
                            .rotationEffect(showBandsTaggle24G ?  .degrees(180):.degrees(0))
                            .disabled(true)
                            .background(Color.clear)
                            .padding(.trailing ,10)
                    }
                }
                .gesture(drag)
               
            }
            .gesture(drag)
            .frame(width:width*1/3, height: 25, alignment: .trailing)
            .disabled(!self.editFlag)
            }
            .gesture(drag)
            .onReceive(mDetail.$get24GBandsResult) { result in
                if result != nil && result != mDetail.get24GBandsResult
                {
                    NSLog("get24GSSIDSecurity")
                    if result?.data?.enabled_ssidNum != nil {
                        self.mWiFiBands24G = (result?.data?.enabled_ssidNum)!.description
                        if ((result?.data?.radio_enable) != nil) {
                            self.mWireless24G = ((result?.data?.radio_enable) == 1 ? true:false)
                        }
                        
//                        let myDataQueue = DispatchQueue(label: "DataQueue",
//                                                        qos: .userInitiated,
//                                                        attributes: .concurrent,
//                                                        autoreleaseFrequency: .workItem,
//                                                        target: nil)
//                        myDataQueue.async(flags: .barrier) {
//                        
//                        for iuuid in 0..<(result?.data?.enabled_ssidNum)! {
//                           // DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [unowned self] in
//                            DispatchQueue.main.async {
//                            Detail.get24GSSIDSecurity(ssid_index:"\(i)")
//                            }
//                          //  }
//                        }
//                        }
                    }
                }
                
            }
            .accessibility(addTraits: .isButton)
            .accessibilityIdentifier("SSID2GNUM")
        }
        .padding(.top,5)
        .gesture(drag)
        .onReceive(mDetail.$get24GSSIDSecurityResult) { _ in
        }
        LazyVStack{
        wirelessDetail(detail: mDetail.wireless24G,bands:Int(self.mWiFiBands24G)!,width: width,flag: true,overText:overText[0])
        }
                
        Toggle(isOn: $mWireless5G)
        {
            Text("5Ghz")
        }
        .toggleStyle(CustomWiBandsToggleStyle(edit: $editFlag))
        .frame(width:width,alignment: .center)
        .padding(.leading,20)
        .padding(.top,15)
        .onTapGesture {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            backPress.wirelessType[1] = self.mWireless5G
            }
        }
        .disabled(!editFlag)
        .gesture(drag)
        .accessibilityIdentifier("Bands5G")
        
        HStack {
            Text("Enable SSID number")
                .frame(width:width*2/3,height: 25,alignment: .leading)
                .font(.InterRegular14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
            
            GeometryReader { geometry in
            Button(action: {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showBandsTaggle5G.toggle()
               // posBandsTaggle = geometry.frame(in: .global).origin
                }
            }) {
                    
                HStack(spacing:5) {
                    Text(self.mWiFiBands5G)
                        .frame(width:width*1/10, height: 25, alignment: .trailing)
                        .padding(.trailing ,10)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .gesture(drag)
                    if editFlag {
                        Image("down")
                            .resizable()
                            .frame(width: 10, height: 6)
                            .rotationEffect(showBandsTaggle5G ?  .degrees(180):.degrees(0))
                            .disabled(true)
                            .background(Color.clear)
                            .padding(.trailing ,10)
                    }
                }
                .gesture(drag)
               
            }
            .gesture(drag)
            .frame(width:width*1/3, height: 25, alignment: .trailing)
            .disabled(!self.editFlag)
          }
            .gesture(drag)
            .onReceive(mDetail.$get5GBandsResult) { result in
                if result != nil && result != mDetail.get5GBandsResult
                {
                    NSLog("get5GSSIDSecurity")
                    if result?.data?.enabled_ssidNum != nil {
                        self.mWiFiBands5G = (result?.data?.enabled_ssidNum)!.description
                        if ((result?.data?.radio_enable) != nil) {
                            self.mWireless5G = ((result?.data?.radio_enable) == 1 ? true:false)
                        }
                        
//                        for iuuid in 0..<(result?.data?.enabled_ssidNum)! {
//                           // DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [unowned self] in
//                            DispatchQueue.main.async {
//                            Detail.get5GSSIDSecurity(ssid_index:"\(i)")
//                            }
//                          //  }
//                        }
                    }
                }
                
            }
            .accessibility(addTraits: .isButton)
            .accessibilityIdentifier("SSID5GNUM")
        }
        .padding(.top,5)
        .gesture(drag)
        .onReceive(mDetail.$get5GSSIDSecurityResult) { _ in
            
        }
        LazyVStack{
        wirelessDetail5G(detail: mDetail.wireless5G,bands:Int(self.mWiFiBands5G)!,width: width,flag: true,overText:overText[1])
        }
        if support6G.contains(detail.model!)
        {
        Toggle(isOn: $mWireless6G)
        {
            Text("6Ghz")
        }
        .toggleStyle(CustomWiBandsToggleStyle(edit: $editFlag))
        .frame(width:width,alignment: .center)
        .padding(.leading,20)
        .padding(.top,15)
        .onTapGesture {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            backPress.wirelessType[2] = self.mWireless6G
            }
        }
        .disabled(!self.editFlag)
        .gesture(drag)
        .accessibilityIdentifier("Bands6G")
        
        HStack {
            Text("Enable SSID number")
                .frame(width:width*2/3,height: 25,alignment: .leading)
                .font(.InterRegular14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
            GeometryReader {geometry in
            Button(action: {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showBandsTaggle6G.toggle()
                //posBandsTaggle = geometry.frame(in: .global).origin
                }
            }) {
                    
                HStack(spacing:5) {
                    Text(self.mWiFiBands6G)
                        .frame(width:width*1/10, height: 25, alignment: .trailing)
                        .padding(.trailing ,10)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .gesture(drag)
                    if editFlag {
                        Image("down")
                            .resizable()
                            .frame(width: 10, height: 6)
                            .rotationEffect(showBandsTaggle6G ?  .degrees(180):.degrees(0))
                            .disabled(true)
                            .background(Color.clear)
                            .padding(.trailing ,10)
                    }
                }
                .gesture(drag)
               
            }
            .gesture(drag)
            .frame(width:width*1/3, height: 25, alignment: .trailing)
            .disabled(!editFlag)
            }
            .gesture(drag)
            .onReceive(mDetail.$get6GBandsResult) { result in
                if result != nil && result != mDetail.get6GBandsResult
                {
                    NSLog("get6GSSIDSecurity")
                    if result?.data?.enabled_ssidNum != nil {
                        self.mWiFiBands6G = (result?.data?.enabled_ssidNum)!.description
                        if ((result?.data?.radio_enable) != nil) {
                            self.mWireless6G = ((result?.data?.radio_enable) == 1 ? true:false)
                        }
//                        for iuuid in 0..<(result?.data?.enabled_ssidNum)! {
//                           // DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [unowned self] in
//                            DispatchQueue.main.async {
//                            Detail.get6GSSIDSecurity(ssid_index:"\(i)")
//                            }
//                          //  }
//                        }
                    }
                }
                
            }
            .accessibility(addTraits: .isButton)
            .accessibilityIdentifier("SSID6GNUM")
        }
        .padding(.top,5)
        .gesture(drag)
        .onReceive(mDetail.$get5GSSIDSecurityResult) { _ in
        }
        LazyVStack{
        wirelessDetail6G(detail: mDetail.wireless6G,bands:Int(self.mWiFiBands6G)!,width: width,flag: true,overText:overText[2])
        }
        }
        }
    }
    @ViewBuilder
    func edit24GAuth(width:CGFloat,lable:Int,iuuid:Int)->some View{
        if funcPressed == 2 {
        HStack(spacing:0){
        Text(mDetail.wireless24G[lable].value!)
            .frame(width: width/2-50, height: 35, alignment:.leading)
            .background(iuuid%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
            .padding(.leading, 15)
            .font(.InterMedium12)
            .foregroundColor(Color.c0x242527)
            .clipped()
            .gesture(drag)
            
            Image("down")
                .resizable()
                .frame(width: 10, height: 6)
                .rotationEffect(showAuthType24GFlag ?  .degrees(180):.degrees(0))
                .disabled(true)
                .background(Color.clear)
                .padding(.trailing ,20)
        }
        .gesture(drag)
        .frame(width:width/2,height:40)
        .overlay(
            RoundedCorner(radius: 0,corners: .topLeft)
                .stroke(Color.c0x696A6B, lineWidth: 1)
         )
        .onTapGesture {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showAuthType24GFlag = true
                self.mAuthROW = lable
                if let idx = mAuthArray.firstIndex(where: { $0 == mDetail.wireless24G[lable].value! }) {
                    mAuthArraySelected = idx
                }
                else
                {
                    mAuthArraySelected = 0
                }
            }

        }
        .accessibility(addTraits: .isButton)
        .accessibilityIdentifier("SSID2GAuth\(lable)")
        }
    }
    @ViewBuilder
    func edit24G(width:CGFloat,lable:Int,overText:Int,iuuid:Int)->some View {
        if funcPressed == 2 {
        HStack(spacing:0) {
                TextField("\(mDetail.wireless24G[lable].value!)",text:$mDetail.wireless24G[lable].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                    if editingChanged {
                        self.lastHoveredId = mDetail.lasttag[lable + overText]
                    }
                   })
                    .frame(width: width/2-30, height: 35, alignment:.leading)
                    .background(iuuid%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
                    .font(.InterMedium12)
                    .textFieldStyle(MyWiBandsTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[lable + overText], width: width/2-30, height: 35))
                    .keyboardType(.default)
                    .accessibilityIdentifier("psk2G\(lable)")
                    .onReceive(Just(mDetail.wireless24G[lable].value)) { _ in
                        if self.lastHoveredId == mDetail.lasttag[lable + overText]{
                        if mDetail.wireless24G[lable].name == "Pre-shared Key"
                        {
                            if mDetail.wireless24G[lable - 1].value == "None" || mDetail.wireless24G[lable - 1].value == "WEP" || mDetail.wireless24G[lable - 1].value == "OWE"
                            {
                                return
                            }
                            else{
                            if mDetail.wireless24G[lable].value!.count >= mWPAMin && mDetail.wireless24G[lable].value!.count <= mWPAMax
                            {
                                let reg = "[^\\x00-\\xff]"
                                let rege = try? NSRegularExpression(pattern: reg, options: .caseInsensitive)
                                let matchs = rege!.matches(in: mDetail.wireless24G[lable].value!, options: [], range: NSRange(location: 0, length: mDetail.wireless24G[lable].value!.count))
                                if matchs.isEmpty {
                                    self.pskAlert = false
                                }
                                else {
                                    self.pskAlert = true
                                }
//                                let isAlphanumerics = Detail.wireless24G[lable].value!.rangeOfCharacter(from: nonAlphanumericCharacters) == nil
//                                if isAlphanumerics
//                                {
//                                    self.pskAlert = false
//                                }
//                                else
//                                {
//                                    self.pskAlert = true
//                                }
                            }
                            else
                            {
                                self.pskAlert = true
                            }
                            }
                        }
                        else if  mDetail.wireless24G[lable].name == "SSID"
                        {
                            if mDetail.wireless24G[lable].value!.count >= mSSIDMin && mDetail.wireless24G[lable].value!.count <= mSSIDMax
                            {
                                 let reg = "[^\\x00-\\xff]"
                                 let rege = try? NSRegularExpression(pattern: reg, options: .caseInsensitive)
                                 let matchs = rege!.matches(in: mDetail.wireless24G[lable].value!, options: [], range: NSRange(location: 0, length: mDetail.wireless24G[lable].value!.count))
                                 if matchs.isEmpty {
                                     self.mSSIDAlert = false
                                 }
                                 else {
                                     self.mSSIDAlert = true
                                 }
                            }
                            else
                            {
                                self.mSSIDAlert = true
                            }
                        }
                        }
                    }
        }
        .frame(width:(CGFloat)(width/2),height:40)
        .overlay(
            RoundedCorner(radius: 0,corners: .topLeft)
                .stroke(Color.c0x696A6B, lineWidth: 1)
         )
        }
    }
    @ViewBuilder
    func wirelessDetail(detail:[WirelessBody],bands:Int,width:CGFloat,flag:Bool,overText:Int)->some View{
        if funcPressed == 2 {
        ForEach(0..<bands, id:\.self) { iuuid in
                  
            GridStack(rows: 4, columns: 1) { row, col in
                if editFlag {
                    if row == 0 {
                        Text("\(detail[row+iuuid*4].name!)\(iuuid+1)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    else
                    {
                        Text("\(detail[row+iuuid*4].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    
                    if detail[row+iuuid*4].name == "Authentication Type" {
                        let lable = row + iuuid * 4
                        edit24GAuth(width: width, lable: lable,iuuid:iuuid)
                    }
                    else if detail[row+iuuid*4].name == "Pre-shared Key" && ( mDetail.wireless24G[row+iuuid*4 - 1].value == "None" || mDetail.wireless24G[row+iuuid*4 - 1].value == "WEP" || mDetail.wireless24G[row+iuuid*4 - 1].value == "OWE" )
                    {
                        Text("")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                              .clipped()
                       
                    }
                    else
                    {
                        let lable = row + iuuid * 4
                        edit24G(width: width, lable: lable,overText: overText,iuuid:iuuid)
                    }
                }
                else
                {
                    if row == 0 {
                        Text("\(detail[row+iuuid*4].name!)\(iuuid+1)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    else
                    {
                        Text("\(detail[row+iuuid*4].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    
                    if detail[row+iuuid*4].name == "Authentication Type" || detail[row+iuuid*4].name == "SSID"  {
                        
                        //MarqueeWiBands24GText(text: Detail.wireless24G[row+i*4].value!)
                       // Text("\(Detail.wireless24G[row+i*4].value!)")
                        MarqueeWiBands24GText(mDetail: mDetail, index: row+iuuid*4, width:width/2-30)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .background(iuuid%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
                            .padding(.horizontal, 15)
                            .font(.InterMedium12)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 0,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)

                         )
                    }
                    else{
                    Text("\(mDetail.wireless24G[row+iuuid*4].value!)")
                    .frame(width: width/2-30, height: 40, alignment:.leading)
                    .padding(.horizontal, 15)
                          .font(.InterMedium12)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius: 3,corners: .topLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                          .clipped()
                    }
                }
            }
            .font(.InterMedium12)
            .background(iuuid%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
            .foregroundColor(Color.c0x242527)
            .cornerRadius(3)
            .padding(.top,12)
        }
        }
    }
    @ViewBuilder
    func edit5GAuth(width:CGFloat,lable:Int,iuuid:Int)->some View{
        if funcPressed == 2 {
        HStack(spacing:0){
        Text(mDetail.wireless5G[lable].value!)
            .frame(width: width/2-50, height: 35, alignment:.leading)
            .background(iuuid%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
            .padding(.leading, 15)
            .font(.InterMedium12)
            .foregroundColor(Color.c0x242527)
            .clipped()
            .gesture(drag)
            
            Image("down")
                .resizable()
                .frame(width: 10, height: 6)
                .rotationEffect(showAuthType5GFlag ?  .degrees(180):.degrees(0))
                .disabled(true)
                .background(Color.clear)
                .padding(.trailing ,20)
        }
        .gesture(drag)
        .frame(width:width/2,height:40)
        .overlay(
            RoundedCorner(radius: 0,corners: .topLeft)
                .stroke(Color.c0x696A6B, lineWidth: 1)
         )
        .onTapGesture {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showAuthType5GFlag = true
                self.mAuthROW = lable
               // print("tap:\(self.AuthROW)")
                if let idx = mAuthArray.firstIndex(where: { $0 == mDetail.wireless5G[lable].value! }) {
                    mAuthArraySelected = idx
                }
                else
                {
                    mAuthArraySelected = 0
                }
            }

        }
        .accessibility(addTraits: .isButton)
        .accessibilityIdentifier("SSID5GAuth\(lable)")
        }
    }
    @ViewBuilder
    func edit5G(width:CGFloat,lable:Int,overText:Int,iuuid:Int)->some View {
        if funcPressed == 2 {
        HStack(spacing:0) {
           
                TextField("\(mDetail.wireless5G[lable].value!)",text:$mDetail.wireless5G[lable].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                    if editingChanged {
                        
                        self.lastHoveredId = mDetail.lasttag[lable + overText]
                       
                    }
                   })
                .frame(width: CGFloat(width/2)-30, height: 35, alignment:.leading)
                    .background(iuuid%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
                    .font(.InterMedium12)
                    .textFieldStyle(MyWiBandsTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[lable + overText], width: width/2-30, height: 35))
                    .keyboardType(.default)
                    .accessibilityIdentifier("psk5G\(lable)")
                    .onReceive(Just(mDetail.wireless5G[lable].value)) { _ in
                        if self.lastHoveredId == mDetail.lasttag[lable + overText]{
                        if mDetail.wireless5G[lable].name == "Pre-shared Key"
                        {
                            if mDetail.wireless5G[lable - 1].value == "None" || mDetail.wireless5G[lable - 1].value == "WEP" || mDetail.wireless5G[lable - 1].value == "OWE"
                            {
                                return
                            }
                            if mDetail.wireless5G[lable].value!.count >= mWPAMin && mDetail.wireless5G[lable].value!.count <= mWPAMax
                            {
                                let reg = "[^\\x00-\\xff]"
                                let rege = try? NSRegularExpression(pattern: reg, options: .caseInsensitive)
                                let matchs = rege!.matches(in: mDetail.wireless5G[lable].value!, options: [], range: NSRange(location: 0, length: mDetail.wireless5G[lable].value!.count))
                                if matchs.isEmpty {
                                    self.pskAlert = false
                                }
                                else {
                                    self.pskAlert = true
                                }
//                                let isAlphanumerics = Detail.wireless5G[lable].value!.rangeOfCharacter(from: nonAlphanumericCharacters) == nil
//                                if isAlphanumerics
//                                {
//                                    self.pskAlert = false
//                                }
//                                else
//                                {
//                                    self.pskAlert = true
//                                }
                            }
                            else
                            {
                                self.pskAlert = true
                            }
                        }
                        else if  mDetail.wireless5G[lable].name == "SSID"
                        {
                            if mDetail.wireless5G[lable].value!.count >= mSSIDMin && mDetail.wireless5G[lable].value!.count <= mSSIDMax
                            {
                                let reg = "[^\\x00-\\xff]"
                                let rege = try? NSRegularExpression(pattern: reg, options: .caseInsensitive)
                                let matchs = rege!.matches(in: mDetail.wireless5G[lable].value!, options: [], range: NSRange(location: 0, length: mDetail.wireless5G[lable].value!.count))
                                if matchs.isEmpty {
                                    self.mSSIDAlert = false
                                }
                                else {
                                    self.mSSIDAlert = true
                                }
                            }
                            else
                            {
                                self.mSSIDAlert = true
                            }
                        }
                        }
                    }
           
        }
        .frame(width:(CGFloat)(width/2),height:40)
        .overlay(
            RoundedCorner(radius: 0,corners: .topLeft)
                .stroke(Color.c0x696A6B, lineWidth: 1)
         )
        }
    }
    @ViewBuilder
    func wirelessDetail5G(detail:[WirelessBody],bands:Int,width:CGFloat,flag:Bool,overText:Int)->some View{
        if funcPressed == 2 {
        ForEach(0..<bands, id:\.self) { iuuid in
        
            GridStack(rows: 4, columns: 1) { row, col in
                if editFlag {
                    if row == 0 {
                        Text("\(detail[row+iuuid*4].name!)\(iuuid+1)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    else
                    {
                        Text("\(detail[row+iuuid*4].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                           
                    
                    if detail[row+iuuid*4].name == "Authentication Type" {
                        let lable = row+iuuid*4
                        edit5GAuth(width: width, lable: lable,iuuid:iuuid)
                    }
                    else if mDetail.wireless5G[row+iuuid*4].name == "Pre-shared Key" && ( mDetail.wireless5G[row+iuuid*4 - 1].value == "None" || mDetail.wireless5G[row+iuuid*4 - 1].value == "WEP" || mDetail.wireless5G[row+iuuid*4 - 1].value == "OWE" )
                    {
                       
                        Text("")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                              .clipped()
                       
                    }
                    else
                    {
                        let lable = row + iuuid * 4
                        edit5G(width:width,lable:lable,overText:overText,iuuid:iuuid)
                    }
                }
                else
                {
                    if row == 0 {
                        Text("\(detail[row+iuuid*4].name!)\(iuuid+1)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    else
                    {
                        Text("\(detail[row+iuuid*4].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    
                    if detail[row+iuuid*4].name == "Authentication Type" || detail[row+iuuid*4].name == "SSID" {
                       
                        MarqueeWiBands5GText(mDetail: mDetail, index: row+iuuid*4, width:width/2-30)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .background(iuuid%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
                            .padding(.horizontal, 15)
                            .font(.InterMedium12)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 0,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                                   
                         )
                        
                    }
                    else{
                    Text("\(mDetail.wireless5G[row+iuuid*4].value!)")
                    .frame(width: width/2-30, height: 40, alignment:.leading)
                    .padding(.horizontal, 15)
                          .font(.InterMedium12)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius: 3,corners: .topLeft)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                    }
                }
            }
            .font(.InterMedium12)
            .foregroundColor(Color.c0x242527)
            .background(iuuid%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
            .cornerRadius(3)
            .padding(.top,12)

        }
        }
    }
    @ViewBuilder
    func edit6GAuth(width:CGFloat,lable:Int,iuuid:Int)->some View {
        if funcPressed == 2 {
        HStack(spacing:0){
        Text(mDetail.wireless6G[lable].value!)
            .frame(width: width/2-50, height: 35, alignment:.leading)
            .background(iuuid%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
            .padding(.leading, 15)
            .font(.InterMedium12)
            .foregroundColor(Color.c0x242527)
            .clipped()
            .gesture(drag)

            Image("down")
                .resizable()
                .frame(width: 10, height: 6)
                .rotationEffect(showAuthType6GFlag ?  .degrees(180):.degrees(0))
                .disabled(true)
                .background(Color.clear)
                .padding(.trailing ,20)
        }
        .gesture(drag)
        .frame(width:width/2,height:40)
        .overlay(
            RoundedCorner(radius: 0,corners: .topLeft)
           .stroke(Color.c0x696A6B, lineWidth: 1)
         )
        .onTapGesture {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showAuthType6GFlag = true
                self.mAuthROW = lable
               // print("tap:\(self.AuthROW)")
                if let idx = mAuth6GArray.firstIndex(where: { $0 == mDetail.wireless6G[lable].value! }) {
                    mAuthArraySelected = idx
                }
                else
                {
                    mAuthArraySelected = 0
                }
            }

        }
        .accessibility(addTraits: .isButton)
        .accessibilityIdentifier("SSID6GAuth\(lable)")
        }
    }
    @ViewBuilder
    func edit6G(width:CGFloat,lable:Int,overText:Int,iuuid:Int)->some View {
        if funcPressed == 2 {
        HStack(spacing:0) {
                TextField("\(mDetail.wireless6G[lable].value!)",text:$mDetail.wireless6G[lable].value.toUnwrapped(defaultValue: ""), onEditingChanged: { (editingChanged) in
                    if editingChanged {
                        
                        self.lastHoveredId = mDetail.lasttag[lable + overText]
                    }
                   })
                    .frame(width: width/2-30, height: 35, alignment:.leading)
                    .background(iuuid%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
                    .font(.InterMedium12)
                    .textFieldStyle(MyWiBandsTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[lable + overText], width: width/2-30, height: 35))
                    .keyboardType(.default)
                    .accessibilityIdentifier("psk6G\(lable)")
                    .onReceive(Just(mDetail.wireless6G[lable].value)) { _ in
                        if self.lastHoveredId == mDetail.lasttag[lable + overText]{
                        if mDetail.wireless6G[lable].name == "Pre-shared Key"
                        {
                            if mDetail.wireless6G[lable - 1].value == "None" || mDetail.wireless6G[lable - 1].value == "WEP" || mDetail.wireless6G[lable - 1].value == "OWE"
                            {
                                return
                            }
                            if mDetail.wireless6G[lable].value!.count >= mWPAMin && mDetail.wireless6G[lable].value!.count <= mWPAMax
                            {
                                let reg = "[^\\x00-\\xff]"
                                let rege = try? NSRegularExpression(pattern: reg, options: .caseInsensitive)
                                let matchs = rege!.matches(in: mDetail.wireless6G[lable].value!, options: [], range: NSRange(location: 0, length: mDetail.wireless6G[lable].value!.count))
                                if matchs.isEmpty {
                                    self.pskAlert = false
                                }
                                else {
                                    self.pskAlert = true
                                }
//                                let isAlphanumerics = Detail.wireless6G[lable].value!.rangeOfCharacter(from: nonAlphanumericCharacters) == nil
//                                if isAlphanumerics
//                                {
//                                    self.pskAlert = false
//                                }
//                                else
//                                {
//                                    self.pskAlert = true
//                                }
                            }
                            else
                            {
                                self.pskAlert = true
                            }
                        }
                        else if  mDetail.wireless6G[lable].name == "SSID"
                        {
                            if mDetail.wireless6G[lable].value!.count >= mSSIDMin && mDetail.wireless6G[lable].value!.count <= mSSIDMax
                            {
                                let reg = "[^\\x00-\\xff]"
                                let rege = try? NSRegularExpression(pattern: reg, options: .caseInsensitive)
                                let matchs = rege!.matches(in: mDetail.wireless6G[lable].value!, options: [], range: NSRange(location: 0, length: mDetail.wireless6G[lable].value!.count))
                                if matchs.isEmpty {
                                    self.mSSIDAlert = false
                                }
                                else {
                                    self.mSSIDAlert = true
                                }
                            }
                            else
                            {
                                self.mSSIDAlert = true
                            }
                        }
                        }
                    }
            
        }
        .frame(width:(CGFloat)(width/2),height:40)
        .overlay(
            RoundedCorner(radius: 0,corners: .topLeft)
           .stroke(Color.c0x696A6B, lineWidth: 1)
         )
        }
    }
    @ViewBuilder
    func wirelessDetail6G(detail:[WirelessBody],bands:Int,width:CGFloat,flag:Bool,overText:Int)->some View {
        if funcPressed == 2 {
        ForEach(0..<bands, id:\.self) { iuuid in
           
            GridStack(rows: 4, columns: 1) { row, col in
                if editFlag {
                    if row == 0 {
                        Text("\(detail[row+iuuid*4].name!)\(iuuid+1)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                 .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    else
                    {
                        Text("\(detail[row+iuuid*4].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                 .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    
                    if detail[row+iuuid*4].name == "Authentication Type" {
                        let lable = row + iuuid * 4
                        edit6GAuth(width: width, lable: lable,iuuid:iuuid)
                    }
                    else if detail[row+iuuid*4].name == "Pre-shared Key" && ( mDetail.wireless6G[row+iuuid*4 - 1].value == "None" || mDetail.wireless6G[row+iuuid*4 - 1].value == "WEP" || mDetail.wireless6G[row+iuuid*4 - 1].value == "OWE" )
                       
                    {
                        Text("")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                              .clipped()
                        
                    }
                    else
                    {
                        let lable = row + iuuid * 4
                        edit6G(width: width, lable: lable, overText: overText,iuuid:iuuid)
                    }
                }
                else
                {
                    if row == 0 {
                        Text("\(detail[row+iuuid*4].name!)\(iuuid+1)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                 .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    else
                    {
                        Text("\(detail[row+iuuid*4].name!)")
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                              .font(.InterMedium12)
                              .foregroundColor(Color.c0x242527)
                              .overlay(
                                  RoundedCorner(radius: 3,corners: .topLeft)
                                 .stroke(Color.c0x696A6B, lineWidth: 1)
                               )
                    }
                    
                    if detail[row+iuuid*4].name == "Authentication Type" || detail[row+iuuid*4].name == "SSID" {
                        
                        MarqueeWiBands6GText(mDetail: mDetail, index: row+iuuid*4, width:width/2-30)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .background(iuuid%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
                            .padding(.horizontal, 15)
                            .font(.InterMedium12)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 0,corners: .topLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                                   
                         )
                       
                    }
                    else{
                    Text("\(mDetail.wireless6G[row+iuuid*4].value!)")
                    .frame(width: width/2-30, height: 40, alignment:.leading)
                    .padding(.horizontal, 15)
                          .font(.InterMedium12)
                          .foregroundColor(Color.c0x242527)
                          .overlay(
                              RoundedCorner(radius: 3,corners: .topLeft)
                             .stroke(Color.c0x696A6B, lineWidth: 1)
                           )
                    }
                }
            }
            .font(.InterMedium12)
            .foregroundColor(Color.c0x242527)
            .background(iuuid%2 == 0 ? Color(hex:0xFFFFFF):Color(hex:0xF4F4F4))
            .cornerRadius(3)
            .padding(.top,12)

        }
        }
    }
    @ViewBuilder
    func wirelessClients(width:CGFloat)->some View {
        if funcPressed == 3 {
        HStack {
            Text("Wireless Clients")
                .frame(width:width/2,height: 30,alignment: .leading)
                .font(.InterRegular18)
                .foregroundColor(Color.c0x242527)
                    .padding(.leading,20)
            Button(action: {
                
                self.blurAmount = 2
                
                mDetail.getClientsCount = 0
                mDetail.wirelessClients.removeAll(keepingCapacity: true)
                mDetail.mobilekick.removeAll(keepingCapacity: true)
                mDetail.mobileIPSSID.removeAll(keepingCapacity: true)
                DispatchQueue.main.async {
                mDetail.getWirelessClients(GHz: "2")
                mDetail.getWirelessClients(GHz: "5")
                mDetail.getWirelessClients(GHz: "6")
                }
                var currentNetworkInfo: [String: Any] = [:]
                    getNetworkInfo { (wifiInfo) in

                      currentNetworkInfo = wifiInfo
                       // NSLog("currentNetworkInfo : \(currentNetworkInfo["SSID"])")
                        
                        var arr  = self.getIPAddress()
                      //  NSLog("arr : \(arr)")
                        
                        mDetail.mCurrentIP = arr
                        mDetail.mCurrentSSID = "\(currentNetworkInfo["SSID"])"
               }
                
                self.showwirelessClients = true
                
                
            }) {
                Image("spinner9")
                    .resizable()
                    .frame(width:25,height: 25)
                    .padding(.trailing,10)
                
            }
            .frame(width:width/2,height: 30,alignment: .trailing)
            .padding(.trailing,40)
            .onReceive(mDetail.$getClientsCount) { count in
                if support6G.contains(detail.model!)
                {
                    if count == 3
                    {
                        
                        self.showwirelessClients = false
                        self.blurAmount = 0
                        
                    }
                }
                else
                {
                    if count == 2
                    {
                        
                        self.showwirelessClients = false
                        self.blurAmount = 0
                       
                    }
                }
                
            }
            .accessibility(addTraits: .isButton)
            .accessibilityIdentifier("connectclients")
            
        }
         //   ZStack(){
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {

                        ForEach(0..<mDetail.wirelessClients.count/10, id: \.self) { iuuid in
                               
                            HStack(spacing:5) {
                                    VStack(spacing:0){
                                        showWirelessClientsList(iuuid:iuuid,width: width-40)
                                    }
                                    .frame(width:width,height: 120)
                                    .overlay(
                                        RoundedCorner(radius: 3,corners: .allCorners)
                                            .stroke(Color.c0x696A6B, lineWidth: 1)
                                     )
                                    .accessibility(addTraits: .isButton)
                                    .accessibilityIdentifier("client")
                                    
                                    Button(action: {
                                        
                                        mDetail.deleteClients(i: iuuid, radio: mDetail.mobilekick[iuuid].radio!, ssid_index: mDetail.mobilekick[iuuid].ssid_index!, client_mac: mDetail.mobilekick[iuuid].client_mac!)
                                        self.offsets.remove(at: iuuid)
                                        
                                    }) {
                                        Image("cross")
                                            .resizable()
                                            .frame(width:25,height: 25)
                                            .opacity(mDetail.mobilekick[iuuid].current == true ? 0.3:1.0)
                                    }
                                    .frame(width:50,height:50)
                                    .accessibilityIdentifier("deleteclient")
                                    
                                }
                                .padding(.top,5)
                                .padding(.trailing, -63)
                                .padding(.bottom,2)
                                .offset(x: offsets[iuuid].width)
                                .disabled(mDetail.mobilekick[iuuid].current == true ? true:false)
                                .gesture(
                                    DragGesture( coordinateSpace: .global)
                                        .onChanged { gesture in
                                            withAnimation(.spring()) {
                                                self.offsets[iuuid] = gesture.translation
                                                if offsets[iuuid].width > 50 {
                                                    self.offsets[iuuid] = .zero
                                                }
                                            }
                                          //  NSLog("x : \(gesture.location.x) , y : \(gesture.location.y)")
                                        }
                                        .onEnded { _ in
                                            withAnimation(.spring()) {
                                                    if self.offsets[iuuid].width < -50 {
                                                    self.offsets[iuuid].width = -50
                                                }
                                            }
                                        }
                                )
                         } // foreach
                    }//vstack
                    .frame(width:width+20,alignment: .center)
                    .padding(.leading,25)
                    .padding(.top,5)
                    
                }//scroll
                .frame(width:width,alignment: .center)
        }
    }
   @ViewBuilder
    func showWirelessClientsList(iuuid:Int,width:CGFloat)->some View
    {
        if funcPressed == 3 {
        ForEach(0..<5,id:\.self) { index in
            if index <= 1 {
                HStack{
                    Text(mDetail.wirelessClients[iuuid*10+index*2].value!)
                        .font(.InterMedium12)
                     .multilineTextAlignment(.leading)
                     .foregroundColor(.c0x242527)
                    .frame(width:width/2,height: 25,alignment: .leading)

                    Text(mDetail.wirelessClients[iuuid*10+index*2+1].value!)
                        .font(.InterMedium12)
                    .multilineTextAlignment(.trailing)
                    .foregroundColor(.c0x242527)
                    .frame(width:width/2,height: 25,alignment: .trailing)
                }
                .frame(height:25)
                .padding(.top,-5)
            }
            else
            {
                HStack{
                    Text("\(mDetail.wirelessClients[iuuid*10+index*2].name!): \(mDetail.wirelessClients[iuuid*10+index*2].value!)")
                        .font(.InterMedium12)
                     .multilineTextAlignment(.leading)
                    .foregroundColor(.c0x242527)
                    .frame(width:width/2+50,height: 25,alignment: .leading)

                    Text("\(mDetail.wirelessClients[iuuid*10+index*2+1].name!): \(mDetail.wirelessClients[iuuid*10+index*2+1].value!)")
                        .font(.InterMedium12)
                    .multilineTextAlignment(.trailing)
                    .foregroundColor(.c0x242527)
                    .frame(width:width/2-50,height: 25,alignment: .trailing)
                }
                .frame(height:25)
                .padding(.top,-5)
            }
        }
        }
    }
    
    @ViewBuilder
    func dateAndTime(width:CGFloat)->some View {
        if funcPressed == 4 {
        Text("Date And Time")
                .frame(height: 30)
                .font(.InterRegular18)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
        
        VStack{
            
                Text("Local Time")
                .frame(width:width,height: 30,alignment: .leading)
                    .font(.InterRegular14)
                    .foregroundColor(Color.c0x242527)
                    .padding(.top,12)
            
            if !self.editFlag || self.onNTPServer {
            GridStack(rows: 6, columns: 1) { row, col in
                if row == 0
                {
                    Text("\(mDetail.mLocalTime[row].name!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .topLeft)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                   
                    Text("\(mDetail.mLocalTime[row].value!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .topRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                }
                else if row == 5
                {
                    Text("\(mDetail.mLocalTime[row].name!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomLeft)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                   
                    Text("\(mDetail.mLocalTime[row].value!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                }
                else
                {
                    Text("\(mDetail.mLocalTime[row].name!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 0,corners: .topLeft)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                   
                    Text("\(mDetail.mLocalTime[row].value!)")
                        .font(.InterMedium12)
                        .frame(width: width/2-30, height: 40, alignment:.leading)
                        .padding(.horizontal, 15)
                        .foregroundColor(Color.c0x242527)
                        .overlay(
                            RoundedCorner(radius: 0,corners: .topRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                    }
                }
                .frame(width: width)
                .padding(.top,12)
                .background(Color.clear)
            }
            else if editFlag && !self.onNTPServer
            {
                GridStack(rows: 6, columns: 1) { row, col in
                    if row == 0
                    {
                        Text("\(mDetail.mLocalTime[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .topLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                        HStack(spacing:0){
                        Text("\(mDetail.mLocalTime[row].value!)")
                                .frame(width: width/2-50, height: 35, alignment:.leading)
                                .background(Color.white)
                                .padding(.leading, 15)
                                .font(.InterMedium12)
                                .foregroundColor(Color.c0x242527)
                                .gesture(drag)
                        
                            Image("down")
                                .resizable()
                                .frame(width: 10, height: 6)
                                .rotationEffect(showLocalTimeYear ?  .degrees(180):.degrees(0))
                                .disabled(true)
                                .background(Color.clear)
                                .padding(.trailing ,20)
                        }
                        .gesture(drag)
                        .frame(width: width/2,height:40)
                        .onTapGesture {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            self.showLocalTimeYear = true
                            }

                        }
                        .overlay(
                            RoundedCorner(radius: 3,corners: .topRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                        .accessibility(addTraits: .isButton)
                        .accessibilityIdentifier("LocalTime0")
                    }
                    else if row == 5
                    {
                        Text("\(mDetail.mLocalTime[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                        
                        HStack(spacing:0){
                        Text("\(mDetail.mLocalTime[row].value!)")
                                .frame(width: width/2-50, height: 35, alignment:.leading)
                                .background(Color.white)
                                .padding(.leading, 15)
                                .font(.InterMedium12)
                                .foregroundColor(Color.c0x242527)
                                .gesture(drag)
                        
                            Image("down")
                                .resizable()
                                .frame(width: 10, height: 6)
                                .rotationEffect(showLocalTimeSeconds ?  .degrees(180):.degrees(0))
                                .disabled(true)
                                .background(Color.clear)
                                .padding(.trailing ,20)
                        }
                        .gesture(drag)
                        .frame(width: width/2,height:40)
                        .onTapGesture {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            self.showLocalTimeSeconds = true
                            }
                        }
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                        .accessibility(addTraits: .isButton)
                        .accessibilityIdentifier("LocalTime5")
                    }
                    else
                    {
                        Text("\(mDetail.mLocalTime[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 0,corners: .topLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                       
                        
                        
                        HStack(spacing:0){
                        Text("\(mDetail.mLocalTime[row].value!)")
                                .frame(width: width/2-50, height: 35, alignment:.leading)
                                .background(Color.white)
                                .padding(.leading, 15)
                                .font(.InterMedium12)
                                .foregroundColor(Color.c0x242527)
                                .gesture(drag)

                            Image("down")
                                .resizable()
                                .frame(width: 10, height: 6)
                                .rotationEffect(showLocalTimeMonth ?  .degrees(180):.degrees(0))
                                .disabled(true)
                                .background(Color.clear)
                                .padding(.trailing ,20)
                        }
                        .gesture(drag)
                        .frame(width: width/2,height:40)
                        .onTapGesture {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                              if row == 1
                              {
                                  self.showLocalTimeMonth = true
                              }
                              else if row == 2
                              {
                                  self.showLocalTimeDay = true
                              }
                              else if row == 3
                              {
                                  self.showLocalTimeHours = true
                              }
                              else if row == 4
                              {
                                  self.showLocalTimeMinutes = true
                              }
                            }
                        }
                        .overlay(
                            RoundedCorner(radius: 0,corners: .bottomRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                        .accessibility(addTraits: .isButton)
                        .accessibilityIdentifier("LocalTime\(row)")
                   }
              } //Gra..
              .frame(width: width)
              .padding(.top,12)
            } // else
            
            Button(action: {
               
            }) {
                
                if !editFlag || self.onNTPServer {
                    HStack(spacing:0) {
                        Text("Acquire Current Time From Your Phone")
                            .frame( height: 30, alignment: .center)
                            .padding(.bottom,5)
                            .font(.InterMedium14)
                            .foregroundColor(Color(hex:0xD6D8DA))
                            .background(
                                RoundedRectangle(cornerRadius: 5, style: .continuous)
                                    .fill(Color(hex:0xFAFAFB))
                                    .padding(.bottom,5)
                                    .padding(.leading,-5)
                                    .padding(.trailing,-3)
                                   
                            )
                    }
                    .padding(.trailing,5)
                    .background(
                        RoundedRectangle(cornerRadius: 5, style: .continuous)
                            .stroke(Color(hex:0xD6D8DA), lineWidth: 1)
                            .padding(.bottom,5)
                            .padding(.leading,-6)
                            
                    )
                }
                else
                {
                    HStack(spacing:0) {
                        Text("Acquire Current Time From Your Phone")
                            .frame( height: 30, alignment: .center)
                            .padding(.bottom,5)
                            .font(.InterMedium14)
                            .foregroundColor(Color.c0x242527)
                            .background(
                                RoundedRectangle(cornerRadius: 5, style: .continuous)
                                    .fill(Color(hex:0xFFFFFF))
                                    .padding(.bottom,5)
                                    .padding(.leading,-5)
                                    .padding(.trailing,-3)
                            )
                    }
                    .padding(.trailing,5)
                    .background(
                        RoundedRectangle(cornerRadius: 5, style: .continuous)
                            .stroke(Color.c0x696A6B, lineWidth: 1)
                            .padding(.bottom,5)
                            .padding(.leading,-6)
                            
                    )
                    .onTapGesture {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let formattedDate = dateFormatter.string(from: Date())
                       // print(formattedDate)
                        let newinput = formattedDate.split(separator: " ")
                        let yymmdd = String(newinput[0]).split(separator: "-")
                        let uptime = String(newinput[1]).regex(pattern: "\\d{2}")
                        mDetail.mLocalTime[0].value = String(yymmdd[0])
                        mDetail.mLocalTime[1].value = mDetail.month[Int(yymmdd[1])!-1]
                        mDetail.mLocalTime[2].value = String(yymmdd[2])
                        mDetail.mLocalTime[3].value = String(uptime[0])
                        mDetail.mLocalTime[4].value = String(uptime[1])
                        mDetail.mLocalTime[5].value = String(uptime[2])
                    }
                    .accessibility(addTraits: .isButton)
                    .accessibilityIdentifier("TimeFromYourPhone")
                    
                }
               
            }
            .frame( width:width,height: 30, alignment: .trailing)
            .padding(.top,5)
            
        } //VStack
        .frame(width: width)
        .padding(.leading,20)
        .padding(.top,5)
        .background(Color.clear)
  
        VStack{
          
            Text("NTP Time Server")
                    .frame(width:width,height: 30,alignment: .leading)
                    .font(.InterRegular14)
                    .foregroundColor(Color.c0x242527)
                    .padding(.top,12)
            
            Toggle(isOn: $onNTPServer)
            {
                Text("NTP")
            }
            .toggleStyle(CustomNTP1ToggleStyle(edit: $editFlag))
            .frame(width:width,alignment: .center)
            .padding(.top,5)
            .onTapGesture {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
               // backPress.wirelessType[2] = Wireless6G
                self.onNTPServer.toggle()
                if self.onNTPServer
                {
                    self.mFromNTPServerlag = true
                }
                else
                {
                    self.mFromNTPServerlag = false
                }
                }
            }
            .disabled(!self.editFlag)
            .accessibilityIdentifier("ToggleNTPServer")
            .background(Color.clear)
            
            if self.editFlag {
                GridStack(rows: 3, columns: 1) { row, col in
                    if row == 0
                    {
                        Text("\(mDetail.mNTPTimeServer[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .topLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                        ZStack{
                            HStack(spacing:0) {
                                if mDaylightSaving {
                                Text("Enable")
                                    .frame(width:width/2-50, height: 35, alignment: .leading)
                                    .padding(.leading,15)
                                    .font(.InterMedium12)
                                    .foregroundColor(Color.c0x242527)
                                    .gesture(drag)
                                    //.foregroundColor(Color(hex: 0x818384))
                                }
                                else
                                {
                                    Text("Disable")
                                        .frame(width:width/2-50, height: 35, alignment: .leading)
                                        .padding(.leading,15)
                                        .font(.InterMedium12)
                                        .foregroundColor(Color.c0x242527)
                                        .gesture(drag)
                                       // .foregroundColor(Color(hex: 0x818384))
                                }

                                Image("down")
                                    .resizable()
                                    .frame(width: 10, height: 6)
                                    .rotationEffect(showNTPServerType ?  .degrees(180):.degrees(0))
                                    .disabled(true)
                                    .background(Color.clear)
                                    .padding(.trailing ,20)
                            }
                            .gesture(drag)
                            //.padding(.horizontal , 10)
                            .onTapGesture {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                    self.mDaylightSaving.toggle()
                                //    NSLog("\(self.DaylightSaving == true ? 1:0)" )
                                }
                            }
                            .accessibility(addTraits: .isButton)
                            .accessibilityIdentifier("ToggleDaylightSaving")
                        }
                        .frame(width: width/2, height: 40, alignment:.leading)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .topRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                        
                    }
                    else if row == 2
                    {
                        Text("\(mDetail.mNTPTimeServer[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                        HStack(spacing:0){
                        TextField("\(mDetail.mNTPTimeServer[row].value!)",text: $mNTPinterval, onEditingChanged: { (editingChanged) in
                            
                            if editingChanged {
                               
                                self.lastHoveredId = mDetail.lasttag[2]
                            }
                             
                        })
                            .font(.InterMedium12)
                            .frame(width: 30, height: 35, alignment:.leading)
                            .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[2], width: 30, height: 35))
                            .padding(.leading,-20)
                            .accessibilityIdentifier("NTPTimeServerHours")
                            
                            Text("(Hours)")
                                .frame(width: width/4-30,alignment:.leading)
                                .font(.InterMedium12)
                                .foregroundColor(Color.c0x242527)
                                .padding(.leading,20)
                                
                        }
                        .frame(width: width/2, height: 40, alignment:.center)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )
                        //.disabled(!self.onNTPServer)
                    }
                    else
                    {
                        Text("\(mDetail.mNTPTimeServer[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 80, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 0,corners: .topLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
      
                        ZStack(alignment:.top){
                            VStack(spacing:0) {
                            HStack(spacing:0) {
                                Text(mNTPServerType)
                                    .frame(width:width/2-50, height: 35, alignment: .leading)
                                    .padding(.leading,15)
                                    .font(.InterMedium12)
                                   // .foregroundColor(Color(hex: 0x818384))
                                    .foregroundColor(Color.c0x242527)
                                    .gesture(drag)

                                Image("down")
                                    .resizable()
                                    .frame(width: 10, height: 6)
                                    .rotationEffect(showNTPServerType ?  .degrees(180):.degrees(0))
                                    .disabled(true)
                                    .background(Color.clear)
                                    .padding(.trailing ,20)
                            }
                            .gesture(drag)
                            .onTapGesture {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                self.showNTPServerType = true
                                }
                               
                            }
                            .accessibility(addTraits: .isButton)
                            .accessibilityIdentifier("NTPServerType")
                                
                                TextField(mFromNTPServer,text:$mFromNTPServer, onEditingChanged: { (editingChanged) in
                                
                                if editingChanged {

                                    self.lastHoveredId = mDetail.lasttag[3]
                                   
                                }
                                
                                })

                                .frame(width: width/2-50, height: 35, alignment:.leading)
                                .background(Color.white)
                                .font(.InterMedium12)
                                .textFieldStyle(MyNewTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[3], width: width/2-50, height: 35))
                                .padding(.horizontal,15)
                                .disabled(self.mFromNTPServerlag)
                                .accessibilityIdentifier("FromNTPServerName")
                                
                            }
                                
                        }.frame(width: width/2, height: 80, alignment:.center)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                           // .disabled(!self.onNTPServer)
                            
                    }
                }
                .frame(width: width,alignment: .center)
                
            }
            else
            {
                GridStack(rows: 3, columns: 1) { row, col in
                    if row == 0
                    {
                        Text("\(mDetail.mNTPTimeServer[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .topLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                        if mDaylightSaving {
                            Text("Enable")
                                .font(.InterMedium12)
                                .frame(width: width/2-30, height: 40, alignment:.leading)
                                .padding(.horizontal, 15)
                                .foregroundColor(Color.c0x242527)
                                .overlay(
                                    RoundedCorner(radius: 3,corners: .topRight)
                                   .stroke(Color.c0x696A6B, lineWidth: 1)
                            )
                        }
                        else
                        {
                            Text("Disable")
                                .font(.InterMedium12)
                                .frame(width: width/2-30, height: 40, alignment:.leading)
                                .padding(.horizontal, 15)
                                .foregroundColor(Color.c0x242527)
                                .overlay(
                                    RoundedCorner(radius: 3,corners: .topRight)
                                   .stroke(Color.c0x696A6B, lineWidth: 1)
                            )
                        }
                    }
                    else if row == 2
                    {
                        Text("\(mDetail.mNTPTimeServer[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                        Text("\(mDetail.mNTPTimeServer[row].value!) (Hours)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 40, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 3,corners: .bottomRight)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                    }
                    else
                    {
                        Text("\(mDetail.mNTPTimeServer[row].name!)")
                            .font(.InterMedium12)
                            .frame(width: width/2-30, height: 80, alignment:.leading)
                            .padding(.horizontal, 15)
                            .foregroundColor(Color.c0x242527)
                            .overlay(
                                RoundedCorner(radius: 0,corners: .topLeft)
                               .stroke(Color.c0x696A6B, lineWidth: 1)
                             )
                        ZStack(alignment:.top){
                            VStack(spacing:0) {
                                Text(self.mNTPServerType)
                                    .font(.InterMedium12)
                                    .frame(width: width/2-30, height: 40, alignment:.leading)
                                    .padding(.horizontal, 15)
                                    .foregroundColor(Color.c0x242527)
                                    
                                Text(self.mFromNTPServer)
                                    .font(.InterMedium12)
                                    .frame(width: width/2-30, height: 40, alignment:.leading)
                                    .padding(.horizontal, 15)
                                    .foregroundColor(Color.c0x242527)
                            }
                        }
                        .frame(width: width/2, height: 80, alignment:.center)
                        .overlay(
                            RoundedCorner(radius: 3,corners: .bottomRight)
                           .stroke(Color.c0x696A6B, lineWidth: 1)
                         )

                    }//else
                }
                .frame(width: width,alignment: .center)
                .disabled(true)
                
            }
            
            
        }
        .frame(width: width)
        .padding(.leading,20)
        .padding(.top,5)
   
        VStack{
            
                Text("Time Zone")
                    .frame(width:width,height: 30,alignment: .leading)
                    .font(.InterRegular14)
                    .foregroundColor(Color.c0x242527)
                    .padding(.top,12)
                
            HStack(spacing:0) {
               // Text(self.timeZone)
                if editFlag {
                    Text(self.timeZone)
                        .frame(width:width-20, height: 40, alignment: .leading)
                        .padding(.bottom,5)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                }
                else {
                    if mDetail.mTimeZone != ""
                    {
                MarqueeTimeZoneText(mDetail: mDetail, width: width-20)
                    .frame(width:width-20, height: 40, alignment: .leading)
                    .padding(.bottom,5)
                    .font(.InterRegular14)
                    .foregroundColor(Color.c0x242527)
                    .gesture(drag)
                    }
                }
                if editFlag {
                
                Image("down")
                    .resizable()
                    .frame(width: 10, height: 6)
                    .rotationEffect(showTimeZone ?  .degrees(180):.degrees(0))
                    .disabled(true)
                    .background(Color.clear)
                    .padding(.trailing ,0)
                }
            }
            .gesture(drag)
            .onTapGesture {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showTimeZone = true
                }
            }
            .accessibility(addTraits: .isButton)
            .accessibilityIdentifier("Opentimezone")
        }
        .frame(width: width)
        .padding(.leading,20)
        .padding(.top,5)
        .disabled(!self.editFlag)
        .onReceive(mDetail.$getDateAndTimeResult) { result in
            if result != nil && self.requestDate == true
            {
                if result?.error_code == 0 {
              //  NSLog("\(result)")
                self.requestDate = false
                self.blurAmount = 0
                self.mNTPinterval = result?.data?.ntp_update_interval!.description ?? "24"
                self.onNTPServer = (result?.data?.ntp_enabled == 0 ? false:true)
                self.mFromNTPServer = result?.data?.ntp_server_name ?? "0.sophos.pool.ntp.org"
                self.mDaylightSaving = (((result?.data?.auto_daylight_save) == 1) ? true:false)
                    self.timeZone = mTimeZoneList[((result?.data?.time_zone)!)]
                
                if self.mFromNTPServer == "0.sophos.pool.ntp.org"
                {
                    self.mNTPServerType = "Global"
                }
                else if self.mFromNTPServer == "0.africa.pool.ntp.org"
                {
                    self.mNTPServerType = "Africa"
                }
                else if self.mFromNTPServer == "0.asia.pool.ntp.org"
                {
                    self.mNTPServerType = "Asia"
                }
                else if self.mFromNTPServer == "0.europe.pool.ntp.org"
                {
                    self.mNTPServerType = "Europe"
                }
                else if self.mFromNTPServer == "0.north-america.pool.ntp.org"
                {
                    self.mNTPServerType = "North America"
                }
                else if self.mFromNTPServer == "0.oceania.pool.ntp.org"
                {
                    self.mNTPServerType = "Oceania"
                }
                else if self.mFromNTPServer == "0.south-america.pool.ntp.org"
                {
                    self.mNTPServerType = "South America"
                }
                else
                {
                    self.mNTPServerType = "User-Defined"
                    self.mFromNTPServerlag = true
                }
                }
                else
                {
                    self.requestDate = false
                    self.blurAmount = 0
                    self.showErr = true
                    self.blurAmount = 2
                }
            }
            
        }
       
            
        }
    }
    @ViewBuilder
    func pingTest(width:CGFloat)->some View {
        if funcPressed == 5 {
        Text("Ping Test")
                .frame(height: 30)
                .font(.InterRegular18)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
                
        
        VStack{
            ZStack(alignment:.top){
                Text(self.pingIPplaceholder)
                    .foregroundColor(Color(hex:0x888888))
                    .font(.InterRegular14)
                    .frame(width: width,height: 40,alignment:.leading)
                    .padding(.leading,30)
                    .padding(.top,5)
                    
            HStack(spacing:0){
                TextField("",text:$pingIP, onEditingChanged: { (editingChanged) in
                    if editingChanged {
                        self.lastHoveredId = mDetail.lasttag[1]
                        self.pingIPplaceholder = ""
                    }
                })
                    .accessibilityIdentifier("pingIP")
                    .frame(height: 40,alignment:.trailing)
                    .font(.InterRegular14)
                    .textFieldStyle(MyTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[1], width: width-50, height: 40))
                   // .keyboardType(.decimalPad)
                    .onChange(of: pingIP) {
                       if $0 == ""
                        {
                           self.pingIPplaceholder = "Destination Address"
                       }
                        else
                        {
                            self.pingIPplaceholder = ""
                        }
                    }
                    
           
            }
            .frame(width: width)
            .padding(.leading,20)
            .padding(.top,5)
            }

            Button(action: {
                
                mDetail.pingTest(address: pingIP,isIPv4: self.pingIP.isIPv4(),isIPv6: self.pingIP.isIPv6())
                    self.showWaitPingRequest = true
                    self.blurAmount = 2
            }) {
                    
                HStack(spacing:0) {
                    Text("Execute")
                        .frame( height: 40, alignment: .center)
                        .padding(.bottom,2)
                        
                        .font(.InterMedium14)
                        .foregroundColor(self.pingIP.isIpAddress() ? Color.c0x242527:Color(hex:0xD6D8DA))
                        .background(
                            RoundedRectangle(cornerRadius: 5, style: .continuous)
                                .fill(self.pingIP.isIpAddress() ? Color(hex:0xFFFFFF):Color(hex:0xFAFAFB))
                                .padding(.bottom,3)
                                .padding([.leading,.trailing],-9)
                                .padding(.top,3)
                        )
                }
                .background(
                    RoundedRectangle(cornerRadius: 3, style: .continuous)
                        .stroke(self.pingIP.isIpAddress() ? Color.c0x696A6B:Color(hex:0xD6D8DA), lineWidth: 1)
                        .padding(.bottom,2)
                        .padding([.leading,.trailing],-10)
                        .padding(.top,2)
                        
                )
               
            }
            .disabled(!self.pingIP.isIpAddress())
            .frame( width:width,height: 40, alignment: .trailing)
            .padding(.top,5)
            .accessibilityIdentifier("pingIPExecute")
            
            Text("Result")
                .frame(width:width,height: 30,alignment: .leading)
                .font(.InterMedium14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
                .padding(.top,-8)
            
            VStack{
                ScrollView(.vertical, showsIndicators: false){
                        ScrollViewReader { pageScroller in
                        VStack{
                            Text(self.pingMessage)
                            .font(.InterMedium12)
                            .foregroundColor(Color.c0x242527)
                            .padding(.leading,5)
                            .id(0)
      
                        }
                        .onReceive(mDetail.$pingMessage){ result in
                            
                            self.pingMessage = result
                          //  self.progressValue = 1.0
                            self.showWaitPingRequest = false
                            blurAmount = 0.0
                           // self.sysReloadflag = false
                        }
                        .frame(maxWidth: .infinity , alignment: .leading)
                        }
                    }
                
            }
            .frame(width:width,height:250,alignment: .leading) //(width/3+width/2)
            .padding(.leading,20)
            .background(
                RoundedRectangle(cornerRadius: 5, style: .continuous)
                    .stroke(Color.c0x696A6B, lineWidth: 1)
                    .padding(.bottom,5)
                    .padding(.leading,20)
                    .padding(.top,-8)
            )
            
        }
        .padding(.leading,-5)
        }
    }
    @ViewBuilder
    func tracerouteTest(width:CGFloat)->some View {
        if funcPressed == 6 {
        Text("Traceroute Test")
                .frame(height: 30)
                .font(.InterRegular18)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
        
        VStack{
            ZStack(alignment:.top){
                Text(self.tracerouteIPplaceholder)
                    .foregroundColor(Color.c0x696A6B)
                    .font(.InterRegular14)
                    .frame(width: width,height: 40,alignment:.leading)
                    .padding(.leading,30)
                    .padding(.top,5)
                    
            HStack(spacing:0){
                TextField("",text:$tracerouteIP, onEditingChanged: { (editingChanged) in
                    if editingChanged {
                        
                        self.lastHoveredId = mDetail.lasttag[1]
                        self.tracerouteIPplaceholder = ""
                    }
                })
                    
                    .accessibilityIdentifier("tracerouteIP")
                    .frame(height: 40,alignment:.trailing)
                    .font(.InterRegular14)
                    .textFieldStyle(MyTableTextFieldStyle(lastHoveredId: $lastHoveredId,id: mDetail.lasttag[1], width: width-50, height: 40))
                    //.keyboardType(.decimalPad)
                    .onChange(of: tracerouteIP) {
                       if $0 == ""
                        {
                           self.tracerouteIPplaceholder = "Destination Address"
                       }
                        else
                        {
                            self.tracerouteIPplaceholder = ""
                        }
                    }
            }
            .frame(width: width)
            .padding(.leading,20)
            .padding(.top,5)
        }
            
            Button(action: {
              //  if tracerouteIP.isIpAddress() {
                    mDetail.tracerouteTest(address: tracerouteIP)
                    //Detail.startPinging(ip: tracerouteIP,times: 10)
                    self.showWaitPingRequest = true
                    self.blurAmount = 2
              //  }
                
            }) {
                    
                HStack(spacing:0) {
                    Text("Execute")
                        .frame( height: 40, alignment: .center)
                        .padding(.bottom,2)
                        .font(.InterMedium14)
                        .foregroundColor(self.tracerouteIP.isIpAddress() ? Color.c0x242527:Color(hex:0xD6D8DA))
                        .background(
                            RoundedRectangle(cornerRadius: 3, style: .continuous)
                                .fill(self.tracerouteIP.isIpAddress() ? Color(hex:0xFFFFFF):Color(hex:0xFAFAFB))
                                .padding(.bottom,3)
                                .padding([.leading,.trailing],-9)
                                .padding(.top,3)
                                //.stroke(focused ? Color.gray : Color.gray, lineWidth: 1)
                                
                        )
                    
                }
                .background(
                    RoundedRectangle(cornerRadius: 3, style: .continuous)
                        .stroke(self.tracerouteIP.isIpAddress() ? Color.c0x696A6B:Color(hex:0xD6D8DA), lineWidth: 1)
                        .padding(.bottom,2)
                        .padding([.leading,.trailing],-10)
                        .padding(.top,2)
                    )
               
            }
            .frame( width:width,height: 40, alignment: .trailing)
            .padding(.top,5)
            .accessibilityIdentifier("tracerouteIPExecute")
            
            Text("Result")
                .frame(width:width,height: 30,alignment: .leading)
                .font(.InterMedium14)
                .foregroundColor(Color.c0x242527)
                .padding(.leading,20)
                .padding(.top,-8)
            
            VStack{
                ScrollView(.vertical, showsIndicators: false){
                        ScrollViewReader { pageScroller in
                        VStack{
                            Text(self.tracerouteMessage)
                            .font(.InterMedium12)
                            .foregroundColor(Color.c0x242527)
                            .padding(.leading,5)
                            .id(0)
      
                        }
                        .onReceive(mDetail.$tracerouteMessage){ result in
                            self.tracerouteMessage = result //+ "\n" + self.tracerouteMessage
                            withAnimation {
                                pageScroller.scrollTo(0, anchor: .bottom)
                            }
                            self.showWaitPingRequest = false
                            blurAmount = 0.0
                        }
                        .frame(maxWidth: .infinity , alignment: .leading)
                        }
                    }
                
            }
            .frame(width:width,height:250,alignment: .leading) //(width/3+width/2)
            .padding(.leading,20)
            .background(
                RoundedRectangle(cornerRadius: 3, style: .continuous)
                    .stroke(Color.c0x696A6B, lineWidth: 1)
                    .padding(.bottom,5)
                    .padding(.leading,20)
                    .padding(.top,-8)
            )
            
        }
        .padding(.leading,-5)
        }
    }
    @ViewBuilder
    func showsave(width:CGFloat)->some View {
        if self.save {
            ZStack {
                VStack(spacing:0){
            Text("Changes have been saved successfully")
                    .frame(width:width,height: 30)
                    .font(.InterRegular18)
                    .foregroundColor(Color.white)
                    .background(Color.c0x00851D)
                    .shadow(radius: 10)
                
            }
            }.popup(isPresented: $save, with: $save) { item in
               
            }
            .frame(width:width,height: 35)
            .position(x: width/2, y: 110)
            .onAppear {
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.save = false
                }
            }
        }
        if self.saveErrFlag {
            ZStack {
                VStack(spacing:0){
            Text("  Invalid IP Address : \(saveErr)")
                    .multilineTextAlignment(.leading)
                    .lineLimit(2)
                    .frame(width:width,height: 40,alignment: .leading)
                    .font(.InterRegular16)
                    .foregroundColor(Color.white)
                    .background(Color.c0xDA3E00)
                    .shadow(radius: 10)
            }
            }.popup(isPresented: $saveErrFlag, with: $saveErrFlag) { item in
               
            }
            .frame(width:width,height: 40)
            .position(x: width/2, y: 110)
            .onAppear {
                //Detail.system[0].value = value
               // Detail.system[1].value = value1
                DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                    self.saveErrFlag = false
                    self.saveErr = ""
                }
            }
        }
    }
    @ViewBuilder
    func showDialog(tag:String,width:CGFloat,height:CGFloat,xpos:CGFloat,ypos:CGFloat,content:String) -> some View {
        if self.showAlert {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                    Text(content)
                        .multilineTextAlignment(.center)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .position(x: geometry.size.width/2, y: geometry.size.height/3)
                    
                    HStack()  {
                        Button{
                            backPress.rebootFlag = true
                            backPress.mSSDPDiscvoer.removeAll(keepingCapacity: true)
                            mDetail.mobileIPSSID.removeAll(keepingCapacity: true)
                            mDetail.getClientsCount = 0
                            mDetail.getWirelessClients(GHz: "2")
                            mDetail.getWirelessClients(GHz: "5")
                            if support6G.contains(detail.model!)
                            {
                            mDetail.getWirelessClients(GHz: "6")
                            }
                            self.check = true
                            
                        } label: {
                            Text("Yes")
                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
                                .font(.InterMedium14)
                                .foregroundColor(.white)
                                .background(Color.c0x005BC8)
                                .cornerRadius(5)
                        }
                        .accessibilityIdentifier("rebootok")
                        .frame(width: 100, height: 40)
                        .onReceive(mDetail.$getClientsCount) { results in
                                if self.check {
                                  //  NSLog("$getClientsCount : \(Results)")
                                    
                                    var currentNetworkInfo: [String: Any] = [:]
                                    getNetworkInfo { (wifiInfo) in

                                      currentNetworkInfo = wifiInfo
                                    //    NSLog("currentNetworkInfo : \(currentNetworkInfo["SSID"])")
                                        
                                        let arr  = self.getIPAddress()
                                    //    NSLog("arr : \(arr)")
                                        
                                        let amap = mDetail.mobileIPSSID.filter { $0.value == arr ||  $0.value == "\(currentNetworkInfo["SSID"])"}
                                        //NSLog("a : \(a)")
                                        
                                        if support6G.contains(detail.model!)
                                        {
                                            if results == 3 //3 //6g API failed
                                            {
                                                if amap.isEmpty
                                                {
                                                 //   NSLog("putReboot isEmpty")
                                                    mDetail.putReboot()
                                                    self.showAlert = false
                                                    self.showWait = true
                                                    mDetail.mobileIPSSID.removeAll(keepingCapacity: true)
                                                    self.check = false
                                                    self.showCurrentAPReboot = false
                                                    self.sysReloadTime = 120
                                                    self.sysReloadTimeFlag = false
                                                }
                                                else
                                                {
                                               //     NSLog("putReboot")
                                                    self.showCurrentAPReboot = true
                                                    mDetail.mobileIPSSID.removeAll(keepingCapacity: true)
                                                    self.check = false
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if results == 2
                                            {
                                                if amap.isEmpty
                                                {
                                                    mDetail.putReboot()
                                                    self.showAlert = false
                                                    self.showWait = true
                                                    mDetail.mobileIPSSID.removeAll(keepingCapacity: true)
                                                    self.check = false
                                                    self.showCurrentAPReboot = false
                                                    self.sysReloadTime = 120
                                                    self.sysReloadTimeFlag = false
                                                }
                                                else
                                                {
                                                    self.showCurrentAPReboot = true
                                                    mDetail.mobileIPSSID.removeAll(keepingCapacity: true)
                                                    self.check = false
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        
                        Spacer()
                        Button{
                            self.showAlert = false
                            self.blurAmount = 0
                        } label: {
                            ZStack{
                            RoundedRectangle(cornerRadius: 5)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                            Text("No")
                                .font(.InterMedium14)
                                .foregroundColor(Color.c0x242527)
                            }.frame(width: 100, height: 40)
                        }
                        .accessibilityIdentifier("rebootno")
                      }//HStack
                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
                    } // GeometryReader
                }
                .frame(width:width-200,height: 200,alignment: .center)
                .background(Color.white)
                
                
            }.popup(isPresented: $showAlert, with: $showAlert) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width-150,height: 200,alignment: .center)
            .position(x: xpos, y: ypos)
        }
        
        if self.showErr {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                    Text("Could not connect to the server.")
                        .multilineTextAlignment(.center)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .position(x: geometry.size.width/2, y: geometry.size.height/3)
                    
                    HStack()  {
                        Button{
                            
                            self.showErr = false
                        } label: {
                            Text("Yes")
                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
                                .font(.InterMedium14)
                                .foregroundColor(.white)
                                .background(Color.c0x005BC8)
                                .cornerRadius(5)
                        }.frame(width: 100, height: 40)
                            .accessibilityIdentifier("notconnect")
                      }//HStack
                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
                    } // GeometryReader
                }
                .frame(width:width-200,height: 200,alignment: .center)
                .background(Color.white)
                
                
            }.popup(isPresented: $showErr, with: $showErr) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width-150,height: 200,alignment: .center)
            .position(x: xpos, y: ypos)
        }
        if self.showCurrentAPReboot {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                    Text("Rebooting this AP will temporarily turn off any active networks that the AP is providing and disconnect wireless clients that are connected to these networks")
                        .multilineTextAlignment(.center)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .position(x: geometry.size.width/2, y: geometry.size.height/3)
                    
                    HStack()  {
                        Button{
                            mDetail.putReboot()
                            self.showAlert = false
                            self.showWait = true
                            self.showCurrentAPReboot = false
                            self.sysReloadTime = 120
                            self.sysReloadTimeFlag = false
                        } label: {
                            Text("Yes")
                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
                                .font(.InterMedium14)
                                .foregroundColor(.white)
                                .background(Color.c0x005BC8)
                                .cornerRadius(5)
                        }.frame(width: 100, height: 40)
                       .accessibilityIdentifier("rebootokok")
                      }//HStack
                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
                    } // GeometryReader
                }
                .frame(width:width-200,height: 200,alignment: .center)
                .background(Color.white)
                
                
            }.popup(isPresented: $showCurrentAPReboot, with: $showCurrentAPReboot) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width-150,height: 200,alignment: .center)
            .position(x: xpos, y: ypos)
        }
    }
    @ViewBuilder
    func showWait(tag:String,width:CGFloat,height:CGFloat,xpos:CGFloat,ypos:CGFloat,content:String) -> some View {
        if self.showWait {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                    Text(content)
                        .multilineTextAlignment(.center)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .position(x: geometry.size.width/2, y: geometry.size.height/3)
                        
                        ProgressView()
                            .progressViewStyle(CircularProgressViewStyle(tint: Color.c0x242527))
                            .scaleEffect(x: 1, y: 1, anchor: .center)
                            .position(x: geometry.size.width/2, y: geometry.size.height/1.5)
                            .onReceive(timer) { iuuid in
                                
                                //NSLog("progressValue : \(progressValue)")
                                if progressValue < 1.0  && progressValue >= 0.0 {
                                    if progressValue >= 0.89
                                    {
                                        progressValue = 1.0
                                        self.timer = Timer.publish(every: 3.0, on: .main, in: .common).autoconnect()
                                    }
                                    else
                                    {
                                        progressValue += 0.0015
                                    }
                                }
                                if progressValue == 1.0 {
                                    
                                   // DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                                   //    NSLog("discoverService : \(i)")
                                        backPress.rebootFlag = false
                                       if backPress.mSSDPDiscvoer.isEmpty
                                       {
                                           timeoutCount += 1
                                           
                                       }
                                       if timeoutCount == 22 //22
                                       {
                                           self.timer.upstream.connect().cancel()
                                           
                                           progressValue = 0.0
                                           self.showWait = false
                                           blurAmount = 0
                                           self.mRebootErr = true
                                           self.mAPnotFound = true
                                       }
                                    
                                       for respone in backPress.mSSDPDiscvoer
                                         {
                                             timeoutCount = 0
                                             let result = SSDPService(host: respone.key, response: respone.value)
                                             if result.mac != nil
                                             {
                                                 if result.mac == detail.mac {
                                                 let ssdp = SSDP(LOCATION: result.location, SERVER: result.server, ST: result.searchTarget, USN: result.uniqueServiceName, Vendor: result.vendor, Mac: result.mac, Model: result.model, Name: result.name, OperatorMode: result.operatorMode, Identity: result.identity, FirmwareVersion: result.firmwareVersion, CPUUtilization: result.cPUUtilization, ConnectedUsers:result.connectedUsers, SNR: result.sNR, MemoryUtilization: result.memoryUtilization, WiFiRadio: result.wiFiRadio, live: "", SOPHOS_INFO: result.sOPHOSINFO)
                                                 
                                                 self.timer.upstream.connect().cancel()
                                                 
                                                 progressValue = 0.0
                                                 self.showWait = false
                                                 blurAmount = 0
                                                 self.mRebootSuccess = true
                                                 }
                                             }
                                        }
                                   // }
                                    
                                }
                            }//onreciver
                        Text("\(self.sysReloadTime) seconds left")
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .multilineTextAlignment(.center)
                        .position(x: geometry.size.width/2, y: geometry.size.height/1.5 + 30)
                        .onReceive(Just(sysReloadTime)) { _ in
                            if self.sysReloadTimeFlag == false
                            {
                             //   NSLog("self.sysReloadTime Result: \(Result)")
                                self.sysReloadTimeFlag = true
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                    self.sysReloadTime -= 1
                                    self.sysReloadTimeFlag = false
                             //       NSLog("self.sysReloadTime: \(self.sysReloadTime)")
                                }
                            }
                        }
                        
                    } // GeometryReader
                }
                .frame(width:width-200,height: 200,alignment: .center)
                .background(Color.white)
                
                
            }.popup(isPresented: $showWait, with: $showWait) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width-150,height: 200,alignment: .center)
            .position(x: xpos, y: ypos)
        }
    }
    func getNetworkInfo(compleationHandler: @escaping ([String: Any])->Void){


       var currentWirelessInfo: [String: Any] = [:]

        if #available(iOS 14.0, *) {

            NEHotspotNetwork.fetchCurrent { network in

                guard let network = network else {
                    compleationHandler([:])
                    return
                }

                let bssid = network.bssid
                let ssid = network.ssid

                //currentWirelessInfo = ["BSSID ": bssid, "SSID": ssid, "SSIDDATA": "<54656e64 615f3443 38354430>"]
                currentWirelessInfo = ["BSSID ": bssid, "SSID": ssid]
                compleationHandler(currentWirelessInfo)
            }
        }

    }
    func getIPAddress() -> String {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }

                guard let interface = ptr?.pointee else { return "" }
                let addrFamily = interface.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    let name: String = String(cString: (interface.ifa_name))
                    if  name == "en0" {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface.ifa_addr, socklen_t((interface.ifa_addr.pointee.sa_len)), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address ?? ""
    }
    @ViewBuilder
    func showRebootSuccess(width:CGFloat)->some View {
        if self.mRebootSuccess {
            ZStack {
                VStack(spacing:0){
            Text("This AP rebooted successfully")
                    .frame(width:width,height: 30)
                    .font(.InterRegular18)
                    .foregroundColor(Color.white)
                    .background(Color.c0x00851D)
                    .shadow(radius: 10)
                
            }
            }.popup(isPresented: $mRebootSuccess, with: $mRebootSuccess) { item in
               
            }
            .frame(width:width,height: 35)
            .position(x: width/2, y: 110)
            .onAppear {
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.mRebootSuccess = false
                }
            }
        }
        if self.mRebootErr {
            ZStack {
                VStack(spacing:0){
            Text("This AP did not reboot succesfully, please try again")
                    .frame(width:width,height: 30)
                    .font(.InterRegular14)
                    .foregroundColor(Color.white)
                    .background(Color.c0xDA3E00)
                    .shadow(radius: 10)
                
            }
            }.popup(isPresented: $mRebootErr, with: $mRebootErr) { item in
               
            }
            .frame(width:width,height: 35)
            .position(x: width/2, y: 110)
            .onAppear {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.mRebootErr = false
                }
            }
        }
    }
    @ViewBuilder
    func showRadioDialog(tag:String,width:CGFloat,height:CGFloat,xpos:CGFloat,ypos:CGFloat,content:String) -> some View {
        if self.showRadioAlert {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                
                VStack() {
                    GeometryReader { geometry in
                    Text(content)
                        .multilineTextAlignment(.center)
                        .font(.InterRegular14)
                        .foregroundColor(Color.c0x242527)
                        .position(x: geometry.size.width/2, y: geometry.size.height/3)
                    
                    HStack()  {
                        Button{
                            let ssdp = SSDP(LOCATION: detail.location!, SERVER: "", ST: "", USN: "", Vendor: "", Mac: detail.mac!, Model:"", Name: detail.appname , OperatorMode: detail.central , Identity: "", FirmwareVersion: "", CPUUtilization: "", ConnectedUsers:"", SNR: "", MemoryUtilization: "", WiFiRadio: detail.wifiradio, live:"", SOPHOS_INFO: "")
                            let result = datas.saveToCurrentEable(detail.mac!, ssdp)
                            
                            if result
                            {
                                mDetail.getOffWiFiResult = 0
                                mDetail.offWiFi(model: detail.model!)
                            }
                            self.check = true
                            
                        } label: {
                            Text("Yes")
                                .frame(minWidth: 0, maxWidth: 100, minHeight: 0, maxHeight: 40)
                                .font(.InterMedium14)
                                .foregroundColor(.white)
                                .background(Color.c0x005BC8)
                                .cornerRadius(5)
                        }
                        .frame(width: 100, height: 40)
                        .onReceive(mDetail.$getOffWiFiResult) { result in
                               // NSLog("OffWiFi Detail.getOffWiFiResult :\(result)")
                                var requests = 2
                                if support6G.contains(detail.model!)
                                {
                                    requests = 3
                                }
                                if result == requests && self.check == true
                                {
                                    self.showRadioAlert = false
                                    blurAmount = 0
                                    mDetail.getOffWiFiResult = 0
                                    self.check = false
                                    mDetail.postsysReload()
                                  //  self.progressValue = 0.0
                                    self.showWIFIAlert = true
                                    self.blurAmount = 2
                                    self.sysReloadTimeCout = false
                                    self.sysReloadTime = 0
                                }
                                 
                            }
                        .accessibilityIdentifier("online")
                        
                        Spacer()
                        Button{
                            self.showRadioAlert = false
                            blurAmount = 0
                        } label: {
                            ZStack{
                            RoundedRectangle(cornerRadius: 5)
                                    .stroke(Color.c0x696A6B, lineWidth: 1)
                            Text("No")
                                .font(.InterMedium14)
                                .foregroundColor(Color.c0x242527)
                            }.frame(width: 100, height: 40)
                        }
                        .accessibilityIdentifier("offline")
                      }//HStack
                    .position(x: geometry.size.width/2, y: geometry.size.height - 35)
                    } // GeometryReader
                }
                .frame(width:width-200,height: 200,alignment: .center)
                .background(Color.white)
                
            }.popup(isPresented: $showRadioAlert, with: $showRadioAlert) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width-150,height: 200,alignment: .center)
            .position(x: xpos, y: ypos)
        }
    }
    @ViewBuilder
    func showList(width:CGFloat,height:CGFloat)->some View {
        if self.showLANIPType {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                      
                            ForEach(mLANIPTypeArray, id: \.self) { iuuid in
                                Spacer()
                                Button {
                                    self.mLANIPType = iuuid
                                    self.showLANIPType = false
                                    blurAmount = 0
                                } label: {
                                    Text(iuuid)
                                        .frame(width: 100,height:40,alignment:.leading)
                                        .font(.InterRegular14)
                                        .foregroundColor(Color.c0x242527)
                                        .multilineTextAlignment(.leading)
                                }
                                .frame(width: 100,height:40,alignment:.leading)
                                .padding(.leading,10)
                                .accessibilityIdentifier("laniptypeone\(iuuid)")
                                Divider()
                            }
                    }
                    .frame(width: 100,alignment: .center)
                    .background(Color.white)
                }
                .frame(width:width/4+20,alignment: .center)
                .onAppear{
                    blurAmount = 2
                    
                }
                
            }.popup(isPresented: $showLANIPType, with: $showLANIPType) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width/3,height: 120,alignment: .center)
            .position(x:width/2,y:height/2)
           
        }
        else if self.showLeaseTimeFlag {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                      
                            ForEach(mLeaseTimeArray, id: \.self) { iuuid in
                                Spacer()
                                Button {
                                    self.showLeaseTimeFlag = false
                                    self.blurAmount = 0
                                    mDetail.mLANDHCPServer[3].value = iuuid
                                } label: {
                                    Text(iuuid)
                                        .frame(width: 100,height:40,alignment:.leading)
                                        .font(.InterRegular14)
                                        .foregroundColor(Color.c0x242527)
                                        .multilineTextAlignment(.leading)
                                    
                                }
                                .frame(width: 100,height:40,alignment:.leading)
                                .padding(.leading,10)
                                .accessibility(addTraits: .isButton)
                                .accessibilityIdentifier("leasetime0\(iuuid)")
                                Divider()
                            }
                       
                    }
                    .frame(width: 100,alignment: .center)
                    .background(Color.white)
                }
                .frame(width:width/4+20,alignment: .center)
                .onAppear{
                    self.blurAmount = 2
                }
                         
            }.popup(isPresented: $showLeaseTimeFlag, with: $showLeaseTimeFlag) { item in
                
            }
            .frame(width:width/3,height: 200,alignment: .center)
            .position(x:width/2,y:height/2)
            
        }
        else if self.mGateWayFromDHCPFlag {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                      
                        ForEach(self.mFromDHCPArray, id: \.self) { iuuid in
                                Spacer()
                                Button {
                                    self.mGateWayFromDHCP = iuuid
                                    self.mGateWayFromDHCPFlag = false
                                    self.blurAmount = 0
                                } label: {
                                    Text(iuuid)
                                        .frame(width: 100,height:40,alignment:.leading)
                                        .font(.InterRegular14)
                                        .foregroundColor(Color.c0x242527)
                                        .multilineTextAlignment(.leading)
                                }
                                .frame(width: 100,height:40,alignment:.leading)
                                .padding(.leading,10)
                                .accessibility(addTraits: .isButton)
                                .accessibilityIdentifier("GateWayflag\(iuuid)")
                                Divider()
                            }
                    }
                    .frame(width: 100,alignment: .center)
                    .background(Color.white)
                }
                .frame(width:width/4+20,alignment: .center)
                .onAppear{
                    self.blurAmount = 2
                }
                
            }.popup(isPresented: $mGateWayFromDHCPFlag, with: $mGateWayFromDHCPFlag) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width/3,height: 120,alignment: .center)
            .position(x:width/2,y:height/2)
            
        }
        else if self.mPriAddrFromDHCPFlag {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                      
                        ForEach(self.mFromDHCPArray, id: \.self) { iuuid in
                               
                                Button {
                                    self.mPriAddrFromDHCP = iuuid
                                    self.mPriAddrFromDHCPFlag = false
                                    self.blurAmount = 0
                                } label: {
                                    Text(iuuid)
                                        .frame(width: 100,height:40,alignment:.leading)
                                        .font(.InterRegular14)
                                        .foregroundColor(Color.c0x242527)
                                        .multilineTextAlignment(.leading)
                                }
                                .frame(width: 100,height:40,alignment:.leading)
                                .padding(.leading,10)
                                .accessibility(addTraits: .isButton)
                                .accessibilityIdentifier("PriAddrflag\(iuuid)")
                                Divider()
                            }
                    }
                    .frame(width: 100,alignment: .center)
                    .background(Color.white)
                }
                .frame(width:width/4+20,alignment: .center)
                .onAppear{
                    self.blurAmount = 2
                }
                
            }.popup(isPresented: $mPriAddrFromDHCPFlag, with: $mPriAddrFromDHCPFlag) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width/3,height: 120,alignment: .center)
            .position(x:width/2,y:height/2)
            
        }
        else if self.mSecAddrFromDHCPFlag {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                      
                        ForEach(self.mFromDHCPArray, id: \.self) { iuuid in
                               
                                Button {
                                    self.mSecAddrFromDHCP = iuuid
                                    self.mSecAddrFromDHCPFlag = false
                                    self.blurAmount = 0
                                } label: {
                                    Text(iuuid)
                                        .frame(width: 100,height:40,alignment:.leading)
                                        .font(.InterRegular14)
                                        .foregroundColor(Color.c0x242527)
                                        .multilineTextAlignment(.leading)
                                }
                                .frame(width: 100,height:40,alignment:.leading)
                                .padding(.leading,10)
                                .accessibility(addTraits: .isButton)
                                .accessibilityIdentifier("SecAddrflag\(iuuid)")
                                Divider()
                            }
                    }
                    .frame(width: 100,alignment: .center)
                    .background(Color.white)
                }
                .frame(width:width/4+20,alignment: .center)
                .onAppear{
                    self.blurAmount = 2
                }
                
            }.popup(isPresented: $mSecAddrFromDHCPFlag, with: $mSecAddrFromDHCPFlag) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width/3,height: 120,alignment: .center)
            .position(x:width/2,y:height/2)
            
        }
        else if self.showBandsTaggle24G || self.showBandsTaggle5G || self.showBandsTaggle6G {
            ZStack(){
                
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack() {
                      
                            ForEach(mSSIDArray, id: \.self) { iuuid in
                                Spacer()
                                Button {
                                    if self.showBandsTaggle24G
                                    {
                                        self.mWiFiBands24G = iuuid
                                        self.showBandsTaggle24G.toggle()
                                    }
                                    else if self.showBandsTaggle5G
                                    {
                                        self.mWiFiBands5G = iuuid
                                        self.showBandsTaggle5G.toggle()
                                    }
                                    else if self.showBandsTaggle6G
                                    {
                                        self.mWiFiBands6G = iuuid
                                        self.showBandsTaggle6G.toggle()
                                    }
                                    self.blurAmount = 0
                                } label: {
                                    Text(iuuid)
                                        .frame(width: 100,height:40,alignment:.center)
                                        .font(.InterRegular14)
                                        .foregroundColor(Color.c0x242527)
                                    
                                }
                                .frame(width: 100,height:40,alignment:.center)
                                .accessibility(addTraits: .isButton)
                                .accessibilityIdentifier("SSID2GNUM\(iuuid)")
                                Divider()
                            }
                       
                    }
                    .frame(width: 100,alignment: .center)
                    .background(Color.white)
                }
                .frame(width:width/4+20,alignment: .center)
                .onAppear{
                    self.blurAmount = 2
                }
                
            }.popup(isPresented: $showBandsTaggle24G, with: $showBandsTaggle24G) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width/3,height: 200,alignment: .center)
            .position(x:width/2,y:height/2)
            
            }
        else if self.showAuthType24GFlag || showAuthType5GFlag {
            //print("showAuthTypeFlag tapped")
            ZStack(){
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                        
                        Text("Choose your Wireless Authentication method")
                            .font(.InterRegular14)
                            .foregroundColor(Color.c0x242527)
                        .frame(width: 280,alignment:.center)
                        .multilineTextAlignment(.center)
                        .padding(.top,10)
                        RadioButtonGroup(items: mAuthArray, selectedId: mAuthArray[mAuthArraySelected]) { selected in
                            if self.showAuthType24GFlag
                            {
                                mDetail.wireless24G[mAuthROW].value = selected
                                self.showAuthType24GFlag = false
                            }
                            else if self.showAuthType5GFlag
                            {
                                mDetail.wireless5G[mAuthROW].value = selected
                                self.showAuthType5GFlag = false
                            }
                            else if self.showAuthType6GFlag
                            {
                                mDetail.wireless6G[mAuthROW].value = selected
                                self.showAuthType6GFlag = false
                            }
                            
                            self.blurAmount = 0
                        }
                        .padding(.leading,5)
                        .frame(width: 280,alignment: .leading)
                        
                    }
                    .frame(width: 280,alignment: .leading)
                }
                .frame(width:width*2/3+20,alignment: .leading)
                .onAppear{
                    self.blurAmount = 2
                }
            }.popup(isPresented: $showAuthType24GFlag, with: $showAuthType24GFlag) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width*2/3+10,height: 400,alignment: .center)
            .background(Color.white)
            .position(x:width/2,y:height/2)
            
       }
        else if showAuthType6GFlag {
            //print("showAuthTypeFlag tapped")
            ZStack(){
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                        
                        Text("Choose your Wireless Authentication method")
                            .font(.InterRegular14)
                            .foregroundColor(Color.c0x242527)
                        .frame(width: 280,alignment:.center)
                        .multilineTextAlignment(.center)
                        .padding(.top,10)
                        RadioButtonGroup(items: mAuth6GArray, selectedId: mAuth6GArray[mAuthArraySelected]) { selected in
                            
                                mDetail.wireless6G[mAuthROW].value = selected
                                self.showAuthType6GFlag = false
                           
                            self.blurAmount = 0
                        }
                        .padding(.leading,5)
                        .frame(width: 280,alignment: .leading)
                        
                    }
                    .frame(width: 280,alignment: .leading)
                }
                .frame(width:width*2/3+20,alignment: .leading)
                .onAppear{
                    self.blurAmount = 2
                }
            }.popup(isPresented: $showAuthType6GFlag, with: $showAuthType6GFlag) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width*2/3+10,height: 200,alignment: .center)
            .background(Color.white)
            .position(x:width/2,y:height/2)
            
       }
        else if self.showLocalTimeYear
                {
                    ZStack(){
                        RoundedRectangle(cornerRadius: 5)
                                .foregroundColor(.white)
                                .shadow(radius: 10)
                                
                        ScrollView(.vertical) {
                            VStack(alignment:.leading) {
                              
                                ForEach(["2017","2018","2019","2020","2021","2022","2023","2024","2025","2026","2027"], id: \.self) { iuuid in
                                        Spacer()
                                        Button {
                                            self.showLocalTimeYear = false
                                            self.blurAmount = 0
                                            mDetail.mLocalTime[0].value = iuuid
                                        } label: {
                                            Text(iuuid)
                                                .frame(width: 100,height:40,alignment:.center)
                                                .font(.InterRegular14)
                                                .foregroundColor(Color.c0x242527)
                                            
                                        }
                                        .frame(width: 100,height:40,alignment:.center)
                                        .accessibilityIdentifier("LocalTimeYear\(iuuid)")
                                        Divider()
                                    }
                               
                            }
                            .frame(width: 100,alignment: .center)
                            .background(Color.white)
                        }
                        .frame(width:width/4+20,alignment: .center)
                        .onAppear{
                            self.blurAmount = 2
                        }
                                 
                    }.popup(isPresented: $showLocalTimeYear, with: $showLocalTimeYear) { item in
                        
                    }
                    .frame(width:width/3,height: 200,alignment: .center)
                    .position(x:width/2,y:height/2)
                    
                }
        else if self.showLocalTimeMonth
                {
                    ZStack(){
                        RoundedRectangle(cornerRadius: 5)
                                .foregroundColor(.white)
                                .shadow(radius: 10)
                                
                        ScrollView(.vertical) {
                            VStack(alignment:.leading) {
                              
                                ForEach(["January","February","March","April","May","June","July","August","September","October","November","December"], id: \.self) { iuuid in
                                        Spacer()
                                        Button {
                                            self.showLocalTimeMonth = false
                                            self.blurAmount = 0
                                            mDetail.mLocalTime[1].value = iuuid
                                        } label: {
                                            Text(iuuid)
                                                .frame(width: 100,height:40,alignment:.leading)
                                                .font(.InterRegular14)
                                                .foregroundColor(Color.c0x242527)
                                                .multilineTextAlignment(.leading)
                                            
                                        }
                                        .frame(width: 100,height:40,alignment:.leading)
                                        .padding(.leading,10)
                                        .accessibilityIdentifier("LocalTimeMonth\(iuuid)")
                                        Divider()
                                    }
                               
                            }
                            .frame(width: 100,alignment: .center)
                            .background(Color.white)
                        }
                        .frame(width:width/4+20,alignment: .center)
                        .onAppear{
                            self.blurAmount = 2
                        }
                                 
                    }.popup(isPresented: $showLocalTimeMonth, with: $showLocalTimeMonth) { item in
                        
                    }
                    .background(
                        RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                    )
                    .frame(width:width/3,height: 200,alignment: .center)
                    .position(x:width/2,y:height/2)
                    
                }
                else if self.showLocalTimeDay
                {
                    ZStack(){
                        RoundedRectangle(cornerRadius: 5)
                                .foregroundColor(.white)
                                .shadow(radius: 10)
                                
                        ScrollView(.vertical) {
                            VStack(alignment:.leading) {
                              
                                ForEach(1..<32) { iuuid in
                                        Spacer()
                                        Button {
                                            self.showLocalTimeDay = false
                                            self.blurAmount = 0
                                            mDetail.mLocalTime[2].value = String(iuuid)
                                        } label: {
                                            Text("\(iuuid)")
                                                .frame(width: 100,height:40,alignment:.center)
                                                .font(.InterRegular14)
                                                .foregroundColor(Color.c0x242527)
                                            
                                        }
                                        .frame(width: 100,height:40,alignment:.center)
                                        .accessibilityIdentifier("LocalTimeDay\(iuuid)")
                                        Divider()
                                    }
                               
                            }
                            .frame(width: 100,alignment: .center)
                            .background(Color.white)
                        }
                        .frame(width:width/4+20,alignment: .center)
                        .onAppear{
                            self.blurAmount = 2
                        }
                                 
                    }.popup(isPresented: $showLocalTimeDay, with: $showLocalTimeDay) { item in
                        
                    }
                    .background(
                        RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                    )
                    .frame(width:width/3,height: 200,alignment: .center)
                    .position(x:width/2,y:height/2)
                    
                }
                else if self.showLocalTimeHours
                {
                    ZStack(){
                        RoundedRectangle(cornerRadius: 5)
                                .foregroundColor(.white)
                                .shadow(radius: 10)
                                
                        ScrollView(.vertical) {
                            VStack(alignment:.leading) {
                              
                                ForEach(1..<25) { iuuid in
                                        Spacer()
                                        Button {
                                            self.showLocalTimeHours = false
                                            self.blurAmount = 0
                                            mDetail.mLocalTime[3].value = String(iuuid)
                                        } label: {
                                            Text("\(iuuid)")
                                                .frame(width: 100,height:40,alignment:.center)
                                                .font(.InterRegular14)
                                                .foregroundColor(Color.c0x242527)
                                            
                                        }
                                        .frame(width: 100,height:40,alignment:.center)
                                        .accessibilityIdentifier("LocalTimeHours\(iuuid)")
                                        Divider()
                                    }
                               
                            }
                            .frame(width: 100,alignment: .center)
                            .background(Color.white)
                        }
                        .frame(width:width/4+20,alignment: .center)
                        .onAppear{
                            self.blurAmount = 2
                        }
                                 
                    }.popup(isPresented: $showLocalTimeHours, with: $showLocalTimeHours) { item in
                        
                    }
                    .background(
                        RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                    )
                    .frame(width:width/3,height: 200,alignment: .center)
                    .position(x:width/2,y:height/2)
                    
                }
                else if self.showLocalTimeMinutes
                {
                    ZStack(){
                        RoundedRectangle(cornerRadius: 5)
                                .foregroundColor(.white)
                                .shadow(radius: 10)
                                
                        ScrollView(.vertical) {
                            VStack(alignment:.leading) {
                              
                                ForEach(0..<61) { iuuid in
                                        Spacer()
                                        Button {
                                            self.showLocalTimeMinutes = false
                                            self.blurAmount = 0
                                            mDetail.mLocalTime[4].value = String(iuuid)
                                        } label: {
                                            Text("\(iuuid)")
                                                .frame(width: 100,height:40,alignment:.center)
                                                .font(.InterRegular14)
                                                .foregroundColor(Color.c0x242527)
                                            
                                        }
                                        .frame(width: 100,height:40,alignment:.center)
                                        .accessibilityIdentifier("LocalTimeMinutes\(iuuid)")
                                        Divider()
                                    }
                               
                            }
                            .frame(width: 100,alignment: .center)
                            .background(Color.white)
                        }
                        .frame(width:width/4+20,alignment: .center)
                        .onAppear{
                            self.blurAmount = 2
                        }
                                 
                    }.popup(isPresented: $showLocalTimeMinutes, with: $showLocalTimeMinutes) { item in
                        
                    }
                    .background(
                        RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                    )
                    .frame(width:width/3,height: 200,alignment: .center)
                    .position(x:width/2,y:height/2)
                    
                }
                else if self.showLocalTimeSeconds
                {
                    ZStack(){
                        RoundedRectangle(cornerRadius: 5)
                                .foregroundColor(.white)
                                .shadow(radius: 10)
                                
                        ScrollView(.vertical) {
                            VStack(alignment:.leading) {
                              
                                ForEach(0..<61) { iuuid in
                                        Spacer()
                                        Button {
                                            self.showLocalTimeSeconds = false
                                            self.blurAmount = 0
                                            mDetail.mLocalTime[5].value = String(iuuid)
                                        } label: {
                                            Text("\(iuuid)")
                                                .frame(width: 100,height:40,alignment:.center)
                                                .font(.InterRegular14)
                                                .foregroundColor(Color.c0x242527)
                                            
                                        }
                                        .frame(width: 100,height:40,alignment:.center)
                                        .accessibilityIdentifier("LocalTimeSeconds\(iuuid)")
                                        Divider()
                                    }
                               
                            }
                            .frame(width:100,alignment: .center)
                            .background(Color.white)
                        }
                        .frame(width:width/4+20,alignment: .center)
                        .onAppear{
                            self.blurAmount = 2
                        }
                                 
                    }.popup(isPresented: $showLocalTimeSeconds, with: $showLocalTimeSeconds) { item in
                        
                    }
                    .background(
                        RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                    )
                    .frame(width:width/3,height: 200,alignment: .center)
                    .position(x:width/2,y:height/2)
                    
                }
                else if self.showTimeZone
                {
                    ZStack(){
                        RoundedRectangle(cornerRadius: 5)
                                .foregroundColor(.white)
                                .shadow(radius: 10)
                                
                        ScrollView(.vertical) {
                            VStack(alignment:.leading) {
                              
                                ForEach(mTimeZoneList, id: \.self) { iuuid in
                                        Spacer()
                                        Button {
                                            self.showTimeZone = false
                                            self.blurAmount = 0
                                            self.timeZone = iuuid
                                        } label: {
                                            MarqueeText(text:"\(iuuid)")
                                                .frame(width: 280,height:40,alignment:.leading)
                                                .font(.InterRegular14)
                                                .foregroundColor(Color.c0x242527)
                                                .multilineTextAlignment(.leading)
                                            
                                        }
                                        .frame(width: 280,height:40,alignment:.leading)
                                        .padding(.leading,10)
                                        .accessibility(addTraits: .isButton)
                                        .accessibilityIdentifier("zone\(iuuid)")
                                        Divider()
                                    }
                               
                            }
                            .frame(width: width*4/5,alignment: .center)
                            .background(Color.white)
                            
                        }
                        .frame(width:width*4/5+20,alignment: .center)
                        .onAppear{
                            self.blurAmount = 2
                        }
                                 
                    }.popup(isPresented: $showTimeZone, with: $showTimeZone) { item in
                        
                    }
                    .background(
                        RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.c0x696A6B, lineWidth: 1)
                    )
                    .frame(width:width*4/5,height: 400,alignment: .center)
                    .position(x:width/2,y:height/2)
                    
                    
                }
        else if self.showNTPServerType
        {
            ZStack(){
                RoundedRectangle(cornerRadius: 5)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                        
                ScrollView(.vertical) {
                    VStack(alignment:.leading) {
                         ForEach(["Global","Africa","Asia","Europe","North America","Oceania","South America","User-Defined"], id: \.self) { iuuid in
                                Spacer()
                                Button {
                                    self.showNTPServerType = false
                                    self.mNTPServerType = iuuid
                                    self.blurAmount = 0
                                    self.mFromNTPServerlag = false
                                    if self.mNTPServerType == "User-Defined"
                                    {
                                        self.mFromNTPServerlag = true
                                        //self.FromNTPServer = "0.sophos.pool.ntp.org"
                                    }
                                    else if self.mNTPServerType == "Global"
                                    {
                                        self.mFromNTPServer = "0.sophos.pool.ntp.org"
                                    }
                                    else if self.mNTPServerType == "Africa"
                                    {
                                        self.mFromNTPServer = "0.africa.pool.ntp.org"
                                    }
                                    else if self.mNTPServerType == "Asia"
                                    {
                                        self.mFromNTPServer = "0.asia.pool.ntp.org"
                                    }
                                    else if self.mNTPServerType == "Europe"
                                    {
                                        self.mFromNTPServer = "0.europe.pool.ntp.org"
                                    }
                                    else if self.mNTPServerType == "North America"
                                    {
                                        self.mFromNTPServer = "0.north-america.pool.ntp.org"
                                    }
                                    else if self.mNTPServerType == "Oceania"
                                    {
                                        self.mFromNTPServer = "0.oceania.pool.ntp.org"
                                    }
                                    else if self.mNTPServerType == "South America"
                                    {
                                        self.mFromNTPServer = "0.south-america.pool.ntp.org"
                                    }
                                } label: {
                                    Text("\(iuuid)")
                                        .frame(width: 100,height:40,alignment:.leading)
                                        .font(.InterRegular14)
                                        .foregroundColor(Color.c0x242527)
                                        .multilineTextAlignment(.leading)
                                    
                                }
                                .frame(width: 100,height:40,alignment:.leading)
                                .padding(.leading,10)
                                .accessibilityIdentifier("NTPServerType\(iuuid)")
                                Divider()
                            }
                       
                    }
                    .frame(width:100,alignment: .center)
                    .background(Color.white)
                }
                .frame(width:width/4+20,alignment: .center)
                .onAppear{
                    self.blurAmount = 2
                }
                         
            }.popup(isPresented: $showNTPServerType, with: $showNTPServerType) { item in
                
            }
            .background(
                RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.c0x696A6B, lineWidth: 1)
            )
            .frame(width:width/3,height: 200,alignment: .center)
            .position(x:width/2,y:height/2)
            
        }
        else if self.showwirelessClients
        {
            VStack() {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.c0x242527))
                    .scaleEffect(x: 2, y: 2, anchor: .center)
            }
            .frame(width:50.0,height: 50.0,alignment: .center)
            .background(Color.clear)
            .position(x:width/2,y:height/2)
            .onAppear
            {
                self.blurAmount = 2
            }
            
        }
        else if self.showWaitPingRequest
        {
            
            VStack() {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.c0x242527))
                    .scaleEffect(x: 2, y: 2, anchor: .center)
            }
            .frame(width:50.0,height: 50.0,alignment: .center)
            .background(Color.clear)
            .position(x:width/2,y:height/2)
            .onAppear
            {
                self.blurAmount = 2
            }
        }
        else if self.showWaitRequest
        {
   //         ZStack(){
            VStack() {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.c0x242527))
                    .scaleEffect(x: 2, y: 2, anchor: .center)
                if editFlag && self.sysReloadTime != 0 {
                    Text("\(self.sysReloadTime) seconds left")
                    .font(.InterMedium18)
                    .foregroundColor(Color.c0x242527)
                    .multilineTextAlignment(.center)
                    .padding(.top,30)
                    .onReceive(Just(sysReloadTime)) { _ in
                        if self.sysReloadTimeFlag == false
                        {
                          //  NSLog("self.sysReloadTime Result: \(Result)")
                            self.sysReloadTimeFlag = true
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                self.sysReloadTime -= 1
                                self.sysReloadTimeFlag = false
                          //      NSLog("self.sysReloadTime: \(self.sysReloadTime)")
                                
                            }
                        }
                    }

                }
                
            }
            .frame(width:width-40,height: 200.0,alignment: .center)
            .background(Color.clear)
            .position(x:width/2,y:height/2)
            .onAppear
            {
                self.blurAmount = 2
            }
        }
        else if self.showWIFIAlert
        {
            VStack() {
                
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.c0x242527))
                    .scaleEffect(x: 2, y: 2, anchor: .center)
                Text("\(self.sysReloadTime) seconds left")
                .font(.InterMedium18)
                .foregroundColor(Color.c0x242527)
                .multilineTextAlignment(.center)
                .padding(.top,30)
                .onReceive(Just(sysReloadTime)) { _ in
                    if self.sysReloadTimeFlag == false
                    {
                      //  NSLog("self.sysReloadTime Result: \(Result)")
                        self.sysReloadTimeFlag = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            self.sysReloadTime -= 1
                            self.sysReloadTimeFlag = false
                      //      NSLog("self.sysReloadTime: \(self.sysReloadTime)")
                        }
                    }
                }
                
            }
            .frame(width:width-40,height: 200.0,alignment: .center)
            .background(Color.clear)
            .position(x:width/2,y:height/2)
            .onAppear
            {
                self.blurAmount = 2.0
            }
            .onReceive(mDetail.$postsysReloadResult) { result in
             //   NSLog("sysReload : \(result)")
                if result?.error_code == 0
                {
              //      NSLog("sysReload : \(result?.data?.reload_time)")
                    let delay = Double((result?.data?.reload_time)!)!
               //     NSLog("sysReload :\(delay) \(result?.data?.reload_time)")
                    if sysReloadTimeCout == false
                    {
                        self.sysReloadTime = Int(delay)
                        sysReloadTimeCout = true
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + delay ) {
                        if mDetail.postsysReloadResult != nil {
                            self.blurAmount = 0
                            self.showWIFIAlert = false
                            mDetail.postsysReloadResult = nil
                            self.save = true
                        }

                    }
                  }
            }
        }
        else if self.showWaitRadio
        {
            VStack() {
                
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.c0x242527))
                    .scaleEffect(x: 2, y: 2, anchor: .center)
            }
            .frame(width:width-40,height: 200.0,alignment: .center)
            .background(Color.clear)
            .position(x:width/2,y:height/2)
            .onAppear
            {
                self.blurAmount = 2.0
            }
        }//else if self.showWaitRadio
        else if self.pskAlert {
            ZStack {
                VStack(spacing:0){
            Text("The pre-shared key should contain at least 8 alphanumeric characters and no matching the hexadecimal byte")
                    .frame(width:width,height: 65, alignment: .center)
                    .font(.InterRegular14)
                    .foregroundColor(Color.white)
                    .background(Color.c0xDA3E00)
                    .shadow(radius: 10)
                    .multilineTextAlignment(.center)
                    .lineLimit(3)
                
            }
            }.popup(isPresented: $pskAlert, with: $pskAlert) { item in
               
            }
            .frame(width:width,height: 35)
            .position(x: width/2, y: 110)
            .onAppear {
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.pskAlert = false
                }
            }
        }
        else if self.mSSIDAlert {
            ZStack {
                VStack(spacing:0){
            Text("The SSID should contain at least 1 character and no matching the hexadecimal byte")
                    .frame(width:width,height: 50, alignment: .center)
                    .font(.InterRegular14)
                    .foregroundColor(Color.white)
                    .background(Color.c0xDA3E00)
                    .shadow(radius: 10)
                    .multilineTextAlignment(.center)
                    .lineLimit(2)
                
            }
            }.popup(isPresented: $mSSIDAlert, with: $mSSIDAlert) { item in
               
            }
            .frame(width:width,height: 35)
            .position(x: width/2, y: 110)
            .onAppear {
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.mSSIDAlert = false
                }
            }
        }
        else if self.requestDate
        {
   //         ZStack(){
            VStack() {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.c0x242527))
                    .scaleEffect(x: 2, y: 2, anchor: .center)
               
                
            }
            .frame(width:width-40,height: 200.0,alignment: .center)
            .background(Color.clear)
            .position(x:width/2,y:height/2)
            .onAppear
            {
                self.blurAmount = 2
            }
        }
        else if self.nameAlert {
            ZStack {
                VStack(spacing:0){
            Text("The Product Name should contain at least 1 alphanumeric characters and no matching the hexadecimal byte")
                    .frame(width:width,height: 65, alignment: .center)
                    .font(.InterRegular14)
                    .foregroundColor(Color.white)
                    .background(Color.c0xDA3E00)
                    .shadow(radius: 10)
                    .multilineTextAlignment(.center)
                    .lineLimit(3)
                
            }
            }.popup(isPresented: $nameAlert, with: $nameAlert) { item in
               
            }
            .frame(width:width,height: 35)
            .position(x: width/2, y: 110)
            .onAppear {
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.nameAlert = false
                }
            }
        }
    }
}

