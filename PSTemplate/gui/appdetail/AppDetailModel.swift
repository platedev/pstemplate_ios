//
//  AppDetailModel.swift
//  PSTemplate
//
//  Created by EDIOS on 2022/1/25.
//

import SwiftUI
import Combine

class AppDetailModel:ObservableObject {
    @Published var myAccess = [MyAccessBody]()
    @Published var myCount:Bool = false
    @Published var system = [SystemBody]()
    @Published var mLANIPAddress = [LANBody]()
    @Published var mLANDNSServers = [LANBody]()
    @Published var mLANDHCPServer = [LANBody]()
    @Published var wireless24G = [WirelessBody]()
    @Published var wireless5G = [WirelessBody]()
    @Published var wireless6G = [WirelessBody]()
    @Published var wirelessClients = [ClientsBody]()
    @Published var mLocalTime = [LocalTimeBody]()
    @Published var mNTPTimeServer = [NTPTimeServerBody]()
    @Published var lasttag:[String] = []
    @Published var mobileIPSSID = [ClientsBody]()
    @Published var mobilekick = [KickClient]()
   // var ping: pingTool?
    @Published var pingMessage = ""
    @Published var tracerouteMessage = ""
    @Published var mTimeZone = ""
    
    var aPAddress = ""
    var mDigestAccount = ""
    var mDigestPw = ""
    var mCurrentIP = ""
    var mCurrentSSID = ""
    var post24GBandsBody = Bands.init(radio_enable: 1, radio_band: 370, enabled_ssidNum: 1, ssidNames: [SSID.init(ssidname: "SSID1", index: 0, vlan_id: 1)], autochannel_enabled: 1, autochannel_range: 0, autochannel_interval: 4, autochannel_force_enabled: 0, channel: 11, channel_bandwidth: 1, bss_basicrateset: 15)
    var post24GSSIDSecurityBody = SSIDSecurity.init(ssid_index: 0, broadcast_ssid: 1, client_isolation: 0, ieee80211k: 0, ieee80211w: 0, load_balance: 128, auth_method: 2, auth_detail: AuthDetail.init(ieee80211r: 0, wpa_type: 5, encrypt_type: 3, wpa_rekey_time: 60, psk_type: 0, psk_value: "123456789012345"), additional_auth: 0, mac_radius_auth: 0, mac_radius_password: "")
    var post24GSSIDSecurity = [SSIDSecurity]()
    var post5GBandsBody = Bands.init(radio_enable: 1, radio_band: 460, enabled_ssidNum: 1, ssidNames: [SSID.init(ssidname: "SSID1", index: 0, vlan_id: 1)], autochannel_enabled: 1, autochannel_range: 2, autochannel_interval: 4, autochannel_force_enabled: 0, channel: 36, channel_bandwidth: 8, bss_basicrateset: 336)
    var post5GSSIDSecurityBody = SSIDSecurity.init(ssid_index: 0, broadcast_ssid: 1, client_isolation: 0, ieee80211k: 0, ieee80211w: 0, load_balance: 128, auth_method: 2, auth_detail: AuthDetail.init(ieee80211r: 0, wpa_type: 5, encrypt_type: 3, wpa_rekey_time: 60, psk_type: 0, psk_value: "123456789012345"), additional_auth: 0, mac_radius_auth: 0, mac_radius_password: "")
    var post5GSSIDSecurity = [SSIDSecurity]()
    var post6GBandsBody = Bands.init(radio_enable: 1, radio_band: 768, enabled_ssidNum: 1, ssidNames: [SSID.init(ssidname: "SSID1", index: 0, vlan_id: 1)], autochannel_enabled: 1, autochannel_range: 2, autochannel_interval: 4, autochannel_force_enabled: 0, channel: 33, channel_bandwidth: 8, bss_basicrateset: 336)
    var post6GSSIDSecurityBody = SSIDSecurity.init(ssid_index: 0, broadcast_ssid: 1, client_isolation: 0, ieee80211k: 0, ieee80211w: 0, load_balance: 128, auth_method: 2, auth_detail: AuthDetail.init(ieee80211r: 0, wpa_type: 5, encrypt_type: 3, wpa_rekey_time: 60, psk_type: 0, psk_value: "123456789012345"), additional_auth: 0, mac_radius_auth: 0, mac_radius_password: "")
    var post6GSSIDSecurity = [SSIDSecurity]()
    var postAdministrationBody = Administration.init(account_name: "admin", account_security: "", account_confirm_security: "", product_name: "AP00AABBCCDD10-test0421", https_port: 443, management_protocols: managementProtocols.init(https: 1, telnet: 1, ssh: 1, snmp: 1), snmp_settings: snmpSettings.init(snmp_version: 0, snmp_get_community: "public", snmp_set_community: "private", snmp_v3_name: "admin", snmp_v3_security: "", snmp_trap_enabled: 0, snmp_trap_community: "public", snmp_trap_manager: ""))
    var postDateAndTimeBody = datetime.init(local_time_year: 2022, local_time_month: 1, local_time_day: 1, local_time_hour: 1, local_time_minute: 1, local_time_seconde: 1, time_zone: 1, ntp_enabled: 1, ntp_server_name: "", ntp_update_interval: 1, auto_daylight_save: 1)
    var postLANPortBody = LANs.init(lanport: [LANport.init(index: 0, duplex: 0, flow_control: 1, az_enabled: 1)], ipv6: IPv6(ipv6_enabled: 1, ipv6_type: 7, ipv6_predix_len: 0, ipv6_address: "", ipv6_gateway: ""), ipv4: IPv4(ipv4_type: 1, ipv4_address: "192.168.2.2", ipv4_subnet_mask: "255.255.255.0", ipv4_default_gateway_type: 1, ipv4_default_gateway: "0.0.0.0", ipv4_dns_primary_type: 1, ipv4_dns_primary: "0.0.0.0", ipv4_dns_secondary_type: 1, ipv4_dns_secondary: "0.0.0.0", dhcp_enabled: 0, dhcp_lease_time: 1, dhcp_start_addr: "192.168.2.120", dhcp_end_addr: "192.168.2.140", dhcp_domain_name: "setup.sophos.com", dhcp_dns_primary: "0.0.0.0", dhcp_dns_secondary: "0.0.0.0") )
    
    @ObservedObject var request: PSNetwork = PSNetwork()
    var anyCancellable = Set<AnyCancellable>()
    private var responses = 0
    @Published var response = 0
    {
        willSet {
            self.objectWillChange.send()
        }
    }
    
    @Published var err = ""
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var showErr = false
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var getRebbotREsult:GETRebootresponse? = GETRebootresponse.init(error_code: 10, error_msg: "", data: nil)
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var getSystemResult:GETresponse? = GETresponse.init(error_code: 10, error_msg: "", data: nil)
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var getAdministrationResult:GETAdministrationresponse? = GETAdministrationresponse.init(error_code: 10, error_msg: "", data: nil)
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var postAdministrationResult:POSTAdministrationresponse? = POSTAdministrationresponse.init(error_code: 10, error_msg: "")
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var getLANResult:GETLANresponse? = GETLANresponse.init(error_code: 10, error_msg: "", data: nil)
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var postLANResult:POSTLANresponse? = POSTLANresponse.init(error_code: 10, error_msg: "")
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var getClientsResult:GETClientsresponse? = GETClientsresponse.init(error_code: 10, error_msg: "", data: nil)
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var getClientsCount:Int = 0
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var get24GBandsResult:GET24GBandsresponse? = GET24GBandsresponse.init(error_code: 10, error_msg: "", data: nil)
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var get5GBandsResult:GET5GBandsresponse? = GET5GBandsresponse.init(error_code: 10, error_msg: "", data: nil)
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var get6GBandsResult:GET6GBandsresponse? = GET6GBandsresponse.init(error_code: 10, error_msg: "", data: nil)
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var post24GBandsResult:POST24GBandresponse? = POST24GBandresponse.init(error_code: 10, error_msg: "")
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var post5GBandsResult:POST5GBandresponse? = POST5GBandresponse.init(error_code: 10, error_msg: "")
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var post6GBandsResult:POST6GBandresponse? = POST6GBandresponse.init(error_code: 10, error_msg: "")
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var get24GSSIDSecurityResult:GET24GSSIDSecurityresponse? =  GET24GSSIDSecurityresponse.init(error_code: 10, error_msg: "", data: nil)
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var get5GSSIDSecurityResult:GET5GSSIDSecurityresponse? =  GET5GSSIDSecurityresponse.init(error_code: 10, error_msg: "", data: nil)
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var get6GSSIDSecurityResult:GET6GSSIDSecurityresponse? =  GET6GSSIDSecurityresponse.init(error_code: 10, error_msg: "", data: nil)
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var post24GSSIDSecurityResult:POST24GSecurityresponse? = POST24GSecurityresponse.init(error_code: 10, error_msg: "")
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var post5GSSIDSecurityResult:POST5GSecurityresponse? = POST5GSecurityresponse.init(error_code: 10, error_msg: "")
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var post6GSSIDSecurityResult:POST6GSecurityresponse? = POST6GSecurityresponse.init(error_code: 10, error_msg: "")
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var getOffWiFiResult:Int = 0
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var postfWiFiResult:Int = 0
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var getDateAndTimeResult:GETDateAndTimeresponse? = GETDateAndTimeresponse.init(error_code: 10, error_msg: "", data: nil)
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
   
    @Published var postDateAndTimeResult:POSTDateAndTimeresponse? = POSTDateAndTimeresponse.init(error_code: 10, error_msg: "")
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var getPingResult:GETPingresponse? = GETPingresponse.init(error_code: 10, error_msg: "", data: nil)
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var getTracerouteResult:GETTracerouteresponse? = GETTracerouteresponse.init(error_code: 10, error_msg: "", data: nil)
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var postsysReloadResult:POSTsysReloadresponse? = POSTsysReloadresponse.init(error_code: 10, error_msg: "", data: nil)
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    @Published var putKickClientREsult:PUTKickClientresponse? = PUTKickClientresponse.init(error_code: 10, error_msg: "")
    {
        willSet {
            
            self.objectWillChange.send()
        }
    }
    
    init() {
       request.objectWillChange.sink { [weak self] (value) in

            guard let self = self else  {
                return
            }
            self.getRebbotREsult = self.request.tResult as? GETRebootresponse
            self.getSystemResult = self.request.tResult as? GETresponse
            self.getAdministrationResult = self.request.tResult as? GETAdministrationresponse
            self.postAdministrationResult = self.request.tResult as? POSTAdministrationresponse
            self.getLANResult = self.request.tResult as? GETLANresponse
           self.postLANResult = self.request.tResult as? POSTLANresponse
            self.getClientsResult = self.request.tResult as? GETClientsresponse
            self.get24GBandsResult = self.request.tResult as? GET24GBandsresponse
            self.get24GSSIDSecurityResult = self.request.tResult as? GET24GSSIDSecurityresponse
            self.get5GBandsResult = self.request.tResult as? GET5GBandsresponse
            self.get5GSSIDSecurityResult = self.request.tResult as? GET5GSSIDSecurityresponse
            self.get6GBandsResult = self.request.tResult as? GET6GBandsresponse
            self.get6GSSIDSecurityResult = self.request.tResult as? GET6GSSIDSecurityresponse
           
           self.post24GBandsResult = self.request.tResult as? POST24GBandresponse
           self.post24GSSIDSecurityResult = self.request.tResult as? POST24GSecurityresponse
           self.post5GBandsResult = self.request.tResult as? POST5GBandresponse
           self.post5GSSIDSecurityResult = self.request.tResult as? POST5GSecurityresponse
           self.post6GBandsResult = self.request.tResult as? POST6GBandresponse
           self.post6GSSIDSecurityResult = self.request.tResult as? POST6GSecurityresponse
           self.getPingResult = self.request.tResult as? GETPingresponse
            self.getTracerouteResult = self.request.tResult as? GETTracerouteresponse
            self.getDateAndTimeResult = self.request.tResult as? GETDateAndTimeresponse
            self.postDateAndTimeResult = self.request.tResult as? POSTDateAndTimeresponse
           self.postsysReloadResult = self.request.tResult as? POSTsysReloadresponse
           self.putKickClientREsult = self.request.tResult as? PUTKickClientresponse
           self.objectWillChange.send()

       }
       .store(in: &anyCancellable)
        
    }
    let month = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    /*Style*/
    func updateEditStyle() {
        for iuuid in 0..<500 {
            lasttag.append(String(iuuid))
        }
    }
    /*Reboot*/
    func putReboot() {
        request.url = "https://\(aPAddress)/ap/systemctl/sysReboot"
        request.method = "PUT"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: GETRebootresponse.self) { result in
           // print(result)
            if result == 200 {
                //let resultBody = self.getRebbotREsult
                //resultBody?.data
            }
            else {
             //   if result == 400 {
                    DispatchQueue.main.async {
                    self.showErr = true
                    self.err = "Invaild Respone Formate"
                    self.getRebbotREsult = nil
                    self.responses = result
                    self.response = self.responses
                    }
             //   }
            }
        }
    }
    /*System*/
    func getSystem(){
        
        request.url = "https://\(aPAddress)/ap/info/system/basic"
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: GETresponse.self) { result in
           // print(result)
            if result == 200 {
                self.responses = result
               // self.response = self.responses
                let resultBody = self.getSystemResult
                
                self.system.removeAll()
                
                var fake = SystemBody(name: "Model", value: resultBody?.data?.model_name)
                self.system.append(fake)
                fake = SystemBody(name: "Product Name", value: resultBody?.data?.product_name)
                self.system.append(fake)
                fake = SystemBody(name: "Uptime", value: resultBody?.data?.uptime)
                self.system.append(fake)
                fake = SystemBody(name: "System Time", value: resultBody?.data?.system_time)
                self.system.append(fake)
                fake = SystemBody(name: "Firmware Version", value: resultBody?.data?.firmware_version)
                self.system.append(fake)
                fake = SystemBody(name: "MAC Address", value: resultBody?.data?.mac_address)
                self.system.append(fake)
                fake = SystemBody(name: "Management VLAN ID", value: resultBody?.data?.management_vlan_id?.description)
                self.system.append(fake)
                fake = SystemBody(name: "IP Address", value: resultBody?.data?.ip_address)
                self.system.append(fake)
                fake = SystemBody(name: "Default Gateway", value: resultBody?.data?.default_gateway)
                self.system.append(fake)
                fake = SystemBody(name: "DNS", value: resultBody?.data?.dns_server)
                self.system.append(fake)
                fake = SystemBody(name: "DHCP Server", value: resultBody?.data?.dhcp_server)
                self.system.append(fake)
                fake = SystemBody(name: "IPv6 Address", value: resultBody?.data?.ipv6_address)
                self.system.append(fake)
                fake = SystemBody(name: "IPv6 Link-Local Address", value: resultBody?.data?.ipv6_link_local_address)
                self.system.append(fake)
            }
            else {
               // if result == 400 {
                    DispatchQueue.main.async {
                    self.showErr = true
                    self.err = "Invaild Respone Formate"
                    self.getSystemResult = nil
                    self.responses = result
                    self.response = self.responses
                    }
//                }
//                else if result == 500 {
//                 //   self.showErr = true
//                 //   self.err = "Could not connect to the server."
//                 //   self.getSystemResult = nil
//                    DispatchQueue.main.async {
//                    self.response = result
//                    }
//                }
//                else
//                {
////                    self.showErr = true
////                    self.err = "The request timed out."
////                    self.getSystemResult = nil
//                    DispatchQueue.main.async { [unowned self] in
//                    self.response = result
//                    }
//                }
            }
        }
        
    }
    func getAdministration(productName:String){
        
        request.url = "https://\(aPAddress)/ap/config/system/administration"
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: GETAdministrationresponse.self) { result in
          //  print(result)
            if result == 200 {
                self.postAdministrationBody.account_name = self.getAdministrationResult?.data?.account_name
                self.postAdministrationBody.account_security = self.getAdministrationResult?.data?.account_security
                self.postAdministrationBody.account_confirm_security = self.getAdministrationResult?.data?.account_confirm_security
                self.postAdministrationBody.product_name = productName
                self.postAdministrationBody.https_port = self.getAdministrationResult?.data?.https_port
                self.postAdministrationBody.management_protocols = self.getAdministrationResult?.data?.management_protocols
                self.postAdministrationBody.snmp_settings = self.getAdministrationResult?.data?.snmp_settings
                self.postAdministration()
            }
            else
            {
                DispatchQueue.main.async {
                self.showErr = true
                self.err = "Invaild Respone Formate"
                self.getSystemResult = nil
                self.responses = result
                self.response = self.responses
                }
            }
        }
    }
    func postAdministration(){
        
        request.url = "https://\(aPAddress)/ap/config/system/administration"
        request.method = "POST"
        var data = Data()
        do{
            let encoder = JSONEncoder()
            data = (try? encoder.encode(postAdministrationBody)) ?? Data()
        
        }
        request.body = data
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: POSTAdministrationresponse.self) { result in
          //  print(result)
            if result == 200 {
                self.postsysReload()
            }
            else
            {
                
            }
        }
    }
    func update() {
        var fake = SystemBody(name: "Model", value: "AP6 840E")
        system.append(fake)
        fake = SystemBody(name: "Product Name", value: "SITE1_WIFI")
        system.append(fake)
        fake = SystemBody(name: "Uptime", value: "00:00:00")
        system.append(fake)
        fake = SystemBody(name: "System Time", value: "2022/12/12 00:00")
        system.append(fake)
        fake = SystemBody(name: "Firmware Version", value: "2.0.4")
        system.append(fake)
        fake = SystemBody(name: "MAC Address", value: "ccc")
        system.append(fake)
        fake = SystemBody(name: "Management VLAN ID", value: "1")
        system.append(fake)
        fake = SystemBody(name: "IP Address", value: "192.168.10.102")
        system.append(fake)
        fake = SystemBody(name: "Default Gateway", value: "192.168.10.1")
        system.append(fake)
        fake = SystemBody(name: "DNS", value: "192.168.10.1 \n 8.8.8.8")
        system.append(fake)
        fake = SystemBody(name: "DHCP Server", value: "192.168.10.1")
        system.append(fake)
        fake = SystemBody(name: "IPv6 Address", value: "---")
        system.append(fake)
        fake = SystemBody(name: "IPv6 Link-Local Address", value: "---")
        system.append(fake)
    }
    func updateR() {
        self.system.removeAll()
        
        var fake = SystemBody(name: "Model", value: getSystemResult?.data?.model_name)
        self.system.append(fake)
        fake = SystemBody(name: "Product Name", value: getSystemResult?.data?.product_name)
        self.system.append(fake)
        fake = SystemBody(name: "Uptime", value: getSystemResult?.data?.uptime)
        self.system.append(fake)
        fake = SystemBody(name: "System Time", value: getSystemResult?.data?.system_time)
        self.system.append(fake)
        fake = SystemBody(name: "Firmware Version", value: getSystemResult?.data?.firmware_version)
        self.system.append(fake)
        fake = SystemBody(name: "MAC Address", value: getSystemResult?.data?.mac_address)
        self.system.append(fake)
        fake = SystemBody(name: "Management VLAN ID", value: getSystemResult?.data?.management_vlan_id?.description)
        self.system.append(fake)
        fake = SystemBody(name: "IP Address", value: getSystemResult?.data?.ip_address)
        self.system.append(fake)
        fake = SystemBody(name: "Default Gateway", value: getSystemResult?.data?.default_gateway)
        self.system.append(fake)
        fake = SystemBody(name: "DNS", value: getSystemResult?.data?.dns_server)
        self.system.append(fake)
        fake = SystemBody(name: "DHCP Server", value: getSystemResult?.data?.dhcp_server)
        self.system.append(fake)
        fake = SystemBody(name: "IPv6 Address", value: getSystemResult?.data?.ipv6_address)
        self.system.append(fake)
        fake = SystemBody(name: "IPv6 Link-Local Address", value: getSystemResult?.data?.ipv6_link_local_address)
        self.system.append(fake)
    }
  
    func getLANPort() {
        request.url = "https://\(aPAddress)/ap/config/network/basic"
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: GETLANresponse.self) { result in
            if result == 200
            {
                self.mLANIPAddress.removeAll(keepingCapacity: true)
                self.mLANDNSServers.removeAll(keepingCapacity: true)
                self.mLANDHCPServer.removeAll(keepingCapacity: true)
                let resultBody = self.getLANResult
               // print("getLANPort : \(resultBody)")
                let data = resultBody?.data?.ipv4
                var fake0 = LANBody(name: "IP Address", value: data?.ipv4_address)
                self.mLANIPAddress.append(fake0)
                fake0 = LANBody(name: "Subnet Mask", value: data?.ipv4_subnet_mask)
                self.mLANIPAddress.append(fake0)
                fake0 = LANBody(name: "Default Gateway", value: data?.ipv4_default_gateway)
                self.mLANIPAddress.append(fake0)
                var fake1 = LANBody(name: "Primary Address", value: data?.ipv4_dns_primary)
                self.mLANDNSServers.append(fake1)
                fake1 = LANBody(name: "Secondary Address", value: data?.ipv4_dns_secondary)
                self.mLANDNSServers.append(fake1)
                
                var fake = LANBody(name: "Starting IP Address", value: data?.dhcp_start_addr)
                self.mLANDHCPServer.append(fake)
                fake = LANBody(name: "Ending IP Address", value: data?.dhcp_end_addr)
                self.mLANDHCPServer.append(fake)
                fake = LANBody(name: "Domain Name", value: data?.dhcp_domain_name)
                self.mLANDHCPServer.append(fake)
                let leastime = webLeaseTimeArray[(data?.dhcp_lease_time)!]
    
                fake = LANBody(name: "Lease Time", value: leastime)
                self.mLANDHCPServer.append(fake)
                fake = LANBody(name: "Primary DNS", value: data?.dhcp_dns_primary)
                self.mLANDHCPServer.append(fake)
                fake = LANBody(name: "Secondary DNS", value: data?.dhcp_dns_secondary)
                self.mLANDHCPServer.append(fake)
                
                self.postLANPortBody.lanport = (resultBody?.data?.lanport)!
                self.postLANPortBody.ipv6 = (resultBody?.data?.ipv6)!
                self.postLANPortBody.ipv4 = (resultBody?.data?.ipv4)!
            }
            else
            {
//                if result == 400 {
//                    DispatchQueue.main.async {
//                    self.showErr = true
//                    self.err = "Invaild Respone Formate"
//                    self.responses = result
//                    self.response = self.responses
//                    self.getSystemResult = GETresponse(error_code:  self.response , error_msg: self.err, data: nil)
//                    }
//                }
//                else
//                {
                    DispatchQueue.main.async {
                    self.showErr = true
                    self.err = "Could not connect to the server"
                    self.responses = result
                    self.response = self.responses
                    self.getSystemResult = GETresponse(error_code:  self.response , error_msg: self.err, data: nil)
                    }
//                }
            }
        }
    }
    func postLANPort(){
        request.url = "https://\(aPAddress)/ap/config/network/basic"
        request.method = "POST"
        var data = Data()
        do{
            let encoder = JSONEncoder()
            data = try! encoder.encode(postLANPortBody)
        }
        request.body = data
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: POSTLANresponse.self) { result in
          //  print(result)
          //  NSLog("postLANResult :\(result) \(self.postLANResult)")
            if result == 200 {
                
                self.getLANPort()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0 ) {
                    self.postsysReload()
                }
            }
            else {
                DispatchQueue.main.async {
                self.showErr = true
                self.err = "Invaild Respone Formate"
                self.postLANResult = nil
                self.responses = result
                self.response = self.responses
                }
            }
        }
    }
    /*WirelessBands*/
    func initBands() {
        for iuuid in 0..<16 {
            var fake = WirelessBody(name: "SSID", value: "Sophos AP6 420E CCDD10_2_\(iuuid+1)")
            self.wireless24G.append(fake)
            fake = WirelessBody(name: "Authentication Type", value: "WPA2-AES")
            self.wireless24G.append(fake)
            fake = WirelessBody(name: "Pre-shared Key", value: "123456789012345")
            self.wireless24G.append(fake)
            fake = WirelessBody(name: "VLAN ID", value: "1")
            self.wireless24G.append(fake)
            self.post24GBandsBody.ssidNames?.append(SSID.init(ssidname: "Sophos AP6 420E CCDD10_2_\(iuuid)", index: iuuid, vlan_id: 1))
            self.post24GSSIDSecurity.append(self.post24GSSIDSecurityBody)
        }
        for iuuid in 0..<16 {
            var fake = WirelessBody(name: "SSID", value: "Sophos AP6 420E CCDD10_5_\(iuuid+1)")
            self.wireless5G.append(fake)
            fake = WirelessBody(name: "Authentication Type", value: "WPA2-AES")
            self.wireless5G.append(fake)
            fake = WirelessBody(name: "Pre-shared Key", value: "123456789012345")
            self.wireless5G.append(fake)
            fake = WirelessBody(name: "VLAN ID", value: "1")
            self.wireless5G.append(fake)
            self.post5GBandsBody.ssidNames?.append(SSID.init(ssidname: "Sophos AP6 420E CCDD10_5_\(iuuid)", index: iuuid, vlan_id: 1))
            self.post5GSSIDSecurity.append(self.post5GSSIDSecurityBody)
        }
        for iuuid in 0..<16 {
            var fake = WirelessBody(name: "SSID", value: "Sophos AP6 420E CCDD10_6_\(iuuid+1)")
            self.wireless6G.append(fake)
            fake = WirelessBody(name: "Authentication Type", value: "WPA3-AES")
            self.wireless6G.append(fake)
            fake = WirelessBody(name: "Pre-shared Key", value: "123456789012345")
            self.wireless6G.append(fake)
            fake = WirelessBody(name: "VLAN ID", value: "1")
            self.wireless6G.append(fake)
            self.post6GBandsBody.ssidNames?.append(SSID.init(ssidname: "Sophos AP6 420E CCDD10_6_\(iuuid)", index: iuuid, vlan_id: 1))
            self.post6GSSIDSecurity.append(self.post5GSSIDSecurityBody)
        }
    }
//    func get24GSSIDSecurity(ssid_index:String) {
//
//        request.url = "https://\(aPAddress)/ap/config/wireless/SSIDSecurity?radio=2g&ssid_index=\(ssid_index)"
//        request.method = "GET"
//        request.DigestAccount = DigestAccount
//        request.DigestPw = DigestPw
//        request.getResult(model: GET24GSSIDSecurityresponse.self) { result in
//          //  print(result)
//            if result == 200 {
//                self.responses = result
//              //  self.response = self.responses
//                let resultBody = self.get24GSSIDSecurityResult
//              //  NSLog("24G SSIDSecurity resultBody :ssid_index =\(ssid_index) \(resultBody?.data?.auth_detail?.psk_value)")
//                if resultBody?.error_code != 0 {
//              //      NSLog("24G SSIDSecurity resultBody err : \(resultBody?.error_code)")
//                }
//                else {
//                if (resultBody?.data?.auth_method)! == 0
//                {
//                    self.wireless24G[Int(ssid_index)!*4+1].value = "None"
//                }
//                else if (resultBody?.data?.auth_method)! == 1
//                {
//                    self.wireless24G[Int(ssid_index)!*4+1].value = "WEP"
//                }
//                else if (resultBody?.data?.auth_method)! == 2
//                {
//                    if (resultBody?.data?.auth_detail?.wpa_type)! == 5 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
//                    {
//                    self.wireless24G[Int(ssid_index)!*4+1].value = "WPA2-AES"
//                    }
//                    else if (resultBody?.data?.auth_detail?.wpa_type)! == 11 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
//                    {
//                        self.wireless24G[Int(ssid_index)!*4+1].value = "WPA2/WPA3 Mixed Mode-AES"
//                    }
//                    else if (resultBody?.data?.auth_detail?.wpa_type)! == 10 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
//                    {
//                        self.wireless24G[Int(ssid_index)!*4+1].value = "WPA3-AES"
//                    }
//                }
//                else if (resultBody?.data?.auth_method)! == 3
//                {
//                    if ((resultBody?.data?.auth_detail?.wpa_type) != nil) {
//                        if (resultBody?.data?.auth_detail?.wpa_type)! == 5 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3 {
//                            self.wireless24G[Int(ssid_index)!*4+1].value = "WPA2-EAP-AES"
//                        }
//                        else if (resultBody?.data?.auth_detail?.wpa_type)! == 10 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3 {
//                            self.wireless24G[Int(ssid_index)!*4+1].value = "WPA3-EAP-AES"
//                        }
//                    }
//                    else {
//                        self.wireless24G[Int(ssid_index)!*4+1].value = "WPA2-EAP-AES"
//                    }
//                }
//                else if (resultBody?.data?.auth_method)! == 5
//                {
//                    self.wireless24G[Int(ssid_index)!*4+1].value = "OWE"
//                }
//                self.wireless24G[Int(ssid_index)!*4+2].value = (resultBody?.data?.auth_detail?.psk_value ?? "")!
//                    self.post24GSSIDSecurityBody.ssid_index = resultBody?.data?.ssid_index
//                    self.post24GSSIDSecurityBody.broadcast_ssid = resultBody?.data?.broadcast_ssid
//                    self.post24GSSIDSecurityBody.client_isolation = resultBody?.data?.client_isolation
//                    self.post24GSSIDSecurityBody.ieee80211k = resultBody?.data?.ieee80211k
//                    self.post24GSSIDSecurityBody.ieee80211w = resultBody?.data?.ieee80211w
//                    self.post24GSSIDSecurityBody.load_balance = resultBody?.data?.load_balance
//                    self.post24GSSIDSecurityBody.auth_method = resultBody?.data?.auth_method
//                    self.post24GSSIDSecurityBody.auth_detail = resultBody?.data?.auth_detail
//                    self.post24GSSIDSecurityBody.additional_auth = resultBody?.data?.additional_auth
//                    self.post24GSSIDSecurityBody.mac_radius_auth = resultBody?.data?.mac_radius_auth
//                    self.post24GSSIDSecurityBody.mac_radius_password = resultBody?.data?.mac_radius_password
//                    self.post24GSSIDSecurity[Int(ssid_index)!] =  self.post24GSSIDSecurityBody
//                }
//            }
//            else
//            {
//                DispatchQueue.main.async {
//                self.showErr = true
//                self.err = "Could not connect to the server"
//                self.responses = result
//                self.response = self.responses
//                self.get24GSSIDSecurityResult = nil
//                }
//            }
//        }
//    }
 //   func get5GSSIDSecurity(ssid_index:String) {
//        request.url = "https://\(aPAddress)/ap/config/wireless/SSIDSecurity?radio=5g&ssid_index=\(ssid_index)"
//        request.method = "GET"
//        request.DigestAccount = DigestAccount
//        request.DigestPw = DigestPw
//        request.getResult(model: GET5GSSIDSecurityresponse.self) { result in
//           // print(result)
//            if result == 200 {
//                self.responses = result
//                self.response = self.responses
//                let resultBody = self.get5GSSIDSecurityResult
//               // NSLog("5G SSIDSecurity resultBody :ssid_index =\(ssid_index) \(resultBody?.data?.auth_detail?.psk_value)")
//                //["None","WPA/WPA2 Mixed Mode-AES","WPA2-AES","WPA2/WPA3 Mixed Mode-AES","WPA3-AES","WPA/WPA2 Mixed Mode-EAP-AES","WPA2-EAP-AES","WPA3-EAP-AES","OWE"]
//                if resultBody?.error_code != 0
//                {
//               //     NSLog("5G SSIDSecurity resultBody err : \(resultBody?.error_code)")
//                }
//                else{
//                if (resultBody?.data?.auth_method)! == 0
//                {
//                    self.wireless5G[Int(ssid_index)!*4+1].value = "None"
//                }
//                else if (resultBody?.data?.auth_method)! == 1
//                {
//                    self.wireless5G[Int(ssid_index)!*4+1].value = "WEP"
//                }
//
//                else if (resultBody?.data?.auth_method)! == 2
//                {
//                    if (resultBody?.data?.auth_detail?.wpa_type)! == 5 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
//                    {
//                        self.wireless5G[Int(ssid_index)!*4+1].value = "WPA2-AES"
//                    }
//                    else if (resultBody?.data?.auth_detail?.wpa_type)! == 11 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
//                    {
//                        self.wireless5G[Int(ssid_index)!*4+1].value = "WPA2/WPA3 Mixed Mode-AES"
//                    }
//                    else if  (resultBody?.data?.auth_detail?.wpa_type)! == 10 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
//                    {
//                        self.wireless5G[Int(ssid_index)!*4+1].value = "WPA3-AES"
//                    }
//                }
//                else if (resultBody?.data?.auth_method)! == 3
//                {
//                    if ((resultBody?.data?.auth_detail?.wpa_type) != nil) {
//                        if (resultBody?.data?.auth_detail?.wpa_type)! == 5 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3 {
//                        self.wireless5G[Int(ssid_index)!*4+1].value = "WPA2-EAP-AES"
//                        }
//                        else if (resultBody?.data?.auth_detail?.wpa_type)! == 10 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3 {
//                            self.wireless5G[Int(ssid_index)!*4+1].value = "WPA3-EAP-AES"
//                        }
//                    }
//                    else {
//                        self.wireless5G[Int(ssid_index)!*4+1].value = "WPA2-EAP-AES"
//                    }
//                }
//                else if (resultBody?.data?.auth_method)! == 5
//                {
//                    self.wireless5G[Int(ssid_index)!*4+1].value = "OWE"
//                }
//                self.wireless5G[Int(ssid_index)!*4+2].value = (resultBody?.data?.auth_detail?.psk_value ?? "")!
//                self.post5GSSIDSecurityBody.ssid_index = resultBody?.data?.ssid_index
//                self.post5GSSIDSecurityBody.broadcast_ssid = resultBody?.data?.broadcast_ssid
//                self.post5GSSIDSecurityBody.client_isolation = resultBody?.data?.client_isolation
//                self.post5GSSIDSecurityBody.ieee80211k = resultBody?.data?.ieee80211k
//                self.post5GSSIDSecurityBody.ieee80211w = resultBody?.data?.ieee80211w
//                self.post5GSSIDSecurityBody.load_balance = resultBody?.data?.load_balance
//                self.post5GSSIDSecurityBody.auth_method = resultBody?.data?.auth_method
//                self.post5GSSIDSecurityBody.auth_detail = resultBody?.data?.auth_detail
//                self.post5GSSIDSecurityBody.additional_auth = resultBody?.data?.additional_auth
//                self.post5GSSIDSecurityBody.mac_radius_auth = resultBody?.data?.mac_radius_auth
//                self.post5GSSIDSecurityBody.mac_radius_password = resultBody?.data?.mac_radius_password
//                self.post5GSSIDSecurity[Int(ssid_index)!] =  self.post5GSSIDSecurityBody
//               }
//            }
//            else
//            {
//                DispatchQueue.main.async {
//                self.showErr = true
//                self.err = "Could not connect to the server"
//                self.responses = result
//                self.response = self.responses
//                self.get5GSSIDSecurityResult = nil
//                }
//            }
//        }
 //   }
//    func get6GSSIDSecurity(ssid_index:String) {
//        request.url = "https://\(aPAddress)/ap/config/wireless/SSIDSecurity?radio=6g&ssid_index=\(ssid_index)"
//        request.method = "GET"
//        request.DigestAccount = DigestAccount
//        request.DigestPw = DigestPw
//        request.getResult(model: GET6GSSIDSecurityresponse.self) { result in
//          //  print(result)
//            if result == 200 {
//                self.responses = result
//                self.response = self.responses
//                let resultBody = self.get6GSSIDSecurityResult
//             //   NSLog("6G SSIDSecurity resultBody : ssid_index =\(ssid_index) \(resultBody?.data?.auth_detail?.psk_value)")
//                if resultBody?.error_code != 0
//                {
//                   // NSLog("6G SSIDSecurity resultBody err : \(resultBody?.error_code)")
//                }
//                else{
//                if (resultBody?.data?.auth_method)! == 2
//                {
//                    if (resultBody?.data?.auth_detail?.wpa_type)! == 10 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
//                    {
//                        self.wireless6G[Int(ssid_index)!*4+1].value = "WPA3-AES"
//                    }
//                }
//
//                else if (resultBody?.data?.auth_method)! == 3
//                {
//                    if ((resultBody?.data?.auth_detail?.wpa_type) != nil) {
//                        if (resultBody?.data?.auth_detail?.wpa_type)! == 10 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
//                        {
//                            self.wireless6G[Int(ssid_index)!*4+1].value = "WPA3-EAP-AES"
//                        }
//                    }
//                    else {
//                        self.wireless6G[Int(ssid_index)!*4+1].value = "WPA3-EAP-AES"
//                    }
//
//                }
//                else if (resultBody?.data?.auth_method)! == 5
//                {
//                    self.wireless6G[Int(ssid_index)!*4+1].value = "OWE"
//                }
//
//                self.wireless6G[Int(ssid_index)!*4+2].value = (resultBody?.data?.auth_detail?.psk_value ?? "")!
//                self.post6GSSIDSecurityBody.ssid_index = resultBody?.data?.ssid_index
//                self.post6GSSIDSecurityBody.broadcast_ssid = resultBody?.data?.broadcast_ssid
//                self.post6GSSIDSecurityBody.client_isolation = resultBody?.data?.client_isolation
//                self.post6GSSIDSecurityBody.ieee80211k = resultBody?.data?.ieee80211k
//                self.post6GSSIDSecurityBody.ieee80211w = resultBody?.data?.ieee80211w
//                self.post6GSSIDSecurityBody.load_balance = resultBody?.data?.load_balance
//                self.post6GSSIDSecurityBody.auth_method = resultBody?.data?.auth_method
//                self.post6GSSIDSecurityBody.auth_detail = resultBody?.data?.auth_detail
//                self.post6GSSIDSecurityBody.additional_auth = resultBody?.data?.additional_auth
//                self.post6GSSIDSecurityBody.mac_radius_auth = resultBody?.data?.mac_radius_auth
//                self.post6GSSIDSecurityBody.mac_radius_password = resultBody?.data?.mac_radius_password
//                self.post6GSSIDSecurity[Int(ssid_index)!] =  self.post6GSSIDSecurityBody
//                }
//            }
//            else {
//                DispatchQueue.main.async {
//                self.showErr = true
//                self.err = "Could not connect to the server"
//                self.responses = result
//                self.response = self.responses
//                self.get6GSSIDSecurityResult = nil
//                }
//            }
//        }
//    }
    func getSSIDs(model:String) {
        request.url = "https://\(aPAddress)/ap/config/wireless/basic?radio=2g"
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: GET24GBandsresponse.self) { result in
          //  print(result)
            if result == 200 {
                self.responses = result
              //  self.response = self.responses
                let resultBody = self.get24GBandsResult
               // NSLog("24G resultBody : \(resultBody?.data?.enabled_ssidNum)")
                for iuuid in 0..<(resultBody?.data?.enabled_ssidNum)! {
                    self.wireless24G[iuuid*4+0].value = (resultBody?.data?.ssidNames![iuuid].ssidname)!
                    self.wireless24G[iuuid*4+3].value = (resultBody?.data?.ssidNames![iuuid].vlan_id)!.description
                }
                self.post24GBandsBody.radio_enable = resultBody?.data?.radio_enable
                self.post24GBandsBody.radio_band = resultBody?.data?.radio_band
                self.post24GBandsBody.enabled_ssidNum = resultBody?.data?.enabled_ssidNum
                for iuuid in 0..<(resultBody?.data?.ssidNames?.count)! {
                    self.post24GBandsBody.ssidNames![iuuid] = (resultBody?.data?.ssidNames![iuuid])!
                }
                self.post24GBandsBody.autochannel_enabled = resultBody?.data?.autochannel_enabled
                self.post24GBandsBody.autochannel_range = resultBody?.data?.autochannel_range
                self.post24GBandsBody.autochannel_interval = resultBody?.data?.autochannel_interval
                self.post24GBandsBody.autochannel_force_enabled = resultBody?.data?.autochannel_force_enabled
                self.post24GBandsBody.channel = resultBody?.data?.channel
                self.post24GBandsBody.channel_bandwidth = resultBody?.data?.channel_bandwidth
                self.post24GBandsBody.bss_basicrateset = resultBody?.data?.bss_basicrateset
//                for i in 0..<(resultBody?.data?.enabled_ssidNum)! {
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [unowned self] in
//                       self.get24GSSIDSecurity(ssid_index:"\(i)")
//                    }
//                }
                for iuuid in 0..<(resultBody?.data?.enabled_ssidNum)! {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [unowned self] in
                        let ssid_index = "\(iuuid)"
                        request.url = "https://\(aPAddress)/ap/config/wireless/SSIDSecurity?radio=2g&ssid_index=\(ssid_index)"
                        request.method = "GET"
                        request.mDigestAccount = mDigestAccount
                        request.mDigestPw = mDigestPw
                        request.getResult(model: GET24GSSIDSecurityresponse.self) { result in
                          //  print(result)
                            if result == 200 {
                                self.responses = result
                              //  self.response = self.responses
                                let resultBody = self.get24GSSIDSecurityResult
                              //  NSLog("24G SSIDSecurity resultBody :ssid_index =\(ssid_index) \(resultBody?.data?.auth_detail?.psk_value)")
                                if resultBody?.error_code != 0 {
                              //      NSLog("24G SSIDSecurity resultBody err : \(resultBody?.error_code)")
                                }
                                else {
                                if (resultBody?.data?.auth_method)! == 0
                                {
                                    self.wireless24G[Int(ssid_index)!*4+1].value = "None"
                                }
                                else if (resultBody?.data?.auth_method)! == 1
                                {
                                    self.wireless24G[Int(ssid_index)!*4+1].value = "WEP"
                                }
                                else if (resultBody?.data?.auth_method)! == 2
                                {
                                    if (resultBody?.data?.auth_detail?.wpa_type)! == 5 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
                                    {
                                    self.wireless24G[Int(ssid_index)!*4+1].value = "WPA2-AES"
                                    }
                                    else if (resultBody?.data?.auth_detail?.wpa_type)! == 11 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
                                    {
                                        self.wireless24G[Int(ssid_index)!*4+1].value = "WPA2/WPA3 Mixed Mode-AES"
                                    }
                                    else if (resultBody?.data?.auth_detail?.wpa_type)! == 10 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
                                    {
                                        self.wireless24G[Int(ssid_index)!*4+1].value = "WPA3-AES"
                                    }
                                }
                                else if (resultBody?.data?.auth_method)! == 3
                                {
                                    if ((resultBody?.data?.auth_detail?.wpa_type) != nil) {
                                        if (resultBody?.data?.auth_detail?.wpa_type)! == 5 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3 {
                                            self.wireless24G[Int(ssid_index)!*4+1].value = "WPA2-EAP-AES"
                                        }
                                        else if (resultBody?.data?.auth_detail?.wpa_type)! == 10 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3 {
                                            self.wireless24G[Int(ssid_index)!*4+1].value = "WPA3-EAP-AES"
                                        }
                                    }
                                    else {
                                        self.wireless24G[Int(ssid_index)!*4+1].value = "WPA2-EAP-AES"
                                    }
                                }
                                else if (resultBody?.data?.auth_method)! == 5
                                {
                                    self.wireless24G[Int(ssid_index)!*4+1].value = "OWE"
                                }
                                self.wireless24G[Int(ssid_index)!*4+2].value = (resultBody?.data?.auth_detail?.psk_value ?? "")!
                                    self.post24GSSIDSecurityBody.ssid_index = resultBody?.data?.ssid_index
                                    self.post24GSSIDSecurityBody.broadcast_ssid = resultBody?.data?.broadcast_ssid
                                    self.post24GSSIDSecurityBody.client_isolation = resultBody?.data?.client_isolation
                                    self.post24GSSIDSecurityBody.ieee80211k = resultBody?.data?.ieee80211k
                                    self.post24GSSIDSecurityBody.ieee80211w = resultBody?.data?.ieee80211w
                                    self.post24GSSIDSecurityBody.load_balance = resultBody?.data?.load_balance
                                    self.post24GSSIDSecurityBody.auth_method = resultBody?.data?.auth_method
                                    self.post24GSSIDSecurityBody.auth_detail = resultBody?.data?.auth_detail
                                    self.post24GSSIDSecurityBody.additional_auth = resultBody?.data?.additional_auth
                                    self.post24GSSIDSecurityBody.mac_radius_auth = resultBody?.data?.mac_radius_auth
                                    self.post24GSSIDSecurityBody.mac_radius_password = resultBody?.data?.mac_radius_password
                                    self.post24GSSIDSecurity[Int(ssid_index)!] =  self.post24GSSIDSecurityBody
                                }
                            }
                            else
                            {
                                DispatchQueue.main.async {
                                self.showErr = true
                                self.err = "Could not connect to the server"
                                self.responses = result
                                self.response = self.responses
                                self.get24GSSIDSecurityResult = nil
                                }
                            }
                        }
                    }
                }
                
                if model.contains("420") {
                    for iuuid in (resultBody?.data?.enabled_ssidNum)!..<8 {
                        self.wireless24G[iuuid*4+0].value = "\((resultBody?.data?.ssidNames![0].ssidname)!)_\(iuuid+1)"
                    }
                }
                else if model.contains("840") {
                    for iuuid in (resultBody?.data?.enabled_ssidNum)!..<16 {
                        self.wireless24G[iuuid*4+0].value = "\((resultBody?.data?.ssidNames![0].ssidname)!)_\(iuuid+1)"
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    self.showErr = true
                    self.err = "Could not connect to the server"
                    self.responses = result
                    self.response = self.responses
                    self.get24GBandsResult = nil
                }
            }
        }
        request.url = "https://\(aPAddress)/ap/config/wireless/basic?radio=5g"
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: GET5GBandsresponse.self) { result in
          //  print(result)
            if result == 200 {
                self.responses = result
               // self.response = self.responses
                let resultBody = self.get5GBandsResult
              //  NSLog("5G resultBody : \(resultBody?.data?.enabled_ssidNum)")
                for iuuid in 0..<(resultBody?.data?.enabled_ssidNum)! {
                    self.wireless5G[iuuid*4+0].value = (resultBody?.data?.ssidNames![iuuid].ssidname)!
                    self.wireless5G[iuuid*4+3].value = (resultBody?.data?.ssidNames![iuuid].vlan_id)!.description
                }
                self.post5GBandsBody.radio_enable = resultBody?.data?.radio_enable
                self.post5GBandsBody.radio_band = resultBody?.data?.radio_band
                self.post5GBandsBody.enabled_ssidNum = resultBody?.data?.enabled_ssidNum
                for iuuid in 0..<(resultBody?.data?.ssidNames?.count)! {
                    self.post5GBandsBody.ssidNames![iuuid] = (resultBody?.data?.ssidNames![iuuid])!
                }
                self.post5GBandsBody.autochannel_enabled = resultBody?.data?.autochannel_enabled
                self.post5GBandsBody.autochannel_range = resultBody?.data?.autochannel_range
                self.post5GBandsBody.autochannel_interval = resultBody?.data?.autochannel_interval
                self.post5GBandsBody.autochannel_force_enabled = resultBody?.data?.autochannel_force_enabled
                self.post5GBandsBody.channel = resultBody?.data?.channel
                self.post5GBandsBody.channel_bandwidth = resultBody?.data?.channel_bandwidth
                self.post5GBandsBody.bss_basicrateset = resultBody?.data?.bss_basicrateset
//                for i in 0..<(resultBody?.data?.ssidNames?.count)! {
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.100) { [unowned self] in
//                       self.get5GSSIDSecurity(ssid_index:"\(i)")
//                    }
//                }
                for iuuid in 0..<(resultBody?.data?.enabled_ssidNum)! {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [unowned self] in
                        let ssid_index = "\(iuuid)"
                        request.url = "https://\(aPAddress)/ap/config/wireless/SSIDSecurity?radio=5g&ssid_index=\(ssid_index)"
                        request.method = "GET"
                        request.mDigestAccount = mDigestAccount
                        request.mDigestPw = mDigestPw
                        request.getResult(model: GET5GSSIDSecurityresponse.self) { result in
                           // print(result)
                            if result == 200 {
                                self.responses = result
                                self.response = self.responses
                                let resultBody = self.get5GSSIDSecurityResult
                               // NSLog("5G SSIDSecurity resultBody :ssid_index =\(ssid_index) \(resultBody?.data?.auth_detail?.psk_value)")
                                //["None","WPA/WPA2 Mixed Mode-AES","WPA2-AES","WPA2/WPA3 Mixed Mode-AES","WPA3-AES","WPA/WPA2 Mixed Mode-EAP-AES","WPA2-EAP-AES","WPA3-EAP-AES","OWE"]
                                if resultBody?.error_code != 0
                                {
                               //     NSLog("5G SSIDSecurity resultBody err : \(resultBody?.error_code)")
                                }
                                else{
                                if (resultBody?.data?.auth_method)! == 0
                                {
                                    self.wireless5G[Int(ssid_index)!*4+1].value = "None"
                                }
                                else if (resultBody?.data?.auth_method)! == 1
                                {
                                    self.wireless5G[Int(ssid_index)!*4+1].value = "WEP"
                                }
                                
                                else if (resultBody?.data?.auth_method)! == 2
                                {
                                    if (resultBody?.data?.auth_detail?.wpa_type)! == 5 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
                                    {
                                        self.wireless5G[Int(ssid_index)!*4+1].value = "WPA2-AES"
                                    }
                                    else if (resultBody?.data?.auth_detail?.wpa_type)! == 11 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
                                    {
                                        self.wireless5G[Int(ssid_index)!*4+1].value = "WPA2/WPA3 Mixed Mode-AES"
                                    }
                                    else if  (resultBody?.data?.auth_detail?.wpa_type)! == 10 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
                                    {
                                        self.wireless5G[Int(ssid_index)!*4+1].value = "WPA3-AES"
                                    }
                                }
                                else if (resultBody?.data?.auth_method)! == 3
                                {
                                    if ((resultBody?.data?.auth_detail?.wpa_type) != nil) {
                                        if (resultBody?.data?.auth_detail?.wpa_type)! == 5 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3 {
                                        self.wireless5G[Int(ssid_index)!*4+1].value = "WPA2-EAP-AES"
                                        }
                                        else if (resultBody?.data?.auth_detail?.wpa_type)! == 10 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3 {
                                            self.wireless5G[Int(ssid_index)!*4+1].value = "WPA3-EAP-AES"
                                        }
                                    }
                                    else {
                                        self.wireless5G[Int(ssid_index)!*4+1].value = "WPA2-EAP-AES"
                                    }
                                }
                                else if (resultBody?.data?.auth_method)! == 5
                                {
                                    self.wireless5G[Int(ssid_index)!*4+1].value = "OWE"
                                }
                                self.wireless5G[Int(ssid_index)!*4+2].value = (resultBody?.data?.auth_detail?.psk_value ?? "")!
                                self.post5GSSIDSecurityBody.ssid_index = resultBody?.data?.ssid_index
                                self.post5GSSIDSecurityBody.broadcast_ssid = resultBody?.data?.broadcast_ssid
                                self.post5GSSIDSecurityBody.client_isolation = resultBody?.data?.client_isolation
                                self.post5GSSIDSecurityBody.ieee80211k = resultBody?.data?.ieee80211k
                                self.post5GSSIDSecurityBody.ieee80211w = resultBody?.data?.ieee80211w
                                self.post5GSSIDSecurityBody.load_balance = resultBody?.data?.load_balance
                                self.post5GSSIDSecurityBody.auth_method = resultBody?.data?.auth_method
                                self.post5GSSIDSecurityBody.auth_detail = resultBody?.data?.auth_detail
                                self.post5GSSIDSecurityBody.additional_auth = resultBody?.data?.additional_auth
                                self.post5GSSIDSecurityBody.mac_radius_auth = resultBody?.data?.mac_radius_auth
                                self.post5GSSIDSecurityBody.mac_radius_password = resultBody?.data?.mac_radius_password
                                self.post5GSSIDSecurity[Int(ssid_index)!] =  self.post5GSSIDSecurityBody
                               }
                            }
                            else
                            {
                                DispatchQueue.main.async {
                                self.showErr = true
                                self.err = "Could not connect to the server"
                                self.responses = result
                                self.response = self.responses
                                self.get5GSSIDSecurityResult = nil
                                }
                            }
                        }
                    }
                }
                if model.contains("420") {
                    for iuuid in (resultBody?.data?.enabled_ssidNum)!..<8 {
                        self.wireless5G[iuuid*4+0].value = "\((resultBody?.data?.ssidNames![0].ssidname)!)_\(iuuid+1)"
                    }
                }
                else if model.contains("840") {
                    for iuuid in (resultBody?.data?.enabled_ssidNum)!..<16 {
                        self.wireless5G[iuuid*4+0].value = "\((resultBody?.data?.ssidNames![0].ssidname)!)_\(iuuid+1)"
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    self.showErr = true
                    self.err = "Could not connect to the server"
                    self.responses = result
                    self.response = self.responses
                    self.get5GBandsResult = nil
                }
            }
        }
        if support6G.contains(model) {
        request.url = "https://\(aPAddress)/ap/config/wireless/basic?radio=6g"
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: GET6GBandsresponse.self) { result in
           // print(result)
            if result == 200 {
                self.responses = result
                self.response = self.responses
                let resultBody = self.get6GBandsResult
               // NSLog("6G resultBody : \(resultBody?.data?.enabled_ssidNum)")
                for iuuid in 0..<(resultBody?.data?.enabled_ssidNum)! {
                    self.wireless6G[iuuid*4+0].value = (resultBody?.data?.ssidNames![iuuid].ssidname)!
                    self.wireless6G[iuuid*4+3].value = (resultBody?.data?.ssidNames![iuuid].vlan_id)!.description
                }
                self.post6GBandsBody.radio_enable = resultBody?.data?.radio_enable
                self.post6GBandsBody.radio_band = resultBody?.data?.radio_band
                self.post6GBandsBody.enabled_ssidNum = resultBody?.data?.enabled_ssidNum
                for iuuid in 0..<(resultBody?.data?.ssidNames?.count)! {
                    self.post6GBandsBody.ssidNames![iuuid] = (resultBody?.data?.ssidNames![iuuid])!
                }
                self.post6GBandsBody.autochannel_enabled = resultBody?.data?.autochannel_enabled
                self.post6GBandsBody.autochannel_range = resultBody?.data?.autochannel_range
                self.post6GBandsBody.autochannel_interval = resultBody?.data?.autochannel_interval
                self.post6GBandsBody.autochannel_force_enabled = resultBody?.data?.autochannel_force_enabled
                self.post6GBandsBody.channel = resultBody?.data?.channel
                self.post6GBandsBody.channel_bandwidth = resultBody?.data?.channel_bandwidth
                self.post6GBandsBody.bss_basicrateset = resultBody?.data?.bss_basicrateset
//                for i in 0..<(resultBody?.data?.ssidNames?.count)! {
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.100) { [unowned self] in
//                       self.get6GSSIDSecurity(ssid_index:"\(i)")
//                    }
//                }
                for iuuid in 0..<(resultBody?.data?.enabled_ssidNum)! {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [unowned self] in
                        let ssid_index = "\(iuuid)"
                        request.url = "https://\(aPAddress)/ap/config/wireless/SSIDSecurity?radio=6g&ssid_index=\(ssid_index)"
                        request.method = "GET"
                        request.mDigestAccount = mDigestAccount
                        request.mDigestPw = mDigestPw
                        request.getResult(model: GET6GSSIDSecurityresponse.self) { result in
                          //  print(result)
                            if result == 200 {
                                self.responses = result
                                self.response = self.responses
                                let resultBody = self.get6GSSIDSecurityResult
                             //   NSLog("6G SSIDSecurity resultBody : ssid_index =\(ssid_index) \(resultBody?.data?.auth_detail?.psk_value)")
                                if resultBody?.error_code != 0
                                {
                                   // NSLog("6G SSIDSecurity resultBody err : \(resultBody?.error_code)")
                                }
                                else{
                                if (resultBody?.data?.auth_method)! == 2
                                {
                                    if (resultBody?.data?.auth_detail?.wpa_type)! == 10 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
                                    {
                                        self.wireless6G[Int(ssid_index)!*4+1].value = "WPA3-AES"
                                    }
                                }
                                
                                else if (resultBody?.data?.auth_method)! == 3
                                {
                                    if ((resultBody?.data?.auth_detail?.wpa_type) != nil) {
                                        if (resultBody?.data?.auth_detail?.wpa_type)! == 10 && (resultBody?.data?.auth_detail?.encrypt_type)! == 3
                                        {
                                            self.wireless6G[Int(ssid_index)!*4+1].value = "WPA3-EAP-AES"
                                        }
                                    }
                                    else {
                                        self.wireless6G[Int(ssid_index)!*4+1].value = "WPA3-EAP-AES"
                                    }
                                        
                                }
                                else if (resultBody?.data?.auth_method)! == 5
                                {
                                    self.wireless6G[Int(ssid_index)!*4+1].value = "OWE"
                                }
                                
                                self.wireless6G[Int(ssid_index)!*4+2].value = (resultBody?.data?.auth_detail?.psk_value ?? "")!
                                self.post6GSSIDSecurityBody.ssid_index = resultBody?.data?.ssid_index
                                self.post6GSSIDSecurityBody.broadcast_ssid = resultBody?.data?.broadcast_ssid
                                self.post6GSSIDSecurityBody.client_isolation = resultBody?.data?.client_isolation
                                self.post6GSSIDSecurityBody.ieee80211k = resultBody?.data?.ieee80211k
                                self.post6GSSIDSecurityBody.ieee80211w = resultBody?.data?.ieee80211w
                                self.post6GSSIDSecurityBody.load_balance = resultBody?.data?.load_balance
                                self.post6GSSIDSecurityBody.auth_method = resultBody?.data?.auth_method
                                self.post6GSSIDSecurityBody.auth_detail = resultBody?.data?.auth_detail
                                self.post6GSSIDSecurityBody.additional_auth = resultBody?.data?.additional_auth
                                self.post6GSSIDSecurityBody.mac_radius_auth = resultBody?.data?.mac_radius_auth
                                self.post6GSSIDSecurityBody.mac_radius_password = resultBody?.data?.mac_radius_password
                                self.post6GSSIDSecurity[Int(ssid_index)!] =  self.post6GSSIDSecurityBody
                                }
                            }
                            else {
                                DispatchQueue.main.async {
                                self.showErr = true
                                self.err = "Could not connect to the server"
                                self.responses = result
                                self.response = self.responses
                                self.get6GSSIDSecurityResult = nil
                                }
                            }
                        }
                    }
                }
                if model.contains("420") {
                    for iuuid in (resultBody?.data?.enabled_ssidNum)!..<8 {
                        self.wireless6G[iuuid*4+0].value = "\((resultBody?.data?.ssidNames![0].ssidname)!)_\(iuuid+1)"
                    }
                }
                else if model.contains("840") {
                    for iuuid in (resultBody?.data?.enabled_ssidNum)!..<16 {
                        self.wireless6G[iuuid*4+0].value = "\((resultBody?.data?.ssidNames![0].ssidname)!)_\(iuuid+1)"
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    self.showErr = true
                    self.err = "Could not connect to the server"
                    self.responses = result
                    self.response = self.responses
                    self.get6GBandsResult = nil
                }
            }
        }
        }
    }
    func post24GBands(){
        request.url = "https://\(aPAddress)/ap/config/wireless/basic?radio=2g"
        request.method = "POST"
        var data = Data()
        do{
            let encoder = JSONEncoder()
            data = try! encoder.encode(post24GBandsBody)
           // NSLog("post24GBandsBody : \(post24GBandsBody)")
        }
        request.body = data
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: POST24GBandresponse.self) { result in
           // print(result)
           // NSLog("OffWiFi post24GBands_response :\(result) \(self.getOffWiFiResult)")
            if result == 200 {
                self.getOffWiFiResult += 1
            }
            else {
                DispatchQueue.main.async {
                    self.showErr = true
                    self.err = "Could not connect to the server"
                    self.responses = result
                    self.response = self.responses
                }
            }
        }
    }
//    func post24GSecurity(ssid_index:String){
//
//        request.url = "https://\(aPAddress)/ap/config/wireless/SSIDSecurity?radio=2g&ssid_index=\(ssid_index)"
//        request.method = "POST"
//        var data = Data()
//        do{
//            let encoder = JSONEncoder()
//            data = try! encoder.encode(post24GSSIDSecurity[Int(ssid_index)!])
//
//        }
//        request.body = data
//        request.DigestAccount = DigestAccount
//        request.DigestPw = DigestPw
//        request.getResult(model: POST24GSecurityresponse.self) { result in
//          //  print(result)
//           // NSLog("post24GSecurity:\(ssid_index)")
//            if result == 200 {
//
//            }
//            else
//            {
//                DispatchQueue.main.async {
//                    self.showErr = true
//                    self.err = "Could not connect to the server"
//                    self.responses = result
//                    self.response = self.responses
//                }
//            }
//        }
//    }
    func post5GBands(){
        
        request.url = "https://\(aPAddress)/ap/config/wireless/basic?radio=5g"
        request.method = "POST"
        var data = Data()
        do{
            let encoder = JSONEncoder()
            data = try! encoder.encode(post5GBandsBody)
        
        }
        request.body = data
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: POST5GBandresponse.self) { result in
          //  print(result)
          //  NSLog("OffWiFi post5GBands_response :\(result) \(self.getOffWiFiResult)")
            if result == 200 {
                self.getOffWiFiResult += 1
            }
            else
            {
                DispatchQueue.main.async {
                    self.showErr = true
                    self.err = "Could not connect to the server"
                    self.responses = result
                    self.response = self.responses
                }
            }
        }
    }
//    func post5GSecurity(ssid_index:String){
//
//        request.url = "https://\(aPAddress)/ap/config/wireless/SSIDSecurity?radio=5g&ssid_index=\(ssid_index)"
//        request.method = "POST"
//        var data = Data()
//        do{
//            let encoder = JSONEncoder()
//            data = try! encoder.encode(post5GSSIDSecurity[Int(ssid_index)!])
//
//        }
//        request.body = data
//        request.DigestAccount = DigestAccount
//        request.DigestPw = DigestPw
//        request.getResult(model: POST5GSecurityresponse.self) { result in
//         //   print(result)
//         //   NSLog("post5GSecurity:\(ssid_index)")
//            if result == 200 {
//
//            }
//            else
//            {
//                DispatchQueue.main.async {
//                    self.showErr = true
//                    self.err = "Could not connect to the server"
//                    self.responses = result
//                    self.response = self.responses
//                }
//            }
//        }
//    }
    func post6GBands(){
        
        request.url = "https://\(aPAddress)/ap/config/wireless/basic?radio=6g"
        request.method = "POST"
        var data = Data()
        do{
            let encoder = JSONEncoder()
            data = try! encoder.encode(post6GBandsBody)
        
        }
        request.body = data
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: POST6GBandresponse.self) { result in
          //  print(result)
          //  NSLog("OffWiFi post6GBands_response :\(result) \(self.getOffWiFiResult)")
            if result == 200 {
                self.getOffWiFiResult += 1
            }
            else
            {
                DispatchQueue.main.async {
                    self.showErr = true
                    self.err = "Could not connect to the server"
                    self.responses = result
                    self.response = self.responses
                }
            }
        }
    }
//    func post6GSecurity(ssid_index:String){
//
//        request.url = "https://\(aPAddress)/ap/config/wireless/SSIDSecurity?radio=6g&ssid_index=\(ssid_index)"
//        request.method = "POST"
//        var data = Data()
//        do{
//            let encoder = JSONEncoder()
//            data = try! encoder.encode(post6GSSIDSecurity[Int(ssid_index)!])
//
//        }
//        request.body = data
//        request.DigestAccount = DigestAccount
//        request.DigestPw = DigestPw
//        request.getResult(model: POST6GSecurityresponse.self) { result in
//         //   print(result)
//         //   NSLog("post6GSecurity:\(ssid_index)")
//            if result == 200 {
//
//            }
//            else
//            {
//                DispatchQueue.main.async {
//                    self.showErr = true
//                    self.err = "Could not connect to the server"
//                    self.responses = result
//                    self.response = self.responses
//                }
//            }
//        }
//    }
    func postSSIDs(model:String) {
        self.postfWiFiResult = 0
        request.url = "https://\(aPAddress)/ap/config/wireless/basic?radio=2g"
        request.method = "POST"
        var data = Data()
        do{
            let encoder = JSONEncoder()
            data = try! encoder.encode(post24GBandsBody)
        
        }
        request.body = data
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: POST24GBandresponse.self) { [self] result in
        //    print(result)
        //    NSLog("OffWiFi post24GBands_response :\(result) \(self.getOffWiFiResult)")
            if result == 200 {
                self.postfWiFiResult += 1
                for ssid_index in 0..<(self.post24GBandsBody.enabled_ssidNum)! {
                    self.request.url = "https://\(self.aPAddress)/ap/config/wireless/SSIDSecurity?radio=2g&ssid_index=\(ssid_index)"
                    self.request.method = "POST"
                    var data = Data()
                    do{
                        let encoder = JSONEncoder()
                        data = try! encoder.encode(self.post24GSSIDSecurity[Int(exactly: ssid_index)!])
                    
                    }
                    self.request.body = data
                    self.request.mDigestAccount = self.mDigestAccount
                    self.request.mDigestPw = self.mDigestPw
                    self.request.getResult(model: POST24GSecurityresponse.self) { result in
                    //    print(result)
                    //    NSLog("post24GSecurity:\(ssid_index)")
                        if result == 200 {
                            self.postfWiFiResult += 1
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                self.showErr = true
                                self.err = "Could not connect to the server"
                                self.responses = result
                                self.response = self.responses
                            }
                        }
                    }
                }
            }
            else
            {
                DispatchQueue.main.async {
                self.showErr = true
                self.err = "Could not connect to the server"
                self.responses = result
                self.response = self.responses
                }
            }
        }
        
        request.url = "https://\(aPAddress)/ap/config/wireless/basic?radio=5g"
        request.method = "POST"
        var data5G = Data()
        do{
            let encoder = JSONEncoder()
            data5G = try! encoder.encode(post5GBandsBody)
        
        }
        request.body = data5G
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: POST5GBandresponse.self) { result in
         //   print(result)
         //   NSLog("OffWiFi post5GBands_response :\(result) \(self.getOffWiFiResult)")
            if result == 200 {
                self.postfWiFiResult += 1
                for ssid_index in 0..<(self.post5GBandsBody.enabled_ssidNum)! {
                    self.request.url = "https://\(self.aPAddress)/ap/config/wireless/SSIDSecurity?radio=5g&ssid_index=\(ssid_index)"
                    self.request.method = "POST"
                    var data = Data()
                    do{
                        let encoder = JSONEncoder()
                        data = try! encoder.encode(self.post5GSSIDSecurity[Int(exactly: ssid_index)!])
                    
                    }
                    self.request.body = data
                    self.request.mDigestAccount = self.mDigestAccount
                    self.request.mDigestPw = self.mDigestPw
                    self.request.getResult(model: POST5GSecurityresponse.self) { result in
                     //   print(result)
                     //   NSLog("post5GSecurity:\(ssid_index)")
                        if result == 200 {
                            self.postfWiFiResult += 1
                        }
                        else
                        {
                            DispatchQueue.main.async {
                            self.showErr = true
                            self.err = "Could not connect to the server"
                            self.responses = result
                            self.response = self.responses
                            }
                        }
                    }
                }
            }
            else
            {
                DispatchQueue.main.async {
                self.showErr = true
                self.err = "Could not connect to the server"
                self.responses = result
                self.response = self.responses
                }
            }
        }
        if support6G.contains(model)
        {
            request.url = "https://\(aPAddress)/ap/config/wireless/basic?radio=6g"
            request.method = "POST"
            var data6G = Data()
            do{
                let encoder = JSONEncoder()
                data6G = try! encoder.encode(post6GBandsBody)
            
            }
            request.body = data6G
            request.mDigestAccount = mDigestAccount
            request.mDigestPw = mDigestPw
            request.getResult(model: POST6GBandresponse.self) { result in
            //    print(result)
            //    NSLog("OffWiFi post6GBands_response :\(result) \(self.getOffWiFiResult)")
                if result == 200 {
                    self.postfWiFiResult += 1
//                    for ssid_index in 0..<(self.post6GBandsBody.enabled_ssidNum)! {
//                        self.request.url = "https://\(self.aPAddress)/ap/config/wireless/SSIDSecurity?radio=6g&ssid_index=\(ssid_index)"
//                        self.request.method = "POST"
//                        var data = Data()
//                        do{
//                            let encoder = JSONEncoder()
//                            data = try! encoder.encode(self.post6GSSIDSecurity[Int(exactly: ssid_index)!])
//                        
//                        }
//                        self.request.body = data
//                        self.request.DigestAccount = self.DigestAccount
//                        self.request.DigestPw = self.DigestPw
//                        self.request.getResult(model: POST6GSecurityresponse.self) { result in
//                            print(result)
//                            NSLog("post6GSecurity:\(ssid_index)")
//                            if result == 200 {
//                                self.postfWiFiResult += 1
//                            }
//                            else
//                            {
//                                
//                            }
//                        }
//                    }
                }
                else
                {
                    DispatchQueue.main.async {
                    self.showErr = true
                    self.err = "Could not connect to the server"
                    self.responses = result
                    self.response = self.responses
                    }
                }
            }
        }
        
    }
    func onWiFi(wifiradoi:String,model:String) {
        getOffWiFiResult = 0
      //  NSLog("OffWiFi get24GBands request")
        request.url = "https://\(aPAddress)/ap/config/wireless/basic?radio=2g"
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: GET24GBandsresponse.self) { result in
        //    print(result)
           // NSLog("OffWiFi get24GBands result : \(self.get24GBandsResult?.error_code) ")
            if result == 200 {
                self.responses = result
              //  self.response = self.responses
                let resultBody = self.get24GBandsResult
            //    NSLog("24G resultBody : \(resultBody?.data?.enabled_ssidNum)")
                for iuuid in 0..<(resultBody?.data?.enabled_ssidNum)! {
                    self.wireless24G[iuuid*4+0].value = (resultBody?.data?.ssidNames![iuuid].ssidname)!
                    self.wireless24G[iuuid*4+3].value = (resultBody?.data?.ssidNames![iuuid].vlan_id)!.description
                }
                self.post24GBandsBody.radio_enable = 0
                if wifiradoi == "1" ||  wifiradoi == "3" || wifiradoi == "5" || wifiradoi == "7"
                {
                    self.post24GBandsBody.radio_enable = 1
                }
                self.post24GBandsBody.radio_band = resultBody?.data?.radio_band
                self.post24GBandsBody.enabled_ssidNum = resultBody?.data?.enabled_ssidNum
                for iuuid in 0..<(resultBody?.data?.ssidNames?.count)! {
                    self.post24GBandsBody.ssidNames![iuuid] = (resultBody?.data?.ssidNames![iuuid])!
                }
                self.post24GBandsBody.autochannel_enabled = resultBody?.data?.autochannel_enabled
                self.post24GBandsBody.autochannel_range = resultBody?.data?.autochannel_range
                self.post24GBandsBody.autochannel_interval = resultBody?.data?.autochannel_interval
                self.post24GBandsBody.autochannel_force_enabled = resultBody?.data?.autochannel_force_enabled
                self.post24GBandsBody.channel = resultBody?.data?.channel
                self.post24GBandsBody.channel_bandwidth = resultBody?.data?.channel_bandwidth
                self.post24GBandsBody.bss_basicrateset = resultBody?.data?.bss_basicrateset
                
                self.post24GBands()
            }
            else
            {
                DispatchQueue.main.async {
                self.showErr = true
                self.err = "Could not connect to the server"
                self.responses = result
                self.response = self.responses
                }
            }
        }
        
      //  NSLog("OffWiFi get5GBands request")
        request.url = "https://\(aPAddress)/ap/config/wireless/basic?radio=5g"
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: GET5GBandsresponse.self) { result in
         //   print(result)
         //   NSLog("OffWiFi get5GBands result : \(self.get5GBandsResult?.error_code) ")
            if result == 200 {
                self.responses = result
               // self.response = self.responses
                let resultBody = self.get5GBandsResult
          //      NSLog("5G resultBody : \(resultBody?.data?.enabled_ssidNum)")
                for iuuid in 0..<(resultBody?.data?.enabled_ssidNum)! {
                    self.wireless5G[iuuid*4+0].value = (resultBody?.data?.ssidNames![iuuid].ssidname)!
                    self.wireless5G[iuuid*4+3].value = (resultBody?.data?.ssidNames![iuuid].vlan_id)!.description
                }
                self.post5GBandsBody.radio_enable = 0
                if wifiradoi == "2" ||  wifiradoi == "3" || wifiradoi == "6" || wifiradoi == "7"
                {
                    self.post5GBandsBody.radio_enable = 1
                }
                self.post5GBandsBody.radio_band = resultBody?.data?.radio_band
                self.post5GBandsBody.enabled_ssidNum = resultBody?.data?.enabled_ssidNum
                for iuuid in 0..<(resultBody?.data?.ssidNames?.count)! {
                    self.post5GBandsBody.ssidNames![iuuid] = (resultBody?.data?.ssidNames![iuuid])!
                }
                self.post5GBandsBody.autochannel_enabled = resultBody?.data?.autochannel_enabled
                self.post5GBandsBody.autochannel_range = resultBody?.data?.autochannel_range
                self.post5GBandsBody.autochannel_interval = resultBody?.data?.autochannel_interval
                self.post5GBandsBody.autochannel_force_enabled = resultBody?.data?.autochannel_force_enabled
                self.post5GBandsBody.channel = resultBody?.data?.channel
                self.post5GBandsBody.channel_bandwidth = resultBody?.data?.channel_bandwidth
                self.post5GBandsBody.bss_basicrateset = resultBody?.data?.bss_basicrateset
                
                self.post5GBands()
            }
            else
            {
                DispatchQueue.main.async {
                self.showErr = true
                self.err = "Could not connect to the server"
                self.responses = result
                self.response = self.responses
                }
            }
        }
        if support6G.contains(model)
        {
        NSLog("OffWiFi get6GBands request")
        request.url = "https://\(aPAddress)/ap/config/wireless/basic?radio=6g"
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: GET6GBandsresponse.self) { result in
        //    print(result)
        //    NSLog("OffWiFi get6GBands result : \(self.get6GBandsResult?.error_code) ")
            if result == 200 {
                self.responses = result
                self.response = self.responses
                let resultBody = self.get6GBandsResult
       //         NSLog("6G resultBody : \(resultBody?.data?.enabled_ssidNum)")
                for iuuid in 0..<(resultBody?.data?.enabled_ssidNum)! {
                    self.wireless6G[iuuid*4+0].value = (resultBody?.data?.ssidNames![iuuid].ssidname)!
                    self.wireless6G[iuuid*4+3].value = (resultBody?.data?.ssidNames![iuuid].vlan_id)!.description
                }
                self.post6GBandsBody.radio_enable = 0
                if wifiradoi == "4" ||  wifiradoi == "5" || wifiradoi == "6" || wifiradoi == "7"
                {
                    self.post6GBandsBody.radio_enable = 1
                }
                self.post6GBandsBody.radio_band = resultBody?.data?.radio_band
                self.post6GBandsBody.enabled_ssidNum = resultBody?.data?.enabled_ssidNum
                for iuuid in 0..<(resultBody?.data?.ssidNames?.count)! {
                    self.post6GBandsBody.ssidNames![iuuid] = (resultBody?.data?.ssidNames![iuuid])!
                }
                self.post6GBandsBody.autochannel_enabled = resultBody?.data?.autochannel_enabled
                self.post6GBandsBody.autochannel_range = resultBody?.data?.autochannel_range
                self.post6GBandsBody.autochannel_interval = resultBody?.data?.autochannel_interval
                self.post6GBandsBody.autochannel_force_enabled = resultBody?.data?.autochannel_force_enabled
                self.post6GBandsBody.channel = resultBody?.data?.channel
                self.post6GBandsBody.channel_bandwidth = resultBody?.data?.channel_bandwidth
                self.post6GBandsBody.bss_basicrateset = resultBody?.data?.bss_basicrateset
                self.post6GBands()
            }
            else
            {
                DispatchQueue.main.async {
                self.showErr = true
                self.err = "Could not connect to the server"
                self.responses = result
                self.response = self.responses
                }
            }
        }
        }
    }
    func offWiFi(model:String) {
        getOffWiFiResult = 0
        NSLog("OffWiFi get24GBands request")
        request.url = "https://\(aPAddress)/ap/config/wireless/basic?radio=2g"
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: GET24GBandsresponse.self) { result in
       //     print(result)
      //      NSLog("OffWiFi get24GBands result : \(self.get24GBandsResult?.error_code) ")
            if result == 200 {
                self.responses = result
              //  self.response = self.responses
                let resultBody = self.get24GBandsResult
      //          NSLog("24G resultBody : \(resultBody?.data?.enabled_ssidNum)")
                for iuuid in 0..<(resultBody?.data?.enabled_ssidNum)! {
                    self.wireless24G[iuuid*4+0].value = (resultBody?.data?.ssidNames![iuuid].ssidname)!
                    self.wireless24G[iuuid*4+3].value = (resultBody?.data?.ssidNames![iuuid].vlan_id)!.description
                }
                self.post24GBandsBody.radio_enable = 0
                self.post24GBandsBody.radio_band = resultBody?.data?.radio_band
                self.post24GBandsBody.enabled_ssidNum = resultBody?.data?.enabled_ssidNum
                for iuuid in 0..<(resultBody?.data?.ssidNames?.count)! {
                    self.post24GBandsBody.ssidNames![iuuid] = (resultBody?.data?.ssidNames![iuuid])!
                }
                self.post24GBandsBody.autochannel_enabled = resultBody?.data?.autochannel_enabled
                self.post24GBandsBody.autochannel_range = resultBody?.data?.autochannel_range
                self.post24GBandsBody.autochannel_interval = resultBody?.data?.autochannel_interval
                self.post24GBandsBody.autochannel_force_enabled = resultBody?.data?.autochannel_force_enabled
                self.post24GBandsBody.channel = resultBody?.data?.channel
                self.post24GBandsBody.channel_bandwidth = resultBody?.data?.channel_bandwidth
                self.post24GBandsBody.bss_basicrateset = resultBody?.data?.bss_basicrateset
                
                self.post24GBands()
            }
            else
            {
                DispatchQueue.main.async {
                self.showErr = true
                self.err = "Could not connect to the server"
                self.responses = result
                self.response = self.responses
                }
            }
        }
        
        NSLog("OffWiFi get5GBands request")
        request.url = "https://\(aPAddress)/ap/config/wireless/basic?radio=5g"
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: GET5GBandsresponse.self) { result in
       //     print(result)
    //        NSLog("OffWiFi get5GBands result : \(self.get5GBandsResult?.error_code) ")
            if result == 200 {
                self.responses = result
               // self.response = self.responses
                let resultBody = self.get5GBandsResult
    //            NSLog("5G resultBody : \(resultBody?.data?.enabled_ssidNum)")
                for iuuid in 0..<(resultBody?.data?.enabled_ssidNum)! {
                    self.wireless5G[iuuid*4+0].value = (resultBody?.data?.ssidNames![iuuid].ssidname)!
                    self.wireless5G[iuuid*4+3].value = (resultBody?.data?.ssidNames![iuuid].vlan_id)!.description
                }
                self.post5GBandsBody.radio_enable = 0
                self.post5GBandsBody.radio_band = resultBody?.data?.radio_band
                self.post5GBandsBody.enabled_ssidNum = resultBody?.data?.enabled_ssidNum
                for iuuid in 0..<(resultBody?.data?.ssidNames?.count)! {
                    self.post5GBandsBody.ssidNames![iuuid] = (resultBody?.data?.ssidNames![iuuid])!
                }
                self.post5GBandsBody.autochannel_enabled = resultBody?.data?.autochannel_enabled
                self.post5GBandsBody.autochannel_range = resultBody?.data?.autochannel_range
                self.post5GBandsBody.autochannel_interval = resultBody?.data?.autochannel_interval
                self.post5GBandsBody.autochannel_force_enabled = resultBody?.data?.autochannel_force_enabled
                self.post5GBandsBody.channel = resultBody?.data?.channel
                self.post5GBandsBody.channel_bandwidth = resultBody?.data?.channel_bandwidth
                self.post5GBandsBody.bss_basicrateset = resultBody?.data?.bss_basicrateset
                
                self.post5GBands()
            }
            else
            {
                DispatchQueue.main.async {
                self.showErr = true
                self.err = "Could not connect to the server"
                self.responses = result
                self.response = self.responses
                }
            }
        }
        if support6G.contains(model)
        {
        NSLog("OffWiFi get6GBands request")
        request.url = "https://\(aPAddress)/ap/config/wireless/basic?radio=6g"
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: GET6GBandsresponse.self) { result in
     //       print(result)
    //        NSLog("OffWiFi get6GBands result : \(self.get6GBandsResult?.error_code) ")
            if result == 200 {
                self.responses = result
                self.response = self.responses
                let resultBody = self.get6GBandsResult
   //             NSLog("6G resultBody : \(resultBody?.data?.enabled_ssidNum)")
                for iuuid in 0..<(resultBody?.data?.enabled_ssidNum)! {
                    self.wireless6G[iuuid*4+0].value = (resultBody?.data?.ssidNames![iuuid].ssidname)!
                    self.wireless6G[iuuid*4+3].value = (resultBody?.data?.ssidNames![iuuid].vlan_id)!.description
                }
                self.post6GBandsBody.radio_enable = 0
                self.post6GBandsBody.radio_band = resultBody?.data?.radio_band
                self.post6GBandsBody.enabled_ssidNum = resultBody?.data?.enabled_ssidNum
                for iuuid in 0..<(resultBody?.data?.ssidNames?.count)! {
                    self.post6GBandsBody.ssidNames![iuuid] = (resultBody?.data?.ssidNames![iuuid])!
                }
                self.post6GBandsBody.autochannel_enabled = resultBody?.data?.autochannel_enabled
                self.post6GBandsBody.autochannel_range = resultBody?.data?.autochannel_range
                self.post6GBandsBody.autochannel_interval = resultBody?.data?.autochannel_interval
                self.post6GBandsBody.autochannel_force_enabled = resultBody?.data?.autochannel_force_enabled
                self.post6GBandsBody.channel = resultBody?.data?.channel
                self.post6GBandsBody.channel_bandwidth = resultBody?.data?.channel_bandwidth
                self.post6GBandsBody.bss_basicrateset = resultBody?.data?.bss_basicrateset
                self.post6GBands()
            }
            else
            {
                DispatchQueue.main.async {
                self.showErr = true
                self.err = "Could not connect to the server"
                self.responses = result
                self.response = self.responses
                }
            }
        }
        }
    }
    /*wirelessClients*/
    func getWirelessClients(GHz:String){
        
        request.url = "https://\(aPAddress)/ap/info/wireless/clients?radio=\(GHz)g"
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        
        request.getResult(model: GETClientsresponse.self) { [self] result in
            if result == 200 {
                self.getClientsCount += 1
                let resultBody = self.getClientsResult
                // v 0..<(resultBody?.data?.total_size)! {
                if resultBody?.data?.client_list?.count ?? 0 >= 1 {
                    for data in (resultBody?.data?.client_list)! {
                        var ssid = data.vendor
                        if ssid == ""
                        {   ssid = "Unknown" }
                        var fake = ClientsBody(name: "SSID", value: ssid)
                        self.wirelessClients.append(fake)
                        fake = ClientsBody(name: "radio", value: "\(GHz)GHz")
                        self.wirelessClients.append(fake)
                        fake = ClientsBody(name: "IP Address", value: data.ip_address)
                        self.wirelessClients.append(fake)
                        self.mobileIPSSID.append(fake)
                        fake = ClientsBody(name: "MAC Address", value: data.mac_address)
                        self.wirelessClients.append(fake)
                        fake = ClientsBody(name: "Tx", value: data.tx)
                        self.wirelessClients.append(fake)
                        fake = ClientsBody(name: "Rx", value: data.rx)
                        self.wirelessClients.append(fake)
                        fake = ClientsBody(name: "Signal(%)", value: data.signale_ratio?.description)
                        self.wirelessClients.append(fake)
                        fake = ClientsBody(name: "RSSI(dbm)", value: data.rssi?.description)
                        self.wirelessClients.append(fake)
                        fake = ClientsBody(name: "Connected Time", value: data.connected_time)
                        self.wirelessClients.append(fake)
                        fake = ClientsBody(name: "Idle Time", value: data.idle_time?.description)
                        self.wirelessClients.append(fake)
                        fake = ClientsBody(name: "ssid name", value: data.ssid_name)
                        self.mobileIPSSID.append(fake)
                       if data.ssid_name == mCurrentSSID || data.ip_address == mCurrentIP
                        {
                            let kick = KickClient(radio: "\(GHz)g" , ssid_index: data.ssid_index?.description , client_mac: data.mac_address , current: true)
                            self.mobilekick.append(kick)
                        }
                        else
                        {
                            let kick = KickClient(radio: "\(GHz)g" , ssid_index: data.ssid_index?.description , client_mac: data.mac_address , current: false)
                            self.mobilekick.append(kick)
                        }
                    }
                }
                else
                {
                    var fake = ClientsBody(name: "IP Address" , value: "")
                    self.mobileIPSSID.append(fake)
                    fake = ClientsBody(name: "ssid name", value: "")
                    self.mobileIPSSID.append(fake)
                }
            }
            else
            {
//                if result == 400 {
//                    DispatchQueue.main.async {
//                    self.showErr = true
//                    self.err = "Invaild Respone Formate"
//                    self.responses = result
//                    self.response = self.responses
//                    self.getClientsResult = nil
//                    }
//                }
//                else
//                {
                    DispatchQueue.main.async {
                    self.showErr = true
                    self.err = "Could not connect to the server"
                    self.responses = result
                    self.response = self.responses
                    self.getClientsResult = nil
                    }
//                }
            }
        }
    }
    func deleteClients(i:Int,radio:String, ssid_index:String, client_mac:String) {
        let range = i*10...9+i*10
        if !wirelessClients.isEmpty
        {
            wirelessClients.removeSubrange(range)
        }
        self.putKickClient(radio:radio, ssid_index:ssid_index, client_mac:client_mac)
    }
    func putKickClient(radio:String, ssid_index:String, client_mac:String) {
        request.url = "https://\(aPAddress)/ap/systemctl/wireless/kick_client?radio=\(radio)&ssid_index=\(ssid_index)&client_mac=\(client_mac)"
        request.method = "PUT"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: PUTKickClientresponse.self) { result in
   //         print(result)
            if result == 200 {
               // let resultBody = self.putKickClientREsult
                
            }
            else {
//                if result == 400 {
                    DispatchQueue.main.async {
                    self.showErr = true
                    self.err = "Invaild Respone Formate"
                    self.responses = result
                    self.response = self.responses
                    }
//                }
            }
        }
    }
    /*DateAndTime*/
    func initDateTime() {
        var fake = LocalTimeBody(name: "Year", value: "")
        self.mLocalTime.append(fake)
        fake = LocalTimeBody(name: "Month", value: "")
        self.mLocalTime.append(fake)
        fake = LocalTimeBody(name: "Day", value: "")
        self.mLocalTime.append(fake)
        fake = LocalTimeBody(name: "Hours", value: "")
        self.mLocalTime.append(fake)
        fake = LocalTimeBody(name: "Minutes", value: "")
        self.mLocalTime.append(fake)
        fake = LocalTimeBody(name: "Seconds", value: "")
        self.mLocalTime.append(fake)
        
        var fake1 = NTPTimeServerBody(name: "Auto Daylight Saving", value: "0")
        self.mNTPTimeServer.append(fake1)
        fake1 = NTPTimeServerBody(name: "Server Name", value: "")
        self.mNTPTimeServer.append(fake1)
        fake1 = NTPTimeServerBody(name: "Update Interval", value: "24")
        self.mNTPTimeServer.append(fake1)
    }
    func getDateAndTime() {
        request.url = "https://\(aPAddress)/ap/config/system/dateTime"
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        
        request.getResult(model: GETDateAndTimeresponse.self) { result in
            if result == 200
            {
                let resultBody = self.getDateAndTimeResult
         //       print("getDateAndTime : \(resultBody)")
                var fake = LocalTimeBody(name: "Year", value: resultBody?.data?.local_time_year?.description)
                self.mLocalTime[0] = fake
                fake = LocalTimeBody(name: "Month", value: self.month[(resultBody?.data?.local_time_month ?? 1)-1])
                self.mLocalTime[1] = fake
                fake = LocalTimeBody(name: "Day", value: resultBody?.data?.local_time_day?.description)
                self.mLocalTime[2] = fake
                fake = LocalTimeBody(name: "Hours", value: resultBody?.data?.local_time_hour?.description)
                self.mLocalTime[3] = fake
                fake = LocalTimeBody(name: "Minutes", value: resultBody?.data?.local_time_minute?.description)
                self.mLocalTime[4] = fake
                fake = LocalTimeBody(name: "Seconds", value: resultBody?.data?.local_time_seconde?.description)
                self.mLocalTime[5] = fake
                
                var fake1 = NTPTimeServerBody(name: "Auto Daylight Saving", value: resultBody?.data?.auto_daylight_save?.description)
               // self.NTPTimeServer.append(fake1)
                self.mNTPTimeServer[0] = fake1
                fake1 = NTPTimeServerBody(name: "Server Name", value: resultBody?.data?.ntp_server_name)
                //self.NTPTimeServer.append(fake1)
                self.mNTPTimeServer[1] = fake1
                fake1 = NTPTimeServerBody(name: "Update Interval", value: resultBody?.data?.ntp_update_interval?.description)
               // self.NTPTimeServer.append(fake1)
                self.mNTPTimeServer[2] = fake1
               
                self.mTimeZone = mTimeZoneList[(resultBody?.data?.time_zone)!]
            }
            else
            {
                if result == 400 {
                    DispatchQueue.main.async {
                    self.showErr = true
                    self.err = "Invaild Respone Formate"
                    self.responses = result
                    self.response = self.responses
                    self.getDateAndTimeResult = nil
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        
                    self.showErr = true
                    self.err = "Could not connect to the server"
                    self.responses = result
                    self.response = self.responses
                    self.getDateAndTimeResult = nil
                    }
                }
            }
        }
    }
    func postDateAndTime() {
        request.url = "https://\(aPAddress)/ap/config/system/dateTime"
        request.method = "POST"
        var data = Data()
        do{
            let encoder = JSONEncoder()
            data = try! encoder.encode(postDateAndTimeBody)
        
        }
        request.body = data
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: POSTDateAndTimeresponse.self) { result in
            if result == 200
            {
             //   let resultBody = self.postDateAndTimeResult
           //     print( "POSTDateAndTimeresponse error_code:\(resultBody?.error_code)")
                self.postsysReload()
            }
        }
    }
    /*delete*/
    func delete(mac:String) {
        if let idx = myAccess.firstIndex(where: { $0.mac == mac }) {
            myAccess.remove(at: idx)
        }
        if myAccess.count == 0 {
            myCount = false
        }
    }
    /*PingTest*/
    func pingTest(address:String,isIPv4:Bool,isIPv6:Bool) {
        if isIPv4 {
            request.url = "https://\(aPAddress)/ap/info/utils?tool=ping&address=\(address)"
        }
        else if isIPv6 {
            request.url = "https://\(aPAddress)/ap/info/utils?tool=ping6&address=\(address)"
        }
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        
        request.getResult(model: GETPingresponse.self) { result in
            if result == 200 {
                
                self.pingMessage = (self.getPingResult?.data?.result)!
            }
            else
            {
//                if result == 400 {
//                    DispatchQueue.main.async {
//                    self.showErr = true
//                    self.err = "Invaild Respone Formate"
//                    self.responses = result
//                    self.response = self.responses
//                        self.getPingResult = nil
//                    }
//                }
//                else
//                {
                    DispatchQueue.main.async {
                    self.showErr = true
                    self.err = "Could not connect to the server"
                    self.responses = result
                    self.response = self.responses
                        self.getPingResult = nil
                    }
//                }
            }
        }
    }
    /*TracerouteTest*/
    func tracerouteTest(address:String) {
        request.url = "https://\(aPAddress)/ap/info/utils?tool=traceroute&address=\(address)"
        request.method = "GET"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: GETTracerouteresponse.self) { result in
            if result == 200 {
                self.tracerouteMessage = (self.getTracerouteResult?.data?.result)!
            }
            else
            {
//                if result == 400 {
//                    DispatchQueue.main.async {
//                    self.showErr = true
//                    self.err = "Invaild Respone Formate"
//                    self.responses = result
//                    self.response = self.responses
//                    self.getTracerouteResult = nil
//                    }
//                }
//                else
//                {
                    DispatchQueue.main.async {
                    self.showErr = true
                    self.err = "Could not connect to the server"
                    self.responses = result
                    self.response = self.responses
                    self.getTracerouteResult = nil
                    }
//               }
            }
        }
    }
    func postsysReload() {
        request.url = "https://\(aPAddress)/ap/systemctl/sysReload"
        request.method = "PUT"
        request.mDigestAccount = mDigestAccount
        request.mDigestPw = mDigestPw
        request.getResult(model: POSTsysReloadresponse.self) { result in
            if result == 200
            {
                //let resultBody = self.postsysReloadResult
               // print( "postsysReload error_code:\(resultBody?.error_code)")
            
            }
            else
            {
                DispatchQueue.main.async {
                self.showErr = true
                self.err = "Could not connect to the server"
                self.responses = result
                self.response = self.responses
                }
            }
        }
    }
}
