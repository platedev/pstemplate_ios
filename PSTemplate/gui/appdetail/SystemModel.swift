//
//  SystemModel.swift
//  PSTemplate
//
//  Created by EDIOS on 2022/3/4.
//

import SwiftUI

struct GETRebootresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    var data:reboot?
    
    init(error_code:Int?,error_msg:String?,data:reboot?) {
        self.error_code = error_code
        self.error_msg = error_msg
        self.data = data
    }
}
struct reboot: Decodable, Hashable {
    let reboot_time:String?
    init(reboot_time:String?)
    {
        self.reboot_time = reboot_time
    }
}
//System Info
struct GETresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    var data:InfoSystem?
    
    init(error_code:Int?,error_msg:String?,data:InfoSystem?) {
        self.error_code = error_code
        self.error_msg = error_msg
        self.data = data
    }
}
struct InfoSystem: Decodable, Hashable {
    let model_name:String?
    var product_name:String?
    let uptime:String?
    let system_time:String?
    let firmware_version:String?
    let mac_address:String?
    let management_vlan_id:Int?
    let ip_address:String?
    let default_gateway:String?
    let dns_server:String?
    let dhcp_server:String?
    let ipv6_address:String?
    let ipv6_link_local_address:String?
    
    init(model_name: String?,system_time:String?, product_name: String?,uptime: String?,firmware_version:String?,mac_address:String?,management_vlan_id:Int?,ip_address:String?,default_gateway:String?,dns_server:String?,dhcp_server:String?,ipv6_address:String?,ipv6_link_local_address:String?) {
        self.model_name = model_name
        self.product_name = product_name
        self.uptime = uptime
        self.system_time = system_time
        self.firmware_version = firmware_version
        self.mac_address = mac_address
        self.management_vlan_id = management_vlan_id
        self.ip_address = ip_address
        self.default_gateway = default_gateway
        self.dns_server = dns_server
        self.dhcp_server = dhcp_server
        self.ipv6_address = ipv6_address
        self.ipv6_link_local_address = ipv6_link_local_address
        
    }
}
struct SystemBody: Encodable, Hashable {
    let name:String?
    var value:String?
    init(name: String?, value: String?) {
        self.name = name
        self.value = value
    }
}
struct GETAdministrationresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    var data:Administration?
    init(error_code:Int?,error_msg:String?,data:Administration?) {
        self.error_code = error_code
        self.error_msg = error_msg
        self.data = data
    }
}
struct POSTAdministrationresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
   // var data:String?
    init(error_code:Int?,error_msg:String?) {
        self.error_code = error_code
        self.error_msg = error_msg
       // self.data = data
    }
}
struct Administration: Codable, Hashable {
    var account_name:String?
    var account_security:String?
    var account_confirm_security:String?
    var product_name:String?
    var https_port:Int?
    var management_protocols:managementProtocols?
    var snmp_settings:snmpSettings?
    init(account_name:String?,account_security:String?,account_confirm_security:String?,product_name:String?,https_port:Int?,management_protocols:managementProtocols?,snmp_settings:snmpSettings?)
    {
        self.account_name = account_name
        self.account_security = account_security
        self.account_confirm_security = account_confirm_security
        self.product_name = product_name
        self.https_port = https_port
        self.management_protocols = management_protocols
        self.snmp_settings = snmp_settings
    }
    
}
struct managementProtocols: Codable, Hashable {
    let https:Int?
    let telnet:Int?
    let ssh:Int?
    let snmp:Int?
    
    init(https:Int?,telnet:Int?,ssh:Int?,snmp:Int?)
    {
        self.https = https
        self.telnet = telnet
        self.ssh = ssh
        self.snmp = snmp
    }
}
struct snmpSettings: Codable, Hashable {
    let snmp_version:Int?
    let snmp_get_community:String?
    let snmp_set_community:String?
    let snmp_v3_name:String?
    let snmp_v3_security:String?
    let snmp_trap_enabled:Int?
    let snmp_trap_community:String?
    let snmp_trap_manager:String?
    init(snmp_version:Int?,snmp_get_community:String?,snmp_set_community:String?,snmp_v3_name:String?,snmp_v3_security:String?,snmp_trap_enabled:Int?,snmp_trap_community:String?,snmp_trap_manager:String?)
    {
        self.snmp_version = snmp_version
        self.snmp_get_community = snmp_get_community
        self.snmp_set_community = snmp_set_community
        self.snmp_v3_name = snmp_v3_name
        self.snmp_v3_security = snmp_v3_security
        self.snmp_trap_enabled = snmp_trap_enabled
        self.snmp_trap_community = snmp_trap_community
        self.snmp_trap_manager = snmp_trap_manager
    }
}
////LAN Port ///
struct GETLANresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    var data:LANs?
    
    init(error_code:Int?,error_msg:String?,data:LANs?) {
        self.error_code = error_code
        self.error_msg = error_msg
        self.data = data
    }
}
struct LANs: Codable, Hashable {
    var lanport:[LANport?]
    var ipv6:IPv6?
    var ipv4:IPv4?
    
    init(lanport:[LANport?],ipv6:IPv6?,ipv4:IPv4?) {
        self.lanport = lanport
        self.ipv6 = ipv6
        self.ipv4 = ipv4
        
    }
}
struct LANport: Codable, Hashable {
    var index: Int?
    var duplex: Int?
    var flow_control: Int?
    var az_enabled: Int?
}
struct IPv6: Codable, Hashable {
    var ipv6_enabled: Int?
    var ipv6_type: Int?
    var ipv6_predix_len: Int?
    var ipv6_address: String?
    var ipv6_gateway: String?
}
struct IPv4: Codable, Hashable {
    
    var ipv4_type: Int?
    var ipv4_address: String?
    var ipv4_subnet_mask: String?
    var ipv4_default_gateway_type: Int?
    var ipv4_default_gateway: String?
    var ipv4_dns_primary_type: Int?
    var ipv4_dns_primary: String?
    var ipv4_dns_secondary_type: Int?
    var ipv4_dns_secondary: String?
    var dhcp_enabled: Int?
    var dhcp_lease_time:Int?
    var dhcp_start_addr:String?
    var dhcp_end_addr: String?
    var dhcp_domain_name: String?
    var dhcp_dns_primary: String?
    var dhcp_dns_secondary: String?
    
    init(ipv4_type: Int?,ipv4_address: String?,ipv4_subnet_mask: String?,ipv4_default_gateway_type: Int?,ipv4_default_gateway: String?,ipv4_dns_primary_type: Int?,ipv4_dns_primary: String?,ipv4_dns_secondary_type: Int?,ipv4_dns_secondary: String?,dhcp_enabled: Int?,dhcp_lease_time:Int?,dhcp_start_addr:String?,dhcp_end_addr: String?,dhcp_domain_name: String?,dhcp_dns_primary: String?,dhcp_dns_secondary: String?) {
        self.ipv4_type = ipv4_type
        self.ipv4_address = ipv4_address
        self.ipv4_subnet_mask = ipv4_subnet_mask
        self.ipv4_default_gateway_type = ipv4_default_gateway_type
        self.ipv4_default_gateway = ipv4_default_gateway
        self.ipv4_dns_primary_type = ipv4_dns_primary_type
        self.ipv4_dns_primary = ipv4_dns_primary
        self.ipv4_dns_secondary_type = ipv4_dns_secondary_type
        self.ipv4_dns_secondary = ipv4_dns_secondary
        self.dhcp_enabled = dhcp_enabled
        self.dhcp_lease_time = dhcp_lease_time
        self.dhcp_start_addr = dhcp_start_addr
        self.dhcp_end_addr = dhcp_end_addr
        self.dhcp_domain_name = dhcp_domain_name
        self.dhcp_dns_primary = dhcp_dns_primary
        self.dhcp_dns_secondary = dhcp_dns_secondary
    }
}
struct POSTLANresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
   // var data:String?
    
    init(error_code:Int?,error_msg:String?) {
        self.error_code = error_code
        self.error_msg = error_msg
       // self.data = data
    }
}

struct LANBody: Encodable, Hashable {
    let name:String?
    var value:String?
    
    
    init(name: String?, value: String?) {
        self.name = name
        self.value = value
        
    }
}

//// WIRELESS Bands////
struct GET24GBandsresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    var data:Bands?
    
    init(error_code:Int?,error_msg:String?,data:Bands?) {
        self.error_code = error_code
        self.error_msg = error_msg
        self.data = data
    }
}
struct GET5GBandsresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    var data:Bands?
    
    init(error_code:Int?,error_msg:String?,data:Bands?) {
        self.error_code = error_code
        self.error_msg = error_msg
        self.data = data
    }
}
struct GET6GBandsresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    var data:Bands?
    
    init(error_code:Int?,error_msg:String?,data:Bands?) {
        self.error_code = error_code
        self.error_msg = error_msg
        self.data = data
    }
}
struct GET24GSSIDSecurityresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    var data:SSIDSecurity?
    
    init(error_code:Int?,error_msg:String?,data:SSIDSecurity?) {
        self.error_code = error_code
        self.error_msg = error_msg
        self.data = data
    }
}
struct GET5GSSIDSecurityresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    var data:SSIDSecurity?
    
    init(error_code:Int?,error_msg:String?,data:SSIDSecurity?) {
        self.error_code = error_code
        self.error_msg = error_msg
        self.data = data
    }
}
struct GET6GSSIDSecurityresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    var data:SSIDSecurity?
    
    init(error_code:Int?,error_msg:String?,data:SSIDSecurity?) {
        self.error_code = error_code
        self.error_msg = error_msg
        self.data = data
    }
}
struct Bands: Codable, Hashable {
    var radio_enable:Int?
    var radio_band:Int?
    var enabled_ssidNum:Int!
    var ssidNames:[SSID]?
    var autochannel_enabled:Int?
    var autochannel_range:Int?
    var autochannel_interval:Int?
    var autochannel_force_enabled:Int?
    var channel:Int?
    var channel_bandwidth:Int?
    var bss_basicrateset:Int?
    
    init(radio_enable: Int?, radio_band: Int?,enabled_ssidNum: Int!,ssidNames:[SSID]?,autochannel_enabled:Int?,autochannel_range:Int?,autochannel_interval:Int?,autochannel_force_enabled:Int?,channel:Int?,channel_bandwidth:Int?,bss_basicrateset:Int?) {
        self.radio_enable = radio_enable
        self.radio_band = radio_band
        self.enabled_ssidNum = enabled_ssidNum
        self.ssidNames = ssidNames
        self.autochannel_enabled = autochannel_enabled
        self.autochannel_range = autochannel_range
        self.autochannel_interval = autochannel_interval
        self.autochannel_force_enabled = autochannel_force_enabled
        self.channel = channel
        self.channel_bandwidth = channel_bandwidth
        self.bss_basicrateset = bss_basicrateset
    }
}
struct SSID: Codable, Hashable {
    var ssidname:String?
    var index:Int?
    var vlan_id:Int?
    
    init(ssidname:String?,index:Int?,vlan_id:Int?) {
        self.ssidname = ssidname
        self.index = index
        self.vlan_id = vlan_id
    }
}
struct SSIDSecurity: Codable, Hashable {
    var ssid_index:Int?
    var broadcast_ssid:Int?
    var client_isolation:Int?
    var ieee80211k:Int?
    var ieee80211w:Int?
    var load_balance:Int?
    var auth_method:Int?
    var auth_detail:AuthDetail?
    var additional_auth:Int?
    var mac_radius_auth:Int?
    var mac_radius_password:String?
    
    init(ssid_index:Int?,broadcast_ssid:Int?,client_isolation:Int?,ieee80211k:Int?,ieee80211w:Int?,load_balance:Int?,auth_method:Int?,auth_detail:AuthDetail?,additional_auth:Int?,mac_radius_auth:Int?,mac_radius_password:String?)
    {
        self.ssid_index = ssid_index
        self.broadcast_ssid = broadcast_ssid
        self.client_isolation = client_isolation
        self.ieee80211k = ieee80211k
        self.ieee80211w = ieee80211w
        self.load_balance = load_balance
        self.auth_method = auth_method
        self.auth_detail = auth_detail
        self.additional_auth = additional_auth
        self.mac_radius_auth = mac_radius_auth
        self.mac_radius_password = mac_radius_password
    }
}
struct AuthDetail: Codable, Hashable {
    var ieee80211r:Int?
    var wpa_type:Int!
    var encrypt_type:Int?
    var wpa_rekey_time:Int?
    var psk_type:Int?
    var psk_value:String?
    
    init(ieee80211r:Int?,wpa_type:Int!,encrypt_type:Int?,wpa_rekey_time:Int?,psk_type:Int?,psk_value:String?)
    {
        self.ieee80211r = ieee80211r
        self.wpa_type = wpa_type
        self.encrypt_type = encrypt_type
        self.wpa_rekey_time = wpa_rekey_time
        self.psk_type = psk_type
        self.psk_value = psk_value
    }
}
struct WirelessBody: Encodable, Hashable {
    let name:String?
    var value:String?
    
    
    init(name: String?, value: String?) {
        self.name = name
        self.value = value
        
    }
}
struct GETClientsresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    var data:ClientList?
    
    init(error_code:Int?,error_msg:String?,data:ClientList?) {
        self.error_code = error_code
        self.error_msg = error_msg
        self.data = data
    }
}
struct ClientList:Decodable, Hashable {
    var client_list:[Clients]?
    let total_size:Int?
    
    init(client_list:[Clients]?,total_size:Int?) {
        self.client_list = client_list
        self.total_size = total_size
    }
}
struct Clients: Decodable, Hashable {
    let ssid_name:String?
    let ssid_index:Int?
    var ip_address:String?
    let mac_address:String?
    let tx:String?
    let rx:String?
    let signale_ratio:Int?
    let rssi:Int?
    let connected_time:String?
    let idle_time:Int?
    let vendor:String?
    
    init(ssid_name: String?,ssid_index:Int?, ip_address: String?,mac_address: String?,tx:String?,rx:String?,signale_ratio:Int?,rssi:Int?,connected_time:String?,idle_time:Int?,vendor:String?) {
        self.ssid_name = ssid_name
        self.ssid_index = ssid_index
        self.ip_address = ip_address
        self.mac_address = mac_address
        self.tx = tx
        self.rx = rx
        self.signale_ratio = signale_ratio
        self.rssi = rssi
        self.connected_time = connected_time
        self.idle_time = idle_time
        self.vendor = vendor
        
    }
}
struct ClientsBody: Encodable, Hashable {
    let name:String?
    var value:String?
    
    
    init(name: String?, value: String?) {
        self.name = name
        self.value = value
        
    }
}
struct LocalTimeBody: Encodable, Hashable {
    let name:String?
    var value:String?
    
    
    init(name: String?, value: String?) {
        self.name = name
        self.value = value
        
    }
}
struct NTPTimeServerBody: Encodable, Hashable {
    let name:String?
    var value:String?
    
    
    init(name: String?, value: String?) {
        self.name = name
        self.value = value
        
    }
}
struct GETPingresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    var data:ping?
    init(error_code:Int?,error_msg:String?,data:ping?) {
        self.error_code = error_code
        self.error_msg = error_msg
        self.data = data
    }
}
struct ping: Decodable, Hashable {
    let result:String?
    init(result:String?) {
        self.result = result
    }
}
struct GETTracerouteresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    var data:traceRoute?
    
    init(error_code:Int?,error_msg:String?,data:traceRoute?) {
        self.error_code = error_code
        self.error_msg = error_msg
        self.data = data
    }
}
struct traceRoute: Decodable, Hashable {
    let result:String?
    init(result:String?) {
        self.result = result
    }
}
struct GETDateAndTimeresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    var data:datetime?
    init(error_code:Int?,error_msg:String?,data:datetime?) {
        self.error_code = error_code
        self.error_msg = error_msg
        self.data = data
    }
}
struct datetime: Codable, Hashable {
    var local_time_year:Int?
    var local_time_month:Int?
    var local_time_day:Int?
    var local_time_hour:Int?
    var local_time_minute:Int?
    var local_time_seconde:Int?
    var time_zone:Int!
    var ntp_enabled:Int?
    var ntp_server_name: String?
    var ntp_update_interval:Int?
    var auto_daylight_save:Int?
    init(local_time_year:Int?,local_time_month:Int?,local_time_day:Int?,local_time_hour:Int?,local_time_minute:Int?,local_time_seconde:Int?,time_zone:Int!,ntp_enabled:Int?,ntp_server_name: String?,ntp_update_interval:Int?,auto_daylight_save:Int?) {
        self.local_time_year = local_time_year
        self.local_time_month = local_time_month
        self.local_time_day = local_time_day
        self.local_time_hour = local_time_hour
        self.local_time_minute = local_time_minute
        self.local_time_seconde = local_time_seconde
        self.time_zone = time_zone
        self.ntp_enabled = ntp_enabled
        self.ntp_server_name = ntp_server_name
        self.ntp_update_interval = ntp_update_interval
        self.auto_daylight_save = auto_daylight_save
    }
}
struct POSTDateAndTimeresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    //var data:String?
    
    init(error_code:Int?,error_msg:String?) {
        self.error_code = error_code
        self.error_msg = error_msg
       // self.data = data
    }
}
struct POST24GBandresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    //var data:String?
    init(error_code:Int?,error_msg:String?) {
        self.error_code = error_code
        self.error_msg = error_msg
       // self.data = data
    }
}
struct POST5GBandresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
  // var data:String?
    init(error_code:Int?,error_msg:String?) {
        self.error_code = error_code
        self.error_msg = error_msg
    }
}
struct POST6GBandresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    //var data:String?
    init(error_code:Int?,error_msg:String?) {
        self.error_code = error_code
        self.error_msg = error_msg
    }
}
struct POST24GSecurityresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
   // var data:String?
    
    init(error_code:Int?,error_msg:String?) {
        self.error_code = error_code
        self.error_msg = error_msg
    }
}
struct POST5GSecurityresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
  //  var data:String?
    init(error_code:Int?,error_msg:String?) {
        self.error_code = error_code
        self.error_msg = error_msg
       // self.data = data
    }
}
struct POST6GSecurityresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    //var data:String?
    init(error_code:Int?,error_msg:String?) {
        self.error_code = error_code
        self.error_msg = error_msg
       // self.data = data
    }
}
struct POSTsysReloadresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    var data:reload?
    
    init(error_code:Int?,error_msg:String?,data:reload?) {
        self.error_code = error_code
        self.error_msg = error_msg
        self.data = data
    }
}
struct reload:Decodable, Hashable {
    let reload_time:String?
    init(reload_time:String?) {
        self.reload_time = reload_time
    }
}
struct KickClient: Codable, Hashable {
    let radio:String?
    let ssid_index:String?
    let client_mac:String?
    let current:Bool?
    init(radio:String?,ssid_index:String?,client_mac:String?,current:Bool?) {
        self.radio = radio
        self.ssid_index = ssid_index
        self.client_mac = client_mac
        self.current = current
    }
}
struct Kick_Client: Codable, Hashable {
    let radio:String?
    let ssid_index:String?
    let client_mac:String?
    init(radio:String?,ssid_index:String?,client_mac:String?) {
        self.radio = radio
        self.ssid_index = ssid_index
        self.client_mac = client_mac
    }
}
struct PUTKickClientresponse: Decodable, Hashable {
    let error_code:Int?
    let error_msg:String?
    //var data:String?
    init(error_code:Int?,error_msg:String?) {
        self.error_code = error_code
        self.error_msg = error_msg
        //self.data = data
    }
}
